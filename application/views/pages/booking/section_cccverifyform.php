<div class="col-sm-12 mb-5 ">
	
	<div class="row border mt-1 pt-1">
  

		<div class="col-sm-12 pb-2 mx-auto" id="cccverify_obox1">			
		     
		 	<?php echo form_open('booking/save', 'name="caller-form" id="" class="" onsubmit="return Booking.confirm()" '); ?>
		 		 
		 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
		 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">
		 		<input type="hidden" name="section" value="cccverify">  
 

				<div class="mx-auto p-2 bg-yellow ">
					<h4 class="text-center">Cell Care Collector is marked as "NO" </h4>
				</div>

				<div id="cccverify_ibox1">
				    <div class=" mt-2 p-2 mx-auto bg-yellow ">
				    	<div class="text-center">
					    	<h5>ADVISE CALLER</h5>
					    	<p>We understand the doctor / hospital will be performing the collection</p>
					    	<p>Is this correct ?</p>
				        </div>
				       
				    </div>

				    <div class="col-sm-12 mx-auto text-center mt-2">
				    	<button type="button" class="btn btn-sm btn-primary" onclick="OnCallVerify.btn('Yes')">YES</button> 
				    	<button name="oncallcccverify" value="1"  class="btn btn-sm btn-primary ml-5">No</button> 
				    </div>
			    </div>

			    <div id="cccverify_ibox2" style="display: none">
			    	
				    <div class=" mt-2 p-2 mx-auto bg-yellow ">
				    	<div class="text-center">
					    	<h5>Advise Caller</h5>
					    	<!-- <p>You will need to remind the hospital they need to do the collection</p>
					    	<p>Thank you for your call</p> -->
					    	<p>Please remind your doctor or midwife they need to do the collection.</p>
					    	<p>You should call back within 2hrs after the birth to book the courier for pick up.</p>
				        </div>
				       
				    </div>

				    <div class="col-sm-12 mx-auto text-center mt-2">
				    	<button  class="btn btn-sm btn-primary">Submit</button> 
				    </div>

			    </div>

			</form>

		</div>


		<div id="labourprogress-dynamic-content"></div>

	</div>
</div>