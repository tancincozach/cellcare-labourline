<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  " role="document">
        <div class="modal-content bg-light">
            <div class="modal-header">
                <h5 class="modal-title">New Reminder</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php 
                echo form_open('booking/open', 'name="Reminder-form" id="Reminder-form" class="col-sm-12" onsubmit="return BookingDetailsNavbar.reminder_save(this)" '); 


                $template = 'Client ID: '.$customer->CustomerID.' - Client Name: '.$customer->FirstName.' '.$customer->LastName.' ';
            ?>
                <input type="hidden" name="tran_id" value="<?php echo $transaction->tran_id; ?>" > 
                <input type="hidden" name="timezone" value="<?php echo h_get_state_tz($customer->State); ?>" > 
                <input type="hidden" name="state" value="<?php echo trim($customer->State); ?>" > 

                <div class="modal-body">

                    <p class="bg-yellow p-2 text-dark">This reminder will be inserted into the Welldone Escalation System upon submit</p>
                    <p class="bg-yellow-orig p-2 text-dark "><strong class="myblink2">Set all reminder in <?php echo h_check_state($customer->State); ?> Time</strong></p>



                    <table class="table table-sm">
                        <tr>
                            <td>Reminder Type</td>
                            <td>
                                <?php $i=0; foreach($reminder_type as $reminder): ?>
                                    <div class="custom-control custom-radio">
                                      <input type="radio" id="reminder_type_<?php echo $reminder->id; ?>" name="reminder_type" class="custom-control-input" value="<?php echo $reminder->content; ?>" <?php echo $i==0?'required=""':'' ?>>
                                      <label class="custom-control-label" for="reminder_type_<?php echo $reminder->id; ?>"><?php echo $reminder->content; ?></label>
                                    </div>
                                <?php $i++; endforeach; ?>
                            </td>
                        <tr>
                            <td>Message</td>
                            <td>
                                <textarea class="form-control" name="message" rows="6"><?php echo $template; ?></textarea>
                            </td>
                            <tr>
                                <td colspan="2">Expires</td> 
                            </tr>
                            <tr id="r_opt_1">
                                <td>
                                    <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input" checked="" onclick="BookingDetailsNavbar.reminder_expiry_opt_sel('r_opt_1')">
                                      <label class="custom-control-label" for="customRadioInline1">Option 1</label>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-control-sm reminder_option" name="reminder_expirey1" id="reminder_expirey" data-toggle="datetimepicker" data-target="#reminder_expirey" value="" required="">
                                </td>
                            </tr>
                            <tr id="r_opt_2">
                                <td>
                                    <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input" onclick="BookingDetailsNavbar.reminder_expiry_opt_sel('r_opt_2')">
                                      <label class="custom-control-label" for="customRadioInline2">Option 2</label>
                                    </div>
                                </td>
                                <td>
                                    <select class="form-control form-control-sm reminder_option" name="reminder_expirey2" disabled="disabled" required="">
                                        <option value="">Select</option>
                                        <option value="10">10 Mins</option>
                                        <option value="15">15 Mins</option>
                                        <option value="30">30 Mins</option>
                                        <option value="60">1 Hour</option>
                                        <option value="90">1.5 Hours</option>
                                        <option value="120">2 Hours</option>
                                        <option value="150">2.5 Hours</option>
                                        <option value="180">3 Hours</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Agent Name</td>
                                <td><?php echo $this->user->name; ?></td>
                            </tr>
                        </tr>
                    </table>

                </div>

                <div class="mb-5 pb-3">                    
                    <button type="button" class="btn btn-secondary btn-sm float-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button> 
                </div>
                
            
            </form>
        </div>
    </div>
</div>