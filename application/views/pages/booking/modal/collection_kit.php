<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Collection Kit: Call On-Call Nurse for advise</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php 


                echo form_open('booking/save', 'name="CollectionKit-form" id="CollectionKit-form" class="col-sm-12" onsubmit="return Booking.confirm()" '); ?>

                <input type="hidden" name="section" value="lp_Q">
                <input type="hidden" name="qType" value="Collection_Kit">            
                <input type="hidden" name="qVal" value="0">            
                <input type="hidden" name="CustomerID" value="<?php echo @$CustomerID; ?>">            
                <input type="hidden" name="tran_id" value="<?php echo @$tran_id; ?>"> 
                
                <?php if(isset($issue_type)): ?>
                <input type="hidden" name="issue_type" value="<?php echo @$issue_type; ?>">            
                <?php endif; ?>

                <div class="modal-body">
                    <p><?php echo $content; ?></p>
                </div>
                <div class="modal-footer">                    
                    <button type="submit" class="btn btn-primary"><?php echo $button_text; ?></button> 
                </div>
            </form>
        </div>
    </div>
</div>