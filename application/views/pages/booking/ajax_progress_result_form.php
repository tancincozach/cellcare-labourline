



<div class="row border-top border-dark mt-4">

	 
	<div class="col-sm-12 mb-3 text-center mx-0 px-0"> 
		<h5 class="bg-light p-2 text-center" id="pSelectResult">Result</h5>
	</div>

	<div class="col-sm-12 mb-3 text-center"> 
		<div class="col-sm-8 text-center bg-yellow p-3 mx-auto">
			<h4><?php echo $progress_row->action1; ?></h4>
			<?php echo str_replace(PHP_EOL, '<br />', @$progress_row->script); ?>
		</div>
		<div class="col-sm-8 text-center bg-yellow p-3 mt-1 mx-auto">
			<strong>Advise Caller</strong><br/>
			<?php echo str_replace(PHP_EOL, '<br />', @$progress_row->result_text); ?>
		</div>

	</div>


	<?php if( !isset($progress_row->no_advice_script) ): ?>
	<div id="lbas_div_dynamic" class="col-sm-12 mb-2 mt-2 border-top bg-yellow" > 
	 
		<div class="col-sm-8 text-center  p-3 mx-auto">
		 	<?php echo form_open('', 'name="LabourProgress-script-form" id="LabourProgress-script-form" class="col-sm-12" onsubmit="return LabourProgress.advise_script_save(this)" '); ?>
				<input type="hidden" name="call_id" value="<?php echo @$call_id; ?>">

				<?php if( isset($redirect) ): ?>
				<input type="hidden" name="redirect" value="<?php echo @$redirect; ?>">
				<?php endif; ?>

				<table class="border table table-sm bg-light">
					<tr>
						<th colspan="3" class="bg-light">
							Script to say to client - Tick which script was used - this will autolog		
						</th>
					</tr>
					<?php 
					$advise_scripts = $this->db->get('list_advise_script')->result();
					foreach ($advise_scripts as $row): 

						if( !empty($transaction->baby) AND strpos(' '.$row->type, strtoupper($transaction->baby)) === false ){
							continue;
						}

					?>
					<tr>
						<td width="25%"><?php echo $row->type ?></td>
						<td width="70%"><?php echo stripslashes($row->script_text); ?></td>	 						
						<td width="5%">
							<input type="checkbox" name="advise_script[]" value="<?php echo stripslashes($row->script_text); ?>">
						</td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="4" class="text-center">

							<button type="submit" class="btn btn-sm btn-primary mt-3">Save and CONTINUE to RESULT</button>
						</td>
					</tr>
					 
				</table>
			</form>
		</div>

	</div>
	<?php endif; ?>
	 
	<div id="lbas_div_dynamic_forms" class="mt-2">

		<?php 
			if( $progress_row->callactivity == 'oncall'):
				echo $this->load->view('pages/booking/section_oncallform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
			endif; 
		?>

		<?php 
			if( $progress_row->callactivity == 'collector'):
				echo $this->load->view('pages/booking/section_collectorform', array('customer'=>$customer, 'transaction'=>$transaction, 'progress_row'=>$progress_row), TRUE);
			endif; 
		?>

		<?php 
			if( $progress_row->callactivity == 'collector_oncall'):
				echo $this->load->view('pages/booking/section_collector_oncallform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
			endif; 
		?>

	</div>

</div>
 