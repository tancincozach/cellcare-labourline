var Users = (function () {

  	// locally scoped Object
	var myObject = {};

	// declared with `var`, must be "private"
	var privateMethod = function () {};


	myObject.confirm = function ( message ) {

		if( confirm(message) ){

			return true;
		}else{
			return false;	
		}

		
	};

	myObject.search = function(){

	}


  	return myObject;

})();


$(function(){


	$(".toggle-password").click(function() {

		  $(this).toggleClass("fa-eye fa-eye-slash");
		  var input = $($(this).attr("toggle"));
		  if (input.attr("type") == "password") {
		    input.attr("type", "text");
		  } else {
		    input.attr("type", "password");
		  }
		});

});