<div class="col-sm-12 mb-5 ">
    
    <div class="row border mt-1 p-5">

        <div class="col-sm-8 pt-2 pb-5 mx-auto">            
            <h4 class="text-center">Calling for another reason</h4>
            
            <?php echo form_open('', 'name="caller-form" id="caller-form" class=""  '); ?>

                <input type="hidden" name="issue_type" value="Calling for another reason"> 
 

                <div class="col-sm-8 mx-auto mt-3">

                    <div class="row text-center">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="CallerName"  placeholder="Caller Name" value="<?php echo @$customer->PartnersFirstName; ?>" required="">
                        </div>
                        <div class="col-sm-6">
                            <input type="text"  maxlength="10" minlength="10" class="form-control" name="CallerPhone"  placeholder="Caller Phone" value="" required="">
                        </div>                        
                    </div>
                   
                </div>  

                <div class="col-sm-8 mx-auto pt-3">

                    <div class="text-center">
                        <p><strong>Advise Caller</strong></p>
                        <p>I am sorry you have called the labour line, I will transfer you</p>
                        <p>though to our main line and someone will be able to assist</p>
                        <p>TRANSFER TO:  1800 071 075</p>
                    </div>
                   

                    <div class="col-sm-8 mx-auto text-center mt-2">
                        <button class="btn btn-sm btn-primary">Submit</button>
                    </div>

                </div>      

  

            </form>

        </div>

    </div>


</div>