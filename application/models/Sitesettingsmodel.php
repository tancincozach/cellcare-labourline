<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitesettingsmodel extends CI_Model{

    var $table = 'configs';
    var $id = 'name';

    function add($set){
  
        return $this->db->insert($this->table, $set);
    }

    function update($id, $set){
        
        $this->db->where($this->id, $id);
        return $this->db->update($this->table, $set);

    }

    function update_by_name($name, $set){
        
        $query = $this->db->get_where($this->table, array('name'=>$name));

        if( $query->num_rows() > 0 ){

            $this->db->where('name', $name);
            return $this->db->update($this->table, $set);

        }else{
            $set['name'] = $name;
            return $this->add($set);
        }

    }   

    function delete($id){

        $this->db->where($this->id, $id);   
        return $this->db->delete($this->table);
    }

    function get_row($where){

        $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    function get_results($where=array()){

        if(!empty($where))
            $this->db->where($where);

        $query = $this->db->get($this->table);
        return $query->result();        
    }

 
}