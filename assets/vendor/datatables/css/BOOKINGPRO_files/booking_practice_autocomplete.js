
$( document ).ready(function(){
    
     
     $( "#practice_autocomplete" ).autocomplete({
        minLength: 2,
        //source: 'ajax/search_patient_match',
        source: function(request, response) {

            var searchtxt = (request.term).trim();

            if( searchtxt.length >= 2 ){

                $.ajax({
                    url: 'ajax/search_practice_match/',
                    dataType: 'json',
                    data: {
                        featureClass: 'P',
                        style: 'full',
                        maxRows: 12,
                        term: searchtxt
                    },
                    // The success event handler will display "No match found" if no items are returned.
                    success: function(data) {
                         
                        var result;
                        
                        if (!data || data.length === 0 ) {
                            result = [
                                {value:'', label:'No match found.', desc:'Please enter'}
                            ];
                        }
                        else {
                            result = data;
                        }
                        response(result);
                    },

                    error: function(request, status, error){
                        //alert(request.responseText);
                        alert('Session expired.');
                        //window.location = document.URL;
                    },

                    always: function(){

                        if( $("#practice_autocomplete").hasClass('ui-autocomplete-loading') ){
                            $("#practice_autocomplete").removeClass('ui-autocomplete-loading');
                        }
                    }

                });

            }else{

                if( $("#practice_autocomplete").hasClass('ui-autocomplete-loading') ){
                    $("#practice_autocomplete").removeClass('ui-autocomplete-loading');
                }
            }
        },
        open: function(event, ui) {
            $('#no_match_practice').val('');
        },
        focus: function( event, ui ) {
            var me = $(this);
            
            //console.log(ui.item.id);

            if( typeof ui.item.id !== "undefined"){
                me.val(ui.item.label);
            }

            return false;
        },
        select: function( event, ui ) {
 
            var me = $(this);

            //exisintg
            if( typeof ui.item.id !== "undefined"){
                
                me.val(ui.item.label);
 
            //new
            }else{
                var search = $('#practice_autocomplete').val();
                $('#no_match_practice').val(search);
            }

            //console.log(ui.item);

            return false;
        }
    })
    .data("uiAutocomplete")._renderItem = function( ul, item ) {
      return $( '<li style="cursor: pointer">' )
        .append( "<a><strong>" + item.label + "</strong><br>" + item.desc + "</a>" )
        .appendTo( ul );
    };       

   
    $("#practice_autocomplete").trigger($.Event("keypress", { keyCode: 40 }));
});