<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends MY_Controller {

	public function __construct(){

		parent::__construct();

		$this->load->model('Customermodel');
		$this->load->model('Transactionmodel');

		unset($this->view_data['maintain']);
	}


	public function index()
	{
		
		$this->load->view('template', $this->view_data);
 

	}

	public function current_calls($params=''){
		 

		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data['filters'] = $params;
 
		 
		//$query_params = $this->build_where($params);

		$query_params = '';
		
		if( $_GET and isset($_GET['search']) )	{
			$search = trim($params['search']);

			$where_str = " ( customers.CustomerID LIKE '%$search%' ";
			$where_str .= " OR concat(customers.FirstName, ' ', customers.LastName) LIKE '%$search%' ";
			$where_str .= " OR CallerName LIKE '%$search%' ) ";
			$query_params['where_str'] = $where_str;
		}

		$per_page = 50;
		$query_params['where']['tran_status >'] = 0;
		$query_params['where']['tran_type'] = 'Booking';
		$query_params['sorting'] = ' reminder_order DESC '; 
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;  
		$appt_results 				= $this->Transactionmodel->listing($query_params);

		//echo $this->db->last_query();

		 
		$params_query = @http_build_query($params);

        $p_config["base_url"] 		= base_url() . "current_calls/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		$this->view_data['data'] = $data; 
		$this->view_data['view_file'] = 'pages/booking/current_calls';
		$this->view_data['menu_active'] = 'currentcalls'; 	
		$this->view_data['js_file'] = '<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>'; 
		$this->view_data['js_file'] .= ' <script src="assets/vendor/mcautocomplete/mcautocomplete.js"></script>'; 		
		$this->view_data['js_file'] .= ' <script src="assets/js/pages/ac_booking_search_customer.js?v='.JS_V_bookingCA.'"></script>';

		$this->load->vars($this->view_data);

		$this->load->view('template');
 	
	}

	public function all_calls($params=''){


		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data['filters'] = $params;
 
		 
		//$query_params = $this->build_where($params);

		$query_params = '';
		
		if( $_GET and isset($_GET['search']) )	{
			$search = trim($params['search']);

			$where_str = " ( customers.CustomerID LIKE '%$search%' ";
			$where_str .= " OR concat(customers.FirstName, ' ', customers.LastName) LIKE '%$search%' ";
			$where_str .= " OR CallerName LIKE '%$search%' ) ";
			$query_params['where_str'] = $where_str;
		}

		$per_page = 50;
		
		$query_params['where']['tran_type'] = 'Booking';
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;  
		$appt_results 				= $this->Transactionmodel->listing($query_params);

		//echo $this->db->last_query();

		 
		$params_query = @http_build_query($params);

        $p_config["base_url"] 		= base_url() . "current_calls/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		$this->view_data['data'] = $data; 
		$this->view_data['view_file'] = 'pages/booking/all_calls';
		$this->view_data['menu_active'] = 'allcalls'; 	
		$this->view_data['js_file'] = '<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>'; 
		$this->view_data['js_file'] .= ' <script src="assets/vendor/mcautocomplete/mcautocomplete.js"></script>'; 		
		$this->view_data['js_file'] .= ' <script src="assets/js/pages/ac_booking_search_customer.js?v='.JS_V_bookingCA.'"></script>';

		$this->load->vars($this->view_data);

		$this->load->view('template'); 
	}

	public function open($CustomerID=''){

		$this->load->model('Callsmodel');

		try {
			
			$data = array('section'=>'', 'tab');
			$section = isset($_GET['section'])?$_GET['section']:'caller';
			$issue_type = isset($_GET['issue_type'])?$_GET['issue_type']:'1';

			$data ['issue_type'] = $issue_type;

			if( empty($CustomerID) ) throw new Exception("Customer not recognized", 1);
			if( !is_numeric($CustomerID) ) throw new Exception("Customer not recognized", 1);
			

			//1: verify customer 
			$customer = $this->Customermodel->details( array('where'=>array('customers.CustomerID'=>$CustomerID)) );
			
			if( !isset($customer->CustomerID) ) throw new Exception("Customer not recognized", 1);

			//2: check transaction if already exist
			$transaction = $this->Transactionmodel->row( array('where'=>array('CustomerID'=>$CustomerID)) );
 		
 			if( isset($_GET['show_next_form']) ){
				$data['show_next_form'] = $_GET['show_next_form'];
 			}
			
			if( $customer->IsMotherCollnAppEligible == 'Yes' OR $customer->IsPartnerCollnAppEligible == 'Yes'  ){
				$data['APP_popup'] = 1;
			}

			$data['tab'] = @$_GET['tab'];
			$data['customer'] = $customer;
			$data['transaction'] = $transaction; 

			if( count($transaction) == 0 ){

				$section = 'caller';

			}else{			  
				$edd = format_datetime($customer->EDD); 
				$early_edd = 0;
				if( strtotime($edd) > strtotime('now') ){
					 
					$dStart = new DateTime('now');
					$dEnd  = new DateTime($edd);
					$dDiff = $dStart->diff($dEnd);
					$early_edd = $dDiff->format('%r%a'); // use for point out relation: smaller/greater
				}

				//if(  !in_array($issue_type, array('INDUCTION', 'CAESAR')) ){

					 	if($transaction->confirm_hospital != 1){
					 		$section = 'hospital';
					 	}elseif( $customer->CellCareCollector == 'No' AND $transaction->CellCareCollector_verify=='' ){
							$section = 'cccverify';
						}elseif( trim($customer->ObsHistoryNotes) == '' AND @$transaction->ObsHistoryNotes_verify==''){

							if( $transaction->confirm_hospital == 1 ){
								$section = 'oncall';
							}

						//Premature Birth
						}elseif( $early_edd > 29 AND empty($transaction->edd_premature_verify) ) {

							$section = 'oncall';
							$data['show_next_form'] = 'Premature Birth';
						}

				//}
 
			} 


			if( in_array($section, array('oncall', 'collector', 'clienthospital')) ){

				$this->db->where('call_group', $section);
				$this->db->where('call_res_status', '1');
				$this->db->select('call_res_id, call_res_text, distance_required, reminder_popup');
				// $callresponse = $this->db->get('call_responses');
				// $callresponse = $callresponse->result();
				$callresponse = $this->db->get('call_responses')->result();
				// $cr_opt = array(''=>'');
				// foreach ($callresponse as $row) {
				// 	$cr_opt[$row->call_res_id] = $row->call_res_text;
				// }
				// $data['callresponses']  = $cr_opt;
				$data['callresponses']  = $callresponse;


				if( $section == 'oncall' ) {
					$data['cellcare_oncall_details'] = $this->Callsmodel->get_cellcare_oncall_details();					 
				}
			

			}elseif($section == 'labourprogress'){

				
				$data['advise_scripts'] = $this->db->get('list_advise_script')->result();
  
			}

			//$data['more_collectors'] = $this->db->get('list_collectors')->result();


			$data['calls_result'] = $this->Callsmodel->basic_results(array('where'=>array('tran_id'=>@$transaction->tran_id), 'state'=>$customer->State));

			$data['section'] = $section;

			
			$this->view_data['data'] = $data; 
			$this->view_data['view_file'] = 'pages/booking/details';
			$this->view_data['menu_active'] = 'details';
			$this->view_data['js_file'] .= PHP_EOL.'<script src="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.full.min.js"></script>';  //datetimepicker
			$this->view_data['js_file'] .= PHP_EOL.'<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>';
			$this->view_data['js_file'] .= PHP_EOL.'<script src="assets/vendor/mcautocomplete/mcautocomplete.js"></script>';
			$this->view_data['js_file'] .= PHP_EOL.'<script src="assets/js/pages/ac_booking_open_hospital.js"></script>'; //auto complete
			$this->view_data['css_file'] = '<link href="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">';  //datetimepicker


			$this->load->vars($this->view_data);

			$this->load->view('template');
			//$this->load->view('template', $this->view_data);

		} catch (Exception $e) {
			echo $e->getMessage();
		}

	}

	public function save(){
		
		$this->load->library("Cma_external");
		$this->load->model('Callsmodel');
		$this->load->model('Callresponsemodel');
		$this->load->model('Sitesettingsmodel');

		$post = $this->input->post();

		try {
			
			if( !$_POST ) throw new Exception("Error Processing Request", 1);			
			
			$cma = $this->Sitesettingsmodel->get_row(array('name'=>'CMA_SETTINGS'));
			$cma = json_decode($cma->value);

			$section = @$post['section'];
			$issue_type = @$post['issue_type']; 
			$issue_type_uri = (isset($issue_type))?'&issue_type='.$issue_type:'';

			switch ($section) {
				
				//First time
				case 'caller':
					$CustomerID = $post['CustomerID'];
					$tran_id = @$post['tran_id'];
					
					$_uri_segment = array();

					$flash_success = array();
					$flash_error = array();

					$set= array();
					
					$first_call = ''; 

					$set['CustomerID'] 		= $post['CustomerID'];
					//$set['client_status'] 	= $post['client_status'];
					$set['CallerName'] 		= $post['CallerName'];
					$set['CallerPhone'] 	= trim($post['CallerPhone']);
					$set['ClientRelation'] 	= $post['ClientRelation'];
					$set['agent_name'] 		= $this->user->name;
					$set['tran_updated'] = date('Y-m-d H:i:s');

					$_uri_segment['CallerName'] = $set['CallerName'];
					$_uri_segment['CallerPhone'] = $set['CallerPhone'];


					//The "best phone # from now on"  This just needs to be STAMPED in the client record
					//if( $post['PartnersMobilePhone'] != $set['CallerPhone'] AND $set['ClientRelation'] == 'CLIENT RELATION' ){
					if( !empty($post['best_no_to_call']) AND $set['ClientRelation'] == 'CLIENT RELATION' ){
						$set['best_no_to_call'] = $set['CallerPhone'];
					}

					if( $set['ClientRelation'] == 'COLLECTOR' ) {
						$set['collector']		= $set['CallerName'];
						$set['collector_phone']	= $set['CallerPhone'];

					}

					// if( $set['ClientRelation'] == 'ON CALL' ) {
					// 	$set['collector'] 		= $set['CallerName'];
					// 	$set['collector_phone'] = $set['CallerPhone'];
					// }


					$flash_success[] = 'Successfully submitted, please proceed to Confirm Hospital';

					if( $tran_id == '' ){

						$first_call = 1;

						if( !$tran_id=$this->Transactionmodel->insert($set) ){
							$flash_error[] = 'Unable to proceed something went wrong';
						} 
					}else{
						unset($set['CustomerID']);
						unset($set['CallerName']);
						unset($set['CallerPhone']);
						unset($set['ClientRelation']);
						if( !$this->Transactionmodel->update($tran_id, $set) ){
							$flash_error[] = 'Unable to proceed something went wrong';
						} 
					}

					//if( strpos(' '.$flash_msg, 'success')){
						 
						$callset = array();
						$callset['tran_id'] 		= $tran_id;
						$callset['caller_name'] 	= $post['CallerName'];
						$callset['caller_phone'] 	= $post['CallerPhone'];
						$callset['ClientRelation'] 	= $post['ClientRelation'];
						$callset['call_type'] 		= 'caller';
						$callset['agent_name'] 		= $this->user->name;
						$callset['user_id'] 		= $this->user->agent_id;
						$this->Callsmodel->insert($callset);
						 
					//}  
 					
 					if( !empty($tran_id) ){
 						$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));
 					}


					switch ($post['ClientRelation']) {
						case 'COLLECTOR':
							$flash_success[] = 'Successfully submitted, please proceed COLLECTOR Activity Form';
							$section = 'collector';
							break;
						
						case 'ON CALL':
							$flash_success[] = 'Successfully submitted, please proceed ON CALL Activity Form';
							$section = 'oncall';
							break;
						
						default:
							$section = 'hospital';
							if( in_array($issue_type, array('CAESAR', 'INDUCTION')) ){
								
								$section = (isset($tran_row->confirm_hospital) AND @$tran_row->confirm_hospital==1)?'oncall':'hospital';								
								$_uri_segment['issue_type'] = $issue_type;

							}elseif( $first_call == '' ){
								$flash_success = array();
								$flash_success[] = 'Successfully submitted, please proceed Labour Progress Form';
								$section = 'labourprogress';
							}
							break;
					}

					if( count($flash_success) > 0 ){
						$this->session->set_flashdata('flash_success', implode('|',$flash_success));	
					}

					if( count($flash_error) > 0 ){
						$this->session->set_flashdata('flash_error', implode('|',$flash_error));	
					}

					$_uri_segment['section'] = $section;

					redirect('booking/open/'.$CustomerID.'?'.http_build_query($_uri_segment)).$issue_type_uri;


					break;
				
				case 'hospital':

					//AJAX SUBMIT

					$tran_id = $post['tran_id'];
					$CustomerID = $post['CustomerID'];
					//$issue_type = $post['issue_type'];

					$set = array();
					$callset = array();

					$flash_success = array();
					$flash_error = array();
					$call_log = array();


					if( isset($post['collection_kit']) ){
						$set['collection_kit'] = $post['collection_kit'];
					}


					$set['confirm_hospital'] = $post['confirm_hospital']; 
					

					if( isset($post['sched_induction_d']) ){
						$set['sched_induction'] = format_datetime($post['sched_induction_d']);
						$set['sched_induction_ta'] = $post['sched_induction_ta'];
						$set['sched_induction_type'] = $post['sched_induction_type'];

						$set['sched_induction_notes'] = addslashes($post['sched_induction_notes']);

						$call_log['Cell Care to organise collector'] = null;
						//$call_log['Baby Status'] = 'Scheduled Induction - Action by CellCare '.$post['sched_induction_d'].' '.$post['sched_induction_t'];
						$call_log['Baby Status'] = 'Scheduled Induction - '.$post['sched_induction_d'];
						$call_log['Time of Admission'] = $post['sched_induction_ta'];
						$call_log['Induction Type'] = $post['sched_induction_type'];
						$call_log['Induction Notes'] = $set['sched_induction_notes'];

						$flash_success[] = 'Scheduled Induction';


						$sched_dt = $set['sched_induction'];
						$time_of_admission = $post['sched_induction_ta'];
					}

					if( isset($post['sched_ceasar_d']) ){
						$set['sched_ceasar'] = format_datetime($post['sched_ceasar_d']);
						$set['sched_ceasar_ta'] = $post['sched_ceasar_ta'];
						$set['sched_ceasar_to'] = $post['sched_ceasar_to'];
						$set['sched_ceasar_notes'] = addslashes($post['sched_ceasar_notes']); 

						$call_log['Cell Care to organise collector'] = null;
						$call_log['Baby Status'] = 'Scheduled C-Section - '.$post['sched_ceasar_d'];
						$call_log['Time of Admission'] = $post['sched_ceasar_ta'];
						$call_log['Time of Operation'] = $post['sched_ceasar_to'];
						$call_log['C-Section Notes'] = $set['sched_ceasar_notes'];

						$flash_success[] = 'Scheduled C-Section';

						$sched_dt = $set['sched_ceasar'];
						$time_of_admission = $post['sched_ceasar_ta'];
						$sched_ceasar_operation = $post['sched_ceasar_to'];

						//reminder here
						//Once we have the TIME .. Reminder set for 10mins 

						if( !in_array(@$sched_ceasar_operation, array('AM', 'PM', 'UNKNOWN')) ){
							//$temp_TO = format_datetime($post['sched_ceasar_d'].' '.$post['sched_ceasar_to']);
							$temp_TO = $sched_dt.' '.$sched_ceasar_operation;
							$temp_TO = date('Y-m-d H:i:s',strtotime($temp_TO.' -10 minutes '));
							$set['reminder'] = $temp_TO;
						}

					}



					//$call_log['Hospital Details'] = $this->Commonmodel->hash('confirm_hosptail',$post['confirm_hospital'], 1);
					//$call_log['Hospital Details'] = '';
					$hospital_details = array();
					$hospital_details[]	= @$post['Hospital'];
					$hospital_details[] = @$post['HospitalStreet'];
					$hospital_details[] = @$post['HospitalCity'];
					$hospital_details[] = @$post['HospitalRegion'];
					$hospital_details[] = @$post['HospitalPostalCode'];
					$hospital_details[] = @$post['HospitalPhone'];

					$call_log['Hospital Details'] = implode(', ', $hospital_details);

					if( empty($set['confirm_hospital']) AND !empty($post['Hospital']) ){
						$set['confirm_hospital'] = 1;
					}


					if( $this->Transactionmodel->update($tran_id, $set) ){
						 
						$flash_success[] = 'Hospital Form successfully submitted.';
						 
						$hospital_update = $this->Customermodel->update_hospital($CustomerID, $post);


						if( $hospital_update == '1' ){
							
							$flash_msg[] = 'Hospital successfully changed.';
							$call_log['Hospital Changed'] = 'Yes';
							//$call_log['Hospital'] = @$post['Hospital'];

						}elseif( $hospital_update == '2' ){
							$flash_success[] = 'No change on Hospital';
						}else{
							$flash_error[] = 'Error on processing hospital update.';
						}

					}else{
						$flash_error[] = 'Error on processing transaction';
					}


					
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'hospital';
					$callset['more_text'] 		= json_encode( $call_log );
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					$call_id = $this->Callsmodel->insert($callset);

					if( count($flash_success) > 0 ){
						$this->session->set_flashdata('flash_success', implode('|',$flash_success));	
					}

					if( count($flash_error) > 0 ){
						$this->session->set_flashdata('flash_error', implode('|',$flash_error));	
					}


					$json_data = array();


					if( in_array($issue_type, array('INDUCTION', 'CAESAR')) ){
						


						$datetime1 = new DateTime($sched_dt);
						$datetime2 = new DateTime('now'); 

						$interval = $datetime1->diff($datetime2);						

						if( isset($post['collection_kit']) AND !$post['collection_kit'] ){

							$json_data['modal'] = 1;
							
							$json_data['title'] = 'Collection Kit: Call On-Call Nurse for advise';
							$json_data['header1'] = 'Advise Caller';
														 
							$json_data['body'] = 'We will contact the On-Call Nurse who will call you and make arrangements to provide a kit.<br />';
							$json_data['body'] .= 'Please keep your phone on and expect a return call within 15 minutes.<br />';

							$json_data['redirect_btn_txt'] = 'Proceed to On-Call Nurse';
							$json_data['redirect_url'] = 'booking/open/'.$CustomerID.'?section=oncall'.$issue_type_uri; 

						}elseif( $interval->format('%a') > 2 ){
							$json_data['modal'] = 1;

							$json_data['title'] = 'CELL CARE to organise Collector';
							$json_data['header1'] = '';

							$json_data['body'] = 'Thank you, we will organise a Collector for this day & time. <br />';
							$json_data['body'] .= 'If anything changes please call us back Immediately'; 

							$json_data['redirect_btn_txt'] = 'Continue';
							$json_data['redirect_url'] = 'booking/open/'.$CustomerID.'?section=callhistory'.$issue_type_uri;
						}else{
							$json_data['status'] = 1;

							if( in_array(@$sched_induction_ta, array('AM', 'PM', 'UNKNOWN')) ){
								$json_data['redirect'] = 'booking/open/'.$CustomerID.'?section=oncall&issue_type='.$issue_type.'&warn'.$issue_type.'=1';
							}elseif( in_array(@$sched_ceasar_operation, array('AM', 'PM', 'UNKNOWN')) ){
								$json_data['redirect'] = 'booking/open/'.$CustomerID.'?section=oncall&issue_type='.$issue_type.'&warn'.$issue_type.'=1';
							}else{
								$json_data['redirect'] = 'booking/open/'.$CustomerID.'?section=collector&issue_type='.$issue_type.'&warn'.$issue_type.'=1';
							}
						}


					}else{
						$json_data['status'] = 1;
						$json_data['redirect'] = 'booking/open/'.$CustomerID.'?section=labourprogress';
					}


					//CMA AUTO LOG
					$auto_log_p = array();
					$auto_log_p['tran_id'] = $tran_id;
					$auto_log_p['call_id'] = $call_id;
					$auto_log_p['message'] = 'Client ID: '.$CustomerID.PHP_EOL.h_array_to_string($call_log);
					
					$this->Commonmodel->cma_auto_log($auto_log_p);


					echo json_encode($json_data); exit;

					break;

				case 'lp_Q':
					
					$qReturn = $this->lp_Q_save();

					if( isset($qReturn['redirect']) ){

						$this->session->set_flashdata('flash_success', $qReturn['flash_msg']);

					if( count(@$qReturn['flash_success']) > 0 ){
						$this->session->set_flashdata('flash_success', implode('|',$qReturn['flash_success']));	
					}

					if( count($qReturn['flash_error']) > 0 ){
						$this->session->set_flashdata('flash_error', implode('|',$qReturn['flash_error']));	
					}

						redirect($qReturn['redirect']);

					}else{

						echo json_encode($qReturn); exit;
					}

					break; 

				case 'labourprogress':
					

					$CustomerID = $post['CustomerID'];
					$tran_id = $post['tran_id'];
					$progress_selected = @$post['progress_selected'];

					$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));
					$customer = $this->Customermodel->details( array('where'=>array('customers.CustomerID'=>$CustomerID)) );
					//print_r($post);
					//print_r($tran_row);

					if( @$post['waters_broken_dt'] == '__/__/____ __:__' ) $post['waters_broken_dt'] = '';
					if( @$post['epidural_dt'] == '__/__/____ __:__' ) $post['epidural_dt'] = '';
					if( @$post['hormone_started_dt'] == '__/__/____ __:__' ) $post['hormone_started_dt'] = '';
					if( $post['last_exam'] == '__/__/____ __:__' ) $post['last_exam'] = '';
					if( $post['next_exam'] == '__/__/____ __:__' ) $post['next_exam'] = '';
 
 					if( isset($post['waters_broken']) ){
						$post['waters_broken'] = (opt_val($post['waters_broken'])).($post['waters_broken']=='1'?' @ '.$post['waters_broken_dt']:'');
 					}
					

					if( isset($post['epidural']) ){
						$post['epidural'] = (opt_val($post['epidural'])).($post['epidural']=='1'?' @ '.$post['epidural_dt']:'');
					}
					
					if( isset($post['hormone_started']) ){
						$post['hormone_started'] = (opt_val($post['hormone_started'])).($post['hormone_started']=='1'?' @ '.$post['hormone_started_dt']:'');
					}

					$more_text = array();
					$transet = array();

					//if( $tran_row->collection_kit !=  @$post['collection_kit'] AND isset($post['collection_kit']) ){
					if( isset($post['collection_kit']) ){
						$transet['collection_kit'] = $post['collection_kit'];
						$more_text['Do you have Collection Kit ?'] = opt_val($post['collection_kit']);
					}

					//if( $tran_row->waters_broken !=  @$post['waters_broken'] AND isset($post['waters_broken']) ){
					if( isset($post['waters_broken']) ){
						$transet['waters_broken'] = $post['waters_broken'];
						$more_text['Has clients waters broken ?'] = @$post['waters_broken'];
					}

					if( $tran_row->epidural !=  @$post['epidural'] AND isset($post['epidural'])){
						$transet['epidural'] = $post['epidural'];
						$more_text['Has client been given an epidural ?'] = @$post['epidural'];
					}

					//if( $tran_row->hormone_started !=  @$post['hormone_started'] AND isset($post['hormone_started'])){
					if( isset($post['hormone_started'])){
						$transet['hormone_started'] = $post['hormone_started'];
						//$more_text['Has client had a hormone started ?'] = opt_val(@$post['hormone_started']);
						$more_text['Has client had a hormone drip started ?'] = @$post['hormone_started'];
					}
					// if( isset($post['hormone_started']) AND isset($post['hormone_started_type']) ){
					// 	$transet['hormone_started_type'] = $post['hormone_started_type'];
					// 	$more_text['Hormone Drip Type'] = @$post['hormone_started_type'];
					// }


					//if( $tran_row->caesar_induction !=  @$post['caesar_induction']){
					if( isset($post['caesar_induction']) ){
						$transet['caesar_induction'] = $post['caesar_induction'];
						//$more_text['Has client had an induction ?'] = opt_val(@$post['caesar_induction']);
						$more_text['Is Client being induced'] = opt_val(@$post['caesar_induction']);
					}
					if( isset($post['induction_type']) ){
						$transet['caesar_induction_type'] = $post['induction_type'];
						$more_text['Induction Type'] = @$post['induction_type'];
					}

					if( $tran_row->client_status !=  $post['client_status']){

						if( $post['client_status'] != 'NO CHANGE' ){						 
							$transet['client_status'] = $post['client_status'];
						}
						$more_text['CLIENT STATUS'] = $this->Commonmodel->hash('client_status', $post['client_status'], 1);
					}


					// if( $tran_row->caesar_induction !=  $post['caesar_induction']) $set['caesar_induction'] = $post['caesar_induction'];
					// if( $tran_row->waters_broken !=  $post['waters_broken']) $set['waters_broken'] = $post['waters_broken'];
					// if( $tran_row->epidural !=  $post['epidural']) $set['epidural'] = $post['epidural'];
					// if( $tran_row->hormone_started !=  $post['hormone_started']) $set['hormone_started'] = $post['hormone_started'];
					
					if( $tran_row->baby !=  $post['baby']) {
						$transet['baby'] = $post['baby'];

						$more_text['Baby'] = $post['baby'];
					}

					 				
					if( $tran_row->last_exam !=  $post['last_exam']) {
						$transet['last_exam'] = ($post['last_exam'] == '')?'':format_datetime($post['last_exam']);
						$more_text['Last Exam'] = $post['last_exam'];
					}

					if($post['last_exam']==''AND isset($post['lastexam_unknown']) ){
						$more_text['Last Exam'] = 'Unknown';
					}

					if( $tran_row->next_exam !=  $post['next_exam']){
						$transet['next_exam'] = ($post['next_exam'] == '')?'':format_datetime($post['next_exam']);
						$more_text['Next Exam'] = $post['next_exam'];
					} 

					if($post['next_exam']==''AND isset($post['next_exam_unknown']) ){
						$more_text['Next Exam'] = 'Unknown';
					}

					if( isset($post['lp_contract']) ){
						$transet['lp_contract'] = $post['lp_contract'];
						$more_text['Contracting'] = opt_val($post['lp_contract']);
					}

					if( isset($post['freq_contraction']) AND $tran_row->freq_contraction !=  $post['freq_contraction']){
						$transet['freq_contraction'] = $post['freq_contraction'];
						$more_text['Frequency of Contractions'] = opt_freq_contraction($post['freq_contraction']);
					}

					if( isset($post['length_contraction']) AND $tran_row->length_contraction !=  $post['length_contraction']){
						$transet['length_contraction'] = $post['length_contraction'];
						$more_text['Length of Contractions'] = opt_length_contraction($post['length_contraction']);
					}

					if( isset($post['cent_dilated']) AND $tran_row->cent_dilated !=  $post['cent_dilated']){
						$transet['cent_dilated'] = $post['cent_dilated'];
						$more_text['Centimeters Dilated'] = opt_cent_dilated($post['cent_dilated']);
					}


					if( $progress_selected != '' ){
						$progress = $this->Commonmodel->progress_selector_text($progress_selected);
						$more_text['Progress Selector'] = $progress->client_status.' - '.$progress->action1;

						$transet['last_progress_selected'] = $progress_selected; 
					}

					 


					//if( !empty($post['notes']) ) $transet['notes'] = $post['notes'];
					
					//AT HOME
					if( $post['client_status']  == 1 ){
						if( $post['baby'] == 'First' ){
							$more_text['advise_script'] = 'Advised to call back when en-route to hospital';
						}else{
							$more_text['advise_script'] = 'Update Collector & advise to call back when en-route to hospital';
						}
					}

					// else if( !empty($post['advise_script']) ){
					// 	$more_text['advise_script'] = implode('; ', $post['advise_script']);
					// }

					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_notes'] 		= $post['notes'];

					if( !empty($more_text) ){
						$callset['more_text'] 		= json_encode( $more_text );
					}
					 
					$callset['call_type'] 		= 'labourprogress';
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;

					$call_id = $this->Callsmodel->insert($callset);

					// if( !$call_id = $this->Callsmodel->insert($callset) ){
					// 	//error here
					// }


					$this->Transactionmodel->update($tran_id, $transet);

					
					$json_data = array();
					$tpl_data = array();
					$json_data['call_id'] = $call_id;;
					
					if( isset($post['modal_submit']) ) {

						$this->db->where('id', $progress_selected);						
						$progress_row = $this->db->get('progress_selector')->row();


					//At Home
					}elseif( $post['client_status'] == 1 ){

						$progress_row = new stdClass();
						

						if( $post['baby'] == 'First' ){
							$json_data['redirect'] = 'booking/open/'.$CustomerID.'/?&issue_type=1&section=callhistory';
							$progress_row->no_advice_script = '1';
							$progress_row->callactivity = '';
							$progress_row->action1 = 'Advised to call back when en-route to hospital';
							$progress_row->result_text = 'Please call back when you are EN-ROUTE to hospital or call us back at 9am (local time) in the morning if have not gone to hospital earlier';

						}else{

							$progress_row->callactivity = 'collector';
							$progress_row->action1 = 'Update Collector & advise to call back when en-route to hospital';
							$progress_row->result_text = 'We will update the Collector.  Please call back when you are EN-ROUTE to hospital or call us back at 9am (local time) in the morning if have not gone to hospital earlier';

						}


					}elseif( !@$post['lp_contract'] ){

						$progress_row = new stdClass();
						$progress_row->callactivity = 'collector';
						$progress_row->action1 = 'UPDATE Collector';
						$progress_row->result_text = 'We will update the Collector. Please  call back IMMEDIATELY if an internal exam is done to check dilation or if any "talk" of a possible C-Section or rapid change in labour progress, OTHERWISE call back in 2hrs time.';
					 
					}else{

						$progress_row = new stdClass();
						$progress_row->callactivity = 'collector';
						$progress_row->action1 = 'UPDATE Collector';
						$progress_row->result_text = 'We will update the Collector. Please  call back IMMEDIATELY if an internal exam is done to check dilation or if any "talk" of a possible C-Section or rapid change in labour progress, OTHERWISE call back in 2hrs time.';
					}

					if( in_array($progress_row->callactivity, array('oncall', 'collector', 'collector_oncall')) ){

						$pcallactivity = ($progress_row->callactivity=='collector_oncall')?'oncall':$progress_row->callactivity;

						$this->db->where('call_group', $pcallactivity);
						$this->db->where('call_res_status', '1');
						$this->db->select('call_res_id, call_res_text, distance_required'); 
						$callresponse = $this->db->get('call_responses')->result(); 
						$json_data['callresponses']  = $callresponse;


						if( $pcallactivity == 'oncall' OR  $pcallactivity == 'collector_oncall' ) {
							//$json_data['prev_clientrelation_call'] = $this->Callsmodel->get_prev_clientrelation_calls($tran_id);
							$json_data['cellcare_oncall_details'] = $this->Callsmodel->get_cellcare_oncall_details();					 
						}
					}

					 
					$json_data['customer'] = $customer;
					$json_data['transaction'] = $this->Transactionmodel->row( array('where'=>array('CustomerID'=>$CustomerID)) );
					$json_data['progress_row'] = $progress_row;


					$calls_result= $this->Callsmodel->basic_results(array('where'=>array('tran_id'=>$tran_id), 'state'=>$customer->State));
					$tpl_data['calls_result'] = $this->load->view('pages/booking/call_history', array('calls_result'=>$calls_result), TRUE);
					$tpl_data['html'] = $this->load->view('pages/booking/ajax_progress_result_form', $json_data, TRUE);
					
					//CMA AUTO LOG
					$auto_log_p = array();
					$auto_log_p['tran_id'] = $tran_id;
					$auto_log_p['call_id'] = $call_id;
					$_message = 'Client ID: '.$CustomerID.PHP_EOL.h_array_to_string($more_text).PHP_EOL.'Notes :'.$callset['call_notes'];
					$auto_log_p['message'] = $_message;
					
					$this->Commonmodel->cma_auto_log($auto_log_p);

					echo json_encode($tpl_data);

					exit;

					break; 

				case 'collector':

					$CustomerID = $post['CustomerID'];
					$tran_id = $post['tran_id'];

					$customer = $this->Customermodel->details(array('where'=>array('CustomerID'=>$CustomerID)));
					$callresponse = $this->Callresponsemodel->row(array('where'=>array('call_res_id'=>$post['call_res_id'])));

					$show_next_form = @$post['show_next_form'];

					$caller_name = trim($post['caller_name']);
					$caller_phone = trim($post['caller_phone']);
					$collector_distance = trim(@$post['collector_distance']);
					$notes = addslashes(@$post['notes']);


					$callset = array();
					$callset['tran_id'] 		= $tran_id;
					$callset['caller_name'] 	= $caller_name;
					$callset['caller_phone'] 	= $caller_phone;
					$callset['call_res_id'] 	= $post['call_res_id'];
					$callset['call_notes'] 		= $notes;
					
					$more_text = array();

					if( $collector_distance != '' ){
						//$callset['more_text'] 		= json_encode( array('What is your travel time to hospital '=>(isset($post['collector_distance_nochange'])?'No Change':$collector_distance.' mins')) );
						$more_text['What is your travel time to hospital'] = isset($post['collector_distance_nochange'])?'No Change':$collector_distance.' mins';
					}
					
					$more_text['Status'] = $this->statuses[$callresponse->call_res_open];
					 
					if( count($more_text) > 0 ){
						$callset['more_text'] = json_encode($more_text);
					}

					$callset['call_type'] 		= 'collector';
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					
					if( $call_id = $this->Callsmodel->insert($callset) ){

						$transet = array();
						$transet['tran_status'] 		= $callresponse->call_res_open;
						$transet['tran_updated'] 		= date('Y-m-d H:i:s');
						$transet['collector'] 			= $caller_name;						
						$transet['collector_status'] 	= $callresponse->call_res_text;
						$transet['collector_distance'] 	= $collector_distance;
						$transet['collector_phone'] 	= $caller_phone;

						if( $callresponse->reminder > 0){
							$transet['reminder'] 	= date('Y-m-d H:i:s', strtotime('now + '.$callresponse->reminder.' minute'));
						}

						$this->Transactionmodel->update($tran_id, $transet);

						$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));

						//SMS SENT HERE
						//CellCare Client: Barclay Nicolle - 183452 - 0426 542 404 - Flinders Medical Centre - 6/01/2019 - CBT - 427972244 - Bradley Barclay : SMS Sent successfully to: 0414757887
						//** Whenever we DESPATCH or UPDATE  a collector we need to send them an SMS … only send once.
						//** If collector CHANGED .. We need to send the SMS to then new collector ONLY ONCE.
						
						$tran_row->baby = ((trim(@$tran_row->baby)!='')?$tran_row->baby:($customer->PreviousDeliveries>'0'?'Multi':'First'));


						if( $this->Callsmodel->check_sms_sent($tran_id, $caller_phone) AND @$callresponse->send_sms == '1' ){							

							
							//send sms
							// $message = "Cellcare Client: ".@$customer->FirstName.' - '.@$customer->LastName." - ".@$CustomerID." - ".@$customer->MobilePhone;
							// $message .= " - ".@$customer->Hospital." - ".@$customer->EDD;
							// $message .= " - CBT -".@$customer->PartnersMobilePhone." - ".@$customer->PartnersFirstName;
							
							//INDUCTION:  Cellcare Client: Sujatha Narisetty - 190131 - 434792432 - Moruya Hospital - 25/03/2019 - CBT - 434792432 - Sundeep - Booked 9/5/19 16:00 - arrive 1 hour prior								
							//C-SECTION BOOKED 9/5/19 16:00 - Cellcare Client: Sujatha Narisetty - 190131 - 434792432 - Moruya Hospital - 25/03/2019 - CBT - 434792432 - Sundeep - arrive 1 hour prior
				 
				 			/*$sched_type = '';
							$_msg = array();

							if( $tran_row->sched_induction != '' ){
								$_msg[] = "INDUCTION:  Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;
								
								$sched_type = 'induction';

							}else if( $tran_row->sched_ceasar != '' ) {
								$_msg[] = "C-SECTION BOOKED ".date('d/m/Y', strtotime($tran_row->sched_ceasar))." OT: ".$tran_row->sched_ceasar_to;

								if( $tran_row->sched_ceasar != '' AND !in_array($tran_row->sched_ceasar_to, array('AM', 'PM', 'UNKNOWN')) ) {

									$_msg[] = "arrive 1 hour prior";

								}

								$_msg[] = "Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;

								$sched_type = 'c-section';

							}else{
								$_msg[] = "Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;
							}
							

							$_msg[] = $CustomerID;
							$_msg[] = @$tran_row->baby;
							$_msg[] = $customer->MobilePhone;
							$_msg[] = $customer->Hospital;
							$_msg[] = $customer->EDD;
							$_msg[] = 'CBT';
							$_msg[] = $customer->PartnersMobilePhone;
							$_msg[] = $customer->PartnersFirstName;

							if( $tran_row->sched_induction != '' ) {
								$_msg[] = " Booked ".date('d/m/Y', strtotime($tran_row->sched_induction))." ".$tran_row->sched_induction_ta.' - '.@$tran_row->sched_induction_type;
								

							}
							// elseif( $tran_row->sched_ceasar != '' AND !in_array($tran_row->sched_ceasar_to, array('AM', 'PM', 'UNKNOWN')) ) {

							// 	$_msg[] = "arrive 1 hour prior";

							// }
							//else if( $tran_row->sched_ceasar != '' ) $_msg[] = " arrive 1 hour prior";

							$_msg_f = array_filter($_msg);
							$message = implode(' - ', $_msg_f);*/
							 
							$template = $this->Commonmodel->booking_sms_template($tran_row, $customer);
							$message = @$template->message;
							$sched_type = @$template->sched_type;


							//echo $message;
							$email_param = array(); 
							$email_param['tran_id'] = $tran_id;							
							$email_param['call_id'] = $call_id;							
							$email_param['to']		= trim($post['caller_phone']);
							$email_param['message']	= $message;
							$this->Commonmodel->send_sms($email_param);					
						}

						/*if($callresponse->reminder > 0) {

							

							$reminder_message = 'Client ID: '.$customer->CustomerID.' - Client Name: '.$customer->FirstName.' '.$customer->LastName.' ';
							
							$expiry = '';
							
							if( $sched_type == 'induction' AND !in_array($tran_row->sched_induction_ta, array('AM', 'PM', 'UNKNOWN')) ){
								$expiry = strtotime($tran_row->sched_induction.' '.$tran_row->sched_induction_ta+' - '.$callresponse->reminder.' minute');
							}elseif( $sched_type == 'c-section' AND !in_array($tran_row->sched_ceasar_to, array('AM', 'PM', 'UNKNOWN')) ){
								$expiry = strtotime($tran_row->sched_ceasar.' '.$tran_row->sched_ceasar_to+' - '.$callresponse->reminder.' minute');
							}else{
								//$expiry = strtotime('now + '.$callresponse->reminder.' minute');
							}
 							
 							//echo date('d/m/Y H:i:s', $expiry);
 							//exit; 

 							if( $expiry != '' ){

								$a = array();
								$a['message'] 	= $reminder_message;
								$a['site_id'] 	= $cma->site_id;  
								$a['cust_id'] 	= $cma->cust_id;  
								$a['cma_id'] 	= $cma->cma_id;  
								$a['cust_name'] = $cma->cust_name; 
								$a['agent_name']= $this->user->name; 
								$a['expiry'] 	= date('U', $expiry);

								$intensifier_res = $this->cma_external->insert_intensifier($a, 'events');

						 		$audit = array();
			                    $audit['audit_type']	= 'intensifier'; 
			                    $audit['call_id']    	= @$tran_id;
			                    $audit['call_id']    	= @$call_id;
			                    $audit['audit_status']  =  isset($intensifier_res['intensifier_id'])?1:0;
			                    
			                    if( !isset($intensifier_res['intensifier_id']) )
			                        $audit['audit_status_error']=  'Failed to send reminder to Escalation System';
			                    
			                    $audit['message']       = 'Reminder: '.date('d/m/Y H:i:s', $expiry).' Message: '.stripslashes(nl2br($reminder_message)) ; 
	 							
	 							$this->Commonmodel->insert_audit_trail($audit);

 							}
						}*/

						$this->session->set_flashdata('flash_success', 'Collector call Successfully submitted');
					}else{
						$this->session->set_flashdata('flash_error', 'Error on submitting Collector call');
					}

					//CMA AUTO LOG
					$auto_log_p = array();
					$auto_log_p['tran_id'] = $tran_id;
					$auto_log_p['call_id'] = $call_id;
					$auto_log_p['ob1_name'] = $caller_name;
					$auto_log_p['ob1_phone'] = $caller_phone;

					$_message = 'Client ID: '.$CustomerID.PHP_EOL;
					$_message .= 'Name of Caller: '.$caller_name.PHP_EOL;
					$_message .= 'Phone: '.$caller_phone.PHP_EOL;
					$_message .= 'Call Response: '.$callresponse->call_res_text.PHP_EOL;
					$_message .= 'Travel Time To Hospital: '.$collector_distance.' mins '.PHP_EOL;
					$_message .= 'Notes: '.$notes.PHP_EOL;
					$auto_log_p['message'] = $_message;
					
					$this->Commonmodel->cma_auto_log($auto_log_p);


					if( strpos($callresponse->call_res_text, 'AT HOSPITAL') !== false ){
						$this->session->set_flashdata('flash_success', ' Record is successfully closed');
						redirect('dashboard');
					}elseif( $show_next_form == 'Despatch Collector and Call On-Call Nurse' ){
						redirect('booking/open/'.$CustomerID.'?section=oncall&show_next_form='.$show_next_form);
					}elseif($collector_distance > 60){
						redirect('booking/open/'.$CustomerID.'?section=oncall'.$issue_type_uri);
					}else{
						redirect('booking/open/'.$CustomerID.'?section=collector'.$issue_type_uri);
					}
					break;

				case 'oncall':

					$CustomerID = $post['CustomerID'];
					$tran_id = $post['tran_id'];

					$customer = $this->Customermodel->details(array('where'=>array('CustomerID'=>$CustomerID)));
					$callresponse = $this->Callresponsemodel->row(array('where'=>array('call_res_id'=>$post['call_res_id'])));

					$caller_name = trim($post['caller_name']);
					$caller_phone = trim($post['caller_phone']); 
					$notes = addslashes(@$post['notes']);

					$callset = array();
					$callset['tran_id'] 		= $tran_id;
					$callset['caller_name'] 	= $caller_name;
					$callset['caller_phone'] 	= $caller_phone;
					$callset['call_res_id'] 	= $post['call_res_id'];
					$callset['call_notes'] 		= $notes; 
					 
					$callset['call_type'] 		= 'oncall';
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					

					$more_text = array();

					if( @$post['CellCareCollector_verify'] ){
						$more_text['CellCareCollector Verified'] = 'YES';
					}
					
					if( @$post['ObsHistoryNotes_verify'] ){
						$more_text['ObsHistoryNotes Verified'] = 'YES';
					}
					
					if( @$post['edd_premature_verify'] ){						
						$more_text['Premature Birth  Verified'] = !empty($post['edd_premature_verify'])?'YES':'NO';						
					}

					$more_text['Status'] = $this->statuses[$callresponse->call_res_open];

					if( count($more_text) > 0 ){
						$callset['more_text'] = json_encode($more_text);
					}


					if( $call_id = $this->Callsmodel->insert($callset) ){
						$this->session->set_flashdata('flash_success', 'On Call Successfully submitted');

						$transet = array();
						$transet['tran_status'] 		= $callresponse->call_res_open;
						$transet['tran_updated'] 		= date('Y-m-d H:i:s');

						if( isset($post['CellCareCollector_verify']) AND @$post['CellCareCollector_verify'] == 1 ){

							$transet['CellCareCollector_verify'] = 1; 
						}

						if( isset($post['ObsHistoryNotes_verify']) AND @$post['ObsHistoryNotes_verify'] == 1 ){ 
							$transet['ObsHistoryNotes_verify'] = 1;
						}

						if( isset($post['edd_premature_verify']) AND @$post['edd_premature_verify'] == 1 ){ 
							$transet['edd_premature_verify'] = 1;
						}
						
						$this->Transactionmodel->update($tran_id, $transet);

						$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));

						//reminder
						/*if($callresponse->reminder > 0) {


							$expiry = '';
							 
							if( $tran_row->sched_induction != '' AND !in_array($tran_row->sched_induction_ta, array('AM', 'PM', 'UNKNOWN')) ){

								//echo $tran_row->sched_induction.' '.$tran_row->sched_induction_ta.' - '.$callresponse->reminder.' minute';

								$expiry = strtotime($tran_row->sched_induction.' '.$tran_row->sched_induction_ta.' - '.$callresponse->reminder.' minute');
							}elseif( $tran_row->sched_ceasar != '' AND !in_array($tran_row->sched_ceasar_to, array('AM', 'PM', 'UNKNOWN')) ){
								
								//echo $tran_row->sched_ceasar.' '.$tran_row->sched_ceasar_to.' - '.$callresponse->reminder.' minute';

								$expiry = strtotime($tran_row->sched_ceasar.' '.$tran_row->sched_ceasar_to.' - '.$callresponse->reminder.' minute');
							}else{
								//$expiry = strtotime('now + '.$callresponse->reminder.' minute');
							}


							$a = array();

							$reminder_message = 'Client ID: '.$customer->CustomerID.' - Client Name: '.$customer->FirstName.' '.$customer->LastName.' ';
							//$expiry = strtotime('now + '.$callresponse->reminder.' minute');						 

							if( $expiry != '' ){

								$a['message'] 	= $reminder_message;
								$a['site_id'] 	= $cma->site_id;  
								$a['cust_id'] 	= $cma->cust_id;  
								$a['cma_id'] 	= $cma->cma_id;  
								$a['cust_name'] = $cma->cust_name; 
								$a['agent_name']= $this->user->name; 
								$a['expiry'] 	= date('U', $expiry);

								$intensifier_res = $this->cma_external->insert_intensifier($a, 'events');

						 		$audit = array();
			                    $audit['audit_type']	= 'intensifier'; 
			                    $audit['call_id']    	= $tran_id;
			                    $audit['call_id']    	= $call_id;
			                    $audit['audit_status']  =  isset($intensifier_res['intensifier_id'])?1:0;
			                    if( !isset($intensifier_res['intensifier_id']) )
			                        $audit['audit_status_error']=  'Failed to send reminder to Escalation System';
			                    $audit['message']       = 'Reminder: '.date('d/m/Y H:i:s', $expiry).' Message: '.stripslashes(nl2br($reminder_message)) ; 

	 							$this->Commonmodel->insert_audit_trail($audit);
 							}
						}*/


					}else{
						$this->session->set_flashdata('flash_error', 'Error on submitting On Call');
					}



					//CMA AUTO LOG
					$auto_log_p = array();
					$auto_log_p['tran_id'] = $tran_id;
					$auto_log_p['call_id'] = $call_id;
					$auto_log_p['ob1_name'] = $caller_name;
					$auto_log_p['ob1_phone'] = $caller_phone;

					$_message = 'Client ID: '.$CustomerID.PHP_EOL;
					$_message .= 'Name of Caller: '.$caller_name.PHP_EOL;
					$_message .= 'Phone: '.$caller_phone.PHP_EOL;
					$_message .= 'Call Response: '.$callresponse->call_res_text.PHP_EOL;					
					$_message .= 'Notes: '.$notes.PHP_EOL;
					$auto_log_p['message'] = $_message;
					
					$this->Commonmodel->cma_auto_log($auto_log_p);


					redirect('booking/open/'.$CustomerID.'?section=oncall'.$issue_type_uri);
					
					break; 

				case 'clienthospital':

					$CustomerID = $post['CustomerID'];
					$tran_id = $post['tran_id'];

					$callresponse = $this->Callresponsemodel->row(array('where'=>array('call_res_id'=>$post['call_res_id'])));

					$caller_name = trim($post['caller_name']);
					$caller_phone = trim($post['caller_phone']); 
					$notes = addslashes(@$post['notes']);

					$callset = array();
					$callset['tran_id'] 		= $tran_id;
					$callset['caller_name'] 	= $caller_name;
					$callset['caller_phone'] 	= $caller_phone;
					$callset['call_res_id'] 	= $post['call_res_id'];
					$callset['call_notes'] 		= $notes; 
					 
					$callset['call_type'] 		= 'clienthospital';
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					
					if( $call_id = $this->Callsmodel->insert($callset) ){
						$this->session->set_flashdata('flash_success', 'Client/Hospital ON Call Successfully submitted');
					}else{
						$this->session->set_flashdata('flash_error', 'Error on submitting Client/Hospital On Call');
					}



					//CMA AUTO LOG
					$auto_log_p = array();
					$auto_log_p['tran_id'] = $tran_id;
					$auto_log_p['call_id'] = $call_id;
					$auto_log_p['ob1_name'] = $caller_name;
					$auto_log_p['ob1_phone'] = $caller_phone;

					$_message = 'Client ID: '.$CustomerID.PHP_EOL;
					$_message .= 'Name of Caller: '.$caller_name.PHP_EOL;
					$_message .= 'Phone: '.$caller_phone.PHP_EOL;
					$_message .= 'Call Response: '.$callresponse->call_res_text.PHP_EOL;					
					$_message .= 'Notes: '.$notes.PHP_EOL;
					$auto_log_p['message'] = $_message;
					
					$this->Commonmodel->cma_auto_log($auto_log_p);

					redirect('booking/open/'.$CustomerID.'?section=clienthospital');
					break; 

				case 'cccverify':

					$CustomerID = $post['CustomerID'];
					$tran_id = $post['tran_id']; 
					$flash_success = '';
					$flash_error = '';

					$set = array(); 					 
					$set['CellCareCollector_verify'] =  isset($post['oncallcccverify'])?0:1;  

					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$flash_error[] = 'Unable to proceed something went wrong';
					} 

					$callset = array();
					$callset['tran_id'] 		= $tran_id;
					 
					$callset['call_type'] 		= 'oncall';
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					
					$more_text = array();

					if( isset($post['oncallcccverify']) ){						
						$more_text['Ask Caller'] = 'We understand the doctor / hospital will be performing the collection: NO';
					}else{

						$more_text['Ask Caller'] = 'We understand the doctor / hospital will be performing the collection: YES';
						//$more_text['Advise Caller'] = ' You will need to remind the hospital they need to do the collection';
						$more_text['Advise Caller'] = ' Please remind your doctor or midwife they need to do the collection. You should call back within 2hrs after the birth to book the courier for pick up';
						$more_text['Status'] = 'Closed';
					}

					$callset['more_text'] = json_encode($more_text);

					if( $this->Callsmodel->insert($callset) ){
						//$flash_success[] = 'On Call Successfully submitted';
						$flash_success[] = '';

						//update transaction to close
						//We understand the doctor / hospital will be performing the collection: YES
						if( !isset($post['oncallcccverify']) ){
							$transet = array();
							$transet['tran_status'] = 0;
							$this->Transactionmodel->update($tran_id, $transet);
						}

					}else{
						$flash_error[] ='Error on submitting On Call';
					}

					if( count($flash_success)>0 ){
						$this->session->set_flashdata('flash_success', implode('|',$flash_success));	
					}

					if( count($flash_error)>0 ){
						$this->session->set_flashdata('flash_error', implode('|',$flash_error));	
					}
 

					if( isset($post['oncallcccverify']) ){
						redirect('booking/open/'.$CustomerID.'/?section=oncall&oncallcccverify=1');
					}else{
						redirect('dashboard');
					}


					break;
				default:
					# code...
					break;
			}


		} catch (Exception $e) {
			$this->session->set_flashdata('flash_error', 'Unable to proceed something went wrong.');
			redirect('booking/open/'.$post['CustomerID']);
		}

	}


	function lp_Q_save(){

		try {
			
			$post = $this->input->post();
			$qType = $post['qType'];
			$json_return = array();


			$CustomerID = $post['CustomerID'];
			$tran_id = $post['tran_id'];

			switch ($qType) {
				case 'Collection_Kit':


					$set = array();
					$set['collection_kit'] = $post['qVal'];
					
					$json_return['flash_success'][] = "success|Successfully submitted, please proceed to On Call - Call Activity.";
					$json_return['status'] = 1;
					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$json_return['flash_error'][] = "Error on processing Collection Kit Update.";
						$json_return['status'] = 0;
					} 


					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'labourprogress';
					$callset['more_text'] 		= json_encode( array('Do you have Collection Kit ?'=>($post['qVal']?'Yes':'No')) );
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					$this->Callsmodel->insert($callset);

					if( $post['qVal'] == '0' ){
						$json_return['redirect'] = 'booking/open/'.$CustomerID.'?section=oncall'.(isset($post['issue_type'])?'&issue_type='.$post['issue_type']:'');
					}

					return $json_return;

					break;
				case 'Caesar_Induction'; 
					$set = array();
					$set['caesar_induction'] = $post['qVal'];
					
					$json_return['flash_success'][] = "Successfully submitted, please proceed to Confirm Hospital.";
					$json_return['status'] = 1;					
 
					if( !$this->Transactionmodel->update($tran_id, $set) ){						 
						$json_return['flash_error'][] = "Error on processing caesar or induction Update.";
						$json_return['status'] = 0;									
					} 

					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'labourprogress';
					$callset['more_text'] 		= json_encode( array('Are you calling about a caesar or induction ?'=>($post['qVal']?'Yes':'No')) );
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					$this->Callsmodel->insert($callset);

  

					if( $post['qVal'] == '1'){
						//TODO: Go to INDUCTION or CAESAR screen
						//redirect('booking/open/'.$CustomerID.'?section=oncall');
						
						$json_return['redirect'] = 'booking/open/'.$CustomerID.'?section=oncall';
						//$json_return['redirect'] = 'booking/open/'.$CustomerID.'?issue_type=INDUCTION';
						 
					}

					return $json_return;

					break; 				

				/*case 'Waters_Broken':
					
					$set = array();
					$set['waters_broken'] = $post['qVal'];	

					$json_return['flash_msg'] = "success|Successfully submitted, Has clients waters broken.";
					$json_return['status'] = 1;		

					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$json_return['flash_msg'] = "error|Error on Has clients waters broken.";
						$json_return['status'] = 0;	
					}


					$qVal = explode('|', $post['qVal']);
					$qVal = ($qVal[0]?'Yes':'No').(isset($qVal[1])?' @ '.$qVal[1]:'');

					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'labourprogress';
					$callset['more_text'] 		= json_encode( array('Has clients waters broken ?'=>$qVal) );
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					$this->Callsmodel->insert($callset);

					return $json_return;

					break;
				case 'Epidural':
					$set = array();
					$set['epidural'] = $post['qVal'];	

					$json_return['flash_msg'] = "success|Successfully submitted, Has client been given an epidural.";
					$json_return['status'] = 1;		

					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$json_return['flash_msg'] = "error|Error on Has client been given an epidural.";
						$json_return['status'] = 0;	
					}

					$qVal = explode('|', $post['qVal']);
					$qVal = ($qVal[0]?'Yes':'No').(isset($qVal[1])?' @ '.$qVal[1]:'');

					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'labourprogress';
					$callset['more_text'] 		= json_encode( array('Has client been given an epidural?'=>$qVal) );
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					$this->Callsmodel->insert($callset);

					return $json_return;

					break;
				case 'Hormone_Started':
					$set = array();
					$set['hormone_started'] = $post['qVal'];	 

					$json_return['flash_msg'] = "success|Successfully submitted, Has client had a hormone started.";
					$json_return['status'] = 1;		

					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$json_return['flash_msg'] = "error|Error on Has client had a hormone started.";
						$json_return['status'] = 0;	
					}

					$qVal = explode('|', $post['qVal']);
					$qVal = ($qVal[0]?'Yes':'No').(isset($qVal[1])?' @ '.$qVal[1]:'');

					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'labourprogress';
					$callset['more_text'] 		= json_encode( array('Has client had a hormone started ?'=>$qVal) );
					$callset['agent_name'] 		= $this->user->name;
					$callset['user_id'] 		= $this->user->agent_id;
					$this->Callsmodel->insert($callset);

					return $json_return;

					break;
				case 'Client_Status':
					$set = array();
					$set['client_status'] = $post['qVal'];

					$json_return['flash_msg'] = "success|Successfully submitted, CLIENT STATUS.";
					$json_return['status'] = 1;	

					if( !$this->Transactionmodel->update($tran_id, $set) ){
						
						$json_return['flash_msg'] = "error|Error on CLIENT STATUS.";
						$json_return['status'] = 0;	

					}


					$callset = array();
					$callset['tran_id'] 		= $tran_id; 
					$callset['call_type'] 		= 'labourprogress';
					$callset['more_text'] 		= json_encode( array('Client Status'=> @$this->Commonmodel->hash('client_status', $post['qVal'], 1)) );
					$callset['agent_name'] 		= @$this->user->name;
					$callset['user_id'] 		= @$this->user->agent_id;
					$this->Callsmodel->insert($callset);

					return $json_return;

					break;*/

				default:
					# code...
					break;
			}


		} catch (Exception $e) {
			
		}

	}

	public function search($issue_type=1){

		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data['filters'] = $params;
		$data['issue_type'] = $issue_type;

		 
		$params = array('tran_status > '=>0);
		 
		//$query_params = $this->build_where($params);
		$query_params = '';
		$params_query = @http_build_query($params);

		$per_page = 50;
		$query_params['where']['tran_status >'] = 0;
		$query_params['where']['tran_type'] = 'Booking';		
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 
 
		 
		$appt_results 				= $this->Transactionmodel->listing($query_params);

        $p_config["base_url"] 		= base_url() . "register/index/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		$this->view_data['data'] = $data; 
		$this->view_data['view_file'] = 'pages/booking/search';
		$this->view_data['menu_active'] = 'search'; 	
		$this->view_data['js_file'] = '<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>'; 
		$this->view_data['js_file'] .= ' <script src="assets/vendor/mcautocomplete/mcautocomplete.js"></script>'; 		
		$this->view_data['js_file'] .= ' <script src="assets/js/pages/ac_booking_search_customer.js?v='.JS_V_bookingCA.'"></script>';

		$this->load->vars($this->view_data);

		$this->load->view('template');

	}


	public function details($issue_type=1){

		$this->view_data['view_file'] = 'pages/booking/details';
		$this->view_data['menu_active'] = 'details'; 			


		switch ($issue_type) {
			case 1:
				
				break;
			
			default:
				# code...
				break;
		}



		$this->view_data['js_file'] = '<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>'; 

		$this->load->view('template', $this->view_data);

	}

}
