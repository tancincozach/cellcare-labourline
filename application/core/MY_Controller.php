<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	public $logged_in;
	public $user = '';
	
 	public $view_data = array(
 			'container'=>'container', 
 			'view_file'=>'pages/dashboard', 
 			'js_file'=>'', //js by page
 			'css_file'=>'', //css by page
 			'data'=>'',
 			'menu_active'=>'dashboard',
 			'maintain'=> array(
 				'view_file'=>'maintain/general_setting',
 				'menu_active'=>'generalsetting',
 				'data'=>''
 			)
 		);

	public $call_vars = ''; 

	public $notices = '';

	public $notices_collector = '';

	public $contacts_results = '';


	public $notices_count 	= array(1=>0, 2=>0);

  
	function __construct(){

		parent::__construct();


		if( $this->session->userdata('logged_in')){
			$this->set_site_vars();
		}
		 
	}

	function set_site_vars(){
 		
 		$this->load->model('Noticesmodel','notice');
		$this->load->model('Contactsmodel','contacts');

		try {

			$cookie = get_cookie('info');
			$cookie = base64_decode($cookie);
			$cookie = json_decode($cookie);
			 
			$sess = array();
			$sess['logged_in'] 	= 1; 
			$sess['user_id'] 	= @$cookie->user_id;
			$sess['user_lvl'] 	= @$cookie->user_lvl;
			$sess['agent_name'] = @$cookie->agent_name;
			
			if( isset($cookie->agent_id) ){
				$sess['agent_id'] = $cookie->agent_id;
			}
							
			$this->session->set_userdata($sess);

			$this->user = new stdClass();
			$this->user->userid 	= $sess['user_id'];
			$this->user->name 		= $sess['agent_name'];
			$this->user->level 		= $sess['user_lvl'];
			$this->user->agent_id 	= !isset($sess['agent_id'])?60:$sess['agent_id'];

			$this->call_vars = new stdClass();
			$this->call_vars->direction = array('0'=>'OUT', '1'=>'IN'); 


			$this->statuses = array(
				'0'=>'Closed',
				'1'=>'Open',
				'2'=>'Pending'
			);
			
			//NOTICES
			$params['sorting']['sort'] = "date_created";
			$params['sorting']['order'] = "DESC";
			$params['where_str'] = " `notice_status`=1 AND `notice_group`= 1 AND (NOW() <= `expire_date` OR `expire_date` = '0000-00-00 00:00:00' OR `expire_date` IS NULL)";
			$params['limit'] = 15;
			$notices_result = $this->notice->listing($params);	


			$params = array();

			//COLLECTOR NOTICES
			$params['sorting']['sort'] = "date_created";
			$params['sorting']['order'] = "DESC";
			$params['where_str'] = " `notice_status`=1 AND `notice_group`= 2 AND (NOW() <= `expire_date` OR `expire_date` = '0000-00-00 00:00:00' OR `expire_date` IS NULL)";
			$params['limit'] = 15;
			$notices_collector_result = $this->notice->listing($params);

			//echo $this->db->last_query();

			//echo date('Y-m-d H:i:s');

			$params = array();


			$params['where_str'] = " contact_status=1 ";
			$contact_result = $this->contacts->listing($params);	

			$this->notices  = @$notices_result['results'];
			$this->notices_collector  = @$notices_collector_result['results'];
			$this->contacts_results= @$contact_result['results'];



	        if( get_cookie($this->config->item('sess_cookie_name').'-note') == '' ){

				$cook_data = array("s"=>"activate", "n"=>"");
				$cook_data = json_encode($cook_data);

				$cookie = array(
				    'name'   => $this->config->item('sess_cookie_name').'-note',
				    'value'  => $cook_data,
				    'expire' => '86500',
				    //'domain' => base_url(),
				    'path'   => '/'
				    //'prefix' => 'myprefix_',
				   //'secure' => TRUE
				);

				$this->input->set_cookie($cookie);


	        }
 

 			//print_r(get_cookie($this->config->item('sess_cookie_name').'-note'));
			
		} catch (Exception $e) {
			echo $e->get_message();
		}

	}
 
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */