<h1>CMA Setting</h1>


<div class="col-sm-6">

	<?php echo form_open('maintain/cmasettings', 'name="cmasettings"   class=""  '); ?>

		<table class="table table-sm table-borderless">

			<tr>
				<td>Site</td>
				<td>
					<select name="cma_settings[site_id]" required="">
						<option value=""></option>
						<option value="1" <?php echo (@$cma->site_id==1)?'selected="selected"':''; ?>>Nowra</option>
						<option value="2" <?php echo (@$cma->site_id==2)?'selected="selected"':''; ?>>International</option>
						<option value="3" <?php echo (@$cma->site_id==3)?'selected="selected"':''; ?>>Melbourne</option>
						<option value="4" <?php echo (@$cma->site_id==4)?'selected="selected"':''; ?>>Site 4</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>CMA DB</td>
				<td>
					<select required="required" class="" name="cma_settings[cma_db]">
						<option value=""></option>
						<option value="cma_international" <?php echo (@$cma->cma_db == 'cma_international'?'selected':''); ?> >International</option> 
						<option value="cma_melbourne" <?php echo (@$cma->cma_db == 'cma_melbourne'?'selected':''); ?>>Melbourne</option> 
						<option value="cma_nowra" <?php echo (@$cma->cma_db == 'cma_nowra'?'selected':''); ?> >Nowra</option> 
						<option value="cma_site4" <?php echo (@$cma->cma_db == 'cma_site4'?'selected':''); ?> >Site4</option> 
					</select>
				</td>
			</tr>
			<tr>
				<td width="200px">CMA ID</td>
				<td><input type="text" name="cma_settings[cma_id]" value="<?php echo @$cma->cma_id; ?>"></td>
			</tr>
			<tr>
				<td>CUST ID</td>
				<td><input type="text" name="cma_settings[cust_id]" value="<?php echo @$cma->cust_id; ?>"></td>
			</tr>

			<tr>
				<td>Contact ID - issue</td>
				<td><input type="text" name="cma_settings[contact_id_issue]" value="<?php echo @$cma->contact_id_issue; ?>"></td>
			</tr>

			<tr>
				<td>Contact ID - calls made</td>
				<td><input type="text" name="cma_settings[contact_id_callsmade]" value="<?php echo @$cma->contact_id_callsmade; ?>"></td>
			</tr>

			<tr>
				<td>Customer Name</td>
				<td><input type="text" name="cma_settings[cust_name]" value="<?php echo @$cma->cust_name; ?>"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<button class="btn btn-sm btn-primary">Submit</button>
				</td>
			</tr>
		</table>

	</form>

</div>