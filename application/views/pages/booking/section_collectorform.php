
<?php //if( in_array(@$issue_type, array('INDUCTION', 'CAESAR')) AND  (isset($_GET['warnINDUCTION']) OR isset($_GET['warnCAESAR'])) ): ?>

<?php if( @$issue_type == 'INDUCTION' AND  isset($_GET['warnINDUCTION']) ): ?>
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">UPDATE Collector</h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">
		    	 
		    	<!-- <p>Thank you, we will organise a collector for this day & time.  If anything</p>
		    	<p>changes please call us back immediately</p>  -->
		    	<strong>Advise Caller</strong><br/>
		    	<p>Thank you we will organise a Collector for your induction. <br >
		    		Please call us immediately if anything changes, <br />
		    		if labour commences or as soon as waters broken or hormone drip commenced
		    	</p>

	        </div>
				       
		</div>

	</div>
</div>
<?php  endif ?>



<?php if( @$issue_type == 'CAESAR' AND isset($_GET['warnCAESAR']) ): ?>
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">UPDATE Collector</h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">
		    	 
		    	<p>Thank you, we will organise a collector to arrive 1 hour prior to your operation</p>
		    	<p>IF anything changes please call us back immediately</p> 
	        </div>
				       
		</div>

	</div>
</div>
<?php  endif ?>

<div class="col-sm-12 pb-5 mb-5 box-collector">
	<div class="row border mt-1 pt-2 pb-5 ">
		
		<div class="col-sm-12">
			<h4 class="text-center">Call Activity - Collector </h4>
		</div>

<!-- 		<div class="col-sm-8 ml-auto mr-auto">
		 	<table class="table table-sm table-borderless bg-secondary text-white text-center">
				<tr>
					<th rowspan="2" class="text-center align-middle">CALLER DETAILS</th>
					<th>NAME</th>
					<th>PHONE</th>
					<th>RELATIONSHIP</th>
				</tr>
				<tr>
					<td><?php echo @$transaction->CallerName; ?></td>
					<td><?php echo @$transaction->CallerPhone; ?></td>
					<td><?php echo @$transaction->ClientRelation; ?></td> 
				</tr>
			</table>
		</div> -->

		<?php echo form_open('booking/save', 'name="Collector-form" id="Collector-form" class="col-sm-12" onsubmit="return Collector.confirm()" '); ?>
		
	 		<input type="hidden" name="section" value="collector">
	 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
	 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">	
	 		<input type="hidden" name="issue_type" value="<?php echo (@$issue_type=='')?1:$issue_type; ?>">	
	 		<input type="hidden" name="old_collector_distance" value="<?php echo @$transaction->collector_distance; ?>">
	 		
	 		<input type="hidden" name="submit_continue_distanceConfirm" value="0">
	 		<input type="hidden" name="submit_continue_reminderPopup" value="0">

	 		<?php if( isset($progress_row->action1) ): ?>	 			
	 			<input type="hidden" name="show_next_form" value="<?php echo $progress_row->action1; ?>">	
	 		<?php endif; ?>
		 
 			<div class="row">
 				<div class="col-4">
 					<table class="table table-sm table-borderless">
 						<tr>
 							<td><!-- Caller Type --></td>
 							<td>
 								<!-- <select name="call_type" required="">
 									<option value="Collector">Collector</option> 									
 								</select> -->
 								&nbsp;
 							</td>
 						</tr>
 						<tr>
 							<td>Name of Caller</td>
 							<td>
 								<input type="text" class="form-control form-control-sm"  name="caller_name" value="<?php echo isset($_GET['CallerName'])?$_GET['CallerName']:$transaction->collector; ?>" required="">
 							</td>
 						</tr>
 						<tr>
 							<td>Phone</td>
 							<td>
 								<input type="text" class="form-control form-control-sm"  name="caller_phone" value="<?php echo isset($_GET['CallerPhone'])?$_GET['CallerPhone']:$transaction->collector_phone; ?>" required="">
 							</td>
 						</tr>
 						<tr>
 							<td>Call Response</td>
 							<td>
 								<select class="custom-select custom-select-sm" name="call_res_id" onchange="Collector.onCallReponseChange(this)" required="">
 									<option value=""></option>
 								<?php 
									//$attr = 'class="custom-select custom-select-sm"  required="" onchange="Collector.onCallReponseChange(this)"';
 									//echo form_dropdown('call_res_id', @$callresponses, '', $attr );

 									foreach ($callresponses as $row): 
 								?>
 									<option value="<?php echo $row->call_res_id; ?>" <?php echo ($row->distance_required==1)?'data-distanceRequired="1"':''; ?> <?php echo (@$row->reminder_popup == '1')?'data-reminderPopup="1"':''; ?> ><?php echo $row->call_res_text; ?></option>
 								<?php endforeach; ?>
 								</select>
 							</td>
 						</tr>
 					</table>
 				</div>
 				<div class="col-3">
 					<p>Notes</p>
 					<textarea class="form-control form-control-sm" name="notes" rows="5"></textarea>
 					<br />
 					
 					<div id="div_collector_distance">
	 					<p>What is your travel time to hospital</p>  
						<div class="input-group input-group-sm mb-0">
						  	<input type="number" class="form-control" name="collector_distance"  aria-describedby="basic-addon2" min="1" value="<?php //echo @$transaction->collector_distance; ?>" required="">
						  	<div class="input-group-append">
						    	<span class="input-group-text" id="basic-addon2">Min(s)</span>
						  	</div>
						  	<!-- <div class="input-group-append">
								<div class="custom-control ml-2 pt-1">
									<input type="checkbox" class="custom-control-input" id="customCheck1" name="collector_distance_nochange" value="1" onclick="Collector.distance_nochange(this)">
									<label class="custom-control-label font-weight-bold" for="customCheck1">No Change</label>
								</div>						    
						  	</div> -->
						</div> 
					</div>


 					<br />
 					<br />

 					<button class="btn btn-sm btn-primary" type="submit">Submit</button>
 				</div>
 				<div class="col-5">

 					<div class="col-sm-12 m-0 p-0">
	 					<p class="text-center"><strong>Collector Details</strong> <br /> Choose applicable Collector depending on time of day</p>
	 					<table class="table table-sm">
	 						<?php if( $customer->collector1 != '' ): 

	 							$customer->collector1Phone = trim(@$customer->collector1Phone) != ''?str_pad(@$customer->collector1Phone, 10, "0", STR_PAD_LEFT):'';
	 							$customer->collector2Phone = trim(@$customer->collector2Phone) != ''?str_pad(@$customer->collector2Phone, 10, "0", STR_PAD_LEFT):'';
	 							$customer->collector3Phone = trim(@$customer->collector3Phone) != ''?str_pad(@$customer->collector3Phone, 10, "0", STR_PAD_LEFT):'';
	 							$customer->collector4Phone = trim(@$customer->collector4Phone) != ''?str_pad(@$customer->collector4Phone, 10, "0", STR_PAD_LEFT):'';

	 						?>
	 						<tr>
	 							<td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="Collector.select(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector1; ?></button></td> 
	 							<td><?php echo $customer->collector1Phone; ?></td> 
	 							<td><?php echo $customer->collector1Availability; ?></td> 
	 						</tr>
	 						<?php endif; ?>
	 						<?php if( $customer->collector2 != '' ): ?>
	 						<tr>
	 							<td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="Collector.select(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector2; ?></button></td> 
	 							<td><?php echo $customer->collector2Phone; ?></td> 
	 							<td><?php echo $customer->collector2Availability; ?></td> 
	 						</tr>
	 						<?php endif; ?>
	 						<?php if( $customer->collector3 != '' ): ?>
	 						<tr>
	 							<td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="Collector.select(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector3; ?></button></td> 
	 							<td><?php echo $customer->collector3Phone; ?></td> 
	 							<td><?php echo $customer->collector3Availability; ?></td> 
	 						</tr>
	 						<?php endif; ?>
	 						<?php if( $customer->collector4 != '' ): ?>
	 						<tr>
	 							<td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="Collector.select(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector4; ?></button></td> 
	 							<td><?php echo $customer->collector4Phone; ?></td> 
	 							<td><?php echo $customer->collector4Availability; ?></td> 
	 						</tr>
	 						<?php endif; ?>

	 					</table>
 					</div>

	 				<!-- <div class="col-sm-12 m-0 p-0">
		 				<a class="text-center" href="contacts/collectors" target="_blank">Click for list of collectors</a>
	 				</div> -->

 				</div>

 			</div>


		</form>
		 
	</div>
</div>


<div class="foot_content_call_history">
<?php
	if( isset($calls_result)){
    	echo $this->load->view('pages/booking/call_history', array('calls_result'=>$calls_result), TRUE);
	}
?>
</div>