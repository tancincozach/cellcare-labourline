<h2>Contacts</h2>
<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php endif;?>
<div class="table-responsive">

	<div class="form-group">
		<a  href="maintain/contacts/contacts_form"  class="btn btn-success btn-sm"><i class="fas fa-plus-circle"></i>&nbsp;New Contacts</a>		
	</div>
		
		<table class="table table-sm table-bordered table-striped table-hover">
			<thead>
				<th class="table-primary">Title</th>
				<th class="table-primary">Date Created</th>
				<th class="table-primary">Date Updated</th>
				<th class="table-primary">Status</th>
				<th class="table-primary">Option</th>
			</thead>
			<tbody>

			<?php if(isset($results)  && !empty($results)):
					foreach( $results as $row):
				?>
				<tr>
					<td><?php echo @$row->contact_title;?></td>
					<td><?php echo @$row->contact_created;?></td>
					<td><?php echo @$row->contact_updated?></td>
					<td><?php echo isset($row->contact_status) && intval($row->contact_status)==1 ? 'Active':'Inactive';?></td>
					<td>
					<a href="maintain/contacts/contacts_form/<?php echo @$row->contact_id;?>" class="btn btn-info btn-sm" /><i class="fas fa-pencil-alt"></i>&nbsp;Edit</a>
					<a  onclick="return Misc.confirm('Are you sure to delete/inactivate this contact ?');" href="maintain/contacts/?del=<?php echo @$row->contact_id;?>" class="btn btn-danger btn-sm" /><i class="fas fa-trash-alt"></i>&nbsp;Delete</a>
					
					</td>
				</tr>
			<?php 
					endforeach;
					?>
						<tr>
							<td colspan="5">
							<?php echo $links;?>
							<?php echo $showing;?>
							</td>
						</tr>
					<?php
				else:
			?>
				<tr>
					<td colspan="5"> No Contact(s) Found</td>
				</tr>
			<?php endif;?>
			</tbody>
		</table>
</div>