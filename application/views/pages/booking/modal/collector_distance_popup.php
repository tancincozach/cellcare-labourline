
<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Collector is more the 60mins from hospital</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h5 >Agent Note</h5>
                <p>Call the On-call Nurse and advise Collector travel time > than 60 minutes and 
                    discuss how despatch criteria needs to be amended
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="Collector.collectorform_submit()">Proceed on On Call Nurse</button> 
            </div>
        </div>
    </div>
</div>
