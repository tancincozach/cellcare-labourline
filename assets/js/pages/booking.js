
var Booking = (function () {

  	// locally scoped Object
	var myObject = {};

	// declared with `var`, must be "private"
	var privateMethod = function () {};


	myObject.confirm = function () {

		/*if( confirm('Please confirm submit') )
			return true;

		return false;*/

		return true;
	};

	myObject.search = function(){

	}


	//IMPORTANT NOTES
	myObject.in_btn_toggle = function(tran_id, v){
		if( v == 1 ){
			$('#in_btn_edit').hide();
			$('#in_notes_div').show()  
		}else{			

			if( v == 2 ){
				var notes = $('textarea[name="important_notes"]').val();
				$.post("ajax/important_notes_save/?_t="+(new Date().getTime()), {tran_id, notes}, function(json){
					if( json.status != 1 ){
						alert(json.msg);
					}
					window.location = document.URL;
				},'json' )
			}


			$('#in_btn_edit').show();
			$('#in_notes_div').hide();  
		}
	}

	//Obstetric Notes 
	myObject.obsnote_btn_toggle = function(tran_id, v){
		if( v == 1 ){
			$('#obsnote_btn_edit').hide();
			$('#obsnote_div').show()  
		}else{			

			if( v == 2 ){
				var notes = $('textarea[name="ObsHistoryNotes_edit"]').val();
				$.post("ajax/ObsHistoryNotes_edit_save/?_t="+(new Date().getTime()), {tran_id, notes}, function(json){
					if( json.status != 1 ){
						alert(json.msg);
					}
					window.location = document.URL;
				},'json' )
			}


			$('#obsnote_btn_edit').show();
			$('#obsnote_div').hide();  
		}
	}

	//Edit C-Section
	myObject.editcsection_btn_toggle = function(tran_id, v){

		var csection_btn_edit = $('#csection_btn_edit');
		var schedceasar_display = $('#schedceasar_display');
		var schedceasar_edit = $('#schedceasar_edit');

		if( v == 1 ){
			csection_btn_edit.hide();
			schedceasar_display.hide();
			schedceasar_edit.show();
		}else{			

			if( v == 2 ){

				//if( confirm('Confirm Update C-Section') ){

					form = $('#edit-sched-csection');				
					var data = $(form).serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {});				 
					data.tran_id = tran_id
					$.post("ajax/csection_edit_save/?_t="+(new Date().getTime()), {data}, function(json){
						if( json.status != 1 ){
							alert(json.msg);
						}
						window.location = document.URL;
						//alert(json.msg);
					},'json' );

				//}
			}


			csection_btn_edit.show();
			schedceasar_display.show();
			schedceasar_edit.hide(); 
		}
	}

	//Edit INDUCTION
	myObject.editinduction_btn_toggle = function(tran_id, v){

		var btn = $('#induction_btn_edit');
		var display = $('#induction_display');
		var edit = $('#induction_edit');

		if( v == 1 ){
			btn.hide();
			display.hide();
			edit.show();
		}else{			

			if( v == 2 ){

				//if( confirm('Confirm Update INDUCTION') ){
					form = $('#edit-sched-induction');				
					var data = $(form).serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {});				 
					data.tran_id = tran_id
					$.post("ajax/induction_edit_save/?_t="+(new Date().getTime()), {data}, function(json){
						
						if( json.status != 1 ){
							alert(json.msg);
						}

						window.location = document.URL;
					},'json' );
				//}
			}


			btn.show();
			display.show();
			edit.hide(); 
		}
	}

	// myObject.toggleNextDiv = function(elem){

	// 	$(elem).next('div').toggle();

	// }
	
	myObject.app_popup_elgible = function(){

		var form = $('#caller-form');

		var tran_id = form.find('input[name="tran_id"]').val();
		var app_elligible_shown = form.find('input[name="app_elligible_shown"]').val();

		//console.log('tran_id: '+tran_id+' '+app_elligible_shown);

		if( app_elligible_shown != '1' ){
			$.get( "ajax/app_eligible_modal/?_t="+(new Date().getTime()), {tran_id}, function(html) {

			  	modal = $(html).modal({keyboard:false, backdrop:true});
			  	modal.on('hidden.bs.modal', function (e) {
				  	modal.modal('dispose');	

				  	$('.modal').remove();
	 
				});  
			}); 
		}
		
	}
	



  	return myObject;

})();


var BookingDetailsNavbar = (function(){

	var myObject = {};
	var modal = null;

	myObject.reminder_click_btn = function(event, tran_id){

		event.preventDefault();

		$.get( "ajax/reminder_modal_form/?_t="+(new Date().getTime()), {tran_id}, function(html) {

		  	modal = $(html).modal({keyboard:false, backdrop:true});
		  	modal.on('hidden.bs.modal', function (e) {
			  	modal.modal('dispose');	

			  	$('.modal').remove();

			  	//$('#reminder_expirey').off();
			});


		    
	 		$('#reminder_expirey').datetimepicker({
	 			format:'d/m/Y H:i',
	 			mask: true 
	 		});
 
		}); 

		//return false;
	}

	myObject.reminder_expiry_opt_sel = function(id){
		$('.reminder_option').attr('disabled', 'disabled');

		$('#'+id).find('.reminder_option').removeAttr('disabled');
	}

	myObject.reminder_save = function(form){

		//alert( $(form).find('input[name="reminder_expirey"]').val() )

		//var data = JSON.stringify( $(form).serializeArray() );

		var data = $(form).serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {});
  		


		if (typeof data.reminder_expirey1 !== 'undefined') {

			if(data.reminder_expirey1 == '__/__/____ __:__'){
				alert('Expiry must be set');
				return false;
			} 

		} 

		//if( confirm('Confirm Submit') ){
 
			$.post('ajax/reminder_save', data, function(res){

				if( res.status) {
					//alert('Reminder successfully set');
					modal.modal('hide');
					window.location = document.URL;
				}else alert(res.error_msg);

			}, 'json');

		//}


		return false;

	}


	myObject.smscollector_click_btn = function(event, tran_id){
		//console.log(event);

		event.preventDefault();

		$.get( "ajax/smscollector_modal_form/?_t="+(new Date().getTime()), {tran_id}, function(html) {

		  	modal = $(html).modal({keyboard:false, backdrop:true});
		  	modal.on('hidden.bs.modal', function (e) {
			  	modal.modal('dispose');	

			  	$('.modal').remove();

			  	//$('#reminder_expirey').off();
			});

		}); 

		return false;
	}



	myObject.smscollector_copy = function(elem){
		 

		var form = $('#SMS-Collector-form'),
 			ptr = $(elem).parents('tr'),
			trchildren = ptr.children('td');
		//console.log(trchildren[1].textContent);
		form.find('input[name="caller_name"]').val($(elem).text());
		form.find('input[name="caller_phone"]').val(trchildren[1].textContent);

		return false;
	}

	myObject.smscollector_save = function(form){
 

		var data = $(form).serializeArray().reduce(function(m,o){ m[o.name] = o.value; return m;}, {});
  		


		if (typeof data.reminder_expirey1 !== 'undefined') {

			if(data.reminder_expirey1 == '__/__/____ __:__'){
				alert('Expiry must be set');
				return false;
			} 

		} 

		//if( confirm('Confirm Manual Send SMS') ){
 
			$.post('ajax/smscollector_save', data, function(res){

				if( res.status) {
					//alert('Manual Send SMS Successfully sent');
					modal.modal('hide');
					window.location = document.URL;
				}else alert(res.error_msg);

			}, 'json');

		//}

		return false;

	}


	return myObject;

})();


var LabourProgress = (function(){

	var myObject = {}

	var LabourProgressform = $('#LabourProgress-form');
	var CustomerID = LabourProgressform.find('input[name="CustomerID"]').val();
	var tran_id = LabourProgressform.find('input[name="tran_id"]').val();

	var progress = 0;

	var modal = null;

	function question_save_btn(elem, str){

		str = str||'text-primary';

		if( $(elem).hasClass('text-danger') ){
			$(elem).removeClass('text-danger');
			$(elem).addClass(str);
		}
	}

	function remove_duplicate_history(){
 

		ch = $('.foot_content_call_history');

		if( ch.length > 1 ){
			$(ch).each(function(key, obj){
				console.log(key);
				console.log(obj);

				if( (ch.length-1) != key ){
					$(obj).remove();
				}

			});
		}

	}

	myObject.init = function(){
 
         
        if( $('#id_watersbroken_dt').length > 0 ){ 
	 		$('#id_watersbroken_dt').datetimepicker({
	 			format:'d/m/Y H:i',
	 			mask: true,
				maxTime:(new Date()),
	 			maxDate: "+0m",
	 			onChangeDateTime:function(current_time,$input){
			    	//alert($input.val())
			    	console.log($input.val());

			    	if( $('select[name="waters_broken"]').val() == '1' ){

			    		if( $('#btn_Waters_Broken').hasClass() == false ){
			    			//$('#btn_Water_Broken').addClass('blink');
			    			$('#btn_Waters_Broken').addClass('text-danger');

			    			$('input[name="waters_broken_dt"]').removeClass('border border-danger');
			    		}
			    	}else{
			    		//$('#btn_Water_Broken').removeClass('blink');
			    		$('#btn_Waters_Broken').removeClass('text-danger');
			    	}

			  	}
	 		});

	       
        }

        if( $('#id_epidural_dt').length > 0 ){ 
	 		$('#id_epidural_dt').datetimepicker({
	 			format:'d/m/Y H:i',
	 			mask: true,
	 			maxTime:(new Date()),
	 			maxDate: "+0m",
	 			onChangeDateTime:function(current_time,$input){
			    	//alert($input.val())
			    	console.log($input.val());

			    	if( $('select[name="epidural"]').val() == '1' ){

			    		if( $('#btn_Epidural').hasClass() == false ){
			    			//$('#btn_Epidural').addClass('blink');
			    			$('#btn_Epidural').addClass('text-danger');

			    			$('input[name="water_broken_dt"]').removeClass('border border-danger');
			    		}
			    	}else{
			    		//$('#btn_Epidural').removeClass('blink');
			    		$('#btn_Epidural').removeClass('text-danger');
			    	}

			  	}
	 		});
	     
 		}

 		if( $('#id_hormone_started_dt').length > 0 ){ 
	 		$('#id_hormone_started_dt').datetimepicker({
	 			format:'d/m/Y H:i',
	 			mask: true,
	 			maxTime:(new Date()),
	 			maxDate: "+0m",
	 			onChangeDateTime:function(current_time,$input){
			    	//alert($input.val())
			    	console.log($input.val());

			    	if( $('select[name="hormone_started"]').val() == '1' ){

			    		if( $('#btn_Hormone_Started').hasClass() == false ){
			    			//$('#btn_Hormone_Started').addClass('blink');
			    			$('#btn_Hormone_Started').addClass('text-danger');

			    			$('input[name="water_broken_dt"]').removeClass('border border-danger');
			    		}
			    	}else{
			    		//$('#btn_Hormone_Started').removeClass('blink');
			    		$('#btn_Hormone_Started').removeClass('text-danger');
			    	}

			  	}
	 		});
 
 		}

 		if( $('#id_lastexam_dt').length > 0 ){ 
	 		$('#id_lastexam_dt').datetimepicker({
	 			format:'d/m/Y H:i',
	 			mask: true
	 		}); 
 		}

 		if( $('#id_nextexam_dt').length > 0 ){ 
	 		$('#id_nextexam_dt').datetimepicker({
	 			format:'d/m/Y H:i',
	 			mask: true
	 		}); 
 		}


 		if( LabourProgressform.find('input[name="collection_kit"]').length > 0 ){ 
	 		$('.cls_collectionkit').on('click', function(){
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();

	 			if( elem_val == 0 ){ 
 

					$.get( "ajax/collection_kit_modal/?_t="+(new Date().getTime()), {CustomerID, tran_id}, function(html) {
		 
					  	var modal = $(html).modal({keyboard:false});
					  	modal.on('hidden.bs.modal', function (e) {
						  	$(elem).prop('checked', false);
						});
					}); 

	 			}

	 		});
 		}


 		if( LabourProgressform.find('input[name="caesar_induction"]').length > 0 ){ 
	 		$('.cls_caesarinduction').on('click', function(){
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();

	 			/*if( elem_val == 1 ){ 
 

					$.get( "ajax/caesar_induction_modal/?_t="+(new Date().getTime()), {CustomerID, tran_id}, function(html) {
		 
					  	var modal = $(html).modal({keyboard:false});
					  	modal.on('hidden.bs.modal', function (e) {
						  	$(elem).prop('checked', false);
						});
					});

	 			}*/
	 			var induction_type = $('.cls_tinduction_type');

	 			induction_type.hide();
	 			induction_type.find('input').attr('disabled', 'disabled');

	 			if( elem_val == 1 ){
	 				induction_type.show();
	 				induction_type.find('input').removeAttr('disabled');
	 			}


	 		});
 		}


 		if( LabourProgressform.find('.cls_tinduction_type input[name="induction_type"]').length > 0 ){ 
	 		$('.cls_tinduction_type input[name="induction_type"]').on('click', function(){
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();

	 			if( elem_val =='Catheter' 
	 				|| elem_val =='Tablet' 
	 				|| elem_val =='Gel' 
	 				|| elem_val =='Tape'
	 				|| elem_val =='Balloon' ){

					$.get( "ajax/induction_type_modal/?_t="+(new Date().getTime()), {}, function(html) {
		 
					  	var modal = $(html).modal({keyboard:false,backdrop:false});
					  	modal.on('hidden.bs.modal', function (e) {
						  	
						});
					});
	 			}

	 		});
 		}

 		if( LabourProgressform.find('input[name="waters_broken"]').length > 0 ){


 			fwaters_broken(); 

	 		$('.cls_watersbroken').on('click', function(){
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();
 
 				fwaters_broken(); 	 		

	 		});

	 		function fwaters_broken(){
	 			in_elem = LabourProgressform.find('input[name="waters_broken"]').filter(":checked").val(); 			 
	 			//console.log(in_elem);

	 			in_div = LabourProgressform.find('#id_watersbroken_dt').parents('.div_waters_broken_dt');
	 			in_div.hide();		 
	 			if( in_elem == '1' ){
	 				in_div.show();
	 			}
	 		}

 		} 

 		if( LabourProgressform.find('input[name="epidural"]').length > 0 ){


 			fepidural(); 

	 		$('.cls_epidural').on('click', function(){
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();
 
 				fepidural(); 	 		

	 		});

	 		function fepidural(){
	 			in_elem = LabourProgressform.find('input[name="epidural"]').filter(":checked").val(); 
	 			in_div = LabourProgressform.find('#id_epidural_dt').parents('.div_epidural_dt');
	 			in_div.hide();		 
	 			if( in_elem == '1' ){
	 				in_div.show();
	 			}
	 		}

 		} 

 		if( LabourProgressform.find('input[name="hormone_started"]').length > 0 ){

 			fhormonestarted(); 

	 		$('.cls_hormonestarted').on('click', function(){
 				//console.log($(this));
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();
 
 				fhormonestarted(); 	 		

	 		});

	 		function fhormonestarted(){
	 			var in_elem = LabourProgressform.find('input[name="hormone_started"]').filter(":checked").val(); 
	 			var in_div = LabourProgressform.find('#id_hormone_started_dt').parents('.div_hormone_started_dt');
	 			
	 			in_div.hide();	

	 			//var hormonestarted_type = $('.hormonestarted_row_type');
	 			//hormonestarted_type.hide();
	 			//hormonestarted_type.find('input').attr('disabled', 'disabled');

	 			//console.log('hormone_started: '+in_elem);
	 				 
	 			if( in_elem == '1' ){
	 				in_div.show();
	 		 
	 				//hormonestarted_type.show();
	 				//hormonestarted_type.find('input').removeAttr('disabled');
	 			}

	 		}

 		} 


		if( LabourProgressform.find('select[name="lp_contract"]').length > 0 ){
 
			flp_contract();

	 		$(LabourProgressform.find('select[name="lp_contract"]')).on('change', function(){
 				flp_contract();
	 		});



			function flp_contract(){

				div_row_fc = LabourProgressform.find('.div_row_fc');
				div_row_lc = LabourProgressform.find('.div_row_lc');
				div_row_cd = LabourProgressform.find('.div_row_cd');


				if( LabourProgressform.find('select[name="lp_contract"]').val() == '1' ){

					div_row_fc.find('input, select').removeAttr('disabled');
					div_row_lc.find('input, select').removeAttr('disabled');
					div_row_cd.find('input, select').removeAttr('disabled');

					div_row_fc.show();
					div_row_lc.show();
					div_row_cd.show();
				}else{
					div_row_fc.find('input, select').attr('disabled','disabled');
					div_row_lc.find('input, select').attr('disabled','disabled');
					div_row_cd.find('input, select').attr('disabled','disabled');

					div_row_fc.hide();
					div_row_lc.hide();
					div_row_cd.hide();
				}


			} 


		}


 		/*if( LabourProgressform.find('input[name="client_status"]').length > 0 ){ 
	 		
	 		LabourProgressform.find('input[name="client_status"]').on('click', function(){
 				 
	 			var elem = $(this);
	 			var elem_val = elem.val();
 
 			 	console.log(elem_val);

	 		});

 		}*/


 		
 		
 		this.onChangeClientStatus(1);

 	}
 		
	myObject.submit = function(form){

		form_data = $(form).serialize(); 
		
		last_exam = $(form).find('#id_lastexam_dt').val();		
		last_exam_unknown = $(form).find('#id_lastexam_dt_unknown').is(':checked');

		nextexam = $(form).find('#id_nextexam_dt').val();
		nextexam_unknown = $(form).find('#id_nextexam_dt_unknown').is(':checked');

		custom_pselector = $(form).find('input[name="custom_pselector"]:checked').val();

		client_status = $(form).find('select[name="client_status"]').val();
		orig_client_status = $(form).find('input[name="orig_client_status"]').val();		

		if( orig_client_status != '' ){
 
			client_status_no_change = $(form).find('input[name="client_status_no_change"]'); 

			if( !client_status_no_change.is(':checked') && orig_client_status == client_status ){
				alert('Please TICK the "No Change" checkbox if no changes on CLIENT STATUS');
				return false;
			} 				
		}


		waters_broken_dt = $(form).find('#id_watersbroken_dt').val();
		if( $(form).find('input[name="waters_broken"]:checked').val() == '1' && (waters_broken_dt == '' || waters_broken_dt == '__/__/____ __:__') ){
			alert('Clients waters broken date/time is required');
			return false;
		}

		epidural_dt = $(form).find('#id_epidural_dt').val();
		if( $(form).find('input[name="epidural"]:checked').val() == '1' && (epidural_dt == '' || epidural_dt == '__/__/____ __:__') ){
			alert('Epidural given date/time is required');
			return false;
		}

		hormone_started_dt = $(form).find('#id_hormone_started_dt').val();
		if( $(form).find('input[name="hormone_started"]:checked').val() == '1' && (hormone_started_dt == '' || hormone_started_dt == '__/__/____ __:__') ){
			alert('Hormone drip started date/time is required');
			return false;
		}


 		// console.log(last_exam);
 		// console.log(last_exam_unknown);
 		// console.log(nextexam);
 		// console.log(nextexam_unknown);
 		
 		baby = $(form).find('select[name="baby"]').val();

 		if( client_status == 1 && baby == 'First' ){
 			//continue
 		}else{

			if( (last_exam == '' || last_exam == '__/__/____ __:__') && last_exam_unknown == false){ 
			 
				alert('Please TICK Last Exam Unknown checkbox if its UNKNOWN');
				return false;
				 				
			}
			if( (nextexam == '' || nextexam == '__/__/____ __:__') && nextexam_unknown == false){ 
			 
				alert('Please TICK Next Exam Unknown checkbox if its UNKNOWN');
				return false;
				 				
			}
		}

		//IF AT HOME
		if( client_status == 1){

			$.post("booking/save/?_t="+(new Date().getTime()), form_data, function(response){

				 if (typeof response.html != "undefined") {  

					$('#labourprogress-dynamic-content').html(response.html); 
					$('#LabourProgress-form').hide(); //hide the call activity form
					$('#lbas_div_dynamic_forms').hide(); //hide the call forms
					
					$('html, body').animate({scrollTop:$('#labourprogress-dynamic-content').offset().top}, 'slow');				

					remove_duplicate_history();
					$('.foot_content_call_history').html(response.calls_result);
				}else{

					alert('Unable to save CLIENT STATUS IF AT HOME, please report to admin.');
				}
	
			}, 'json');

			return false;

		}else if( $(form).find('select[name="lp_contract"]').val() != '1' && typeof custom_pselector == "undefined" ){

			$.post("booking/save/?_t="+(new Date().getTime()), form_data, function(response){

				 if (typeof response.html != "undefined") {  

					$('#labourprogress-dynamic-content').html(response.html); 
					$('#LabourProgress-form').hide(); //hide the call activity form
					$('#lbas_div_dynamic_forms').hide(); //hide the call forms
					
					remove_duplicate_history();
					$('.foot_content_call_history').html(response.calls_result);

					$('html, body').animate({scrollTop:$('#labourprogress-dynamic-content').offset().top}, 'slow');

				}else{

					alert('Unable to save CLIENT STATUS IF AT HOME, please report to admin.');
				}
			}, 'json');

			return false;

		}else{


			$.post( "ajax/labourprogress_selector/?_t="+(new Date().getTime()), form_data, function(html) { 

			  	modal = $(html).modal({keyboard:false, backdrop:true});
			  	modal.on('hidden.bs.modal', function (e) {
				  	modal.modal('dispose');	
				  	$('.modal').remove();
				  	
				});

				$('#MLabourProgress-form').submit();
			  	modal.modal('hide');
			}); 
		}

		return false;


	} 


	myObject.modalPSSubmit = function(form){
		
		form_data = $(form).serialize(); 
		//console.log(form_data);
		$.post("booking/save/?_t="+(new Date().getTime()), form_data, function(response){


			if (typeof response.html != "undefined") {

				$('#labourprogress-dynamic-content').html(response.html);


				$('#LabourProgress-form').hide(); //hide the call activity form
				$('#lbas_div_dynamic_forms').hide(); //hide the call forms

				modal.modal('hide');

				remove_duplicate_history();
				$('.foot_content_call_history').html(response.calls_result);

				//$('html, body').animate({scrollTop:$(document).height()}, 'slow');
				//$('html, body').animate({scrollTop:$('#labourprogress-dynamic-content').offset().top}, 'slow');

				//$('html, body').animate({scrollTop:$('#lbas_div_dynamic').offset().top}, 'slow');
				$('html, body').animate({scrollTop:$('#labourprogress-dynamic-content').offset().top}, 'slow');

			}else{

				alert('Unable to save CLIENT STATUS IF AT HOME, please report to admin.');
			}
			
		}, 'json');

		return false;


	}

	myObject.onCheckSelectorToggle = function(elem){
		
		var custom_pselector = $('#LabourProgress-form').find('input[name="custom_pselector"]');
		//console.log(custom_pselector);
		if( $(elem).prop("checked") ){

			//$(elem).parents('tr').find('input[type="checkbox"]').each(function(){
			$(custom_pselector).each(function(){
				$(this).removeAttr('checked');
			 
				$(this).prop("checked", false);
			});

			$(elem).prop('checked', true);
		}

	}

	/*
	//discontinue
	myObject.babyOnChange = function(elem){

		$('.c_td_fc, .c_td_lc').hide();
		if( elem.value != 'First' ) {
			$('.c_td_fc, .c_td_lc').show();
		}

	}*/
         
	myObject.advise_script_save = function(form){
		form_data = $(form).serialize(); 
		//console.log(form_data);
		checked = $(form).find("input[type=checkbox]:checked").length;
		redirect = $(form).find('input[name="redirect"]');

		if( !checked ) {
			alert('You must check at least one checkbox');
			return false;
		}

		$.post("ajax/labourprogress_advicescript_save/?_t="+(new Date().getTime()), form_data, function(response){
		 
			  	//alert(response);

			if(redirect.length > 0) {
				window.location = redirect.val();
			}else{

			  	$('#lbas_div_dynamic').remove();
			  	
			  	$('#lbas_div_dynamic_forms').show(); //show the call forms

			  	remove_duplicate_history();

			  	$(".foot_content_call_history").nextAll().remove();

			 	$('html, body').animate({scrollTop:$('#labourprogress-dynamic-content').offset().top}, 'slow');
			 }
		});

		return false;
	}

	myObject.onChangeClientStatus = function(elem){

		var form = $('#LabourProgress-form');
		var client_status = form.find('select[name="client_status"] option:selected').val();
		var epidural = $('.ctr_tepidural');
		
		var induction = $('.ctr_tinduction'); //.induction_row_cond
		var induction_type = form.find('.ctr_tinduction .induction_row_type');
		var induction_cond_yes = form.find('#lbpcaesarinduction_1'); //induction yes
		 
		var hormonestarted = $('.ctr_thormonestarted'); //.hormonestarted_row_cond
		var hormonestarted_type = form.find('.ctr_thormonestarted .hormonestarted_row_type');	 
		var hormonestarted_cond_yes = form.find('#hormonestarted_1'); //induction yes
		


		epidural.hide();
		epidural.find('input').attr('disabled', 'disabled');

		hormonestarted.hide();
		hormonestarted.find('input').attr('disabled', 'disabled');

		induction.hide();
		induction.find('input, select').attr('disabled', 'disabled');
		
		induction_type.find('input, select').attr('disabled', 'disabled');
		hormonestarted_type.find('input, select').attr('disabled', 'disabled');


		//console.log(hormonestarted_type.find('input, select'));

		progress_selector_row = $('.progress-selector-row');
		progress_selector_row.show();
		 
		baby = form.find('select[name="baby"]').val();

		lp_contract = form.find('select[name="lp_contract"]');
		lp_contract.attr('required', 'required');

		if( client_status == 1  && baby == 'First') {

			progress_selector_row.hide();
			lp_contract.removeAttr('required', 'required');
		}



		if( client_status == '3' ) {
			epidural.show();
			epidural.find('input').removeAttr('disabled', 'disabled');
			
			hormonestarted.show();
			hormonestarted.find('.hormonestarted_row_cond').find('input').removeAttr('disabled', 'disabled');
			
			induction.show();
			induction.find('.induction_row_cond').find('input, select').removeAttr('disabled', 'disabled');

			if( induction_cond_yes.is(':checked') ){
				induction_type.show();				
				induction_type.find('input, select').removeAttr('disabled', 'disabled');
			}
			
			if( hormonestarted_cond_yes.is(':checked') ){
				hormonestarted_type.show();
				hormonestarted_type.find('input, select').removeAttr('disabled', 'disabled');
			}
		}

		//console.log(hormonestarted_type.find('input, select'));
	}

	return myObject;

})();




var Collector = (function(){

	var myObject = {}

	var progress = 0;

	
	myObject.confirm = function () {

		var form = $('#Collector-form');
		collector_distance = form.find('input[name="collector_distance"]:not(:disabled)').val();  
		submit_continue_distanceConfirm = form.find('input[name="submit_continue_distanceConfirm"]');  
		submit_continue_reminderPopup = form.find('input[name="submit_continue_reminderPopup"]');  
		 
		
		if( collector_distance > 60 && submit_continue_distanceconfirm.val() == '0' ){

			// var msg = "Collector is more the 60mins from hospital \n\n";
			// 	msg += "Agent Note \n\n";
			// 	msg += "Call the On-call Nurse and advise Collector travel time > than 60 minutes and \n\ndiscuss how despatch criteria needs to be amended \n\n ";
			// 	msg += "\n\nProceed on On Call Nurse \n\n ";

			//alert('CALL the On Call Nurse and let them know');
			//alert(msg);
 
	 		$.get("ajax/collector_distance_modal/?_t="+(new Date().getTime()), {}, function(response){
	 			//collector_modal_distance_confirm.val(1);
	 			submit_continue_distanceconfirm.val('1');

			  	modal = $(response).modal({keyboard:false, backdrop:true});
			  	modal.on('hidden.bs.modal', function (e) {
				  	modal.modal('dispose');	

				  	$('.modal').remove();
	 				
	 				submit_continue_distanceconfirm.val('0');
				});

			});

			

			return false;
		}


		var call_res_id = form.find('select[name="call_res_id"] option:selected'); 

		if( call_res_id.attr('data-reminderPopup') == '1' && submit_continue_reminderPopup.val() == '0'  ){
			submit_continue_reminderPopup.val('1');
 
 
	 		html = '<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel" aria-hidden="true">'
			+'  <div class="modal-dialog" role="document">'
			+'    <div class="modal-content">'
			+'      <div class="modal-header">'
			+'        <h5 class="modal-title">Reminder to</h5>'
			+'        <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
			+'          <span aria-hidden="true">&times;</span>'
			+'        </button>'
			+'      </div>'
			+'      <div class="modal-body">'
			+'        <div class="text-center"><h3>SET a REMINDER if needed</h3><br /><p>Close alert to continue</p></div>'
			+'      </div>'
			+'      <div class="modal-footer">'
			+'        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$(\'#Collector-form\').submit()" >Close</button> '
			+'      </div>'
			+'    </div>'
			+'  </div>'
			+'</div>';

		  	modal = $(html).modal({keyboard:false, backdrop:false});
		  	modal.on('hidden.bs.modal', function (e) {
			  	modal.modal('dispose');	
			  	$('.modal').remove(); 

			});


			return false;
		}


		// if( confirm('Please confirm submit') )
		// 	return true;

		// return false;

		return true;
	};

	myObject.collectorform_submit = function() {
		
		$('#Collector-form').submit();
	}

	myObject.select = function(elem){

		var form = $('#Collector-form'),
 			ptr = $(elem).parents('tr'),
			trchildren = ptr.children('td');
		//console.log(trchildren[1].textContent);
		form.find('input[name="caller_name"]').val($(elem).text());
		form.find('input[name="caller_phone"]').val(trchildren[1].textContent);
	}

	myObject.distance_nochange = function(elem) {
	 	
	 	var form = $('#Collector-form');
		var nochange = form.find('input[name="collector_distance_nochange"]:checked').val();
		var collector_distance = form.find('input[name="collector_distance"]:not(:disabled)');
		var old_collector_distance = form.find('input[name="old_collector_distance"]').val();
		
		collector_distance.attr('required', 'required');
		collector_distance.val('');
		
		if( nochange == 1 ) {
			collector_distance.removeAttr('required');
			collector_distance.val(old_collector_distance);
		}
	}

	myObject.toggleMsg = function(elem) {
	 
		next_tr = $(elem).closest('tr').next('tr');		 
		next_tr.toggle();
	}

	myObject.onDistanceSel = function(elem){
		
		if(elem.value == 'More than 60 mins'){
			alert('POPUP warning agent that may have to despatch early - script TBC');
		}

	}

	myObject.onCallReponseChange = function(elem){
		//Collector - AT HOSPITAL
		
		/*selected_text = $(elem).find('option:selected').text();
		
		$('#div_collector_distance').find('input').removeAttr('disabled');

		var callresponse = ["Collector - AT HOSPITAL", "Collector - Not called", "Collector - not available"];

		//if( selected_text == 'Collector - AT HOSPITAL' ){
		if( callresponse.indexOf(selected_text) != '-1'){
			$('#div_collector_distance').find('input').attr('disabled', 'disabled');
		}*/

		$('#div_collector_distance').find('input').removeAttr('disabled');

		var selected = $(elem).find('option:selected');
		 
		if( selected.attr('data-distanceRequired') != '1' ){
			$('#div_collector_distance').find('input').attr('disabled', 'disabled');
		}

		// if( selected.attr('data-reminderPopup') == '1' ){
			
		// 	Misc.basicmodal('Reminder to', '<div class="text-center"><h3>SET a REMINDER if needed</h3><br /><p>Close alert to continue</p></div>');
		// }
	}

	return myObject;

})();



var OnCall = (function(){

	var myObject = {}

	var progress = 0;

	myObject.confirm = function () {

		var form = $('#OnCall-form');  
		submit_continue_reminderPopup = form.find('input[name="submit_continue_reminderPopup"]');
		
		var call_res_id = form.find('select[name="call_res_id"] option:selected'); 

		if( call_res_id.attr('data-reminderPopup') == '1' && submit_continue_reminderPopup.val() == '0'  ){
			submit_continue_reminderPopup.val('1');
 
 
	 		html = '<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel" aria-hidden="true">'
			+'  <div class="modal-dialog" role="document">'
			+'    <div class="modal-content">'
			+'      <div class="modal-header">'
			+'        <h5 class="modal-title">Reminder to</h5>'
			+'        <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
			+'          <span aria-hidden="true">&times;</span>'
			+'        </button>'
			+'      </div>'
			+'      <div class="modal-body">'
			+'        <div class="text-center"><h3>SET a REMINDER if needed</h3><br /><p>Close alert to continue</p></div>'
			+'      </div>'
			+'      <div class="modal-footer">'
			+'        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="$(\'#OnCall-form\').submit()" >Close</button> '
			+'      </div>'
			+'    </div>'
			+'  </div>'
			+'</div>';

		  	modal = $(html).modal({keyboard:false, backdrop:false});
		  	modal.on('hidden.bs.modal', function (e) {
			  	modal.modal('dispose');	
			  	$('.modal').remove(); 

			});


			return false;
		}
 

		return true;
	};

	myObject.select = function(elem){

		var form = $('#OnCall-form'),
 			ptr = $(elem).parents('tr'),
			trchildren = ptr.children('td');
		//console.log(trchildren[1].textContent);
		form.find('input[name="caller_name"]').val($(elem).text());
		form.find('input[name="caller_phone"]').val(trchildren[1].textContent);
	}

	myObject.onCallReponseChange = function(elem){
	  
		var selected = $(elem).find('option:selected');
	 
		// if( selected.attr('data-reminderPopup') == '1' ){
			
		// 	Misc.basicmodal('Reminder to', '<div class="text-center"><h3>SET a REMINDER if needed</h3><br /><p>Close alert to continue</p></div>');
		// }
	} 


	return myObject;

})();
 


var HospitalForm = (function () {

  	// locally scoped Object
	var myObject = {};
 	
	myObject.confirm = function (form) {

		
		var issue_type = $(form).find('input[name="issue_type"]').val();
		var table_confirm_hospital = $(form).find('#table_confirm_hospital');
		var table_induction_ceasar = $(form).find('#table_induction_ceasar');

		//console.log(table_induction_ceasar.css());
		console.log(table_induction_ceasar.css('display'));
		console.log(issue_type);
		if( (issue_type=='INDUCTION' || issue_type == 'CAESAR') && table_induction_ceasar.css('display') == 'none' ){

			// table_confirm_hospital.hide();
			// table_induction_ceasar.show();

			// table_induction_ceasar.find('input, select, textarea').removeAttr('disabled');

			myObject.showhide(2);

			return false;
		}

		if( $('#id_sched_d').length > 0 ){

			if($('#id_sched_d').val() == '__/__/____' || $('#id_sched_d').val() == ''){
				alert('Schedule Date must be set');
				return false;
			} 

		}
 
		//if( confirm('Please confirm submit') ){

			data = $(form).serialize();

			$.post("booking/save/?_t="+(new Date().getTime()), data, function(json){
				
				//console.log(json);

				if (typeof json.modal !== 'undefined') {
					
					//alert(json.modal_content);
 
					$.get('ajax/modal_redirect',json, function(html){

					  	modal = $(html).modal({keyboard:false, backdrop:true});
					  	modal.on('hidden.bs.modal', function (e) {
						  	modal.modal('dispose');	

						  	$('.modal').remove();


						  	window.location = json.redirect_url;
			 
						}); 

					});

					//window.location = json.redirect;
				}else{
					//alert('Successfully submitted');




					window.location = json.redirect;
				}


			},'json' );

			
		//}

		return false;
	}; 	

 	myObject.init = function(elem){

 		var hospitalform = $('#Hospital-form');

        if( $('#id_sched_d').length > 0 ){ 
	 		$('#id_sched_d').datetimepicker({
	 			timepicker:false,	 
	 			format:'d/m/Y',
	 			mask: true,
	 			minDate: "+0m",
	 			onChangeDateTime:function(current_time,$input){
			    	//alert($input.val())
			    	//console.log($input.val());
 

			  	}
	 		}); 
        }

        if( $('#edit_id_sched_d').length > 0 ){ 
	 		$('#edit_id_sched_d').datetimepicker({
	 			timepicker:false,	 
	 			format:'d/m/Y',
	 			mask: true,
	 			minDate: "+0m",
	 			onChangeDateTime:function(current_time,$input){ 

			  	}
	 		}); 
        }

 		/*if( hospitalform.find('input[name="collection_kit"]').length > 0 ){ 
	 		$('.cls_collectionkit').on('click', function(){
	 			
	 			var elem = $(this);
	 			var elem_val = elem.val();

	 			var tran_id = hospitalform.find('input[name="tran_id"]').val();
	 			var CustomerID = hospitalform.find('input[name="CustomerID"]').val();
	 			var issue_type = hospitalform.find('input[name="issue_type"]').val();

	 			if( elem_val == 0 ){ 
 

					$.get( "ajax/collection_kit_modal/?_t="+(new Date().getTime()), {CustomerID, tran_id, issue_type}, function(html) {
		 
					  	var modal = $(html).modal({keyboard:false});
					  	modal.on('hidden.bs.modal', function (e) {
						  	$(elem).prop('checked', false);
						});
					}); 

	 			}

	 		});
 		}*/


 		//CHECK if induction or caesar
 		/*confirm_hospital = hospitalform.find('input[name="confirm_hospital"]').val();
 		orig_confirm_hospital = hospitalform.find('input[name="orig_confirm_hospital"]').val();
 		issue_type = hospitalform.find('input[name="issue_type"]').val();

 		if( (confirm_hospital || orig_confirm_hospital) && (issue_type=='INDUCTION' || issue_type=='CAESAR') ){
 			myObject.showhide(2); 
 		}else{
 			myObject.showhide(1);
 		}*/
 		
 		myObject.showhide(1);

 	}

 	myObject.showhide = function(show){

 		var hospitalform = $('#Hospital-form');

		var table_confirm_hospital = $(hospitalform).find('#table_confirm_hospital');
		var table_induction_ceasar = $(hospitalform).find('#table_induction_ceasar');
		var hospital_btn_back = $(hospitalform).find('#hospital-btn-back');

 		if( show == 1 ){ 
			table_confirm_hospital.show();
			table_induction_ceasar.hide();

			table_induction_ceasar.find('input, select, checkbox, textarea').attr('disabled', 'disabled');	

			hospital_btn_back.hide();
			
 		}else{

			table_confirm_hospital.hide();
			table_induction_ceasar.show();

			table_induction_ceasar.find('input, select, checkbox, textarea').removeAttr('disabled');

			hospital_btn_back.show();
 		}

 	}

	myObject.select = function(elem){

		$('#td_search_hospital').hide();
		var form = $('#Hospital-form'); 
		if( elem.value == '0' ){ 
			form.find('input[name="Hospital"]').val('');
			form.find('input[name="HospitalStreet"]').val('');
			form.find('input[name="HospitalCity"]').val('');
			form.find('input[name="HospitalRegion"]').val('');
			form.find('input[name="HospitalPostalCode"]').val('');
			form.find('input[name="HospitalPhone"]').val(''); 

			$('#td_search_hospital').show();
		}else{
			 
			form.find('input[name="Hospital"]').val(form.find('input[name="hide_Hospital"]').val());
			form.find('input[name="HospitalStreet"]').val(form.find('input[name="hide_HospitalStreet"]').val());
			form.find('input[name="HospitalCity"]').val(form.find('input[name="hide_HospitalCity"]').val());
			form.find('input[name="HospitalRegion"]').val(form.find('input[name="hide_HospitalRegion"]').val());
			form.find('input[name="HospitalPostalCode"]').val(form.find('input[name="hide_HospitalPostalCode"]').val())
			form.find('input[name="HospitalPhone"]').val(form.find('input[name="hide_HospitalPhone"]').val()); 

		}

		//alert(elem);
	};

  	return myObject;

})();



var ClientHospital = (function(){

	var myObject = {}

	var progress = 0;


	myObject.select = function(elem){

		var form = $('#ClientHospital-form'),
 			ptr = $(elem).parents('tr'),
			trchildren = ptr.children('td');
		//console.log(trchildren[1].textContent);
		form.find('input[name="caller_name"]').val($(elem).text());
		form.find('input[name="caller_phone"]').val(trchildren[1].textContent);
	}

	return myObject;

})();


var OnCallVerify = (function(){

	var myObject = {}

	var progress = 0;


	myObject.btn = function(v){

		if( v == 'Yes' ){
			$('#cccverify_ibox1').hide();
			$('#cccverify_ibox2').show();
		}
	 
	}

	return myObject;

})();

var Callerform = (function(){

	var myObject = {}

	var progress = 0;


	myObject.onchange_ClientRelation = function(){

		if( $('#div-best-no').length > 0 ){
			var form = $('#caller-form'),  
			best_no_to_call = form.find('input[name="orig_best_no_to_call"]').val();

			ClientRelation = form.find('select[name="ClientRelation"] option:selected').val(); 

			$('#div-best-no').hide();
			if( ClientRelation == 'CLIENT RELATION' && best_no_to_call == '' ){
				$('#div-best-no').show();
			} 

			//console.log(ClientRelation);
			//console.log(form.find('input[name="collector"]').val());

			if( ClientRelation == 'CLIENT RELATION' ) {
				form.find('input[name="CallerName"]').val(form.find('input[name="PartnersFirstName"]').val());
				form.find('input[name="CallerPhone"]').val(form.find('input[name="PartnersMobilePhone"]').val());
			}else if( ClientRelation == 'ON CALL' ) {
				form.find('input[name="CallerName"]').val('');
				form.find('input[name="CallerPhone"]').val('1800005541');
			}else if( ClientRelation == 'COLLECTOR' ){
				
				form.find('input[name="CallerName"]').val(form.find('input[name="collector"]').val());
				form.find('input[name="CallerPhone"]').val(form.find('input[name="collector_phone"]').val());
			}
	 	}
	}

	myObject.copy_callerphone = function(){
		var form = $('#caller-form'),  
		CallerPhone = form.find('input[name="CallerPhone"]').val();		
		best_no_to_call = form.find('input[name="best_no_to_call"]');

		best_no_to_call.val(CallerPhone);

	}

	myObject.init = function(){
 
		this.onchange_ClientRelation();
	}

	return myObject;

})();


$(function(){


	LabourProgress.init();
	HospitalForm.init();
	Callerform.init();

	//APP popup on labour, oncall, and collector
	if( $('#APP_poup').length > 0 ){
		Booking.app_popup_elgible();
	}

	url = window.location.href;
 
	if( url.match(/caller|labourprogress|collector|oncall|hospital|cccverify/gi) ) {
	     
	    //$('html, body').animate({scrollTop:$(document).height()}, 'slow');
	    $('html, body').animate({scrollTop:$('#booking-details-navbar').offset().top}, 'slow');

	}
 

});