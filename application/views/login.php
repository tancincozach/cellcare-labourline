<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<base href="<?php echo base_url(); ?>" />

	<!-- Bootstrap CSS -->
	<link href="assets/vendor/bootstrap-4.2.1/css/bootstrap.min.css" rel="stylesheet">

	<!-- <link href="assets/vendor/jquery/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet"> -->

	<!-- Custom styles for this template -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">     -->    
	<link href="assets/vendor/fontawesome-free-5.6.3/css/all.min.css" rel="stylesheet">
	 
    <base href="<?php echo base_url(); ?>" />

	<title>Cellcare - Labour Line</title>

    <style type="text/css">
        
        html,
        body {
          height: 100%;
        }

        body {
          display: -ms-flexbox;
          display: flex;
          -ms-flex-align: center;
          align-items: center;
          /*padding-top: 40px;*/
          padding-bottom: 40px;
          background-color: #f5f5f5;
        }

        .form-signin {
          width: 100%;
          max-width: 330px;
          padding: 15px;
          margin: auto;
        }
        .form-signin .checkbox {
          font-weight: 400;
        }
        .form-signin .form-control {
          position: relative;
          box-sizing: border-box;
          height: auto;
          padding: 10px;
          font-size: 16px;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="email"] {
          margin-bottom: -1px;
          border-bottom-right-radius: 0;
          border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }


    </style>

</head>
<body>
   	

    <form class="form-signin" method="post">
        <img class="mb-4" src="assets/images/logo1.png" alt=""  height="90">
        <h1 class="h3 mb-3 font-weight-normal">Please login</h1>
        
        <?php if( $this->session->flashdata('login_message') != '' ): ?>    
        <?php echo $this->session->flashdata('login_message') ?>
        <?php endif; ?>

        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" checked>
          <label class="form-check-label" for="inlineRadio1">Client</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2">
          <label class="form-check-label" for="inlineRadio2">CMA</label>
        </div>

        <label for="inputUsername" class="sr-only">Username</label>       
        <input type="text" id="inputUsername" name="inputUsername" class="form-control" placeholder="Username" required autofocus>
        
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="inputPassword" class="form-control" placeholder="Password" required>
  
        <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
        
        <p class="mt-5 mb-3 text-muted text-center">&copy; 2019 Well Done International</p>
    </form>

  </body>
</html>