<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporting extends MY_Controller {

	public function __construct(){

		parent::__construct();

		unset($this->view_data['maintain']);

		$this->view_data['view_file'] = 'pages/reporting';
	}


	public function index()
	{
		
		$this->load->view('template', $this->view_data);
 

	}
}
