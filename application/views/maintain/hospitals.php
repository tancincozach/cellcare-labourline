<h2>Hospitals</h2>

<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;


	echo form_open_multipart('','class="form-inline"' );
?>


	<div class="form-group">

		<input type="hidden" name="upload" value="1">
		
		<input type="file" name="hospital_file"  class="form-control form-control-sm" />&nbsp;


		<button type="submit" class="btn btn-success btn-sm" />Upload</button>
	</div>

</form>
<br/>
<div class="table-responsive">


<?php 
	if(!empty($fields)):
?>	
		<table class="table table-bordered table-sm table-striped">
			<thead>
				<?php 
				$flds=0;
				foreach ($fields as $key => $fld_row) : ?>

					<th class="table-primary"><?php echo $fld_row->value;?></th>
				<?php 
				$flds++;
				endforeach;?>				
			</thead>
			<tbody>
				<?php  
				$ctr=0;

				if(!empty($results)):
					foreach($results as $key=>$row):?>
				<tr>
					<?php foreach ($fields as $fld_row): ?>
							<td>
								<?php echo $results[$ctr]->{$fld_row->code};?>
							</td>
					<?php endforeach ?>
				</tr>				
			<?php 
			$ctr++;
			endforeach;
				else:
			?>
				<tr>
					<td colspan="<?php echo $flds;?>"  >No record(s) found </td>
				</tr>
			<?php endif;?>
			</tbody>
		</table>
	<?php
	  else:
	?>	
	<div class="alert alert-danger" role="alert">
				No field template found for Customer(s)
	</div>

	<?php
	  endif;
	?>
</div>