<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notices extends MY_Controller {

	public function __construct(){

		parent::__construct();

		unset($this->view_data);

		$this->view_data['view_file'] = 'pages/notices';

       $this->load->helper('apps_helper');
	}


	public function index()
	{
					
			
			$this->load->model('Noticesmodel','notice');

			$params = $this->input->get();

			$notice_group = isset($params['grp']) ? $params['grp']:1;

			$this->load->library("pagination");

			$params = $this->input->get();
			$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
			unset($params['per_page']);

			$data = '';

			$data['filters'] = $params;
			$params['grp'] = $notice_group;
			unset($params['view_all']);
			


			//$query_params = $this->build_where($params);
			$query_params = '';
			$params_query = @http_build_query($params);
			unset($params['grp']);
			$per_page =15;
			$query_params['limits']['start'] = $page;
			$query_params['limits']['limit'] = $per_page; 
			$query_params['sorting']['order'] = 'DESC';
			$query_params['sorting']['sort'] = 'date_created';
			$query_params['where_str'] = " `notice_status`=1 AND `notice_group`= {$notice_group} AND (NOW() <= `expire_date` OR `expire_date` = '0000-00-00 00:00:00' OR `expire_date` IS NULL)";


			$results 				    = $this->notice->listing($query_params);


			$p_config["base_url"] 		= base_url() . "notices/?".$params_query;
			$p_config["total_rows"] 	= $results['total_rows'];
			$p_config["per_page"] 		= $per_page;
			$p_config["uri_segment"] 	= 3; 
			$config = $this->Commonmodel->pagination_config($p_config);	 
			$this->pagination->initialize($config);

			$data['results'] 	= $results['results'];
			$data["links"] 		= $this->pagination->create_links();
			$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$results['total_rows'].' entries';
		

		    $this->view_data['view_file'] = 'pages/notices';

		    $data['notices'] = @$data['results'];
		    $data['notice_grp'] = $notice_group;
		    $data['params'] = $params;
			$this->view_data['data'] = $data;

			$this->load->view('template', $this->view_data);
 

	}


	public function notice_form($form_type='new',$notice_id=''){

		$this->load->model('Noticesmodel','notice');

		$data = '';		

		if($_POST){

			try {

				$post = $this->input->post();				

				if(
						empty($post['response_name']) ||
						empty($post['script_content'])
					)

					throw new Exception("Error : Input Required Fields");

					$set['content_title']  = @$post['response_name'];
					$set['content_html']  = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $_REQUEST['script_content'] );				
					$set['expire_date']  = isset($post['expiry_date_hidden'])  && $post['expiry_date_hidden']!="" ?  format_datetime($post['expiry_date_hidden']):"0000-00-00 00:00:00";						
					$set['agent_name'] = $this->session->userdata('agent_name');
					$set['user_id'] = $this->session->userdata('user_id');
					$set['notice_group'] = isset($post['notice_group']) ? $post['notice_group']:1;
					$set['notice_status'] =  @$post['status'];


				if(!empty($post['notice_id'])){

					$set['date_updated']  =  date('Y-m-d H:i:s');
					$notice_id = $this->notice->update($post['notice_id'],$set);

					if(!$notice_id) 
						throw new Exception("Error: Failure to Update a Notice");
					

				}else{

					$notice_id = $this->notice->insert($set);

					if(!$notice_id)
						throw new Exception("Error: Failure to Add a Notice");

				}

				if(!empty($post['notice_id'])){

					$this->session->set_flashdata('msg','Update Successful');
					//redirect('notices/notice_form/'.$post['notice_id']);
					redirect('notices/?grp='.$set['notice_group']);

				}else{
					
					$this->session->set_flashdata('msg','Adding of Notice is Successful');
					redirect('notices/?grp='.$set['notice_group']);
					
				}

					
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());

			}				
		}



		$params = $this->input->get();

		$data['form_type'] = $form_type;

		if(isset($notice_id) && $notice_id!=''){

			$where = array();

			$where['where']['id'] = $notice_id;

			$data['notice_row'] = $this->notice->row($where);			

			$data['form_type'] = 'edit';
		}

		

		$data['notice_grp'] = isset($params['grp']) ? $params['grp']:1;


		$this->view_data['css_file'] = '<link href="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">'; 
		$this->view_data['js_file']  = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.full.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/js/pages/notices.js"></script>'; 
		$this->view_data['data'] = $data;
		$this->view_data['menu_active'] = 'notices';
		$this->view_data['view_file'] = 'pages/notice-form';
		$this->load->view('template', $this->view_data);


	}


}
