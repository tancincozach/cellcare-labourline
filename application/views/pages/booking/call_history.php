
<div class="col-sm-12 mb-5">
    <div class="row border mt-1 ">
        
        <div class="col-sm-12">
            <h4 class="text-center">Call Activity - History </h4>
        </div>

        <div class="col-sm-12">
             
            <table class="table table-sm ">
                <thead>
                    <tr class="bg-feijoa text-dark">
                        <th>Server</th>
                        <th>Local</th>
                        <th>Details</th>
                        <th>Call Type</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i = 1;
                        foreach ($calls_result as $row): 

                        //$dt = explode(' ', $row->call_created);

                        $bg = ($i%2)?'bg-hintofgreen':'bg-white';
                    ?>
                    <tr class="<?php echo $bg; ?>">
                         
                        
                        <td><?php echo date('d/m/Y <b>H:i</b>',strtotime($row->call_created)); ?></td>                        
                        <td><?php echo date('d/m/Y <b>H:i</b>',strtotime($row->cust_timezone)); ?></td>                        
                        <td rowspan="2">
                            <p>
                            <?php

                                if( in_array($row->call_type, array('hospital', 'update'))   ){

                                    $obj = json_decode($row->more_text);

                                    foreach ($obj as $key => $value) {
                                        $label = ucwords(str_replace('_', ' ', $key));
                                        echo '<strong>'.$label.'</strong> '.(($value != '')?': '.$value:'').'<br />';
                                    }
                                    
                                }else{
                                   
                                    //echo '<strong>Caller Type:</strong> '.$row->call_type.'<br />';
                                    if( !empty($row->caller_name) )
                                        echo '<strong>Caller Name:</strong> '.stripslashes($row->caller_name).'<br />';
                                    
                                    if( !empty($row->caller_phone) )
                                        echo '<strong>Caller Phone:</strong> '.$row->caller_phone.'<br />';
                                    
                                    if( $row->ClientRelation != '' ){
                                        echo '<strong>Client Relation:</strong> '.$row->ClientRelation.'<br />';
                                    }

                                    if( $row->call_res_id != '' ){
                                        echo '<strong>Call Response:</strong> '.stripslashes($row->call_res_text).'<br />';                                        
                                    }

                                    if( $row->call_notes != '' ){
                                        echo '<strong>Notes:</strong> '.stripslashes($row->call_notes).'<br />';                                        
                                    }

                                    if( $row->more_text != '' ){
                                        $obj = json_decode($row->more_text);
                                        foreach ($obj as $key => $value) {
                                            $label = ucwords(str_replace('_', ' ', $key));
                                            echo '<strong>'.stripslashes($label).'</strong> '.(($value != '')?': '.stripslashes($value):'').'<br />';
                                        }                                       
                                    }

                                }

                            ?>
                            <p>

                            <?php 

                                $audit_type = array('sms'=>'SMS', 'email'=>'EMAIL', 'intensifier'=>'REMINDER', 'cmaautolog'=>'CMA Auto Log');
                                $audit_type_bg = array('SMS'=>'bg-sms', 'EMAIL'=>'bg-email', 'REMINDER'=>'bg-reminder', 'CMA Auto Log'=>'bg-light');

                                if( $callaudit = $this->Callsmodel->get_audit($row->call_id) ):  
                                

                                    $table = '<table class="table tabl-sm border">';
                                         
                                    foreach($callaudit as $a):

                                        if( $a->audit_type == 'cmaautolog' ) continue;

                                        $success = isset($audit_type[$a->audit_type])?$audit_type_bg[@$audit_type[$a->audit_type]]:'bg-primary';

                                        $table .= '<tr class="'.($a->audit_status?$success:'bg-danger').'" >
                                            <td>'.$audit_type[$a->audit_type].'</td>
                                            <td>'.$a->audit_to.'</td>
                                            <td>'.($a->audit_status?'message_sent':stripslashes($a->audit_status_error)).'</td> 
                                            <td><a href="javascript:void(0)" onclick="Collector.toggleMsg(this)" class="btn btn-sm m-0 p-0">View Message</a></td> 
                                        </tr>
                                        <tr style="display: none">
                                            <td colspan="4">'.stripslashes($a->message).'</td>
                                        </tr>
                                        ';
                                    endforeach;  

                                    echo $table.'</table>';
                                    
                                endif; 
                             ?>
                        </td>
                        <td rowspan="2"><?php echo $row->call_type; ?></td>
                        
                    </tr>
                    <tr class="<?php echo $bg; ?>">
                        <td colspan="2" style="border-top: 0 !important">Agent Name: <?php echo $row->agent_name; ?></td>
                    </tr>
                    <?php $i++; endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>