 
<div class="col-sm-12">

	<h5>All Calls</h5>

	
	<?php echo form_open('all_calls', ' method="get" name="LabourProgress-form" id="LabourProgress-form"  '); ?>
		 
			<div class="row">
				<div class="col">
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">Search</span>
						</div>
						<input type="text" name="search" class="form-control" placeholder="Client Name, ClientID, Caller name" aria-label="Search" aria-describedby="basic-addon1" value="<?php echo trim(@$_GET['search']); ?>">
						<div class="input-group-append">
							<button type="submit" class="btn btn-primary btn-sm">Search</button>
							<a class="btn btn-secondary btn-sm" href="current_calls">Clear</a>
						</div>					
					</div>
					
				</div>
				<div class="col">
					
				</div>
			</div>
		 
	</form>
	
</div>

<div class="col-sm-12">
  
    <table class="tablelist table table-sm table-striped table-bordered">
        <thead>
            <tr class="bg-feijoa text-dark">
                <th>Date Active</th>
                <th>Time Active</th>
                <th>Caller Name</th>
                <th>Client Name</th>
                <th>Client ID</th>
                <th>Baby</th>
                <th>Client Status</th>
                <th>Collector Status</th>
                <th>Hospital Delivering At</th>
                <th>State</th>
                <th>Collector</th>
                <th>Reminders</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $row): 

                $dt = explode(' ', $row->tran_updated);

                if( $row->tran_type == 'Booking' ):
            ?>
            <tr>
                <td><?php echo date('d/m/Y',strtotime($dt[0])); ?></td>
                <td><?php echo date('H:i',strtotime($dt[1])); ?></td>
                <td><?php echo stripslashes($row->CallerName); ?></td>
                <td><?php echo stripslashes($row->FirstName.' '.$row->LastName); ?></td>
                <td><?php echo $row->CustomerID; ?></td>
                <td><?php echo $row->baby!=''?$row->baby:(($row->PreviousDeliveries>0?'Multi':'First')); ?></td>
                <td>
                <?php 

                    if( $row->collector_status == 'Collector - AT HOSPITAL'){
                        echo 'Closed';

                    }elseif( $row->tran_status == '0' ){
                        echo 'Closed';
                                                
                    }elseif( !empty($row->sched_induction) ){
                        
                        if($row->client_status == 3) echo $this->Commonmodel->client_status_text($row->client_status); 
                        else echo 'Scheduled Induction - '.date('d/m/Y', strtotime($row->sched_induction)).' '.$row->sched_induction_ta;

                    }elseif( !empty($row->sched_ceasar) ){
                        
                        if($row->client_status == 3) echo $this->Commonmodel->client_status_text($row->client_status); 
                        else echo 'Scheduled C-Section - '.date('d/m/Y', strtotime($row->sched_ceasar)).' '.$row->sched_ceasar_to;

                    }else{
                        echo $this->Commonmodel->client_status_text($row->client_status); 
                    }
                ?> 
                </td>
                <td><?php echo $row->collector_status; ?></td>
                <td><?php echo $row->Hospital; ?></td>
                <td><?php echo $row->HospitalRegion; ?></td>
                <td><?php echo stripslashes($row->collector); ?></td>
                <td><?php echo $row->reminder; ?></td>
                <td><a href="booking/open/<?php echo $row->CustomerID; ?>">Open</a></td>
            </tr>
            <?php else: ?>

            <tr>
                <td><?php echo date('d/m/Y',strtotime($dt[0])); ?></td>
                <td><?php echo date('H:i',strtotime($dt[1])); ?></td>
                <td colspan="10" class="text-center"><?php echo $row->tran_type; ?></td>
                <td><?php echo $row->agent_name; ?></td>
            </tr>

            <?php endif; endforeach; ?>
        </tbody>
    </table>

    <div class="row">
        <div class="col-sm-6 justify-content-start">
             <?php echo $showing; ?>
        </div>
        <div class="col-sm-6 d-flex justify-content-end">            
            <?php echo $links; ?>
        </div> 
    </div>

</div>