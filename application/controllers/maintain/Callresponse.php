<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Callresponse extends MY_Controller {

	public function __construct(){

		parent::__construct();

		if( $this->user->level == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/callresponse';		
	}



    /**
     * [create/update user accounts]          
     */
    /**
     * @TODO audit trail
     */

	public function index()
	{

		$this->load->model('Callresponsemodel','callresponse');
		$this->load->library("pagination");

		$params = $this->input->get();

		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data = '';

		$data['filters'] = $params;


		$params = array('call_res_status'=>1);

		//$query_params = $this->build_where($params);
		$query_params = '';
		$params_query = @http_build_query($params);

		$per_page = 15;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 
		$query_params['where']      = $params ;

		$results 				    = $this->callresponse->listing($query_params);

		$p_config["base_url"] 		= base_url() . "maintain/callresponse/?".$params_query;
		$p_config["total_rows"] 	= $results['total_rows'];
		$p_config["per_page"] 		= $per_page;
		$p_config["uri_segment"] 	= 3; 
		$config = $this->Commonmodel->pagination_config($p_config);	 
		$this->pagination->initialize($config);

		$data['results'] 	= $results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$results['total_rows'].' entries';

		$params = $this->input->get();

		if(isset($params['del'])){

			try {

			$set['call_res_status'] =  0 ;
			$set['call_res_updated'] =  date('Y-m-d H:i:s') ;

			$call_response_id = $this->callresponse->update($params['del'],$set);

				if(empty($call_response_id)){

					throw new Exception("Error: Unable to delete/inactive the Call Response");
					
				}

				$this->session->set_flashdata('msg','Call Response successfuly deleted/inactivated.');

				redirect('maintain/callresponse');
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());
				
			}

		}
	
			
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'call-response';
		$this->load->view('template', $this->view_data);
	}



	public function callresponse_form( $call_response_id = ''){

		$this->load->model('Callresponsemodel','callresponse');

		$data = '';		

		if($_POST){

			try {

				$post = $this->input->post();


				if(empty($post['response_name']))

					throw new Exception("Error : Input Required Fields");

				$set['call_res_text']  = @$post['response_name'];;
				$set['script_text']  = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $_REQUEST['script_content'] );
				$set['call_group']  = @$post['group'];				
				$set['call_res_open'] =  @$post['status'];
				$set['reminder'] = @$post['reminder'];
				$set['reminder_popup'] = @$post['reminder_popup'];
				$set['agent_name'] = $this->session->userdata('agent_name');
				$set['user_id'] = $this->session->userdata('user_id');
				$set['call_res_status'] = @$post['is_active'];

				$set['send_sms'] = isset($post['send_sms'])?1:0;
				$set['distance_required'] = isset($post['distance_required'])?1:0;

				if(!empty($post['call_response_id'])){

		
					$call_response_id = $this->callresponse->update($post['call_response_id'],$set);

					if(!$call_response_id) 
						throw new Exception("Error: Failure to Update Call Response");
					

				}else{

					$call_response_id = $this->callresponse->insert($set);

					if(!$call_response_id)
						throw new Exception("Error: Failure to Add Call Response");

				}

				if(!empty($post['call_response_id'])){

					$this->session->set_flashdata('msg','Update Successful');
					redirect('maintain/callresponse/callresponse_form/'.$post['call_response_id']);

				}else{
					
					$this->session->set_flashdata('msg','Adding of Call Response is Successful');
					redirect('maintain/callresponse');
					
				}

					
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());

			}				
		}





		if($call_response_id!=''){

			$where = array();

			$where['where']['call_res_id'] = $call_response_id;

			$data['call_response_row'] = $this->callresponse->row($where);			


		}
		$this->view_data['js_file'] = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/js/pages/call_response.js"></script>'; 
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'call-response';
		$this->view_data['maintain']['view_file'] = 'maintain/callresponse-form';
		$this->load->view('template', $this->view_data);

	}


}
