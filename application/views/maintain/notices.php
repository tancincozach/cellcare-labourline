<h2>Notices</h2>
<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 

if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php endif;?>
<div class="table-responsive">

	<div class="form-group">
		<a  href="maintain/notices/notice_form"  class="btn btn-success btn-sm"><i class="fas fa-plus-circle"></i>&nbsp;New Notice</a>			

		<a  href="maintain/notices/<?php echo isset($filters['view_all'])? '':'?view_all=1'?>"  class="btn btn-primary btn-sm">
		<i class="fas fa-plus-book"></i>&nbsp;<?php echo isset($filters['view_all']) ? 'Show active ONLY':'View ALL';?>

</a><br/>
	
	</div>
		
	<table class="table table-sm table-bordered table-striped  notice_lists" width="100%"> 
			<thead>
				<th class="table-primary">Name</th>
				<th class="table-primary">Content</th>
				<th class="table-primary">Agent Name</th>
				<th class="table-primary">Date Created</th>
				<th class="table-primary">Expiry</th>				
				<th class="table-primary">Date Updated</th>
				<th class="table-primary">Status</th>
				<th class="table-primary">Option</th>
			</thead>
			<tbody>

			<?php if(isset($results_notices)  && !empty($results_notices)):
					foreach( $results_notices as $row):
				?>
				<tr <?php echo (strtotime(date('Y-m-d H:i:s'))>=strtotime($row->expire_date) && $row->expire_date!='0000-00-00 00:00:00' ? 'class="table-danger"':'')?>>
					<td><?php echo @$row->content_title;?></td>
					<td><?php echo @$row->content_html;?></td>
					<td><?php echo @$row->agent_name;?></td>
					<td><?php echo @$row->date_created;?></td>
					<td><?php echo @$row->expire_date;?></td>
					<td><?php echo @$row->date_updated;?></td>
					<td><?php echo ((int)$row->notice_status==1 ? '<span class="badge badge-success">Active</span>':'<span class="badge badge-warning">Inactive</span>');?></td>
					<td>
							<a href="maintain/notices/notice_form/<?php echo @$row->id;?>" class="btn btn-info btn-sm"  title="Edit" /><i class="fas fa-pencil-alt"></i></a>
							<a onclick="return Misc.confirm('Are you sure to delete/inactivate this notice ?');"  title="Delete" href="maintain/notices/notice_form/<?php echo @$row->id;?>/?del=<?php echo @$row->id;?>" class="btn btn-danger btn-sm" /><i class="fa fa-trash" aria-hidden="true"></i></a>
					
					</td>
				</tr>
			<?php 
					endforeach;
				?>
	

				<?php
				else:
			?>
				<tr>
					<td colspan="7"> No Notice(s) Found</td>
				</tr>
			<?php endif;?>
			</tbody>
		</table>	
<br/>
<h2>Collector Notices</h2>
	<table  class="table table-sm table-bordered table-striped notice_lists" width="100%"> 
			<thead>
				<th class="table-primary">Name</th>
				<th class="table-primary">Content</th>
				<th class="table-primary">Agent Name</th>
				<th class="table-primary">Date Created</th>
				<th class="table-primary">Expiry</th>				
				<th class="table-primary">Date Updated</th>
				<th class="table-primary">Status</th>
				<th class="table-primary">Option</th>
			</thead>
			<tbody>

			<?php if(isset($results_collector_notices)  && !empty($results_collector_notices)):
					foreach( $results_collector_notices as $row):
				?>
				<tr <?php echo (strtotime(date('Y-m-d H:i:s'))>=strtotime($row->expire_date) ? 'class="table-danger"':'')?> >
					<td><?php echo @$row->content_title;?></td>
					<td><?php echo @$row->content_html;?></td>
					<td><?php echo @$row->agent_name;?></td>
					<td><?php echo @$row->date_created;?></td>
					<td><?php echo @$row->expire_date;?></td>
					<td><?php echo @$row->date_updated;?></td>
					<td><?php echo ((int)$row->notice_status==1 ? '<span class="badge badge-success">Active</span>':'<span class="badge badge-warning">Inactive</span>');?></td>

					<td>
							<a href="maintain/notices/notice_form/<?php echo @$row->id;?>" class="btn btn-info btn-sm"  title="Edit" /><i class="fas fa-pencil-alt"></i></a>
							<a onclick="return Misc.confirm('Are you sure to delete/inactivate this notice ?');"  title="Delete" href="maintain/notices/notice_form/<?php echo @$row->id;?>/?del=<?php echo @$row->id;?>" class="btn btn-danger btn-sm" /><i class="fa fa-trash" aria-hidden="true"></i></a>
					
					</td>
				</tr>
			<?php 
					endforeach;
				?>
	

				<?php
				else:
			?>
				<tr>
					<td colspan="7"> No Notice(s) Found</td>
				</tr>
			<?php endif;?>
			</tbody>
		</table>
</div>