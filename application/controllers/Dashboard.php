<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){

		parent::__construct();

		unset($this->view_data['maintain']);
	}


	public function index()
	{
		
		$this->view_data['js_file'] = '<script src="assets/js/pages/dashboard.js?"></script>'; 
		$this->load->view('template', $this->view_data);
 

	}

	public function other(){

		$this->load->model('Transactionmodel');  

		 

		try {

			if( $_POST ){

				$post = $this->input->post(); 
				  
	 
	 			$set = array(); 
				$set['CallerName'] 		= $post['CallerName']; 
				$set['CallerPhone'] 	= $post['CallerPhone']; 
				$set['agent_name'] 		= $this->user->name; 
				$set['tran_status']		= 0; 
				$set['tran_type'] 		= $post['issue_type']; 
				$set['tran_updated'] = date('Y-m-d H:i:s');
				
				if( !$this->Transactionmodel->insert($set) ){
					throw new Exception("Error on save", 1);
				}
				$flash_success = array();
				$flash_success[] = $post['issue_type'].' Successfully submitted.';
				$this->session->set_flashdata('flash_success', implode('|',$flash_success));	
				redirect('dashboard');

			}

			$data = '';

			$this->view_data['data'] = $data; 
			$this->view_data['menu_active'] = 'other'; 
			$this->view_data['view_file'] = 'pages/other-callingforanotherreason';
			$this->load->vars($this->view_data);

			$this->load->view('template');

		} catch (Exception $e) {
			
			echo $e->getMessage(); 
		}

	 
	}

	public function call_register(){

		$this->load->model('Transactionmodel');

		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data['filters'] = $params;
 
		 
		//$query_params = $this->build_where($params);

		$query_params = '';
		
		if( $_GET and isset($_GET['search']) )	{
			$search = trim($params['search']);

			$where_str = " ( tran_type LIKE '%$search%' ) ";
			 
			$query_params['where_str'] = $where_str;
		}

		$per_page = 50;
		$query_params['where']['tran_type !='] = 'Booking';
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;  
		$appt_results 				= $this->Transactionmodel->listing_basic($query_params);

		//echo $this->db->last_query();

		 
		$params_query = @http_build_query($params);

        $p_config["base_url"] 		= base_url() . "call_register/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		$this->view_data['data'] = $data; 
		$this->view_data['view_file'] = 'pages/call_register';
		$this->view_data['menu_active'] = 'call_register'; 	
		$this->view_data['js_file'] = '<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>'; 
		$this->view_data['js_file'] .= ' <script src="assets/vendor/mcautocomplete/mcautocomplete.js"></script>'; 		
		$this->view_data['js_file'] .= ' <script src="assets/js/pages/ac_booking_search_customer.js?v='.JS_V_bookingCA.'"></script>';

		$this->load->vars($this->view_data);

		$this->load->view('template');

	}

	public function reminders(){

		$this->load->model('Callsmodel');

		$this->load->library("pagination");

		$data = '';

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);
 
  
		$per_page = 20; 
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page;  
		$appt_results = $this->Callsmodel->get_all_reminders($query_params);

		//echo $this->db->last_query();

		 
		$params_query = @http_build_query($params);

        $p_config["base_url"] 		= base_url() . "reminders/?".$params_query;
        $p_config["total_rows"] 	= $appt_results['total_rows'];
        $p_config["per_page"] 		= $per_page;
        $p_config["uri_segment"] 	= 3; 
        $config = $this->Commonmodel->pagination_config($p_config);	 
        $this->pagination->initialize($config);

        $data['results'] 	= $appt_results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$appt_results['total_rows'].' entries';


		$this->view_data['data'] = $data; 
		$this->view_data['view_file'] = 'pages/reminders';
		$this->view_data['menu_active'] = 'reminders'; 	
		//$this->view_data['js_file'] = '<script src="assets/js/pages/booking.js?v='.JS_V_booking.'"></script>'; 
		//$this->view_data['js_file'] .= ' <script src="assets/vendor/mcautocomplete/mcautocomplete.js"></script>'; 		
		//$this->view_data['js_file'] .= ' <script src="assets/js/pages/ac_booking_search_customer.js?v='.JS_V_bookingCA.'"></script>';

		$this->load->vars($this->view_data);

		$this->load->view('template');

	}
}
