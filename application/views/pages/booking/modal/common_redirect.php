<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo $title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
 
            <div class="modal-body text-center">
                 
                <h4><?php echo $header1; ?></h4>

                <p><?php echo stripslashes($body); ?></p>

            </div>
            <div class="modal-footer">              
                <a class="btn btn-sm btn-primary" href="<?php echo $redirect_url; ?>"><?php echo $redirect_btn_txt; ?></a>
            </div>
           
        </div>
    </div>
</div>