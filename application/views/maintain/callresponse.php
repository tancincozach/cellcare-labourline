<h2>Call Response</h2>
<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php endif;?>
<div class="table-responsive">

	<div class="form-group">
		<a  href="maintain/callresponse/callresponse_form"  class="btn btn-success btn-sm"><i class="fas fa-plus-circle"></i>&nbsp;New Call Response</a>		
	</div>
		
	<table class="table table-sm table-bordered table-striped">
			<thead>
				<th class="table-primary">Name</th>
				<th class="table-primary">Group</th>
				<th class="table-primary">Reminder</th>
				<th class="table-primary">Reminder POPUP</th>
				<th class="table-primary">Open/Closed</th>				
				<th class="table-primary">Send SMS <br />(Only works on Collector GROUP)</th>				
				<th class="table-primary">Required Travel Time (Collector)</th>				
				<th class="table-primary">Option</th>
			</thead>
			<tbody>

			<?php if(isset($results)  && !empty($results)):
					foreach( $results as $row):
				?>
				<tr>
					<td><?php echo @$row->call_res_text;?></td>
					<td><?php echo @$row->call_group;?></td>
					<td><?php echo @$row->reminder?></td>
					<td><?php echo (@$row->reminder_popup)?'<strong class="text-success">Yes</strong>':'No'; ?></td>
					<!-- <td><?php echo isset($row->call_res_open) && intval($row->call_res_open)==1 ? '<strong class="text-success">Open</strong>':'Closed';?></td> -->
					<td><?php echo $this->statuses[$row->call_res_open]; ?></td>
					<td><?php echo isset($row->send_sms) && intval($row->send_sms)==1 ? '<strong class="text-success">Yes</strong>':'No';?></td>
					<td><?php echo isset($row->distance_required) && intval($row->distance_required)==1 ? '<strong class="text-success">Yes</strong>':'No';?></td>
					<td>
					<a href="maintain/callresponse/callresponse_form/<?php echo @$row->call_res_id;?>" class="btn btn-info btn-sm" /><i class="fas fa-pencil-alt"></i>&nbsp;Edit</a>
					<a  onclick="return Misc.confirm('Are you sure to delete/inactivate this call response ?');" href="maintain/callresponse/?del=<?php echo @$row->call_res_id;?>" class="btn btn-danger btn-sm" /><i class="fas fa-user-times"></i>&nbsp;Delete</a>
					
					</td>
				</tr>
			<?php 
					endforeach;
					?>
						<tr>
							<td colspan="4">
							<?php echo $links;?>
							<?php echo $showing;?>
							</td>
						</tr>
					<?php
				else:
			?>
				<tr>
					<td colspan="4"> No Call Response(s) Found</td>
				</tr>
			<?php endif;?>
			</tbody>
		</table>
</div>