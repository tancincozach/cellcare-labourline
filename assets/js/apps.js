var Misc = (function () {

  	// locally scoped Object
	var myObject = {};

	// declared with `var`, must be "private"
	var privateMethod = function () {};


	myObject.confirm = function (msg) {

		if( confirm(msg) )
			return true;

		return false;
	};

	myObject.search = function(){

	}

	myObject.basicmodal = function(title, body){

		html = '<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel" aria-hidden="true">'
		+'  <div class="modal-dialog" role="document">'
		+'    <div class="modal-content">'
		+'      <div class="modal-header">'
		+'        <h5 class="modal-title">'+title+'</h5>'
		+'        <button type="button" class="close" data-dismiss="modal" aria-label="Close">'
		+'          <span aria-hidden="true">&times;</span>'
		+'        </button>'
		+'      </div>'
		+'      <div class="modal-body">'
		+'        '+body
		+'      </div>'
		+'      <div class="modal-footer">'
		+'        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> '
		+'      </div>'
		+'    </div>'
		+'  </div>'
		+'</div>';

	  	modal = $(html).modal({keyboard:false, backdrop:false});
	  	modal.on('hidden.bs.modal', function (e) {
		  	modal.modal('dispose');	
		  	$('.modal').remove(); 

		}); 
	}




  	return myObject;

})();


var MyNote = (function () {

  	// locally scoped Object
	var myObject = {};

	// declared with `var`, must be "private"
	var privateMethod = function () {};


	myObject.show = function () {
  

		//$("#mystickynote").sticky({topSpacing:0});

		//console.log(sticky);

		return false;
	};

  	return myObject;

})();



var StickyNotes = {
	ck_name: cookie_name+'-note',
	cls: 'mynote',
	_init: function() {

  		var coo = this._parse();
  	  
		var div = '<div class="'+this.cls+' '+coo.s+'">'
			+'<a href="javascript:void(0)" onclick="StickyNotes.activate(\'\')" title="Hide Note" class="close-mynote"><i class="fas fa-chevron-right"></i></a>'
			+'<form>'
			+'<strong>My Note</strong>'
			+'<textarea rows="10" id="mynote-input" onkeyup="StickyNotes.onWriteText(this)" >'+coo.n+'</textarea>'
			+'</form>'
			+ '</div>';

		$('#mystickynote').html('');
		$('#mystickynote').append(div);
	},

	_parse: function(){
		
		var coo = $.cookie(this.ck_name); 

	 	coo = JSON.parse(coo);

	 	return coo;
	},

	activate: function(show) {

		var coo = this._parse();
	 	
		var arr_data = {};
		arr_data["s"] = show;
		arr_data["n"] = coo.n;

		arr_data = JSON.stringify(arr_data);

		$.cookie(this.ck_name, arr_data, { path: '/',  json: true });

		$('.'+this.cls).toggleClass("activate");
	},

	onWriteText: function(field) {

		var coo = this._parse();

		var noteTxt = $(field).val();
 
		var arr_data = {};
		arr_data["s"] = coo.s;
		arr_data["n"] = noteTxt;

		arr_data = JSON.stringify(arr_data);

		$.cookie(this.ck_name, arr_data, { path: '/' });

	}

} 


$(function(){

	//StickyNotes._init();

});