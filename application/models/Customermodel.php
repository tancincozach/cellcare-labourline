<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customermodel extends CI_Model { 


    function insert($set){

        $this->db->db_debug = FALSE;

        try {

            
            if( empty($set) ) throw new Exception("Empty insert data");

            
            $cust_id = $this->_add($set);

            if( $cust_id > 0 ){

                $this->db->db_debug = TRUE;

                return $cust_id;
            }else{
                            
                    $this->_add($set);
            
            }
            

        } catch (Exception $e) {
            return false;
        }

    }

     function _add($set){

        if( $this->db->insert('customers', $set) ){             

            return $this->db->insert_id();
        }else{
            return false;
        }

    }

  	
    function update($cust_id, $set){

        $cust_id = trim($cust_id);

        try {
            
            if( empty($cust_id) ) throw new Exception("cust_id is required", 1);
            if( empty($set) ) throw new Exception("set param is required", 1);
                        
            $this->db->where('CustomerID', $cust_id);
            return $this->db->update('customers', $set);

        } catch (Exception $e) {
            return false;
        }

    }

      function delete($cust_id){

        $cust_id = trim($cust_id);

        try {
            
            if( empty($cust_id) ) throw new Exception("cust_id is required", 1);
                        
            $this->db->where('CustomerID', $cust_id);
            return  $this->db->delete('customers');

        } catch (Exception $e) {
            return false;
        }

    }



  	function row($params){

  		try {
  			
 			if( empty($params) ) throw new Exception("Params is required", 1);

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

  			$query = $this->db->get('customers');

  			return $query->row();

  		} catch (Exception $e) {
  			return 0;
  		}
  	}

    /**
     * Get customer details
     * @param  array $params where, where_str
     * @return object customer details
     */
    /**
     * @TODO connect to customer_update table to get the updated hospital
     */
    function details($params){

        try {
            
            if( empty($params) ) throw new Exception("Params is required", 1);

            //where clause
            if(isset($params['where'])){
                $this->db->where($params['where']);
            }       

            if(isset($params['where_str']) && $params['where_str']!='' ){
                $this->db->where($params['where_str'], null, false);
            }

            //$this->db->join('customers_update', 'customers_update.CustomerID=customers.CustomerID', 'LEFT OUTER');
            $customer = $this->db->get('customers'); 
            $customer = $customer->row();

            if( !isset($customer->CustomerID) ) return 0;

            $this->db->where('CustomerID', $customer->CustomerID);
            $update = $this->db->get('customers_update');
            if( $update->num_rows() ){
                $update = $update->row();
                $customer->Hospital         = $update->Hospital;
                $customer->HospitalStreet   = $update->HospitalStreet;
                $customer->HospitalCity     = $update->HospitalCity;
                $customer->HospitalRegion   = $update->HospitalRegion;
                $customer->HospitalPostalCode = $update->HospitalPostalCode;
                $customer->HospitalPhone    = $update->HospitalPhone;
            }

            return $customer;

        } catch (Exception $e) {
            return 0;
        }

    }
  
    /**
     * AJAX  Results for CUSTOMER DATA TABLES
     * @param  array $params where, where_str, select, result
     * @return object/array/false
     */
    function ajax_result_with_pagination($params){

      try {
        
      if( empty($params) ) throw new Exception("Params is required", 1);
      
      $this->db->select('code');
      $this->db->where(array('type'=>'customerfield'));

      $custom_fields_qry = $this->db->get('hashes');

      $result['custom_fields'] = $custom_fields_qry->result();

      $row_col = array();

      foreach ($result['custom_fields'] as $row_column) {

        $row_col[] = $row_column->code;

      }

      //where clause
      if(isset($params['where'])){
        $this->db->where($params['where']);
      }   

      if(isset($params['where_str']) && $params['where_str']!='' ){
        $this->db->where($params['where_str'], null, false);
      }   

      if(isset($params['select']) && $params['select']!='' ){
        $this->db->select($params['select'], null, false);
      }

      if(isset($params['search_value']) && $params['search_value']!=''){

         $i = 0;

          foreach($row_col as $item){
              
              if($params['search_value']){

                  if($i===0){

                      $this->db->group_start();
                      $this->db->like($item, $params['search_value']);
                  }else{
                      $this->db->or_like($item,$params['search_value']);
                  }
                  
                  if(count($row_col) - 1 == $i){
                      $this->db->group_end();
                  }
              }
              $i++;
          }
      }

         
        if(isset($params['order'])){
            $this->db->order_by($row_col[$params['col_index']], $params['col_sort']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }


      //limits
      if(isset($params['limits'])){

          $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
          $query = $this->db->get('customers');

          if(isset($params['result']) AND @$params['result'] == 'array'){
            $result = $query->result_array();
          }else{
            $result = $query->result();
          }
          
      }else{

          $this->db->select("COUNT(*)  as total_records");
          $query = $this->db->get('customers');
          $row = $query->row();
          $result['total_records'] =  $row->total_records;
      }

      return $result;

      } catch (Exception $e) {
        return 0;
      }
    }
  
  	/**
  	 * Basic Results
  	 * @param  array $params where, where_str, select, result
  	 * @return object/array/false
  	 */
  	function basic_results($params){

  		try {
  			
 			if( empty($params) ) throw new Exception("Params is required", 1);

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}		

			if(isset($params['select']) && $params['select']!='' ){
				$this->db->select($params['select'], null, false);
			}

  			$query = $this->db->get('customers');

  			return (isset($params['result']) AND @$params['result'] == 'array')?$query->result_array():$query->result();

  		} catch (Exception $e) {
  			return 0;
  		}
  	}


    /**
     * [update_hospital description]
     * @param  int $CustomerID [description]
     * @param  array $params     [description]
     * @return int             1=sucess, 2=change 0=error
     */
    /**
     * @TODO audit trail
     */
    function update_hospital($CustomerID, $params){

        try {
            
            if( empty($CustomerID) ) throw new Exception("Update Hospital: missing CustomerID", 1);
            
            $this->db->where('CustomerID', $CustomerID);
            $query = $this->db->get('customers');            
            $hospital = $query->row(); 

            if( trim($hospital->Hospital) == trim($params['Hospital'])
                AND trim($hospital->HospitalStreet) == trim($params['HospitalStreet'])
                AND trim($hospital->HospitalCity) == trim($params['HospitalCity'])
                AND trim($hospital->HospitalRegion) == trim($params['HospitalRegion'])
                AND trim($hospital->HospitalPostalCode) == trim($params['HospitalPostalCode'])
                AND trim($hospital->HospitalPhone) == trim($params['HospitalPhone'])
            ){
                return 2;
            }


            $this->db->where('CustomerID', $CustomerID);
            $query = $this->db->get('customers_update');            
            $hospital = $query->row();

            if( count($hospital) > 0 ){

                if( trim($hospital->Hospital) == trim($params['Hospital'])
                    AND trim($hospital->HospitalStreet) == trim($params['HospitalStreet'])
                    AND trim($hospital->HospitalCity) == trim($params['HospitalCity'])
                    AND trim($hospital->HospitalRegion) == trim($params['HospitalRegion'])
                    AND trim($hospital->HospitalPostalCode) == trim($params['HospitalPostalCode'])
                    AND trim($hospital->HospitalPhone) == trim($params['HospitalPhone'])
                ){
                    return 2;
                }
            }



            $set = array();
            $set['Hospital']            = $params['Hospital'];
            $set['HospitalStreet']      = $params['HospitalStreet'];
            $set['HospitalCity']        = $params['HospitalCity'];
            $set['HospitalPostalCode']  = $params['HospitalPostalCode'];
            $set['HospitalRegion']      = $params['HospitalRegion'];
            $set['HospitalPhone']       = $params['HospitalPhone'];
            $set['cust_u_lastupdate']   = date('Y-m-d H:i:s');

            if( $query->num_rows() > 0){
                //update 
                $this->db->where('CustomerID', $CustomerID);
                if( $this->db->update('customers_update', $set) ) return 1;
                else return false;
            }else{
                //insert
                $set['CustomerID']          = $CustomerID;
                $set['cust_u_lastupdate']   = date('Y-m-d H:i:s'); 
                
                if( $this->db->insert('customers_update', $set) ) return 1;
                else return false;
            }


        } catch (Exception $e) {
            return false;
        }


    }

}