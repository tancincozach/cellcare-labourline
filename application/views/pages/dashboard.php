
<?php FLASH_SESSION_MSG(); ?>

<section class="jumbotron text-center col-sm-12 ">
    
    <h1 class="">Welcome to Cell Care Labour Line</h1>
    <p class="" style="font-size: 18px">Please note your calls are recorded for training & quality purposes</p>
    <p class="" style="font-size: 18px">This is ........, How can I help you ?</p>
     
    
    <div class="list-group col-sm-6 m-auto pt-2">
        <a class="list-group-item list-group-item-action mb-1 bg-labourrelated font-weight-bold" href="booking/search/1">Labour Related</a>
        <a class="list-group-item list-group-item-action mb-1 bg-csection font-weight-bold" href="booking/search/CAESAR">Reporting C-Section</a>
        <a class="list-group-item list-group-item-action mb-1 bg-induction font-weight-bold" href="booking/search/INDUCTION">Reporting Induction</a>
        <a class="list-group-item list-group-item-action bg-anotherreason font-weight-bold" href="dashboard/other/" >Calling for another Reason</a>
    </div>

    <div class="row mt-5">
        <div class="col-sm-6 text-center ml-auto mr-auto">
            <button class="btn btn-primary" type="button" onclick="Dashboard.auto_submit('Disconnect on Greeting')">Disconnect on Greeting</button>       
            <button class="btn btn-primary ml-4" type="button" onclick="Dashboard.auto_submit('Wrong Number')">Wrong Number</button>
        </div>        
    </div>
</section>