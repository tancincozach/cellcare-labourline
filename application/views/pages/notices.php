<h2><?php echo $notice_grp==2 ? "Collector ":""?>Notices</h2>
<br/><br/>
<?php

//print_r($params);

?>
<div class="table-responsive">
	<a  href="notices/notice_form/new/?grp=<?php echo  $notice_grp?>"  class="btn btn-success btn-sm">
		<i class="fas fa-plus"></i>&nbsp;&nbsp;Create Notice</a>


	<table class="table table-bordered table-sm">
		<thead>
			<tr class="<?php echo ($notice_grp==1) ? "bg-info":"bg-primary"?>">
				<th  style="color:white">Title</th>
				<th  style="color:white">Content</th>
				<th  style="color:white">Date Created</th>
				<th  style="color:white">Date Expiry</th>
				<th  style="color:white">Status</th>
				<th  style="color:white">Agent</th>
				<th  style="color:white"></th>
			</tr>
		</thead>

		<tbody>
				<?php 
				if(!empty($notices)):
					foreach ($notices as $row) :
					
				?>		
				<tr <?php echo (strtotime(date('Y-m-d H:i:s'))>=strtotime($row->expire_date) && $row->expire_date!='0000-00-00 00:00:00' ? 'class="table-danger"':'')?>>
					<td><?php echo $row->content_title?></td>
					<td><?php echo stripcslashes($row->content_html)?></td>
					<td><?php echo date('Y-m-d H:i:s',strtotime($row->date_created));?></td>
					<td><?php 
					if($row->expire_date!='0000-00-00 00:00:00'){
						echo date('Y-m-d H:i:s',strtotime($row->expire_date));
					}else{
						echo '0000-00-00 00:00:00';
					}
						
					?></td>
					<td><?php echo (int)$row->notice_status==1 ? "Active":"Inactive";?></td>
					<td><?php echo $row->agent_name;?></td>
					<td>
						<a href="notices/notice_form/edit/<?php echo $row->id; ?>/?grp=<?php echo  $notice_grp?>"><i class="fas fa-pencil-alt"></i></a>
					</td>
				</tr>
					<?php
					endforeach;

					?>
					<tr>
						<td colspan="7">
						<?php echo $links;?>
						<?php echo $showing;?>
						</td>
					</tr>
					<?php
				else:
			?>
			<tr>
				<td colspan="6">No Record(s) Found.</td>
			</tr>
			<?php endif;?>
		</tbody>
		
	</table>
</div>
	
