<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  " role="document">
        <div class="modal-content bg-light">
            <div class="modal-header">
                <h5 class="modal-title">Progress Selector</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php 
                echo form_open('booking/save', 'name="MLabourProgress-form" id="MLabourProgress-form" class="col-sm-12" onsubmit="return LabourProgress.modalPSSubmit(this)" '); 
            ?>
                <input type="hidden" name="modal_submit" value="1" >

                <?php 
                    foreach($post_data as $name=>$val){

                        if( is_array($val) ){
                            foreach ($val as $index => $sub_val) {
                                echo '<input type="hidden" name="'.@$name.'[]" value="'.@$sub_val.'" >';
                            }
                        }else{
                            echo '<input type="hidden" name="'.@$name.'" value="'.@$val.'" >';
                        }

                    
                    } 
                ?>

                <div class="modal-body">
                    <table class="table table-sm border text-center">
                        <tr>
                            <th width="25%">Baby</th>
                            <th width="25%">Frequency of Contractions</th>
                            <th width="25%">Length of Contractions</th>
                            <th width="25%">Centimeters Dilated</th>
                        </tr>
                        <tr>
                            <td><?php echo @$post_data['baby']; ?></td>
                            <td><?php echo opt_freq_contraction(@$post_data['freq_contraction']); ?></td>
                            <td><?php echo opt_length_contraction(@$post_data['length_contraction']); ?></td>
                            <td><?php echo opt_cent_dilated(@$post_data['cent_dilated']); ?></td>
                        </tr>
                    </table>

                    <table class="table  table-sm border">
                    <?php foreach($progress as $row): 

                        if( count($progress) == 1) $for_checked = 'checked="checked"';
                    ?>
                        <tr>
                            <td>
                                <input type="radio" name="progress_selected" value="<?php echo $row->id ?>" required="required" <?php echo @$for_checked; ?> >
                            </td>
                            <td class="font-weight-bold">
                                <?php echo $row->client_status; ?> 
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </table>

                </div>
                <div class="mb-5 pb-3">                    
                    <button type="button" class="btn btn-secondary btn-sm float-left" data-dismiss="modal"> <i class="fas fa-arrow-left"></i> Back</button>
                    <button type="submit" class="btn btn-primary btn-sm float-right">Continue <i class="fas fa-arrow-right"></i></button> 
                </div>
                
            
            </form>
        </div>
    </div>
</div>