<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notices extends MY_Controller {

	public function __construct(){

		parent::__construct();


		if( $this->user->level == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/notices';		
	}



    /**
     * [create/update notices]          
     */
    /**
     * @TODO audit trail
     */

	public function index()
	{

		$this->load->model('Noticesmodel','notice');

		$params = $this->input->get();

		$this->load->library("pagination");

		$params = $this->input->get();
		//$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data = '';

		$data['filters'] = $params;



		if(isset($params['view_all'])){
			$params['notice_status'] = 1;	
		}

		unset($params['view_all']);
		
		$params['notice_group'] = 1;

		$query_params = '';
		$params_query = @http_build_query($params);

		$per_page = 15;

		$query_params['sorting']['order'] = 'DESC';
		$query_params['sorting']['sort']  = 'date_created';
		$query_params['where'] = $params;

	

		$data['results_notices']  = $this->notice->listing($query_params,0);

		$params['notice_group'] = 2;

		$query_params = '';
		$params_query = @http_build_query($params);

		$per_page = 15;

		$query_params['sorting']['order'] = 'DESC';
		$query_params['sorting']['sort'] = 'date_created';
		$query_params['where'] = $params;


		$data['results_collector_notices'] 	= $this->notice->listing($query_params,0);		 

	
		$this->view_data['css_file'] = '<link rel="stylesheet" type="text/css" href="assets/vendor/datatables/css/dataTables.bootstrap4.css"/>'; 
		$this->view_data['js_file']  = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>';
		$this->view_data['js_file'] .= '<script src="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.full.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/vendor/datatables/js/jquery.dataTables.min.js"></script>'; 		
		$this->view_data['js_file'] .= '<script src="assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/js/pages/notices.js"></script>'; 
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'notices';

		$this->load->view('template', $this->view_data);
	}



	public function notice_form( $notice_id = ''){

		$this->load->model('Noticesmodel','notice');

		$data = '';		

		$params = $this->input->get();

		if(isset($params['del'])){

			try {

				$set = array();


				if(!$this->notice->delete($params['del'])){

					throw new Exception("Error: Unable to Delete this notice");
					
				}

				$this->session->set_flashdata('msg','Notice successfuly deleted.');

				redirect('maintain/notices');
				
			} catch (Exception $e) {

				$this->session->set_flashdata('error',$e->getMessage());
				
				redirect('maintain/notices');
			}

		}


		if($_POST){

			try {

				$post = $this->input->post();


				if(
						empty($post['response_name']) ||
						empty($post['expiry_date']) ||
						empty($post['script_content'])
					)

					throw new Exception("Error : Input Required Fields");

	
					$set['content_title']  = @$post['response_name'];
					$set['content_html']  = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $_REQUEST['script_content'] );				
					$set['expire_date']  = isset($post['expiry_date_hidden'])  && $post['expiry_date_hidden']!="" ?  format_datetime($post['expiry_date_hidden']):"0000-00-00 00:00:00";						
					$set['agent_name'] = $this->session->userdata('agent_name');
					$set['user_id'] = $this->session->userdata('user_id');
					$set['notice_group'] = isset($post['notice_group']) ? $post['notice_group']:1;
					$set['notice_status'] =  @$post['status'];



				if(!empty($post['notice_id'])){

					$set['date_updated']  =  date('Y-m-d H:i:s');
					$notice_id = $this->notice->update($post['notice_id'],$set);

					if(!$notice_id) 
						throw new Exception("Error: Failure to Update a Notice");
					

				}else{

					$notice_id = $this->notice->insert($set);

					if(!$notice_id)
						throw new Exception("Error: Failure to Add a Notice");

				}

				if(!empty($post['notice_id'])){

					$this->session->set_flashdata('msg','Update Successful');
					redirect('maintain/notices/notice_form/'.$post['notice_id']);

				}else{
					
					$this->session->set_flashdata('msg','Adding of Notice is Successful');
					redirect('maintain/notices');
					
				}

					
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());

			}				
		}





		if($notice_id!=''){

			$where = array();

			$where['where']['id'] = $notice_id;

			$data['notice_row'] = $this->notice->row($where);			


		}
			
		$this->view_data['css_file'] = '<link href="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">'; 
		$this->view_data['js_file']  = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/vendor/jquery/datetimepicker/jquery.datetimepicker.full.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/js/pages/notices.js"></script>'; 
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'notices';
		$this->view_data['maintain']['view_file'] = 'maintain/notice-form';
		$this->load->view('template', $this->view_data);

	}


}
