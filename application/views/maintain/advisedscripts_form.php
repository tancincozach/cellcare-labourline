<h1>Advised Scripts</h1>


<?php
 
	FLASH_SESSION_MSG();

	echo form_open('',' class="form-horizontal" method="POST" onsubmit="return confirm(\'Confirm Submit\')" ');
?>
	<input type="hidden" name="id" value="<?php echo @$row->id;?>">
	<input type="hidden" name="form_type" value="<?php echo @$form_type;?>">

	<div class="row">
		<div class="col-md-8">

			<div class="table-responsive">
				
				<table class="table">
					<tbody>
						<tr>
							<td>Type</td>
							<td>
								<?php
									$script_type = explode(' / ', @$row->type); 
								?>

								<div class="custom-control custom-checkbox">
								  <input type="checkbox" class="custom-control-input" id="customCheck1" name="script_type[]" value="FIRST" <?php echo in_array('FIRST', $script_type)?'checked="checked"':''; ?>   >
								  <label class="custom-control-label" for="customCheck1">FIRST</label>
								</div>
								<div class="custom-control custom-checkbox">
								  <input type="checkbox" class="custom-control-input" id="customCheck2" name="script_type[]" value="MULTI" <?php echo in_array('MULTI', $script_type)?'checked="checked"':''; ?>   >
								  <label class="custom-control-label" for="customCheck2">MULTI</label>
								</div>

							</td> 
						<tr>
							<td>Script</td>
							<td><input type="text" name="script_text" class="form-control form-control-sm" value="<?php echo @$row->script_text;?>" required="" /></td>
						</tr>
						
						<tr>
							<td></td>
							<td>
								<button type="submit" class="btn btn-info">Submit</button>
								<a href="maintain/others/advisedscripts" class="btn btn-primary">Cancel</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>
	</div>

 
</form>