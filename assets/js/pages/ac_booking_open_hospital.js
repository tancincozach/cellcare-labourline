



$(function(){

	//AUTOCOMPLETE search hospital
	$( "#auto-hospital-search-box" ).mcautocomplete({
	    // These next two options are what this plugin adds to the autocomplete widget.
	    showHeader: true,
	    columns: [
	    {
	        name: 'Hospital',
	        width: '40%',
	        valueField: 'Hospital'
	    },
	    {
	        name: ' Address',
	        width: '60%',
	        valueField: 'Address'
	    }],

	    // Event handler for when a list item is selected.
	    select: function(event, ui) { 

	        var form =  $('#Hospital-form');

	        $('#auto-hospital-search-box').val(ui.item.Hospital);

	        form.find('input[name="Hospital"]').val(ui.item.Hospital);
	        form.find('input[name="HospitalStreet"]').val(ui.item.HospitalStreet);
	        form.find('input[name="HospitalCity"]').val(ui.item.HospitalCity);
	        form.find('input[name="HospitalRegion"]').val(ui.item.HospitalRegion);
	        form.find('input[name="HospitalPostalCode"]').val(ui.item.HospitalPostalCode);
	        form.find('input[name="HospitalPhone"]').val(ui.item.HospitalPhone);

	        if( ui.item.Hospital == '' ){
	        	form.find('input[name="Hospital"]').focus();
	        }


	        return false;
	    },
	    focus : function(event, ui) {
			var me = $(this);

	        return false;
	    },
	    // The rest of the options are for configuring the ajax webservice call.
	    minLength: 2,
	    source: function(request, response) {
	        $.ajax({
	            url: 'ajax/search_hospital/',
	            dataType: 'json',
	            data: {
	                featureClass: 'P',
	                style: 'full',
	                maxRows: 12,
	                name_startsWith: request.term
	            },
	            // The success event handler will display "No match found" if no items are returned.
	            success: function(data) {
	            	 
	                var result;
	                
	                if (!data || data.length === 0 ) {
	                    result = [
	                        {Hospital:'', Address:'No match found. Select here and manually type the hospital details below '}
                        ];
	                }
	                else {
	                    result = data;
	                }
	                response(result);
	            }
	        });
	    }
	});	


});