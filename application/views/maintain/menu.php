

<div class="list-group">
	<a href="maintain/generalsettings" class="p-1 list-group-item  <?php echo (@$menu_active=='generalsetting')?'active':''; ?>">General Settings</a>
	<a href="maintain/contacts" class="p-1 list-group-item  <?php echo (@$menu_active=='contacts')?'active':''; ?>">Contacts</a>
	<a href="maintain/cmasettings" class="p-1 list-group-item  <?php echo (@$menu_active=='cma_settings')?'active':''; ?>">CMA Settings</a>
	<a href="maintain/callresponse" class="p-1 list-group-item  <?php echo (@$menu_active=='call-response')?'active':''; ?>">Call Response</a>
	<a href="maintain/users" class="p-1 list-group-item  <?php echo (@$menu_active=='user')?'active':''; ?>">Users</a> 
	<a href="maintain/customers" class="p-1 list-group-item  <?php echo (@$menu_active=='customers')?'active':''; ?>">Customer</a> 
	<a href="maintain/hospitals" class="p-1 list-group-item  <?php echo (@$menu_active=='hospitals')?'active':''; ?>"> Hospital</a> 
	<a href="maintain/notices" class="p-1 list-group-item  <?php echo (@$menu_active=='notices')?'active':''; ?>"> Notices</a> 
	<a class="p-1 list-group-item" ><h5 class="mb-0">List</h5></a>
	<a href="maintain/others/advisedscripts" class="p-1 list-group-item  <?php echo (@$menu_active=='advisescripts')?'active':''; ?>"> Advised Scripts</a> 
	<a href="maintain/others/on_call_details" class="p-1 list-group-item  <?php echo (@$menu_active=='on_call_details')?'active':''; ?>">On Call Details</a> 
	<a href="maintain/others/collectors" class="p-1 list-group-item  <?php echo (@$menu_active=='collector_details')?'active':''; ?>">Collector Details</a> 
	<a href="maintain/others/remindertype" class="p-1 list-group-item  <?php echo (@$menu_active=='remindertype')?'active':''; ?>">Reminder Types</a> 
</div>