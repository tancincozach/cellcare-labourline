<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Callsmodel extends CI_Model { 

    function insert($set){

        try {
            
            if( empty($set) ) throw new Exception("Empty insert data", 1);
            if( empty($set['tran_id']) ) throw new Exception("Client is required", 1);
            
            if( $this->db->insert('calls', $set) ){
                return $this->db->insert_id();

            }else{
                return false;
            }
            

        } catch (Exception $e) {
            return false;
        }

    }
  

    function update($call_id, $set){

        $call_id = trim($call_id);

        try {
            
            if( empty($call_id) ) throw new Exception("call_id is required", 1);
            if( empty($set) ) throw new Exception("set param is required", 1);
                        
            $this->db->where('call_id', $call_id);
            return $this->db->update('calls', $set);

        } catch (Exception $e) {
            return false;
        }

    }

    /**
     * Basic Results
     * @param  array $params where, where_str, select, result
     * @return object/array/false
     */
    function basic_results($params){

        try {

            if( empty($params) ) throw new Exception("Params is required", 1);

            //where clause
            if(isset($params['where'])){
                $this->db->where($params['where']);
            }   

            if(isset($params['where_str']) && $params['where_str']!='' ){
                $this->db->where($params['where_str'], null, false);
            }   

            // if(isset($params['select']) && $params['select']!='' ){
            //     $this->db->select($params['select'], null, false);
            // }

            $this->db->order_by('call_id', 'DESC');
            $this->db->select('calls.*, call_responses.call_res_text'.(isset($params['state'])?convert_tz($params['state']):''), null, false);
            $this->db->join('call_responses', 'call_responses.call_res_id=calls.call_res_id', 'LEFT OUTER');
            $query = $this->db->get('calls');

            return (isset($params['result']) AND @$params['result'] == 'array')?$query->result_array():$query->result();

        } catch (Exception $e) {
            return 0;
        }
    }


    function get_prev_clientrelation_calls($tran_id){

        try {
            
            $this->db->select('caller_name ,caller_phone');
            
            $this->db->where('tran_id', $tran_id);
            $this->db->where('ClientRelation', 'CLIENT RELATION');
            $this->db->where('call_type', 'caller');
            $this->db->group_by('caller_name, caller_phone');
            $query = $this->db->get('calls');

            return $query->result();


        } catch (Exception $e) {
            
        }

    }

    function get_cellcare_oncall_details(){

        try {
            
            $this->db->select('name ,phone, notes');
            $query = $this->db->get('list_oncalldetails');

            return $query->result();


        } catch (Exception $e) {
            
        }       

    }
 
    /**
     * Check if sms has been sent already to tran_id and caller phone
     * @param   $tran_id 
     * @param   $to      
     * @return  1 doesnt sent yet otherwise 0        
     */
    function check_sms_sent($tran_id, $to){


        try {
            
            $this->db->where('audit_to', trim($to));
            $this->db->where('tran_id', $tran_id);
            $this->db->where('audit_type', 'sms');
            $query = $this->db->get('calls_audit');
            //echo $this->db->last_query(); exit;
            if( $query->num_rows() > 0 ) return 0;
            else return 1;

        } catch (Exception $e) {
            
        }
    }


    function get_all_reminders($params){

        $this->db->select('count(*) as total');
        $this->db->where('call_type', 'reminder');
        $this->db->join('calls_audit', 'calls_audit.call_id = calls.call_id', 'LEFT OUTER');
        $query = $this->db->get('calls');
        $total_rows = $query->row()->total;
        $query->free_result(); //free results



        $this->db->select('more_text, audit_status');
        $this->db->where('call_type', 'reminder');
        $this->db->order_by('calls.call_id', 'desc');
        if(isset($params['limits'])){
            $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
        }
        $this->db->join('calls_audit', 'calls_audit.call_id = calls.call_id', 'LEFT OUTER');
        $query = $this->db->get('calls');
        $result = $query->result();
        $query->free_result(); //free results

        return array('results'=>$result, 'total_rows'=>$total_rows);


    }

    function get_audit($call_id){

        try {
            
            $this->db->where('call_id', $call_id);
            $query = $this->db->get('calls_audit');

            return $query->result();

        } catch (Exception $e) {
            
        }

    }

}