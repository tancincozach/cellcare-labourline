<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Collection Kit: Call On Call for advise</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php 


                echo form_open('booking/save', 'name="CaesarInduction-form" id="CaesarInduction-form" class="col-sm-12" onsubmit="return onsubmit="return Booking.confirm()" '); ?>

                <input type="hidden" name="section" value="lp_Q">
                <input type="hidden" name="qType" value="Caesar_Induction">            
                <input type="hidden" name="qVal" value="1">    
                <input type="hidden" name="CustomerID" value="<?php echo @$CustomerID; ?>">            
                <input type="hidden" name="tran_id" value="<?php echo @$tran_id; ?>"> 
                <div class="modal-body">
                    <p><?php echo $content; ?></p>
                </div>
                <div class="modal-footer">                    
                    <button type="submit" class="btn btn-primary"><?php echo $button_text; ?></button> 
                </div>
            </form>
        </div>
    </div>
</div>