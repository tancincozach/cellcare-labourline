<h1>On Call Details</h1>


<?php
 
	FLASH_SESSION_MSG();

	echo form_open('',' class="form-horizontal" method="POST" onsubmit="return confirm(\'Confirm Submit\')" ');
?>
	<input type="hidden" name="id" value="<?php echo @$row->id;?>">
	<input type="hidden" name="form_type" value="<?php echo @$form_type;?>">

	<div class="row">
		<div class="col-md-8">

			<div class="table-responsive">
				
				<table class="table">
					<tbody>
						<tr>
							<td>Name</td>
							<td>								
							 <input type="text" name="name" value="<?php echo @$row->name;?>" class="form-control form-control-sm">
							</td> 
						</tr>
						<tr>
							<td>Phone</td>
							<td>								
							 <input type="text" name="phone" value="<?php echo @$row->phone;?>" class="form-control form-control-sm">
							</td> 
						</tr>
						<tr>
							<td>Notes</td>
							<td><textarea class="form-control" name="script_text" id="script_text"><?php echo @$row->notes;?></textarea></td>
						</tr>
						
						<tr>
							<td></td>
							<td>
								<button type="submit" class="btn btn-info">Submit</button>
								<a href="maintain/others/on_call_details" class="btn btn-primary">Cancel</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>
	</div>

 
</form>