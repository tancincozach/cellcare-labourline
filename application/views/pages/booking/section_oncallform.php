

<?php if( $customer->CellCareCollector == 'No' AND $transaction->CellCareCollector_verify=='0' AND 
			trim($customer->ObsHistoryNotes) == '' AND $transaction->ObsHistoryNotes_verify=='' ): ?>
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">
				Cell Care Collector is marked as NO & Obstetric Notes are blank - <br />
				<strong>On-Call Nurse needs to be contacted</strong>
			</h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">
		    	<h5>Advise Caller</h5>
		    	<p>We do not have any obstetric notes &  Cell Care Collector is no,  I will need to contact the On-Call Nurse</p>
		    	<p>The On-Call Nurse will contact you shortly</p>

		    	<br />
		    	 

		    	<p><strong>THEN</strong> Call the On-Call Nurse and follow their instructions</p>
	        </div>
				       
		</div>

	</div>
</div>
<?php  elseif( $customer->CellCareCollector == 'No' AND $transaction->CellCareCollector_verify=='0'  ): ?>
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">Cell Care Collector is marked as "NO" </h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">
		    	<h5>ADVISE CALLER</h5>
		    	<p>As the Cell Collector is listed as doctor or hospital, I will need to contact</p>
		    	<p>Our On-Call Nurse for advise.  Someone will call you back shortly</p>

		    	<br />
		    	 

		    	<p><strong>THEN</strong> Call the On-Call Nurse and follow their instructions</p>
	        </div>
				       
		</div>

	</div>
</div>
<?php  

	//elseif( $customer->CellCareCollector=='Yes' AND trim($customer->ObsHistoryNotes) == '' AND $transaction->ObsHistoryNotes_verify==''  ): 
	elseif(  trim($customer->ObsHistoryNotes) == '' AND $transaction->ObsHistoryNotes_verify==''  ): 
?>
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">Obstetric Notes are blank - On-Call Nurse needs to be contacted</h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">
		    	<h5>Advise Caller</h5>
		    	<!-- <p>We do not have any obstetric notes so I will need to contact</p> -->
		    	<p>We do not have any obstetric notes so I will need to contact the On-Call Nurse</p>
		    	<p>The On-Call Nurse will contact you shortly</p>

		    	<br />
		    	 

		    	<p><strong>THEN</strong> Call the On-Call Nurse and follow their instructions</p>
	        </div>
				       
		</div>

	</div>
</div>
<?php endif; ?>

<?php if( isset($show_next_form) AND $show_next_form =='Despatch Collector and Call On-Call Nurse'  ): ?>
<!-- You need to the On-Call Nurse and advise the situation| Collector - Despatch -->
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">Call On-Call Nurse </h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">		    	 
		    	<p>You need to call the On-Call Nurse</p>
	        </div>
				       
		</div>

	</div>
</div>
<?php endif; ?>

<?php if( isset($show_next_form) AND $show_next_form =='Premature Birth'  ): ?>
<!-- You need to the On-Call Nurse and advise the situation| Collector - Despatch -->
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<!-- <div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">Call On-Call Nurse </h4>
		</div> -->



	<div class="col-sm-12 mb-3 text-center"> 
		<div class="col-sm-9 text-center bg-yellow p-3 mx-auto">
			<h4>Call On-Call Nurse</h4>
			<p class="ml-auto mr-auto font-weight-bold p-3">Call On-Call Nurse and advise we have a "premature birth" - Check if "Despatch" criteria needs changing due to being premature.</p>
		</div>
		<div class="col-sm-9 text-center bg-yellow p-3 mt-1 mx-auto">
			<strong>Advise Caller</strong><br/>
			<p>
				I will need to contact the On-Call Nurse as your birth is premature.
				Please call back IMMEDIATELY if an internal exam is done to check dilation or 
				if any "talk" of a possible C-Section or rapid change in labour progress,
				OTHERWISE call back in 2hrs time.
	    	</p> 
		</div>

	</div>


		<!-- <div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">		    	 
		    	<p class="col-sm-6 ml-auto mr-auto font-weight-bold p-3">Call On-Call Nurse and advise we have a "premature birth" - Check if "Despatch" criteria needs changing due to being premature.</p>
	        </div>
	    	<div class="text-center">		    	 
		    	<p class="col-sm-6 ml-auto mr-auto font-weight-bold p-3">
					I will need to contact the On-Call Nurse as your birth is premature.
					Please call back IMMEDIATELY if an internal exam is done to check dilation or 
					if any "talk" of a possible C-Section or rapid change in labour progress,
					OTHERWISE call back in 2hrs time.
		    	</p>
	        </div>
				       
		</div> -->

	</div>
</div>
<?php endif; ?>


<?php if( @$issue_type == 'CAESAR' AND isset($_GET['warnCAESAR']) ): ?>
<div class="col-sm-12">
	<div class="row border mt-1 ">

		<div class="mx-auto p-2 bg-yellow col-sm-12">
			<h4 class="text-center">CALL On-Call Nurse</h4>
		</div>

		<div class="mx-auto mt-2 p-2 bg-yellow col-sm-12">
	    	<div class="text-center">
		    	 
		    	<p> On-Call Nurse will then call hospital for operation time & call us back</p> 
	        </div>
				       
		</div>

	</div>
</div>
<?php  endif ?>

<div class="col-sm-12 mb-5 box-oncallnurse">
	<div class="row border mt-1 "> 

		<div class="col-sm-12">
			<h4 class="text-center">Call Activity - On-Call Nurse</h4>
		</div>
		
		<div class="col-sm-8 ml-auto mr-auto">
		 	<table class="table table-sm table-borderless bg-secondary text-white text-center">
				<tr>
					<th rowspan="2" class="text-center align-middle">CALLER DETAILS</th>
					<th>NAME</th>
					<th>PHONE</th>
					<th>RELATIONSHIP</th>
				</tr>
				<tr>
					<td><?php echo @$transaction->CallerName; ?></td>
					<td><?php echo @$transaction->CallerPhone; ?></td>
					<td><?php echo @$transaction->ClientRelation; ?></td> 
				</tr>
			</table>
		</div>


		<?php echo form_open('booking/save', 'name="OnCall-form" id="OnCall-form" class="col-sm-12" onsubmit="return OnCall.confirm()" '); ?>
		
	 		<input type="hidden" name="section" value="oncall">
	 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
	 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">	

	 		<input type="hidden" name="issue_type" value="<?php echo (@$issue_type=='')?1:$issue_type; ?>">	
	 		
	 		<input type="hidden" name="submit_continue_reminderPopup" value="0">
	 		
 			<div class="row">
 				<div class="col-4">
 					<table class="table table-sm table-borderless">
 						<tr>
 							<td width="40%"><!-- Caller Type --></td>
 							<td width="60%">
 								<!-- <select name="call_type" required="">
 									<option value="On Call"></option> 									
 								</select> -->
 								&nbsp;
 							</td>
 						</tr>
 						<tr>
 							<td>Name of Caller</td>
 							<td>
 								<input type="text" class="form-control form-control-sm"  name="caller_name" value="<?php echo @$_GET['CallerName'] ?>" >
 							</td>
 						</tr>
 						<tr>
 							<td>Phone</td>
 							<td>
 								<input type="text" class="form-control form-control-sm"  name="caller_phone" value="<?php echo @$_GET['CallerPhone'] ?>" >
 							</td>
 						</tr>
 						<tr>
 							<td>Call Response</td>
 							<td>
 								<select class="custom-select custom-select-sm" name="call_res_id" required="" onchange="OnCall.onCallReponseChange(this)">
								<?php 
									//$attr = 'class="custom-select custom-select-sm"  ';
 									//echo form_dropdown('call_res_id', @$callresponses, '', $attr );

 									foreach ($callresponses as $row): 
 								?>
 									<option value="<?php echo $row->call_res_id; ?>" <?php echo (@$row->reminder_popup == '1')?'data-reminderPopup="1"':''; ?> ><?php echo $row->call_res_text; ?></option>
 								<?php endforeach; ?>
 								</select>
 							</td>
 						</tr>

 						<?php if( trim($customer->CellCareCollector) == 'No' AND empty($transaction->CellCareCollector_verify)  ):  ?>
 							<tr>
 								<td>Cellcare Collector verified</td>
 								<td>
									<select class="custom-select custom-select-sm" name="CellCareCollector_verify" required="">
	 									<option value=""></option> 									
	 									<option value="0">No</option> 									
	 									<option value="1">Yes</option>
	 								</select>
 								</td>
 							</tr> 

 						<?php //elseif( $customer->CellCareCollector=='Yes' AND trim($customer->ObsHistoryNotes) == '' AND $transaction->ObsHistoryNotes_verify=='' ):  ?>
 						<?php endif; 
 							if( trim($customer->ObsHistoryNotes) == '' AND $transaction->ObsHistoryNotes_verify=='' ): 
 						?>
 							<tr>
 								<td>ObsHistoryNotes verified</td>
 								<td>
									<select class="custom-select custom-select-sm" name="ObsHistoryNotes_verify" required="">
	 									<option value=""></option> 									
	 									<option value="0">No</option> 									
	 									<option value="1">Yes</option>
	 								</select>
 								</td>
 							</tr>
 						<?php endif;

 							if( @$show_next_form =='Premature Birth' AND empty($transaction->edd_premature_verify) ):  
 						?>
 							<tr>
 								<td class="font-weight-bold">EDD Premature Birth verified</td>
 								<td>
									<select class="custom-select custom-select-sm" name="edd_premature_verify" required="">
	 									<option value=""></option> 									
	 									<option value="0">No</option> 									
	 									<option value="1">Yes</option>
	 								</select>
 								</td>
 							</tr>
 						<?php endif; ?>


 					</table>
 				</div>
 				<div class="col-3">
 					<p>Notes</p>
 					<textarea class="form-control form-control-sm" name="notes" rows="5" required=""></textarea>
 					<br />
 					 

 					<button class="btn btn-sm btn-primary" type="submit">Submit</button>
 				</div>
 				<div class="col-5">
 					<p class="text-center"><strong>Cell Care On-Call Nurse Details</strong> - Choose applicable on On-Call Nurse</p>
 					<table class="table table-sm">
 					 
  
 						<?php foreach($cellcare_oncall_details as $pcall): 

 							$pcall->phone = trim(@$pcall->phone) != ''?str_pad(@$pcall->phone, 10, "0", STR_PAD_LEFT):'';
 						?>
 						<tr>
 							<td style="width: 145px;"><button type="button" class="btn btn-sm btn-info p-1" onclick="OnCall.select(this)"><i class="fas fa-arrow-left"></i> <?php echo $pcall->name; ?></button></td> 
 							<td><?php echo $pcall->phone; ?></td>  
 							<td><?php echo $pcall->notes; ?></td>  
 						</tr> 
 						<?php endforeach; ?>

 					</table>
 				</div>
 			</div> 

		</form>
		 
	</div>
</div>

<div class="foot_content_call_history">
<?php
	if( isset($calls_result)){
    	echo $this->load->view('pages/booking/call_history', array('calls_result'=>$calls_result), TRUE);
	}
?>
</div>