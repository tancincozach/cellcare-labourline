<div class="col-sm-12 mb-5 box-hospital">
	<div class="row border mt-1 p-5">
		<div class="col-sm-8 mx-auto">
			

			<?php echo form_open('booking/save', 'name="Hospital-form" id="Hospital-form" class="" onsubmit="return HospitalForm.confirm(this)" '); ?>


		 		<input type="hidden" name="section" value="hospital">
		 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
		 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">

		 		<input type="hidden" name="orig_confirm_hospital" value="<?php echo @$transaction->confirm_hospital; ?>">

		 		<input type="hidden" name="issue_type" value="<?php echo @$issue_type; ?>"> 

		 		<input type="hidden" name="hide_Hospital" value="<?php echo @$customer->Hospital; ?>">		 		
		 		<input type="hidden" name="hide_HospitalStreet" value="<?php echo @$customer->HospitalStreet; ?>">		 		
		 		<input type="hidden" name="hide_HospitalCity" value="<?php echo @$customer->HospitalCity; ?>">		 		
		 		<input type="hidden" name="hide_HospitalRegion" value="<?php echo @$customer->HospitalRegion; ?>">		 		
		 		<input type="hidden" name="hide_HospitalPostalCode" value="<?php echo @$customer->HospitalPostalCode; ?>">		 		
		 		<input type="hidden" name="hide_HospitalPhone" value="<?php echo @$customer->HospitalPhone; ?>">		 		

				<table class="table table-sm table-borderless" id="table_confirm_hospital">
					<tr>
						<td class="pt-2 bg-light" colspan="2">
							<h4 class="text-center">Can I confirm the hospital you are delivering at</h4>
						</td>
					</tr>
					<tr>
						<td><h5>Hospital Details</h5></td>
						<td>
							<div class="row">
								<div class="col-sm-6">
								    <select class="custom-select custom-select-sm font-weight-bold text-primary input-sm" name="confirm_hospital" onchange="HospitalForm.select(this)" >
								      	<option value="1">Confirmed Yes</option>
								      	<option value="0">No delivering at another hospital</option>
								    </select>

								</div>
								<div class="col-sm-6 text-right">
									 
								</div>
							</div>
						</td>
					</tr> 	
					<tr id="td_search_hospital" style="display:none">
						<td colspan="2"> 
						 	 
							<div class="input-group mb-3">
							  	<div class="input-group-append">
							    	<span class="input-group-text" id="basic-addon2"> <i class="fas fa-search"></i>  &nbsp; Search Hospital</span>
							  	</div> 
							  	<input type="text" class="form-control" id="auto-hospital-search-box" placeholder="" aria-describedby="basic-addon2" value="">
							</div>

						</td>
					</tr> 	
					<tr>
						<td>Hospital</td>
						<td>
							<input type="text" class="form-control" name="Hospital"   placeholder="Hospital" value="<?php echo @$customer->Hospital; ?>">
						</td>
					</tr> 	
					<tr>
						<td>Hospital Street</td>
						<td>
							<input type="text" class="form-control" name="HospitalStreet" placeholder="HospitalStreet" value="<?php echo @$customer->HospitalStreet; ?>">
						</td>
					</tr> 	
					<tr>
						<td>Hospital City</td>
						<td>
							<input type="text" class="form-control" name="HospitalCity" placeholder="HospitalCity" value="<?php echo @$customer->HospitalCity; ?>">
						</td>
					</tr> 	
					<tr>
						<td>Hospital Region</td>
						<td>
							<input type="text" class="form-control" name="HospitalRegion" placeholder="HospitalRegion" value="<?php echo @$customer->HospitalRegion; ?>">
						</td>
					</tr> 	
					<tr>
						<td>Hospital Postcode</td>
						<td>
							<input type="text" class="form-control" name="HospitalPostalCode" placeholder="HospitalPostalCode" value="<?php echo @$customer->HospitalPostalCode; ?>">
						</td>
					</tr> 	
					<tr>
						<td>Hospital Phone</td>
						<td>
							<input type="text" class="form-control" name="HospitalPhone" placeholder="HospitalPhone" value="<?php echo @$customer->HospitalPhone; ?>">
						</td>
					</tr> 	 

				</table>


				<table class="table table-sm table-borderless" id="table_induction_ceasar" style="display: none">
					
					<?php if( $issue_type == 'INDUCTION' ): ?>
						<tr>
							<td class="pt-2 bg-light" colspan="2">
								<h5>INDUCTION Schedule</h5>
							</td>
						</tr>
						<tr >
							<td>Date of Induction</td>
							<td>
								<div class="col-6">
									<div class="row">
										<input type="text" class="form-control form-control-sm" name="sched_induction_d" id="id_sched_d" data-toggle="datetimepicker" data-target="#id_sched_d" value="">
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>Time of Admission</td>
							<td>
								<div class="col-6">
									<div class="row">
										<!-- <input type="text" class="form-control form-control-sm" name="sched_induction_t" id="id_sched_t" data-toggle="datetimepicker" data-target="#id_sched_t" value=""> -->
										<select class="custom-select custom-select-sm" name="sched_induction_ta">
											<?php foreach(opt_date_time() as $t): ?>
												<option value="<?php echo $t; ?>"><?php echo $t; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="pb-5">Induction Type</td>
							<td class="pb-5">
								<div class="col-6">
									<div class="row">
										<div>
										<?php 

											foreach(opt_induction_type() as $key=>$val):

										?>
										<div class="custom-control custom-radio d-block" style="float:none !important">
										    <input type="radio" class="custom-control-input" id="induct_type_id2_<?php echo $key; ?>" value="<?php echo $val; ?>" name="sched_induction_type" >
										    <label class="custom-control-label" for="induct_type_id2_<?php echo $key; ?>"><?php echo $val; ?></label>
										</div> 
										<?php endforeach; ?>
										</div>
									</div>
								</div>
							</td>
						</tr>

						<tr>							 
							<td colspan="2" class="pt-2" >
								<h5>INDUCTION Notes</h5>
								<textarea class="form-control form-control-sm" name="sched_induction_notes" required="" disabled=""></textarea>
							</td>
						</tr> 

						<tr>
							<td >Do you have Collection Kit ?</td>
							<td>
								<div class="row">
									<div class="col-6"> 

										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_collectionkit" type="radio" name="collection_kit" id="lbpcollectionkit_1" required="" value="1" <?php echo @$transaction->collection_kit == '1'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcollectionkit_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_collectionkit" type="radio" name="collection_kit" id="lbpcollectionkit_0" value="0" <?php echo @$transaction->collection_kit == '0'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcollectionkit_0">No</label>
										</div>

								</div>
							</td>
							 
						</tr>

					<?php endif; ?>


					<?php if( $issue_type == 'CAESAR' ): ?> 

						<tr>
							<td class="pt-2 bg-light" colspan="2">
								<h5>C-Section Schedule</h5>
							</td>
						</tr>						
						<tr >
							<td class="pt-2" >Date of C-Section</td>
							<td class="pt-2">
								<div class="col-6">
									<div class="row">
										<input type="text" class="form-control form-control-sm" name="sched_ceasar_d" id="id_sched_d" data-toggle="datetimepicker" data-target="#id_sched_d" value="">
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td class="">Time of Admission</td>
							<td class="">
								<div class="col-6">
									<div class="row">
										<!-- <input type="text" class="form-control form-control-sm" name="sched_ceasar_t" id="id_sched_t" data-toggle="datetimepicker" data-target="#id_sched_t" value=""> -->
										<select class="custom-select custom-select-sm" name="sched_ceasar_ta">
											<?php foreach(opt_date_time() as $t): ?>
												<option value="<?php echo $t; ?>"><?php echo $t; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td >Time of Operation</td>
							<td>
								<div class="col-6">
									<div class="row">
										<!-- <input type="text" class="form-control form-control-sm" name="sched_ceasar_t" id="id_sched_t" data-toggle="datetimepicker" data-target="#id_sched_t" value=""> -->
										<select class="custom-select custom-select-sm" name="sched_ceasar_to">
											<?php foreach(opt_date_time() as $t): ?>
												<option value="<?php echo $t; ?>"><?php echo $t; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</td>
						</tr>

						<tr>							 
							<td colspan="2" class="pt-2 bg-light" >
								<h5>C-Section Notes</h5>
								<textarea class="form-control form-control-sm" name="sched_ceasar_notes" required="" disabled=""></textarea>
							</td>
						</tr> 

						<tr>
							<td>Do you have Collection Kit ?</td>
							<td>
								<div class="row">
									<div class="col-6"> 

										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_collectionkit" type="radio" name="collection_kit" id="lbpcollectionkit_1" value="1" required="" <?php echo @$transaction->collection_kit == '1'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcollectionkit_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_collectionkit" type="radio" name="collection_kit" id="lbpcollectionkit_0" value="0" <?php echo @$transaction->collection_kit == '0'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcollectionkit_0">No</label>
										</div>

								</div>
							</td>
							 
						</tr>

					<?php endif; ?>


					<tr>
						<td colspan="2" class="pb-4"></td> 
					</tr>
				</table>


			    <div class="col-sm-8 mx-auto text-center mt-2">
			    	<button class="btn btn-sm btn-secondary mr-3" id="hospital-btn-back" type="button" onclick="HospitalForm.showhide(1)" style="display: none">Back to Hospital Details</button>
			    	<button class="btn btn-sm btn-primary" type="submit">Submit</button>
			    </div>

			</form>
		</div>		
	</div>
</div>

<div class="foot_content_call_history">
<?php
	if( isset($calls_result)){
    	echo $this->load->view('pages/booking/call_history', array('calls_result'=>$calls_result), TRUE);
	}
?>
</div>