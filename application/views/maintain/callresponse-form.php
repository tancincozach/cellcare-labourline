<h1>Call Response</h1>


<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;

 echo form_open('',' class="form-horizontal" method="POST"');?>
	<input type="hidden" name="call_response_id" value="<?php echo @$call_response_row->call_res_id;?>">

<div class="row">
	<div class="col-md-6">
		<div class="table-responsive">			
			<table class="table table-borderless">
					<tbody>
						<tr>
							<td width="150px">Name</td>
							<td><input type="text" name="response_name" class="form-control form-control-sm" value="<?php echo @$call_response_row->call_res_text;?>"></td>
						</tr>
						<tr>
							<td>Script</td>
							<td><textarea class="form-control" name="script_content" id="script_content"><?php echo @$call_response_row->script_text;?></textarea></td>
						</tr>
						<tr>
							<td>Group</td>
							<td>
								<select class="form-control form-control-sm"  name="group">
									<option value="">DEFAULT</option>				
									<option value="oncall" <?php echo isset($call_response_row->call_group) &&  $call_response_row->call_group =='oncall' ? "SELECTED":"";?>>On Call</option>
									<option value="collector" <?php echo isset($call_response_row->call_group) &&  $call_response_row->call_group=='collector' ? "SELECTED":"";?>>Collector</option>
									<option value="clienthospital" <?php echo isset($call_response_row->call_group) &&  $call_response_row->call_group=='clienthospital' ? "SELECTED":"";?>>Client/Hospital</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Log Status</td>
							<td>
								<select class="form-select form-control form-control-sm" name="status">
									<option value="">SELECT</option>
									<?php foreach($this->statuses as $key=>$status):?>
									<option value="<?php echo $key;?>" <?php echo isset($call_response_row->call_res_open) && (int)$call_response_row->call_res_open==$key ? "SELECTED":"";?>><?php echo $status;?></option>	
									<?php endforeach;?>					
								</select>
							</td>
						</tr>
						<tr>
							<td>Reminder</td>
							<td>
								<select class="form-select form-control form-control-sm" name="reminder">
									<option value="">SELECT</option>
									<option value="5" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==5 ? "SELECTED":"";?>>5</option>
									<option value="10" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==10 ? "SELECTED":"";?>>10</option>
									<option value="30" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==30 ? "SELECTED":"";?>>30</option>
									<option value="60" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==60 ? "SELECTED":"";?>>1 hr</option>
									<option value="90" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==90 ? "SELECTED":"";?>>1.5 hr</option>
									<option value="120" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==120 ? "SELECTED":"";?>>2 hrs</option>
									<option value="150" <?php echo isset($call_response_row->reminder) && (int)$call_response_row->reminder==150 ? "SELECTED":"";?>>2.5 hrs</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Reminder POPUP</td>
							<td>
								<select class="form-select form-control form-control-sm" name="reminder_popup">
									<option value="">SELECT</option>
									<option value="1" <?php echo isset($call_response_row->reminder_popup) && (int)$call_response_row->reminder_popup==1 ? "SELECTED":"";?>>Yes</option>
									<option value="0" <?php echo isset($call_response_row->reminder_popup) && (int)$call_response_row->reminder_popup==0 ? "SELECTED":"";?>>No</option>
								</select>
							</td>
						</tr>						
						<tr>
							<td>Send SMS</td>
							<td> 
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" class="custom-control-input" id="send_smscustomCheck1"  name="send_sms" <?php echo (@$call_response_row->send_sms==1)?'checked':''; ?>>
								  	<label class="custom-control-label" for="send_smscustomCheck1"> Only works on Collector GROUP</label>
								</div>

							</td>
						</tr>
						<tr>
							<td>Required COLLECTOR travel time</td>
							<td> 
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" class="custom-control-input" id="distance_customCheck1"  name="distance_required" <?php echo (@$call_response_row->distance_required==1)?'checked':''; ?>>
								  	<label class="custom-control-label" for="distance_customCheck1"> "What is your travel time to hospital"</label>
								</div>

							</td>
						</tr>
						<tr>
							<td>Status</td>
							<td>
								<select class="form-select form-control form-control-sm" name="is_active">
									<option value="">SELECT</option>
									<option value="1" <?php echo isset($call_response_row->call_res_status) && (int)$call_response_row->call_res_status==1 ? "SELECTED":"";?>>Active</option>
									<option value="0" <?php echo isset($call_response_row->call_res_status) && (int)$call_response_row->call_res_status==0 ? "SELECTED":"";?>>Inactive</option>
								</select>
							</td>
						</tr>
					</tbody>
			</table>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="form-input col-md-4">
		<button type="submit" class="btn btn-info">Submit</button>
		<a href="maintain/callresponse" class="btn btn-primary">Cancel</a>
	</div>
</div>	
</form>