<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Convert dd/mm/yyyy HH:mm to yyyy-mm-dd HH:mm
 * @param  datetime $date 
 * @return datetime       
 */
function format_datetime($str)
{
	if( empty($str) ) return $str;
    
    $xstr = explode(' ', $str);
    $d = $xstr[0];
    $t = (!empty($xstr[1]))?' '.$xstr[1]:'';

    $d = explode('/', $d);

    $dt = $d[2].'-'.$d[1].'-'.$d[0].$t;

    return $dt;
}


function debug_print($a) {

	echo '<pre>';
	if( is_array($a) ) print_r($a);
	else echo $a;
	echo '</pre>';

}

function opt_date_time(){

    $begin = new DateTime("00:30");
    $end   = new DateTime("23:00");

    $interval = DateInterval::createFromDateString('30 min');

    $times    = new DatePeriod($begin, $interval, $end);

    $d = array();
    $d[''] = '';
    $d['AM'] = 'AM';
    $d['PM'] = 'PM';
    $d['UNKNOWN'] = 'UNKNOWN';
    $d['00:00'] = '00:00';
    foreach ($times as $time) {
        $t = $time->add($interval)->format('H:i');
        $d[$t] = $t;

    }

    return $d;

}


//OPTIONS

function opt_val($k){

    $radio_vals = array('0'=>'No', '1'=>'Yes', '2'=>'Unknown');
    return isset($k)?@$radio_vals[$k]:0;
}

function opt_waters_broken($k='-1'){

    $opts = array(
        ""=>"",
        "Yes"=>"Yes",
        "No"=>"No"
    ); 
    return ($k=='-1')?$opts:@$opts[$k];
}

function opt_epidural($k='-1'){

    $opts = array(
        ""=>"",
        "Yes"=>"Yes",
        "No"=>"No"
    );

    return ($k=='-1')?$opts:@$opts[$k];
}

function opt_hormone_started($k='-1'){

    $opts = array(
        ""=>"",
        "Yes"=>"Yes",
        "No"=>"No"
    );

    return ($k=='-1')?$opts:@$opts[$k];
}


function opt_contracting($k='-1'){

    $opts = array(
        ""=>"",
        "1"=>"Yes",
        "0"=>"No"
    );

    return ($k=='-1')?$opts:@$opts[$k];
}

function opt_freq_contraction($k='-1'){

    $opts = array(''=>'', 
        '1'=>'1 Min',
        '2'=>'2 Min',
        '3'=>'3 Min',
        '4'=>'4 Min',
        '5'=>'5 Min',
        '6'=>'More than 5 min',
        'unknown'=>'Unknown' 
    );

    return ($k=='-1')?$opts:@$opts[$k];
}

function opt_length_contraction($k='-1'){

    /*$opts = array(''=>'', 
            'l40'=>'Less than 40 secs',
            'g40'=>'Greater than 40 secs',
            'unknown'=>'Unknown'
        );*/

    $opts = array(''=>'', 
            'l40'=>'Less than 40 seconds',
            'g40'=>'40 seconds or more',
            'unknown'=>'Unknown'
        );

    return ($k=='-1')?$opts:@$opts[$k];
}

function opt_cent_dilated($k='-1'){

    $opts = array(''=>'', 
                //'0'=>'0', 
                '1'=>'1', 
                '2'=>'2', 
                '3'=>'3', 
                '4'=>'4', 
                '5'=>'5', 
                '6'=>'6', 
                '7'=>'7', 
                '8'=>'8', 
                '9'=>'9',                                           
                'full'=>'Fully Dilated', 
                'unknown'=>'Unknown' 
            );

    return ($k=='-1')?$opts:@$opts[$k];
}

function opt_baby($k='-1'){

    $opts = array( ''=>'','First'=>'First', 'Multi'=>'Multi');

    return ($k=='-1')?$opts:@$opts[$k];
}

function convert_tz($state){
    $default = 'Australia/Sydney';

    $timezone = array(
        'VIC'=>'Australia/Sydney',
        'NSW'=>'Australia/Sydney',
        'SA'=>'Australia/Adelaide',
        'WA'=>'Australia/Perth',
        'QLD'=>'Australia/Queensland',
        'ACT'=>'Australia/ACT',
        'NT'=>'Australia/Adelaide',
        'TAS'=>'Australia/Sydney'
    );

   return  " ,CONVERT_TZ(call_created,'Australia/NSW','".(isset($timezone[$state])?$timezone[$state]:$default)."') AS 'cust_timezone' ";

}

function h_get_state_tz($state){
    $default = 'Australia/Sydney';

    $timezone = array(
        'VIC'=>'Australia/Sydney',
        'NSW'=>'Australia/Sydney',
        'SA'=>'Australia/Adelaide',
        'WA'=>'Australia/Perth',
        'QLD'=>'Australia/Queensland',
        'ACT'=>'Australia/ACT',
        'NT'=>'Australia/Adelaide',
        'TAS'=>'Australia/Sydney'
    );
   
    return (isset($timezone[$state])?$timezone[$state]:$default);
}

function h_check_state($state){
    $state = strtoupper(trim($state));
    //echo $state;
    $default = 'NSW';
    $timezone = array(
        'VIC'=>'Australia/Sydney',
        'NSW'=>'Australia/Sydney',
        'SA'=>'Australia/Adelaide',
        'WA'=>'Australia/Perth',
        'QLD'=>'Australia/Queensland',
        'ACT'=>'Australia/ACT',
        'NT'=>'Australia/Adelaide',
        'TAS'=>'Australia/Sydney'
    );

    //print_r($timezone);
    return isset($timezone[$state])?$state:$default;
}


function opt_induction_type(){

    $arr = array(
        'Gel',
        'Tape',
        'Balloon',
        'Catheter',
        'Tablet',
        'Break waters',
        'Hormone drip',
        'Break waters & hormone drip',
        'Unknown'
    );

    return $arr;

}

function opt_hormonstarted_type(){

    $arr = array(
        'Collection',
        'Induction',
        'Waters',
        'Hormone',
        'Epidural',
        'Unknown'
    );

    return $arr;

}

function FLASH_SESSION_MSG(){

    $CI =& get_instance(); 

    if(!isset($CI->session)){
        $CI->load->library('session');
    }

    $msg = $CI->session->flashdata();

    $alert_msg = '';

    if( isset($msg['flash_error']) AND !empty($msg['flash_error']) ){

        $flashes = explode('|', $msg['flash_error']);
         
        $alert_msg .= '<div class="alert alert-danger mb-0 alert-dismissible fade show" role="alert">'.implode('<br />', $flashes).' 
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>';
        
    }

    if( isset($msg['flash_warning']) AND !empty($msg['flash_warning']) ){

        $flashes = explode('|', $msg['flash_warning']);
 
        $alert_msg .= '<div class="alert alert-warning mb-0 alert-dismissible fade show" role="alert">'.implode('<br />', $flashes).'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>';
       
    }

    if( isset($msg['flash_success']) AND !empty($msg['flash_success']) ){

        $flashes = explode('|', $msg['flash_success']);

        $alert_msg .= '<div class="alert alert-success mb-0 alert-dismissible fade show" role="alert">'.implode('<br />', $flashes).'
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>';
    }


    echo '<div class="col-sm-12 mb-1 px-0">'.$alert_msg.'</div>';
}

function h_array_to_string($input) {

    if( is_array($input) ){
        $output = implode(PHP_EOL, array_map(
            function ($v, $k) { return sprintf("%s: %s ", $k, $v); },
            $input,
            array_keys($input)
        ));

    }else{
      $output = $input;  
    }

    return $output;

}