



$(function(){


	//AUTOCOMPLETE customer
	$( "#auto-customer-search-box" ).mcautocomplete({
	    // These next two options are what this plugin adds to the autocomplete widget.
	    showHeader: true,
	    columns: [
	    {
	        name: 'Name',
	        width: '60%',
	        valueField: 'Name'
	    },
	    {
	        name: ' Client ID',
	        width: '40%',
	        valueField: 'CustomerID'
	    }],

	    // Event handler for when a list item is selected.
	    select: function(event, ui) { 
	    	//ui.item 
	          
	       	if( typeof ui.item.CustomerID != 'undefined' && ui.item.CustomerID != 'No match found.'){

				url = window.location.href;
			 	xsegment = '';
			 	
				if( url.match(/INDUCTION/gi) ) { 
					xsegment = "&issue_type=INDUCTION";
				}
			 	
				if( url.match(/CAESAR/gi) ) { 
					xsegment = "&issue_type=CAESAR";
				}

	        	window.location = 'booking/open/'+ui.item.CustomerID+'/?section=caller'+xsegment;
	        }else{
	        	alert('Customer not recognise');
	        }

	        return false;
	    },
	    focus : function(event, ui) {
			var me = $(this);

	        return false;
	    },
	    // The rest of the options are for configuring the ajax webservice call.
	    minLength: 2,
	    source: function(request, response) {
	        $.ajax({
	            url: 'ajax/search_customer/',
	            dataType: 'json',
	            data: {
	                featureClass: 'P',
	                style: 'full',
	                maxRows: 12,
	                name_startsWith: request.term
	            },
	            // The success event handler will display "No match found" if no items are returned.
	            success: function(data) {
	            	 
	                var result;
	                
	                if (!data || data.length === 0 ) {
	                    result = [
	                        {Name:'', CustomerID:'No match found.'}
                        ];
	                }
	                else {
	                    result = data;
	                }
	                response(result);
	            }
	        });
	    }
	});	

 

});