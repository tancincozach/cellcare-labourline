<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public function __construct(){

		parent::__construct();

		if( $this->user->level == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/users';		
	}



    /**
     * [create/update user accounts]          
     */
    /**
     * @TODO audit trail
     */

	public function index()
	{
		$this->load->library("pagination");

		$this->load->model('Usermodel','users');

		$params = $this->input->get();



		$data = '';
		

		$params = $this->input->get();
		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data = '';

		$data['filters'] = $params;


		$params = array('user_status'=>1, 'user_lvl < '=>'99');

		//$query_params = $this->build_where($params);
		$query_params = '';
		$params_query = @http_build_query($params);

		$per_page = 10;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 
		$query_params['select'] = 'id,fullname,username,
									case user_lvl
											WHEN 0 THEN "Operator"
											WHEN 1 THEN "Supervisor"
											WHEN 2 THEN "Support/Teamleader"
											WHEN 3 THEN "CCAdmin"
											WHEN 5 THEN "Client"
									 end
									 as user_type_name ';
		$query_params['where']      = $params ;

		$results 				    = $this->users->listing($query_params);

		$p_config["base_url"] 		= base_url() . "maintain/users/?".$params_query;
		$p_config["total_rows"] 	= $results['total_rows'];
		$p_config["per_page"] 		= $per_page;
		$p_config["uri_segment"] 	= 3; 
		$config = $this->Commonmodel->pagination_config($p_config);	 
		$this->pagination->initialize($config);

		$data['results'] 	= $results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$results['total_rows'].' entries';



		if(isset($params['del'])){

			try {

			$set = array();

			
			$set['user_status'] =  0 ;
			$set['last_updated_date'] =  date('Y-m-d H:i:s') ;
			$set['last_update_by'] = $this->session->userdata('agent_name');

			$user_id = $this->users->update($params['del'],$set);

				if(empty($user_id)){

					throw new Exception("Error: Unable to delete/inactive the user");
					
				}

				$this->session->set_flashdata('msg','User successfuly delete/ inactivated.');

				redirect('maintain/users');
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());
				
			}

		}
	

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'user';
		$this->view_data['js_file'] .= '<script src="assets/js/pages/users.js"></script>'; 
		$this->load->view('template', $this->view_data);
	}



	public function user_form( $user_id = ''){

		$this->load->model('Usermodel','users');

		$data = '';		

		if($_POST){

			try {

				$post = $this->input->post();


				if(empty($post['full_name']) || empty($post['uname']))

					throw new Exception("Error : Input Required Fields");

				$set['fullname']  = $post['full_name'];
				$set['username']  = $post['uname'];
				$set['password']  = isset($post['pass']) ? md5($post['pass']):'';
				$set['user_lvl'] = isset($post['utype']) ? $post['utype']:'';
				$set['user_status'] = isset($post['status']) ? $post['status']:'';

				if(!empty($post['user_id'])){

		
					$user_id = $this->users->update($post['user_id'],$set);

					if(!$user_id) 
						throw new Exception("Error: Failure to update user");
					

				}else{

					$user_id = $this->users->insert($set);

					if(!$user_id)
						throw new Exception("Error: Failure to update user");

				}

				if(!empty($post['user_id'])){

					$this->session->set_flashdata('msg','Update Successful');
					redirect('maintain/users/user_form/'.$post['user_id']);

				}else{
					
					$this->session->set_flashdata('msg','Adding of user is Successful');
					redirect('maintain/users');
					
				}

					
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());

			}				
		}





		if($user_id!=''){

			$where = array();

			$where['where']['id'] = $user_id;

			$data['user_row'] = $this->users->row($where);			


		}
		$this->view_data['js_file'] = '<script src="assets/js/pages/users.js"></script>'; 
		$this->view_data['js_file'] = '<script src="assets/js/pages/users.js"></script>'; 
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'user';
		$this->view_data['maintain']['view_file'] = 'maintain/users-form';
		$this->load->view('template', $this->view_data);

	}


}
