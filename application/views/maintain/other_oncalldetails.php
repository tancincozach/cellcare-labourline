
<h2>On Call Details</h2>

<?php FLASH_SESSION_MSG(); ?>

<div class="table-responsive">

	<div class="form-group">
		<a  href="maintain/others/oncalldetails_form/new/"  class="btn btn-success btn-sm"><i class="fas fa-plus-circle"></i>&nbsp;New On Call Details</a>		
	</div>
		
		<table class="table table-sm table-bordered">
			<thead>
				<th width="200px">Name</th>
				<th>Phone</th> 
				<th>Notes</th> 
				<th>Option</th>
			</thead>
			<tbody>

			<?php if(isset($results)  && !empty($results)):
					foreach( $results as $row):
				?>
				<tr>
					<td><?php echo @$row->name;?></td>
					<td><?php echo @$row->phone;?></td>					 
					<td><?php echo @$row->notes;?></td>					 
					<td>
						<a href="maintain/others/oncalldetails_form/edit/<?php echo @$row->id;?>" class="btn btn-info btn-sm py-0 my-0" /><i class="fas fa-pencil-alt"></i>&nbsp;Edit</a>
						<a onclick="return Misc.confirm('Are you sure to delete/inactivate this on call details ?');"  href="maintain/others/oncalldetails_form/delete/<?php echo @$row->id;?>" class="btn btn-danger btn-sm py-0 my-0 " /><i class="fas fa-user-times"></i>&nbsp;Delete</a>					
					</td>
				</tr>
			<?php  endforeach;  
				else:
			?>
				<tr>
					<td colspan="6"> No Records Found</td>
				</tr>
			<?php endif;?>
			</tbody>
		</table>
</div>