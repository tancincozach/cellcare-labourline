<h1>Add/Edit Customer</h1>

<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;

 echo form_open('',' class="form-horizontal" method="POST"');?>
 <?php if(isset($customer->CustomerID)) :?>
	<input type="hidden" name="cust_ID" value="<?php echo $customer->CustomerID?>">
 <?php endif;?>

<div class="row">
	<div class="col-md-5">
			<div class="table-responsive">
				<table class="table table-sm table-borderless">
					<tbody>
					<?php foreach ($fields as $key => $fld_row) : ?>
						<tr>
							<td><label class="font-weight-bold"><?php echo $fld_row->value;?></label></td>
							<td><input type="text" name="<?php echo $fld_row->code;?>" class="form-control form-control-sm" value="<?php echo @$customer->{$fld_row->code};?>"></td>
						</tr>					
					<?php endforeach; ?>	
					</tbody>
				</table>				
			</div>
	</div>	
</div>

<div class="form-group">
	<div class="form-input col-md-4">
		<button type="submit" class="btn btn-info">Submit</button>
		<a href="maintain/customers" class="btn btn-primary">Cancel</a>
	</div>
</div>	
</form>