<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmasettings extends MY_Controller {
	
	public function __construct()	{

			parent::__construct();


		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/cma_settings';	
	}

	public function index(){

		$this->load->model('Sitesettingsmodel');

	 	$data = '';

	 	if($_POST){

	 		$post = $this->input->post();

	 		$set = array();
	 		$set['value'] = json_encode($post['cma_settings']);
	 		$this->Sitesettingsmodel->update_by_name('CMA_SETTINGS', $set);

	 	}

	 	$cma = $this->Sitesettingsmodel->get_row(array('name'=>'CMA_SETTINGS')); 
	 	$data['cma'] = json_decode(@$cma->value);	 	 
 
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'cma_settings';
		$this->load->view('template', $this->view_data);

	}
 

}

?>