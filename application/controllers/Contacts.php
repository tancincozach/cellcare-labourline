<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MY_Controller {

	public function __construct(){

		parent::__construct();

		unset($this->view_data['data']);

		$this->view_data['view_file'] = 'pages/contacts';
	}


	public function index()
	{


		$params = $this->input->get();

		$where = array();

		$where['where']['contact_id'] = @$params['id'];

		$data['contacts'] = $this->contacts->row($where);	
		

		$this->view_data['data'] = @$data;			

		$this->load->view('template', $this->view_data);
 

	}


	public function collectors()
	{


		$params = $this->input->get();

		$this->db->order_by('name', 'ASC');
		$collectors = $this->db->get('list_collectors')->result();

		$data['collectors'] = $collectors;	
		

		$this->view_data['data'] = @$data;			
		$this->view_data['view_file'] = 'pages/collectors';
		$this->load->view('template', $this->view_data);
 

	}
}
