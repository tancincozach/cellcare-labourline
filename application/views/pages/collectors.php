<div class="table-responsive" style="padding-left:20px;">


<h4>Collectors Contact</h4><br/>

<?php echo @$contacts->contact_html;?>

	<table class="table table-sm table-borderless table-striped">
		<thead class="thead-dark">
			<tr>
				<th>Name</th>
				<th>Phone</th>
				<th>Notes</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			if( count($collectors) > 0 ): 
				foreach($collectors as $row):
			?>
			<tr>
				<td><?php echo $row->name; ?></td> 
				<td><?php echo $row->phone; ?></td> 
				<td><?php echo stripslashes($row->notes); ?></td> 
			</tr>
		<?php 
				endforeach;
			else: 
		?>
		<p>No Records Found.</p>
		<?php endif; ?>
		</tbody>
	</table>

</div>
