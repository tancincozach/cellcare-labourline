<section class="jumbotron text-center col-sm-12 mb-1 p-3">
    

    <h4 class="">Can I please confirm the Client Name or Client Number for the person you are calling on behalf of</h4>
 
    <div class="col-sm-4 mx-auto pt-3">
        <input type="text" class="form-control" id="auto-customer-search-box" aria-describedby="searchTextHelp" placeholder="Enter Client Name or Client ID" autofocus>
        <small id="searchTextHelp" class="form-text text-muted"></small>
    </div>

</section>

<div class="col-sm-12">
    <h5>Current Open Calls</h5>
    <table class="tablelist table table-sm table-striped table-bordered">
        <thead>
            <tr class="bg-feijoa text-dark">
                <th>Date Active</th>
                <th>Time Active</th>
                <th>Caller Name</th>
                <th>Client Name</th>
                <th>Client ID</th>
                <th>Baby</th>
                <th>Client Status</th>
                <th>Collector Status</th>
                <th>Hospital Delivering At</th>
                <th>State</th>
                <th>Collector</th>
                <th>Reminders</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $row): 

                $dt = explode(' ', $row->tran_updated);
            ?>
            <tr>
                <td><?php echo date('d/m/Y',strtotime($dt[0])); ?></td>
                <td><?php echo date('H:i',strtotime($dt[1])); ?></td>
                <td><?php echo stripslashes($row->CallerName); ?></td>
                <td><?php echo stripslashes($row->FirstName.' '.$row->LastName); ?></td>
                <td><?php echo $row->CustomerID; ?></td>
                <td><?php echo $row->baby!=''?$row->baby:(($row->PreviousDeliveries>0?'Multi':'First')); ?></td>
                <td>
                <?php 

                    if( $row->collector_status == 'Collector - AT HOSPITAL'){
                        echo 'Closed';
                    }elseif( !empty($row->sched_induction) ){
                        
                        if($row->client_status == 3) echo $this->Commonmodel->client_status_text($row->client_status); 
                        else echo 'Scheduled Induction - '.date('d/m/Y', strtotime($row->sched_induction)).' '.$row->sched_induction_ta;

                    }elseif( !empty($row->sched_ceasar) ){
                        
                        if($row->client_status == 3) echo $this->Commonmodel->client_status_text($row->client_status); 
                        else echo 'Scheduled C-Section - '.date('d/m/Y', strtotime($row->sched_ceasar)).' '.$row->sched_ceasar_to;

                    }elseif( $row->tran_status == '0' ){
                        echo 'Closed';
                    }else{
                        echo $this->Commonmodel->client_status_text($row->client_status); 
                    }
                ?> 
                </td>                
                <td><?php echo $row->collector_status; ?></td>
                <td><?php echo $row->Hospital; ?></td>
                <td><?php echo $row->HospitalRegion; ?></td>
                <td><?php echo stripslashes($row->collector); ?></td>
                <td><?php echo ($row->reminder != '')?date('d/m/Y H:i', strtotime($row->reminder)):''; ?></td>
                <td><a href="booking/open/<?php echo $row->CustomerID.( (isset($issue_type) AND @$issue_type != 1)?'?issue_type='.$issue_type:'' ); ?>">Open</a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>