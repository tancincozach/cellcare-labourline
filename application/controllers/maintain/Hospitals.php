<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hospitals extends MY_Controller {
	
	public function __construct()	{

			parent::__construct();


		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/hospitals';	
	}

	public function index(){

		$this->load->model('Commonmodel','common');

		$data['fields']  = '';
		$data['results'] = '';




		$params = array();

		if(isset($_FILES['hospital_file']['tmp_name'])){

			try {

				$return = $this->upload_hospitals( $_FILES );

				if(array_key_exists('error',$return)){

					throw new Exception($return['error']);
					
				}
				if(array_key_exists('msg',$return)){

					$this->session->set_flashdata('msg',$return['msg']);
					
				}				
				
			} catch (Exception $e) {

				$this->session->set_flashdata('error',$e->getMessage());
				
			}

			redirect('maintain/hospitals');
			
		}



		try {
			
			$params['table'] = 'hospitals';
			$params['where'] = " code  LIKE '%Hospital%'";			

			$result_array = array();


			$result_array  = $this->common->get_csv_table($params);



			if(array_key_exists('error',$result_array)){

				throw new Exception($result_array['error']);
				
			}


			$data['fields'] = @$result_array['custom_fields'];
			$data['results'] = @$result_array['results'];
			

		} catch (Exception $e) {

			$this->session->set_flashdata('error',$e->getMessage());
			
		}

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'hospitals';
		$this->load->view('template', $this->view_data);

	}

	public function  upload_hospitals( $file_data ){


		$this->load->model('Commonmodel','common');

		$return_array = array();


		try {
			
			if(empty($file_data['hospital_file']['tmp_name'])) throw new Exception("Error: No file to be uploaded.");	

			$params['file'] = 'hospital_file';
			$params['table'] = 'hospitals';	
			$params['where'] = " code  LIKE '%Hospital%'";	

	        $return_array = $this->common->import_csv_report($params);


			
		} catch (Exception $e) {
			

			$this->session->set_flashdata('error',$e->getMessage());
			
			
		}

		return $return_array;

	}

}

?>