
<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">APP eligible</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h5 >ADVISE CALLER</h5>
                <!-- <p>I can see you have the Cell Care APP, Please use this app to track your dilation & contractions
                and it will guide you to what you need to do and when you need to contact us
                </p> -->
                <p>I can see you have the Cell Care APP.  Please have the APP active and keep your phone close</p>
                <h5 >THEN</h5>
                <p>Close the alert to continue</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
            </div>
        </div>
    </div>
</div>
