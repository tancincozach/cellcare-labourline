<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Others extends MY_Controller {

	public function __construct(){

		parent::__construct();

		if( $this->user->level == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		//$this->view_data['maintain']['view_file'] = 'maintain/users';		
	}
 
	public function advisedscripts() {
		$data = array();

		$this->db->order_by('type');
		$list_advise_script = $this->db->get('list_advise_script');
		$data['results'] = $list_advise_script->result();

 		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'advisescripts';
		$this->view_data['maintain']['view_file'] = 'maintain/advisedscripts';
		$this->load->view('template', $this->view_data);


	}

	public function advisedscripts_form($form_type='', $id=''){

		try {

			if( empty($form_type) ){

			}
			
			if( isset($_POST['form_type']) AND  @$_POST['form_type'] == 'new'){

				$post = $this->input->post();

				$set = array();
				$set['type'] = implode(' / ', $post['script_type']);
				$set['script_text'] = addslashes($post['script_text']);

				if( $this->db->insert('list_advise_script', $set) ){
					$this->session->set_flashdata('flash_success', "Advised Scripts Successfuly Added");
					redirect('maintain/others/advisedscripts');
				}else{
					$this->session->set_flashdata('flash_error', "Error adding new Script");
					redirect('maintain/others/advisedscripts_form/new/');
				}
			}
			
			if( isset($_POST['form_type']) AND   @$_POST['form_type'] == 'edit'){

				$post = $this->input->post();
				$id = $post['id'];

				$set = array();
				$set['type'] = implode(' / ', $post['script_type']);
				$set['script_text'] = addslashes($post['script_text']);

				$this->db->where('id', $id);
				if( $this->db->update('list_advise_script', $set) ){
					$this->session->set_flashdata('flash_success', "Advised Scripts Successfuly Updated");
					redirect('maintain/others/advisedscripts');
				}else{
					$this->session->set_flashdata('flash_error', "Error updating new Script");
					redirect('maintain/others/advisedscripts_form/edit/'.$id);
				}

			}
			
			if( isset($_GET) AND   @$form_type == 'delete'){

				$this->db->where('id', $id);
				
				if( $this->db->delete('list_advise_script') ){
					$this->session->set_flashdata('flash_success', "Selected Item Successfuly Deleted");
				}else{
					$this->session->set_flashdata('flash_error', "Something went wrong on deleting selected item.");
				}

				redirect('maintain/others/advisedscripts');
			}

			if( $form_type=='edit' AND !empty($id) ){

				$this->db->where('id', $id);
				$data['row'] = $this->db->get('list_advise_script')->row();
			}
			 

			$data['form_type'] = $form_type;

	 		$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'advisescripts';
			$this->view_data['maintain']['view_file'] = 'maintain/advisedscripts_form';
			$this->load->view('template', $this->view_data);
			 



		} catch (Exception $e) {
			$this->session->set_flashdata('flash_error_msg', "Invalid");
			redirect('others/advisedscripts');
		}

	}



	public function on_call_details() {
		$data = array();
		
		$list_oncall_details = $this->db->get('list_oncalldetails');
		$data['results'] = $list_oncall_details->result();

 		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'on_call_details';
		$this->view_data['maintain']['view_file'] = 'maintain/other_oncalldetails';
		$this->load->view('template', $this->view_data);


	}



	public function oncalldetails_form($form_type='', $id=''){

		try {

			if( empty($form_type) ){

			}
			

			$post = $this->input->post();
			
			if( isset($_POST['form_type']) AND  @$_POST['form_type'] == 'new'){



				$set = array();
				$set['name'] = $post['name'];
				$set['phone'] = $post['phone'];
				$set['notes'] = addslashes($post['script_text']);

				if( $this->db->insert('list_oncalldetails', $set) ){
					$this->session->set_flashdata('flash_success', "On Call Details Successfuly Added");
					redirect('maintain/others/on_call_details');
				}else{
					$this->session->set_flashdata('flash_error', "Error adding on call details");
					redirect('maintain/others/oncalldetails_form/new/');
				}
			}
			
			if( isset($_POST['form_type']) AND   @$_POST['form_type'] == 'edit'){

				$post = $this->input->post();
				$id = $post['id'];

				$set = array();
				$set['name'] = $post['name'];
				$set['phone'] = $post['phone'];
				$set['notes'] = addslashes($post['script_text']);

				$this->db->where('id', $id);
				if( $this->db->update('list_oncalldetails', $set) ){
					$this->session->set_flashdata('flash_success', "On Call Details Successfuly Updated");
					redirect('maintain/others/on_call_details');
				}else{
					$this->session->set_flashdata('flash_error', "Error updating new on call details");
					redirect('maintain/others/oncalldetails_form/edit/'.$id);
				}

			}
			
			if( isset($_GET) AND   @$form_type == 'delete'){

				$this->db->where('id', $id);
				
				if( $this->db->delete('list_oncalldetails') ){
					$this->session->set_flashdata('flash_success', "Selected Item Successfuly Deleted");
				}else{
					$this->session->set_flashdata('flash_error', "Something went wrong on deleting selected item.");
				}

				redirect('maintain/others/on_call_details');
			}
				
			if( $form_type=='edit' AND !empty($id) ){

				$this->db->where('id', $id);
				$data['row'] = $this->db->get('list_oncalldetails')->row();
			}
			 

			$data['form_type'] = $form_type;

	 		$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'on_call_details';
			$this->view_data['js_file'] = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'; 
			$this->view_data['js_file'] .= '<script src="assets/js/pages/oncalldetails.js"></script>'; 
			$this->view_data['maintain']['view_file'] = 'maintain/other_oncalldetails_form';
			$this->load->view('template', $this->view_data);
			 



		} catch (Exception $e) {
			$this->session->set_flashdata('flash_error_msg', "Invalid");
			redirect('others/on_call_details');
		}

	}



	public function collectors() {
		$data = array();
		
		$list_oncall_details = $this->db->get('list_collectors');
		$data['results'] = $list_oncall_details->result();

 		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'collector_details';
		$this->view_data['maintain']['view_file'] = 'maintain/other_collector';
		$this->load->view('template', $this->view_data);


	}



	public function collector_form($form_type='', $id=''){

		try {

			if( empty($form_type) ){

			}
			

			$post = $this->input->post();
			
			if( isset($_POST['form_type']) AND  @$_POST['form_type'] == 'new'){



				$set = array();
				$set['name'] = $post['name'];
				$set['phone'] = $post['phone'];
				$set['notes'] = addslashes($post['script_text']);

				if( $this->db->insert('list_collectors', $set) ){
					$this->session->set_flashdata('flash_success', "Collector Details Successfuly Added");
					redirect('maintain/others/collectors');
				}else{
					$this->session->set_flashdata('flash_error', "Error adding on call details");
					redirect('maintain/others/collector_form/new/');
				}
			}
			
			if( isset($_POST['form_type']) AND   @$_POST['form_type'] == 'edit'){

				$post = $this->input->post();
				$id = $post['id'];

				$set = array();
				$set['name'] = $post['name'];
				$set['phone'] = $post['phone'];
				$set['notes'] = addslashes($post['script_text']);

				$this->db->where('id', $id);
				if( $this->db->update('list_collectors', $set) ){
					$this->session->set_flashdata('collectors', "Collector Details Successfuly Updated");
					redirect('maintain/others/collector_form');
				}else{
					$this->session->set_flashdata('flash_error', "Error updating on call details");
					redirect('maintain/others/oncalldetails_form/edit/'.$id);
				}

			}
			
			if( isset($_GET) AND   @$form_type == 'delete'){

				$this->db->where('id', $id);
				
				if( $this->db->delete('list_collectors') ){
					$this->session->set_flashdata('flash_success', "Selected Item Successfuly Deleted");
				}else{
					$this->session->set_flashdata('flash_error', "Something went wrong on deleting selected item.");
				}

				redirect('maintain/others/collectors');
			}
				
			if( $form_type=='edit' AND !empty($id) ){

				$this->db->where('id', $id);
				$data['row'] = $this->db->get('list_collectors')->row();
			}
			 

			$data['form_type'] = $form_type;

	 		$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'collector_details';
			$this->view_data['js_file'] = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'; 
			$this->view_data['js_file'] .= '<script src="assets/js/pages/oncalldetails.js"></script>'; 
			$this->view_data['maintain']['view_file'] = 'maintain/other_collector_form';
			$this->load->view('template', $this->view_data);
			 



		} catch (Exception $e) {
			$this->session->set_flashdata('flash_error_msg', "Invalid");
			redirect('others/collectors');
		}

	}


	public function remindertype() {
		$data = array();
		
		$list = $this->db->get('list_remindertypes');
		$data['results'] = $list->result();

 		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'remindertype';
		$this->view_data['maintain']['view_file'] = 'maintain/other_remindertype';
		$this->load->view('template', $this->view_data);


	}



	public function remindertype_form($form_type='', $id=''){

		try {

			if( empty($form_type) ){

			}
			

			$post = $this->input->post();
			
			if( isset($_POST['form_type']) AND  @$_POST['form_type'] == 'new'){



				$set = array(); 
				$set['content'] = addslashes($post['content']);

				if( $this->db->insert('list_remindertypes', $set) ){
					$this->session->set_flashdata('flash_success', "Reminder Type Successfuly Added");
					redirect('maintain/others/remindertype');
				}else{
					$this->session->set_flashdata('flash_error', "Error adding new Reminder Type");
					redirect('maintain/others/other_remindertype_form/new/');
				}
			}
			
			if( isset($_POST['form_type']) AND   @$_POST['form_type'] == 'edit'){

				$post = $this->input->post();
				$id = $post['id'];

				$set = array(); ;
				$set['content'] = addslashes($post['content']);

				$this->db->where('id', $id);
				if( $this->db->update('list_remindertypes', $set) ){
					$this->session->set_flashdata('flash_success', "Reminder Type Successfuly Updated");
					redirect('maintain/others/remindertype');
				}else{
					$this->session->set_flashdata('flash_error', "Error updating on update Reminder Type");
					redirect('maintain/others/other_remindertype_form/edit/'.$id);
				}

			}
			
			if( isset($_GET) AND   @$form_type == 'delete'){

				$this->db->where('id', $id);
				
				if( $this->db->delete('list_remindertypes') ){
					$this->session->set_flashdata('flash_success', "Selected Item Successfuly Deleted");
				}else{
					$this->session->set_flashdata('flash_error', "Something went wrong on deleting selected item.");
				}

				redirect('maintain/others/remindertype');
			}
				
			if( $form_type=='edit' AND !empty($id) ){

				$this->db->where('id', $id);
				$data['row'] = $this->db->get('list_remindertypes')->row();
			}
			 

			$data['form_type'] = $form_type;

	 		$this->view_data['maintain']['data'] = $data;
			$this->view_data['maintain']['menu_active'] = 'remindertype'; 
			$this->view_data['maintain']['view_file'] = 'maintain/other_remindertype_form';
			$this->load->view('template', $this->view_data);
			 



		} catch (Exception $e) {
			$this->session->set_flashdata('flash_error_msg', "Invalid");
			redirect('others/collectors');
		}

	}


}
