 
<div class="col-sm-12">

	<h5>Reminders</h5>

	
	<?php echo form_open('call_register', ' method="get" name="LabourProgress-form" id="LabourProgress-form"  '); ?>
		 
			<!-- <div class="row">
				<div class="col">
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">Search</span>
						</div>
					
                        <select name="search" class="custom-select">
                            <option value=""></option>
                            <option value="Calling for another Reason" <?php echo trim(@$_GET['search'] == 'Calling for another Reason')?'selected="selected"':''; ?>>Calling for another Reason</option>
                            <option value="Disconnect on Greeting" <?php echo trim(@$_GET['search'] == 'Disconnect on Greeting')?'selected="selected"':''; ?>>Disconnect on Greeting</option>
                            <option value="Wrong Number" <?php echo trim(@$_GET['search'] == 'Wrong Number')?'selected="selected"':''; ?>>Wrong Number</option>
                        </select>
                        <div class="input-group-append">
							<button type="submit" class="btn btn-primary btn-sm">Search</button>
							<a class="btn btn-secondary btn-sm" href="call_register">Clear</a>
						</div>					
					</div>
					
				</div>
				<div class="col">
					
				</div>
			</div> -->
		 
	</form>
	
</div>

<div class="col-sm-12">
  
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark text-white">
                <th>Message</th>
                <th>Expiry</th> 
                <th>Intensifier</th> 
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $row): 

                $json_object = json_decode($row->more_text);

                $remindertime = isset($json_object->CustomerLocalDateTime)?$json_object->CustomerLocalDateTime:$json_object->Reminder;
            ?>

            <tr> 
                <td><?php echo $json_object->Message; ?></td>
                <td><?php echo date('d/m/Y H:i', strtotime($remindertime)).' '.@$json_object->State; ?></td>                
                <td><?php echo $row->audit_status?'Yes':'<strong class="text-danger">No</strong>'; ?></td>                
            </tr>

            <?php  endforeach; ?>
        </tbody>
    </table>

    <div class="row">
        <div class="col-sm-6 justify-content-start">
             <?php echo $showing; ?>
        </div>
        <div class="col-sm-6 d-flex justify-content-end">            
            <?php echo $links; ?>
        </div> 
    </div>

</div>