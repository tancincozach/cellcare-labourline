<h2>Customers</h2>

<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;


	echo form_open_multipart('','class="form-inline"' );
?>


	<div class="form-group">

		<input type="hidden" name="upload" value="1">
		
		<input type="file" name="customer_file"  class="form-control form-control-sm" />&nbsp;


		<button type="submit" class="btn  btn-primary btn-sm" />Upload</button>&nbsp;&nbsp;
		<a href="maintain/customers/customer_form/" class="btn btn-success btn-sm" /><i class="fas fa-plus-circle"></i>New Customer</a>
	</div>

</form>
<br/>

	<table id="customers"  class="table table-bordered table-sm table-striped nowrap" >
			<thead>
				<th class="table-primary">Options</th>
				<?php 
				$flds=0;
				foreach ($fields as $key => $fld_row) : ?>

					<th class="table-primary"><input type="hidden" name="fields" value="<?php echo $fld_row->code;?>"/><?php echo $fld_row->value;?></th>
				<?php 
				$flds++;
				endforeach;?>				
			</thead>
			<tbody>

			</tbody>
			<tfoot>
				<th class="table-primary">Options</th>
				<?php 
				$flds=0;
				foreach ($fields as $key => $fld_row) : ?>

					<th class="table-primary"><?php echo $fld_row->value;?></th>
				<?php 
				$flds++;
				endforeach;?>				
			</tfoot>

		</table>
