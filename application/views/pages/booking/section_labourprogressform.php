
<div class="col-sm-12 mb-5 box-labourprogress">
	<div class="row border mt-1 pt-2">
		
		<div class="col-sm-12">
			<h4 class="text-center">Call Activity - CLIENT RELATION </h4>
		</div>


		<div class="col-sm-8 ml-auto mr-auto">
		 	<table class="table table-sm table-borderless bg-secondary text-white text-center">
				<tr>
					<th rowspan="2" class="text-center align-middle">CALLER DETAILS</th>
					<th>NAME</th>
					<th>PHONE</th>
					<th>RELATIONSHIP</th>
				</tr>
				<tr>
					<td><?php echo @$transaction->CallerName; ?></td>
					<td><?php echo @$transaction->CallerPhone; ?></td>
					<td><?php echo @$transaction->ClientRelation; ?></td> 
				</tr>
			</table>
		</div>


		 

		<?php echo form_open('booking/save', 'name="LabourProgress-form" id="LabourProgress-form" class="col-sm-12" onsubmit="return LabourProgress.submit(this)" '); ?>
		
	 		<input type="hidden" name="section" value="labourprogress">
	 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
	 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">	
	 		<input type="hidden" name="tab" value="">

	 		<input type="hidden" name="orig_client_status" value="<?php echo @$transaction->client_status; ?>">	

	 		<div class="row">
	 		
				<div class="col-sm-6">
					<table class="table table-sm table-striped ">

						<tr class="bg-palegreen">
							<td class="font-weight-bold">CLIENT STATUS - where are you</td>
							<td > 
								<?php 

									$attr = ' class="" style="width=150px !important; margin-right: 3px" id="client_status" required="" onchange="LabourProgress.onChangeClientStatus(this);" ';
									$opt = $this->Commonmodel->hash_array('client_status');
									$opt[''] = '';
									asort($opt);
									echo form_dropdown('client_status', $opt , $transaction->client_status, $attr ); 
								?>

								<?php if(@$transaction->client_status != ''): ?>
									<input type="checkbox" name="client_status_no_change">	No Change
								<?php endif; ?>
							</td>
							<td>
								&nbsp;
							</td>						
						</tr>
						
						<tr>
							<td colspan="4" class="pt-3"></td>
						</tr>

						<tr>
							<td style="width: 247px;">Do you have Collection Kit ?</td>
							<td>
								<div class="row">
									<div class="col-6"> 

										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_collectionkit" type="radio" name="collection_kit" id="lbpcollectionkit_1" value="1" required="" <?php echo @$transaction->collection_kit == '1'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcollectionkit_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_collectionkit" type="radio" name="collection_kit" id="lbpcollectionkit_0" value="0" <?php echo @$transaction->collection_kit == '0'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcollectionkit_0">No</label>
										</div>

								</div>
							</td>
							 
						</tr>
					 
						<tr>
							<td>Has clients waters broken ?</td>
							<td>
								<div class="row">
									<div class="col-6 pr-0"> 
										<?php

											$waters_broken_v = explode(' @ ', @$transaction->waters_broken);
 											//print_r($waters_broken_v);
											// $attr = 'class="custom-select custom-select-sm" id="lbp_caesar_induction" ';
											// echo form_dropdown('waters_broken', opt_waters_broken(), @$waters_broken_v[0], $attr);
										?>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_watersbroken" type="radio" name="waters_broken" id="watersbroken_1" value="1" required="" <?php echo @$waters_broken_v[0] == 'Yes'?'checked':''; ?> >
										  	<label class="form-check-label" for="watersbroken_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_watersbroken" type="radio" name="waters_broken" id="watersbroken_0" value="0" <?php //echo @$waters_broken_v[0] == '0'?'checked':''; ?> >
										  	<label class="form-check-label" for="watersbroken_0">No</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_watersbroken" type="radio" name="waters_broken" id="watersbroken_2" value="2" <?php //echo @$waters_broken_v[0] == '2'?'checked':''; ?> >
										  	<label class="form-check-label" for="watersbroken_2">Unknown</label>
										</div>

									</div>
									<div class="col pl-0 div_waters_broken_dt">
										<input type="text" class="form-control form-control-sm" name="waters_broken_dt" id="id_watersbroken_dt" data-toggle="datetimepicker" data-target="#id_watersbroken_dt" value="<?php echo @$waters_broken_v[0] == 'Yes'?@$waters_broken_v[1]:''; ?>">
									</div>
								</div>
							</td> 						
						</tr>

						<tr class="ctr_tinduction" style="<?php echo ($transaction->client_status != '3')?'display:none':''; ?>">						
							<td>Is Client being induced ?</td>
							<td>
								<div class="row induction_row_cond">
									<div class="col-6"> 

										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_caesarinduction" type="radio" name="caesar_induction" id="lbpcaesarinduction_1" value="1" required="" <?php echo @$transaction->caesar_induction == '1'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcaesarinduction_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_caesarinduction" type="radio" name="caesar_induction" id="lbpcaesarinduction_0" value="0" <?php echo @$transaction->caesar_induction == '0'?'checked':''; ?> >
										  	<label class="form-check-label" for="lbpcaesarinduction_0">No</label>
										</div>

									</div>								 
								</div>
								<div class="row induction_row_type">

									<div class="col-10 cls_tinduction_type" style="<?php echo ($transaction->caesar_induction != '1')?'display:none':''; ?>" > 
										<strong>Induction Type:</strong>

										<?php 
											$i = 0;
											foreach(opt_induction_type() as $key=>$val):

										?>
										<div class="custom-control custom-radio d-block" style="float:none !important">
										    <input type="radio" class="custom-control-input lp_tinduct_type" id="induct_type_id3_<?php echo $key; ?>" value="<?php echo $val; ?>"  <?php echo ($val==$transaction->caesar_induction_type)?'checked':''; ?> name="induction_type" required="" <?php echo ($transaction->caesar_induction != '1')?'disabled="disabled"':''; ?> >
										    <label class="custom-control-label" for="induct_type_id3_<?php echo $key; ?>"><?php echo $val; ?></label>
										</div> 
										<?php $i++; endforeach; ?>
									</div>
								</div>
							</td>
							 						
						</tr>

						<tr class="ctr_thormonestarted" style="<?php echo ($transaction->client_status != '3')?'display:none':''; ?>">						
							<td>Has client had a hormone drip started ?</td>
							<td>
								<div class="row hormonestarted_row_cond">
									<div class="col-6 pr-0"> 
										<?php 	
											 
											$hormone_started_v = explode(' @ ', @$transaction->hormone_started); 
											//print_r($hormone_started_v);
											// $attr = 'class="custom-select custom-select-sm" id="lbp_hormone_started" ';
											// echo form_dropdown('hormone_started', opt_hormone_started(), @$hormone_started_v[0], $attr);
										?>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_hormonestarted" type="radio" name="hormone_started" id="hormonestarted_1" value="1" required="" <?php echo @$hormone_started_v[0] == 'Yes'?'checked':''; ?> >
										  	<label class="form-check-label" for="hormonestarted_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_hormonestarted" type="radio" name="hormone_started" id="hormonestarted_0" value="0" <?php //echo @$hormone_started_v[0] == 'No'?'checked':''; ?> >
										  	<label class="form-check-label" for="hormonestarted_0">No</label>
										</div>
									</div>
									<div class="col pl-0 div_hormone_started_dt">
										<input type="text" class="form-control form-control-sm" name="hormone_started_dt" id="id_hormone_started_dt" data-toggle="datetimepicker" data-target="#id_hormone_started_dt" value="<?php echo @$hormone_started_v[0] == 'Yes'?@$hormone_started_v[1]:''; ?>">
									</div>
								</div>

								<!-- <div class="row hormonestarted_row_type" style="<?php echo (@$hormone_started_v[0] != '1')?'display:none':''; ?>">

									<div class="col-10"  > 
										<strong>Type:</strong>

										<?php 
											$i = 0;
											foreach(opt_hormonstarted_type() as $key=>$val):

										?>
										<div class="custom-control custom-radio d-block" style="float:none !important">
										    <input type="radio" class="custom-control-input lp_thormone_type" id="hormone_type_id3_<?php echo $key; ?>" value="<?php echo $val; ?>"  <?php echo ($val==$transaction->hormone_started_type)?'checked':''; ?> name="hormone_started_type" required="" <?php echo (@$hormone_started_v[0] != '1')?'disabled="disabled"':''; ?> >
										    <label class="custom-control-label" for="hormone_type_id3_<?php echo $key; ?>"><?php echo $val; ?></label>
										</div> 
										<?php $i++; endforeach; ?>
									</div>
								</div> -->
							</td> 					
						</tr>

						<tr class="ctr_tepidural" style="<?php echo ($transaction->client_status != '3')?'display:none':''; ?>" >
							<td>Has client been given an epidural ?</td>
							<td>
								<div class="row">
									<div class="col-6 pr-0"> 
										<?php 	
											$epidural_v = explode(' @ ', @$transaction->epidural);
											//print_r($epidural_v);
											// $attr = 'class="custom-select custom-select-sm" id="lbp_epidural" ';
											// echo form_dropdown('epidural', opt_epidural(), @$epidural_v[0], $attr);
										?>	
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_epidural" type="radio" name="epidural" id="epidural_1" value="1" required="" <?php echo @$epidural_v[0] == 'Yes'?'checked':''; ?> >
										  	<label class="form-check-label" for="epidural_1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
										  	<input class="form-check-input cls_epidural" type="radio" name="epidural" id="epidural_0" value="0" <?php //echo @$epidural_v[0] == '0'?'checked':''; ?>>
										  	<label class="form-check-label" for="epidural_0">No</label>
										</div>
									</div>
									<div class="col pl-0 div_epidural_dt">
										<input type="text" class="form-control form-control-sm" name="epidural_dt" id="id_epidural_dt" data-toggle="datetimepicker" data-target="#id_epidural_dt" value="<?php echo @$epidural_v[0] == 'Yes'?@$epidural_v[1]:''; ?>">
									</div>
								</div>
							</td> 
						</tr>
 
					</table>

				</div>

				<div class="col-sm-6 progress-selector-row">
					<div class="bg-light text-center p-1">
						<h5>Labour Progress Selector</h5>
						<p class="pb-0 mb-0">ASK CALLER below each call and update accordingly</p>
					</div>
					<div class="bg-darkgray">
						<div class="row p-1">
							 
							<div class="col-sm-6">
								<div class="row my-2">
									<div class="col-sm-5 pr-0">
										First or Multi
									</div>
									<div class="col-sm-7 pl-0">
										

										<?php 
											//echo $transaction->baby;
											$attr = ' class="custom-select custom-select-sm" required="" ';
											echo form_dropdown('baby', opt_baby() , ((trim(@$transaction->baby)!='')?$transaction->baby:($customer->PreviousDeliveries>'0'?'Multi':'First')), $attr ); 
										?>

									</div>
								</div>
								<div class="row my-2">
									<div class="col-sm-5 pr-0">
										Last Exam
									</div>
									<div class="col-sm-7 pl-0">
										
										<input type="text" class="form-control form-control-sm" name="last_exam" id="id_lastexam_dt" data-toggle="datetimepicker" data-target="#id_lastexam_dt" value="<?php echo ( in_array(@$transaction->next_exam, array('', '0000-00-00 00:00:00')))?'':date('d/m/Y H:i', strtotime($transaction->next_exam)); ?>" >

										<div class="form-group form-check mb-0">
											<input type="checkbox" class="form-check-input mt-0" name="lastexam_unknown" id="id_lastexam_dt_unknown">
											<label class="form-check-label" for="id_lastexam_dt_unknown">Unknown</label>
										</div>

									</div>
								</div>
								<div class="row my-2">
									<div class="col-sm-5 pr-0">
										Next Exam
									</div>
									<div class="col-sm-7 pl-0">
										
										<input type="text" class="form-control form-control-sm" name="next_exam" id="id_nextexam_dt" data-toggle="datetimepicker" data-target="#id_nextexam_dt" value="">
									
										<div class="form-group form-check mb-0">
											<input type="checkbox" class="form-check-input mt-0" name="next_exam_unknown" id="id_nextexam_dt_unknown">
											<label class="form-check-label" for="id_nextexam_dt_unknown">Unknown</label>
										</div>

									</div>
								</div>
							</div>
							<div class="col-sm-6">
								
								<!--div_row_cd-->
								<div class="row my-2 ">
									<div class="col-sm-5 pr-0">
										Dilation (cms)
									</div>
									<div class="col-sm-7 pl-0">
									<?php 
										$attr = ' class="custom-select custom-select-sm" required="required"';
										echo form_dropdown('cent_dilated', opt_cent_dilated() , @$transaction->cent_dilated, $attr ); 
									?>
									</div>
								</div>

								<div class="row my-2">
									<div class="col-sm-5 pr-0">
										Contracting
									</div>
									<div class="col-sm-7 pl-0">
	 								<?php
										$attr = ' class="custom-select custom-select-sm" required="required"';
										echo form_dropdown('lp_contract', opt_contracting() , @$transaction->lp_contract, $attr ); 
									?>
									</div>
								</div> 

								<div class="row my-2 div_row_fc">
									<div class="col-sm-5 pr-0">
										Frequency of Contractions
									</div>
									<div class="col-sm-7 pl-0">
	 								<?php
										$attr = ' class="custom-select custom-select-sm" required="required"';
										echo form_dropdown('freq_contraction', opt_freq_contraction() , @$transaction->freq_contraction, $attr ); 
									?>
									</div>
								</div>
								
								<div class="row my-2 div_row_lc">
									<div class="col-sm-5 pr-0">
										Length of Contractions
									</div>
									<div class="col-sm-7 pl-0">
									<?php 
										$attr = ' class="custom-select custom-select-sm" required="required"';
										echo form_dropdown('length_contraction', opt_length_contraction() , @$transaction->length_contraction, $attr ); 
									?>
									</div>
								</div> 

							</div>
							<div class="col-sm-6">
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" id="check_sel1" name="custom_pselector" class="custom-control-input" onclick="LabourProgress.onCheckSelectorToggle(this)" value="Urge to push OR ready to push">
								  	<label class="custom-control-label" for="check_sel1">Urge to push OR ready to push</label>
								</div>
								<div class="custom-control custom-checkbox">
								  	<input type="checkbox" id="check_sel2" name="custom_pselector" class="custom-control-input" onclick="LabourProgress.onCheckSelectorToggle(this)" value="Emergency C-Section or possiblity of C-Section">
								  	<label class="custom-control-label" for="check_sel2">Emergency C-Section or possiblity of C-Section</label>
								</div>
							</div>
							<div class="col-sm-6">
								<?php if( strtolower(trim(@$customer->ObsHistoryNotesAlert)) == 'yes'): ?>
								 
									<div class="bg-navyblue text-white p-1 blink">											
										<strong>Obstetric History ALERT</strong>
										Ensure you check obstetric history
										for important information
									</div>
								 
								<?php endif; ?>
							</div>
						</div>
					</div>

					<!-- <table class="table table-sm table-borderless bg-darkgray">
						<tr>
							<td colspan="2"></td>
							<td colspan="2"></td>							
						</tr>
						<tr>
							<td width="20%">First or Multi</td>
							<td width="30%">

								<?php 
									//echo $transaction->baby;
									$attr = ' class="custom-select custom-select-sm" required="" ';
									echo form_dropdown('baby', opt_baby() , ((trim(@$transaction->baby)!='')?$transaction->baby:($customer->PreviousDeliveries=='0'?'First':'Multi')), $attr ); 
								?>
							</td>
							<td class="c_td_fc" width="20%">Frequency of Contractions</td>
							<td class="c_td_fc" width="30%">
 								<?php
									$attr = ' class="custom-select custom-select-sm" required="required"';
									echo form_dropdown('freq_contraction', opt_freq_contraction() , @$transaction->freq_contraction, $attr ); 
								?>
							</td>
						</tr>
						<tr>
							<td>Last Exam</td>
							<td>
								<input type="text" class="form-control form-control-sm" name="last_exam" id="id_lastexam_dt" data-toggle="datetimepicker" data-target="#id_lastexam_dt" value="<?php echo ( in_array(@$transaction->next_exam, array('', '0000-00-00 00:00:00')))?'':date('d/m/Y H:i', strtotime($transaction->next_exam)); ?>" >

								<div class="form-group form-check mb-0">
									<input type="checkbox" class="form-check-input mt-0" name="lastexam_unknown" id="id_lastexam_dt_unknown">
									<label class="form-check-label" for="id_lastexam_dt_unknown">Unknown</label>
								</div>

							</td>
							<td class="c_td_lc" >Length of Contractions</td>
							<td class="c_td_lc" >
								<?php 
									$attr = ' class="custom-select custom-select-sm" required="required"';
									echo form_dropdown('length_contraction', opt_length_contraction() , @$transaction->length_contraction, $attr ); 
								?>
							</td>
						</tr>
						<tr>
							<td>Next Exam</td>
							<td>
								<input type="text" class="form-control form-control-sm" name="next_exam" id="id_nextexam_dt" data-toggle="datetimepicker" data-target="#id_nextexam_dt" value="">
							
								<div class="form-group form-check mb-0">
									<input type="checkbox" class="form-check-input mt-0" name="next_exam_unknown" id="id_nextexam_dt_unknown">
									<label class="form-check-label" for="id_nextexam_dt_unknown">Unknown</label>
								</div>
							</td>
							<td>Dilation (cms)</td>
							<td>								 
								<?php 
									$attr = ' class="custom-select custom-select-sm" required="required"';
									echo form_dropdown('cent_dilated', opt_cent_dilated() , @$transaction->cent_dilated, $attr ); 
								?>
							</td>
						</tr>

						<tr>
							<td colspan="4" class="pb-2">
								<div class="row">
									<div class="col-sm-7 pr-0">
										<div class="custom-control custom-checkbox">
										  	<input type="checkbox" id="check_sel1" name="custom_pselector" class="custom-control-input" onclick="LabourProgress.onCheckSelectorToggle(this)" value="Urge to push OR ready to push">
										  	<label class="custom-control-label" for="check_sel1">Urge to push OR ready to push</label>
										</div>
										<div class="custom-control custom-checkbox">
										  	<input type="checkbox" id="check_sel2" name="custom_pselector" class="custom-control-input" onclick="LabourProgress.onCheckSelectorToggle(this)" value="Emergency C-Section or possiblity of C-Section">
										  	<label class="custom-control-label" for="check_sel2">Emergency C-Section or possiblity of C-Section</label>
										</div>

									</div>
									<?php if( strtolower(trim(@$customer->ObsHistoryNotesAlert)) == 'yes'): ?>
									<div class="col-sm-5 pl-0">
										<div class="bg-navyblue text-white p-1">											
											<strong>Obstetric History ALERT</strong>
											Ensure you check obstetric history
											for important information
										</div>
									</div>
								<?php endif; ?>
								</div>
							</td>
							 						 
						</tr> 
						 
					</table> -->

					 
				</div>

	 		</div>

	 		<div class="row">
 
	 			<div class="col-sm-12 mx-auto mt-2">
 					<div class="row">
	 					<div class="col-sm-8 align-center ml-auto mr-auto">
	 						<textarea name="notes" rows="4" class="form-control form-control-sm" placeholder="Notes"></textarea>	 						
	 					</div>
	 					<div class="col-sm-12 mst-2 text-center bg-light p-1">
	 						 
 							<button type="submit" class="btn btn-sm btn-primary">Submit</button>	 							
	 						 
	 					</div>
 					</div>
	 			</div>
	 		</div>

		</form> 


		<div id="labourprogress-dynamic-content" class="col-sm-12">
			
		</div>

	</div>
</div>

<div class="foot_content_call_history">
<?php
	if( isset($calls_result)){
    	echo $this->load->view('pages/booking/call_history', array('calls_result'=>$calls_result), TRUE);
	}
?>
</div>