<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content bg-light">
            <div class="modal-header">
                <h5 class="modal-title">Manual SMS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <?php 
                echo form_open('booking/open', 'name="SMS_Collector_form" id="SMS-Collector-form" class="col-sm-12" onsubmit="return BookingDetailsNavbar.smscollector_save(this)" '); 


                $template = 'Client ID: '.$customer->CustomerID.' - Client Name: '.$customer->FirstName.' '.$customer->LastName.' ';
            ?>
                <input type="hidden" name="tran_id" value="<?php echo $transaction->tran_id; ?>" > 
                <input type="hidden" name="CustomerID" value="<?php echo $customer->CustomerID; ?>" > 

                <div class="modal-body">

                    <!-- <p class="bg-yellow p-2 text-dark">This will allow to manually send SMS.</p> -->

                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table table-sm table-borderless">
                             
                                <tr>
                                    <td>Name of Recipient</td>
                                    <td>
                                        <input type="text" class="form-control form-control-sm"  name="caller_name" required="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>
                                        <input type="text" class="form-control form-control-sm"  name="caller_phone" required="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Message</td>
                                    <td>
                                        <textarea class="form-control form-control-sm" rows="10" name="sms_message"><?php echo @$message; ?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-sm-6">
                            <p class="text-center"><strong>Collector Details</strong><!--  <br /> Choose applicable Collector depending on time of day --></p>
                            <table class="table table-sm">
                                <?php if( $customer->collector1 != '' ): ?>
                                <tr>
                                    <td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="BookingDetailsNavbar.smscollector_copy(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector1; ?></button></td> 
                                    <td><?php echo $customer->collector1Phone; ?></td> 
                                     
                                </tr>
                                <?php endif; ?>
                                <?php if( $customer->collector2 != '' ): ?>
                                <tr>
                                    <td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="BookingDetailsNavbar.smscollector_copy(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector2; ?></button></td> 
                                    <td><?php echo $customer->collector2Phone; ?></td> 
                                    
                                </tr>
                                <?php endif; ?>
                                <?php if( $customer->collector3 != '' ): ?>
                                <tr>
                                    <td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="BookingDetailsNavbar.smscollector_copy(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector3; ?></button></td> 
                                    <td><?php echo $customer->collector3Phone; ?></td> 
                                    
                                </tr>
                                <?php endif; ?>
                                <?php if( $customer->collector4 != '' ): ?>
                                <tr>
                                    <td><button type="button" class="btn btn-sm btn-info pt-0 pb-0" onclick="BookingDetailsNavbar.smscollector_copy(this)"><i class="fas fa-arrow-left"></i><?php echo $customer->collector4; ?></button></td> 
                                    <td><?php echo $customer->collector4Phone; ?></td> 
                                     
                                </tr>
                                <?php endif; ?>

                            </table>
                        </div>
                    </div>

                </div>

                <div class="mb-5 pb-3">                    
                    <button type="button" class="btn btn-secondary btn-sm float-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-sm float-right">Send</button> 
                </div>
                
            
            </form>
        </div>
    </div>
</div>