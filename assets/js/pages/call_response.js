

var Call_Response = (function () {

  	// locally scoped Object
	var myObject = {};

	// declared with `var`, must be "private"
	var privateMethod = function () {};


	myObject.confirm = function ( message ) {

		if( confirm(message) ){

			return true;
		}

    	return false;	
			
	};
	myObject.search = function(){

	}


  	return myObject;

})();


var Editor = (function () {

  	// locally scoped Object
	var myObject = {};

	// declared with `var`, must be "private"
	var privateMethod = function () {};


	myObject.create = function ( id ) {


	var style_formats = [					 
					{title : 'Header 2', block : 'h2' },
					{title : 'Header 3', block : 'h3' },
					{title : 'Header 4', block : 'h4' }			 
				];
			var plugins = [
					'advlist autolink lists link image charmap print preview anchor',
					'searchreplace visualblocks code fullscreen',
					'insertdatetime media table contextmenu paste code table textcolor'
					/*,'filemanager'*/
			    ];

			var toolbar =  'undo redo | styleselect | bold italic underline| fontsizeselect '+
						   '|alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor  | table';

			tinymce.init({
		      setup: function (editor) {
		                      editor.on('change', function () {
		                          tinymce.triggerSave();
		                      });
		                },
				selector: id,
				document_base_url: '<?php echo base_url(); ?>',
				theme: "silver",
				height: 250,
				cleanup : true,
				plugins: plugins,
				convert_urls: false,
				menubar:false,
				statusbar: false,
				toolbar:toolbar,						
				style_formats : style_formats,
				table_adv_tab: true,
				table_cell_adv_tab: true,
				table_row_adv_tab: true,
				image_advtab: true,
				paste_as_text:true,
				external_filemanager_path : 'maintains/filemanager_files', 

				template_replace_values : {
					username : "Some User",
					staffid : "991234"
				}

			});	

	};

	myObject.remove = function( id ){

			tinymce.execCommand('mceFocus', false, (id=='' ? 'textarea' : id ) );
			tinymce.execCommand('mceRemoveEditor', false,(id=='' ? 'textarea' : id ));
			tinymce.remove(id);	

	}


  	return myObject;

})();


$(function(){


	Editor.create('textarea#script_content');	


});