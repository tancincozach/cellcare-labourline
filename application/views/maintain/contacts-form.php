<h1>Contacts</h1>

<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;

 echo form_open('',' class="form-horizontal" method="POST"');?>
	<input type="hidden" name="contact_id" value="<?php echo @$contacts_row->contact_id;?>">

<div class="row">
	<div class="col-md-6">
			<div class="table-responsive table-borderless">
				<table class="table">
					<tbody>
						<tr>
							<td>Name</td>
							<td><input type="text" name="title" class="form-control form-control-sm" value="<?php echo @$contacts_row->contact_title;?>"></td>
						</tr>
						<tr>
							<td>Content</td>
							<td>
								<textarea class="form-control" name="content" id="content"><?php echo @$contacts_row->contact_html;?></textarea>
							</td>
						</tr>
						<tr>
							<td>Status</td>
							<td>
									<select class="form-select form-control form-control-sm" name="status">
									<option value="">SELECT</option>
									<option value="1" <?php echo isset($contacts_row->contact_status) && (int)$contacts_row->contact_status==1 ? "SELECTED":"";?>>Active</option>
									<option value="0" <?php echo isset($contacts_row->contact_status) && (int)$contacts_row->contact_status==0 ? "SELECTED":"";?>>Inactive</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>				
			</div>
	</div>	
</div>

<div class="form-group">
	<div class="form-input col-md-4">
		<button type="submit" class="btn btn-info">Submit</button>
		<a href="maintain/contacts" class="btn btn-primary">Cancel</a>
	</div>
</div>	
</form>