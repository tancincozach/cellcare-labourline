<div class="col-sm-12 mb-5 box-callerform">
	
	<div class="row border mt-1 p-5">

		<div class="col-sm-8 pt-2 pb-5 mx-auto">			
		    <h4 class="text-center">Can I please have your name & phone number</h4>
		 	
		 	<?php echo form_open('booking/save', 'name="caller-form" id="caller-form" class="" onsubmit="return Booking.confirm()" '); 

		 		if(isset($transaction)){
		 			$transaction->collector_phone = (trim(@$transaction->collector_phone) != '')?str_pad(@$transaction->collector_phone, 10, "0", STR_PAD_LEFT):'';
		 		}

		 		if(isset($customer)){
			 		@$customer->PartnersMobilePhone = (trim(@$customer->PartnersMobilePhone) != '')?str_pad(@$customer->PartnersMobilePhone, 10, "0", STR_PAD_LEFT):'';
			 	}
		 	?>
		 		 
		 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
		 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">
		 		<input type="hidden" name="section" value="caller"> 
		 		<input type="hidden" name="issue_type" value="<?php echo @$issue_type; ?>"> 

		 		<input type="hidden" name="PartnersFirstName" value="<?php echo trim(@$customer->PartnersFirstName); ?>">
		 		<input type="hidden" name="PartnersMobilePhone" value="<?php echo trim(@$customer->PartnersMobilePhone); ?>">
		 		
		 		<input type="hidden" name="collector" value="<?php echo trim(@$transaction->collector); ?>">
		 		<input type="hidden" name="collector_phone" value="<?php echo trim(@$transaction->collector_phone); ?>">

		 		<input type="hidden" name="orig_best_no_to_call" value="<?php echo trim(@$transaction->best_no_to_call); ?>"> 

		 		<?php if(@$customer->CellCareCollector == 'No' AND @$transaction->CellCareCollector_verify == ''): ?>
		 			<input type="hidden" name="CellCareCollector_verify" value="0"> 
		 		<?php endif; ?>

		 		<?php if(trim(@$customer->ObsHistoryNotes) == '' AND @$transaction->ObsHistoryNotes_verify == ''): ?>
		 			<input type="hidden" name="ObsHistoryNotes_verify" value="0"> 
		 		<?php endif; ?>


		 		<?php if( @$APP_popup == 1 ): ?>
		 		<input type="hidden" name="APP_poup" id="APP_poup" value="1">
		 		<input type="hidden" name="app_elligible_shown" value="<?php echo trim(@$transaction->app_elligible_shown); ?>">
		 		<?php endif; ?>

			    <div class="col-sm-8 mx-auto">

			    	<div class="row text-center">
				    	<div class="col-sm-6">
				    		<input type="text" class="form-control" name="CallerName"  placeholder="Caller Name" value="<?php echo @$customer->PartnersFirstName; ?>" required="">
				    	</div>
				    	<div class="col-sm-6">
				    		<input type="text"  maxlength="10" minlength="10" class="form-control" name="CallerPhone"  placeholder="Caller Phone" value="<?php echo @$customer->PartnersMobilePhone; ?>" required="">
				    	</div>
				    	<small class="text-muted text-center col-sm-12">Partners name will self populate - if not partner - override</small>
			        </div>
			       
			    </div>		

 
			    <div class="col-sm-8 mx-auto border bg-light" id="div-best-no" >
			    	<div class="row text-center m-3"> 
			     
			    		<p class="mb-0 font-weight-bold">Best Phone Number to user from now on</p>
			    		
			    	
						<div class="input-group mb-3">
							<!-- <input type="text"  maxlength="10" minlength="10" class="form-control" name="best_no_to_call"  placeholder="Best Phone Number" value="" aria-describedby="best_no_to_call-addon2"> -->
							<input type="text" class="form-control" maxlength="10" minlength="10" class="form-control" name="best_no_to_call" placeholder="Best Phone Number" aria-label="Best Phone Number" aria-describedby="best_no_to_call-addon2">
 
							<div class="input-group-append">
								<button class="btn btn-primary btn-sm" type="button" id="best_no_to_call-addon2" onclick="Callerform.copy_callerphone()" ><i class="fas fa-copy"></i> Copy Caller Phone</button>
							</div>

						</div>

				    	 		    	
			        </div>			       
			    </div>		 

			    <h4 class="text-center">What is your relationship with the client</h4>
			 
			    <div class="col-sm-10 mx-auto">

			    	<div class="row text-center">
				    	<div class="col-sm-6 mx-auto">
		    		      	<select name="ClientRelation" class="form-control" onchange="Callerform.onchange_ClientRelation()">
						        <option selected value="CLIENT RELATION">CLIENT RELATION</option>
						        <option  value="COLLECTOR">COLLECTOR</option>
						        <option  value="ON CALL">ON-CALL NURSE</option>
						    </select>
				    	</div>
				    	 
				    	<small class="text-muted text-center col-sm-12">
				    		Relationship with client will self populate - CLIENT RELATION - if not partner or client relation - override
				    	</small>
			        </div>
			       
			    </div>

			    <div class="col-sm-8 mx-auto text-center mt-2">
			    	<button class="btn btn-sm btn-primary">Submit</button>
			    </div>

			</form>

		</div>

	</div>


</div>