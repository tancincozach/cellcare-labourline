<h2>ADD/EDIT USER</h2>

<style type="text/css">
	
.field-icon {
	  float: right;
	  margin-left: -25px;
	  margin-top: -25px;
	  margin-right:5px;
	  position: relative;
	  z-index: 2;
}


</style>


<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;

 echo form_open('',' class="form-horizontal" method="POST"');?>
<input type="hidden" name="user_id" value="<?php echo (isset($user_row->id) ? $user_row->id:"") ;?>">
<div class="form-group">
		<label class="control-label col-md-4">Full Name</label>
		<div class="form-input col-md-4">
			<input type="text" name="full_name" class="form-control form-control-sm" value="<?php echo @$user_row->fullname;?>">
		</div>
</div>
<div class="form-group">
		<label class="control-label col-md-4">Username</label>
		<div class="form-input col-md-4">
			<input type="text" name="uname" class="form-control form-control-sm" value="<?php echo @$user_row->username;?>">
		</div>
</div>

<div class="form-group">
		<label class="control-label col-md-4">Password</label>
		<div class="form-input col-md-4">
			<input id="password-field" type="password" class="form-control" name="pass" >
              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
		</div>
</div>


<div class="form-group">
		<label class="control-label col-md-4">User Type</label>
		<div class="form-input col-md-3">
			<select name="utype" class="form-control form-control-sm">
				<option value="" >Select User Type</option>
				<option value="0" <?php echo (@$user_row->user_lvl==0) ?'SELECTED':'';?>>Operator</option>
				<option value="1" <?php echo (@$user_row->user_lvl==1) ?'SELECTED':'';?>>Supervisor</option>
				<option value="2" <?php echo (@$user_row->user_lvl==2) ?'SELECTED':'';?>>Support/Teamleader</option>
				<option value="3" <?php echo (@$user_row->user_lvl==3) ?'SELECTED':'';?>>CCAdmin</option>
				<option value="5" <?php echo (@$user_row->user_lvl==5) ?'SELECTED':'';?>>Client - Admin</option>
				<option value="51" <?php echo (@$user_row->user_lvl==51) ?'SELECTED':'';?>>Client - No Maintenance Access</option>
			</select>
		</div>
</div>

<div class="form-group">
		<label class="control-label col-md-4">Status</label>
		<div class="form-input col-md-3">
			<select class="form-select form-control form-control-sm" name="status">
				<option value="1" <?php echo isset($user_row->user_status) && (int)$user_row->user_status==1 ? "SELECTED":"";?>>Active</option>
				<option value="0" <?php echo isset($user_row->user_status) && (int)$user_row->user_status==0 ? "SELECTED":"";?>>Inactive</option>
			</select>
		</div>
</div
<div class="form-group">
	<div class="form-input col-md-4">
		<button type="submit" class="btn btn-info btn-sm">Submit</button>
		<a href="maintain/users"  class="btn btn-primary btn-sm">Cancel</a>
	</div>
</div>	
</form>