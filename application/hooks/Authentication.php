<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication {

    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
       
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance(); 

    }

    public function check_login(){
        
        $this->CI->load->helper('url');
          
        //echo ' - Checking login credentials- ';

        if(!isset($this->CI->session)){
            $this->CI->load->library('session');
        }
 
        //echo ' Authentication->check_login ';

        $cookie = get_cookie('info');
        $cookie = base64_decode($cookie);
        $cookie = json_decode($cookie);
        
        //echo $this->CI->session->userdata('logged_in');
        //print_r($cookie);

        if( !$this->CI->session->userdata('logged_in')  ) {

            if( isset($cookie->logged_in) ){ 
                $sess = array();
                $sess['logged_in']  = $cookie->logged_in;
                $sess['user_id']    = $cookie->user_id;
                $sess['user_lvl']   = $cookie->user_lvl;
                $sess['agent_name'] = $cookie->agent_name;
                $sess['client_id']  = @$cookie->client_id;

                $this->CI->session->set_userdata($sess);

                $base64_encode_str = base64_encode(json_encode($sess));         
                set_cookie('info',$base64_encode_str,28800);
            }


        }

        if( !$this->CI->session->userdata('logged_in') OR empty($cookie) ){ 
            

            if(!isset($cookie->logged_in)){ 
                //$this->CI->session->set_flashdata('login_message', '<p>Session expired please login</p>');
                
                if( !in_array('login', $this->CI->uri->segments) ){
                    redirect(base_url().'login');
                }

            }else{
                if( in_array('login', $this->CI->uri->segments) ){
                    redirect(base_url().'dashboard');
                }
            }
        }else{
            if( in_array('login', $this->CI->uri->segments) ){
                redirect(base_url().'dashboard');
            }
        }

    }

    public function bar()
    {
        $this->CI->config->item('base_url');
    }
}