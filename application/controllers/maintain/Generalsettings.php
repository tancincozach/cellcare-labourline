<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generalsettings extends MY_Controller {

	public function __construct(){

		parent::__construct();

		if( $this->user->level == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/general_setting';		 

	}

	public function index(){
		$this->load->model('Sitesettingsmodel');

		$data = array();
		$data['sms'] = $this->Sitesettingsmodel->get_row(array('name'=>'ENABLE_EMAIL'))->value;
		$data['email'] = $this->Sitesettingsmodel->get_row(array('name'=>'ENABLE_EMAIL'))->value;

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'generalsetting';
		$this->load->view('template', $this->view_data); 
	}
}
