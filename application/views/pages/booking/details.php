<h1>Details</h1>


<?php 
	
	// echo '<pre>';
	// print_r($this->load->get_vars());
	// echo '</pre>';

 	FLASH_SESSION_MSG();

 	$issue_type_uri = (isset($issue_type))?'&issue_type='.$issue_type:'';
 	$d_navbar_url = 'booking/open/'.$customer->CustomerID.'/?'.$issue_type_uri;
?>

<div class="col-sm-12 booking-details">

	<div class="row border booking-client-details pt-1 bg-light">
		<div class="col-sm-5">
			<table class="table table-sm table-borderless table-shrink">
				<tr class="font-weight-bold" style="font-size: 18px">
					<td>Client Name</td>
					<td><?php echo @$customer->FirstName.' '.$customer->LastName; ?></td>
				</tr>
				<tr class="font-weight-bold" style="font-size: 18px">
					<td>Client ID</td>
					<td><?php echo @$customer->CustomerID; ?></td>
				</tr>
				<tr>
					<td>Mothers Phone</td>
					<td><?php echo @$customer->MobilePhone; ?></td>
				</tr>
				<tr>
					<td>Partners First Name</td>
					<td><?php echo @$customer->PartnersFirstName; ?></td>
				</tr>
				<tr>
					<td>Partners Phone</td>
					<td><?php echo @$customer->PartnersMobilePhone; ?></td>
				</tr>
				
				<?php if(trim(@$transaction->best_no_to_call) != ''): ?>
				<tr class="font-weight-bold text-danger border" style="font-size: 16px !important;">
					<td>Best number to call from now </td>
					<td><?php echo @$transaction->best_no_to_call; ?></td>
				</tr>
				<?php endif; ?>

				<tr>
					<td>State</td>
					<td><?php echo @$customer->State; ?></td>
				</tr>
				<tr>
					<td>Chinese Speaking</td>
					<td><?php echo (@$customer->ChineseSpeaking=="Yes")?'Yes':'No'; ?></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Is APP ELIGIBLE</td>
					<td><?php echo (@$customer->IsMotherCollnAppEligible =='Yes' OR @$customer->IsPartnerCollnAppEligible=='Yes' )?'Yes':'No' ; ?></td>
				</tr>
				<!-- <tr>
					<td>Is Mother Colln App Eligible</td>
					<td><?php echo @$customer->IsMotherCollnAppEligible; ?></td>
				</tr>
				<tr>
					<td>Is Partner Colln App Eligible</td>
					<td><?php echo @$customer->IsPartnerCollnAppEligible; ?></td>
				</tr> -->
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Obstetrician</td>
					<td><?php echo @$customer->Obstetrician; ?></td>
				</tr>
				<tr>
					<td>Estimated Delivery Date</td>
					<td><?php echo @$customer->EDD; ?></td>
				</tr>
				<tr class="font-weight-bold" style="font-size: 18px">
					<td><strong><?php echo ((trim(@$transaction->baby)!='')?$transaction->baby:($customer->PreviousDeliveries>'0'?'Multi':'First')); ?></strong></td>
					<td></td>
				</tr>
			</table>
		</div>		

		<div class="col-sm-3">
			<div class="col-sm-12 bg-hintofgreen p-2">

				<h5>Obstetric History <?php echo ( @$transaction->ObsHistoryNotes_verify == '1' )?'<i class="fas fa-check mt-1 text-success"></i>':''; ?> </h5>
				
				<p><?php 
					if(empty($customer->ObsHistoryNotes) ){
						echo @$transaction->ObsHistoryNotes_edit;
					}else{
						echo @$customer->ObsHistoryNotes; 
					}

				?></p>

				<?php if(empty($customer->ObsHistoryNotes) ): ?>
					<button type="button" id="obsnote_btn_edit" class="btn btn-sm pt-0 pb-0 float-right" onclick="Booking.obsnote_btn_toggle(<?php echo @$transaction->tran_id; ?>,1);" > <i class="fas fa-edit"></i> Edit</button>
				
					<div class="col-sm-12 m-0 p-0">
					 
						<div class="border p-2 bg-light" id="obsnote_div" style="display:none">
							<textarea class="form-control" name="ObsHistoryNotes_edit" rows="6" ><?php echo $transaction->ObsHistoryNotes_edit; ?></textarea>
							<br />
							<button type="button" id="obsnote_btn_save" class="btn btn-sm btn-primary pt-0 pb-0" onclick="Booking.obsnote_btn_toggle(<?php echo @$transaction->tran_id; ?>, 2);" >Save</button>
							<button type="button" id="obsnote_btn_cancel" class="btn btn-sm btn-secondary pt-0 pb-0" onclick="Booking.obsnote_btn_toggle(<?php echo @$transaction->tran_id; ?>,3);"  >Cancel</button>
						</div>
					</div>
				<?php endif; ?>
				
				<span class="clearfix"></span>

			</div>
		</div>

		<div class="col-sm-4">
			<table class="table table-sm table-borderless table-shrink">
				<tr class="font-weight-bold <?php echo (@$customer->CellCareCollector == 'Yes' OR @$transaction->CellCareCollector_verify == 1 )?'bg-lightblue':'bg-warning'; ?>">
					<td>Cell Care Collector</td>
					<td>
					<?php 
						echo @$customer->CellCareCollector; 

						if( @$transaction->CellCareCollector_verify == '1' ){
							echo '<i class="fas fa-check float-right mt-1"></i>';
						}
					?>						
					</td>
				</tr>
				<tr class="font-weight-bold <?php echo (@$transaction->confirm_hospital == '')?'bg-warning border-top ':'bg-lightblue border-top'; ?> ">
					<td>Hospital Details</td>
					<td><?php
						if( !isset($transaction) ){
							echo 'Not Confirm';
						}elseif( $transaction->confirm_hospital == '' ) {
							echo '<a href="booking/open/'.$customer->CustomerID.'/?section=hospital" style="color: #fff !important; display: block">Not Confirm <i class="fas fa-edit float-right mt-1"></i></a>';
						}else{
							echo "YES";
						}

					?></td>
				</tr>
				<tr class="font-weight-bold">
					<td>Hospital</td>
					<td><?php echo @$customer->Hospital; ?></td>
				</tr>
				<tr>
					<td>Hospital Street</td>
					<td><?php echo @$customer->HospitalStreet; ?></td>
				</tr>
				<tr>
					<td>Hospital City</td>
					<td><?php echo @$customer->HospitalCity; ?></td>
				</tr>
				<tr>
					<td>Hospital Region</td>
					<td><?php echo @$customer->HospitalRegion; ?></td>
				</tr>
				<tr>
					<td>Hospital Postcode</td>
					<td><?php echo @$customer->HospitalPostalCode; ?></td>
				</tr>
				<tr>
					<td>Hospital Phone</td>
					<td><?php echo @$customer->HospitalPhone; ?></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Storage Product</td>
					<td><?php echo @$customer->StorageProduct; ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td></td>
					<td class="text-right">
						<button class="btn btn-sm btn-hintofgreen font-weight-bold pt-1 pb-1">
							<?php
								//echo $this->Commonmodel->client_status_text(@$transaction->client_status);
								echo @$this->Commonmodel->hash('client_status', @$transaction->client_status, 1);
							?>
						</button>
					</td>
				</tr>
			</table>			

		</div>
	</div>

	<div class="row border booking-labour-progress mt-2 pt-1 bg-light">
		<div class="col-sm-12">
			<h5>Labour Progress</h5>
		</div>

		<div class="col-sm-4">
			<table class="table table-sm table-borderless table-shrink">
				<tr>
					<td>Last Dilation Exam</td>
					<td><?php echo (in_array(@$transaction->last_exam, array('', '0000-00-00 00:00:00')) )?'':date('d/m/Y H:i', strtotime(@$transaction->last_exam)); ?></td>
				</tr>
				<tr>
					<td>Next Dilation Exam</td>
					<td><?php echo (in_array(@$transaction->next_exam, array('', '0000-00-00 00:00:00')) )?'':date('d/m/Y H:i', strtotime(@$transaction->next_exam)); ?></td>					 
				</tr>				
			</table>
		</div>
		<div class="col-sm-5">
			<table class="table table-sm table-borderless table-shrink">
				<tr>
					<td width="40%">Waters Broken</td>
					<td><?php echo @$transaction->waters_broken; ?></td>
				</tr>
				<tr>
					<td>Epidural</td>
					<td><?php echo @$transaction->epidural; ?></td>
				</tr>
				<tr>
					<td>Hormone started</td>
					<td><?php 
						echo @$transaction->hormone_started; 
						echo ( @$transaction->hormone_started != 'No' AND @$transaction->hormone_started_type != '' ) ? ' ('.$transaction->hormone_started_type.')':'';
					?></td>
				</tr>
				<tr>
					<td>Induction</td>
					<td><?php 						 
						echo (@$transaction->caesar_induction != '')?opt_val(@$transaction->caesar_induction):''; 
						echo ( @$transaction->caesar_induction == 1 AND $transaction->caesar_induction_type != '') ? ' ('.$transaction->caesar_induction_type.')':'';
					?></td>
				</tr>				
			</table>
		</div>
		<div class="col-sm-3">
			<table class="table table-sm table-borderless table-shrink">
				<tr width="40%">
					<td>Frequency of Contractions</td>
					<td><?php echo opt_freq_contraction(@$transaction->freq_contraction); ?></td>
				</tr>
				<tr>
					<td>Length of Contractions</td>
					<td><?php echo opt_length_contraction(@$transaction->length_contraction); ?></td>
				</tr>
				<tr>
					<td>Dilation (cms)</td>
					<td><?php echo opt_cent_dilated(@$transaction->cent_dilated); ?></td>
				</tr>				
			</table>
		</div>
		
		<?php if( @$transaction->last_progress_selected != '' ): ?>
		<div class="col-sm-12 bg-lightblue p-1">
			<?php $last_progress =  $this->Commonmodel->progress_selector_text(@$transaction->last_progress_selected); ?>
			Last progress selected: <span class="font-weight-bold"><?php echo @$last_progress->client_status.' - '.@$last_progress->action1; ?></span>
		</div>
		<?php endif; ?>

	</div>

	<div class="row border booking-collector mt-2 bg-light pt-1">
		<div class="col-sm-12">
			<h5>Collector performing collection</h5>
		</div>
		<div class="col-sm-9">
			<table class="table table-sm table-borderless table-shrink">
				<tr>
					<td width="30%">Collector performing collection</td>
					<td><?php echo @$transaction->collector; ?></td>					
				</tr>
				<tr>
					<td width="30%">Phone</td>
					<td><?php echo @$transaction->collector_phone; ?></td>					
				</tr>
				<tr>
					<td>Distance from Hospital</td>
					<td><?php echo (@$transaction->collector_distance != '')?$transaction->collector_distance.' min(s)':''; ?></td>
				</tr>
			</table>
		</div>
		<div class="col-sm-3 text-right">
			<?php if(!empty($transaction->collector_status)): ?>
				<button class="btn btn-hintofgreen btn-sm font-weight-bold pt-1 pb-1" type="button" ><?php echo @$transaction->collector_status; ?></button>
			<?php endif; ?>
		</div>
	</div>

	<div class="row border booking-important-notes mt-2 pt-1 <?php echo trim(@$transaction->notes) == '' ? 'bg-lightblue':'bg-yellow-orig'; ?> ">
		<div class="col-sm-12">
			<h5>Important Notes <button type="button" id="in_btn_edit" class="btn btn-sm pt-0 pb-0" onclick="Booking.in_btn_toggle(<?php echo @$transaction->tran_id; ?>,1);" > <i class="fas fa-edit"></i> Edit</button></h5> 
		</div>

		<div class="col-sm-10 ml-auto mr-auto">
			<p class="text-center"><?php echo @$transaction->notes; ?></p>
			<div class="border p-2 bg-light" id="in_notes_div" style="display:none">
				<textarea class="form-control" name="important_notes" ><?php echo $transaction->notes; ?></textarea>
				<br />
				<button type="button" id="in_btn_save" class="btn btn-sm btn-primary pt-0 pb-0" onclick="Booking.in_btn_toggle(<?php echo @$transaction->tran_id; ?>, 2);" >Save</button>
				<button type="button" id="in_btn_cancel" class="btn btn-sm btn-secondary pt-0 pb-0" onclick="Booking.in_btn_toggle(<?php echo @$transaction->tran_id; ?>,3);"  >Cancel</button>
			</div>
		</div>
	</div>

	<?php if( !empty($transaction->sched_ceasar) ): ?>
	<div class="row border booking-c-section mt-2 pt-1 bg-pink">
		<div class="col-sm-12">
			<h5>C-Section Schedule <button type="button" id="csection_btn_edit" class="btn btn-sm pt-0 pb-0" onclick="Booking.editcsection_btn_toggle(<?php echo @$transaction->tran_id; ?>,1);" > <i class="fas fa-edit"></i> Edit</button> </h5> 
		</div>

		<div class="col-sm-9">
			<?php echo form_open('', 'name="edit-sched-csection" id="edit-sched-csection" class="" onsubmit="return HospitalForm.detailsAjaxSave(this)" '); ?>


				<table class="table table-sm table-borderless table-shrink" id="schedceasar_display">
					<tr>
						<td width="30%">Date of C-Section</td> 					
						<td><?php echo date('d/m/Y',strtotime(@$transaction->sched_ceasar)); ?></td>					
					</tr>
					<tr>
						<td>Time of Admission</td>
						<td><?php echo @$transaction->sched_ceasar_ta; ?></td>					
					</tr>
					<tr>
						<td>Time of Operation</td>
						<td><?php echo $transaction->sched_ceasar_to; ?></td>
					</tr>
					<tr>
						<td>C-Section Notes</td>
						<td><?php echo $transaction->sched_ceasar_notes; ?></td>
					</tr>
				</table>

				<table class="table table-sm table-borderless table-shrink" style="display: none" id="schedceasar_edit">
					<tr>
						<td width="30%">Date of C-Section</td>
						<td> 
							<div class="col-3">
								<div class="row">
									<input type="text" class="form-control form-control-sm" name="edit_sched_ceasar_d" id="edit_id_sched_d" data-toggle="datetimepicker" data-target="#edit_id_sched_d" value="<?php echo date('d/m/Y',strtotime(@$transaction->sched_ceasar)); ?>">
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Time of Admission</td>
						<td>  
							<div class="col-3">
								<div class="row">								
									<select class="custom-select custom-select-sm" name="edit_sched_ceasar_ta">
										<?php foreach(opt_date_time() as $t): ?>
											<option value="<?php echo $t; ?>" <?php echo ($transaction->sched_ceasar_ta==$t)?'selected="selected"':''; ?>  ><?php echo $t; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Time of Operation</td>
						<td> 
							<div class="col-3">
								<div class="row">								
									<select class="custom-select custom-select-sm" name="edit_sched_ceasar_to">
										<?php foreach(opt_date_time() as $t): ?>
											<option value="<?php echo $t; ?>" <?php echo ($transaction->sched_ceasar_to==$t)?'selected="selected"':''; ?> ><?php echo $t; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>C-Section Notes</td>
						<td> 
							<div class="col-6">
								<div class="row">
									<textarea class="form-control form-control-sm" name="edit_sched_ceasar_notes" required=""><?php echo $transaction->sched_ceasar_notes; ?></textarea>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="button" id="in_btn_save" class="btn btn-sm btn-primary pt-0 pb-0" onclick="Booking.editcsection_btn_toggle(<?php echo @$transaction->tran_id; ?>, 2);" >Save</button>
							<button type="button" id="in_btn_cancel" class="btn btn-sm btn-secondary pt-0 pb-0" onclick="Booking.editcsection_btn_toggle(<?php echo @$transaction->tran_id; ?>,3);"  >Cancel</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<?php endif; ?>

	<?php if( !empty($transaction->sched_induction) ): ?>
	<div class="row border booking-c-section mt-2 pt-1 bg-pink">
		<div class="col-sm-12">
			<h5>INDUCTION Schedule <button type="button" id="induction_btn_edit" class="btn btn-sm pt-0 pb-0" onclick="Booking.editinduction_btn_toggle(<?php echo @$transaction->tran_id; ?>,1);" > <i class="fas fa-edit"></i> Edit</button> </h5> 
		</div>

		<div class="col-sm-9">

			<?php echo form_open('', 'name="edit-sched-induction" id="edit-sched-induction" class="" '); ?>

				<table class="table table-sm table-borderless table-shrink" id="induction_display">
					<tr>
						<td width="30%">Date of Induction</td>
						<td><?php echo date('d/m/Y',strtotime(@$transaction->sched_induction)); ?></td>					
					</tr>
					<tr>
						<td>Time of Admission</td>
						<td><?php echo @$transaction->sched_induction_ta; ?></td>
					</tr>
					<tr>
						<td>Induction Type</td>
						<td><?php echo $transaction->sched_induction_type; ?>
						</td>
					</tr>
					<tr>
						<td>INDUCTION Notes</td>
						<td><?php echo $transaction->sched_induction_notes; ?></td>
					</tr>
				</table>

				<table class="table table-sm table-borderless table-shrink" style="display: none" id="induction_edit">
					<tr>
						<td width="30%">ate of Induction</td>
						<td> 
							<div class="col-3">
								<div class="row">
									<input type="text" class="form-control form-control-sm" name="edit_sched_induction_d" id="edit_id_sched_d" data-toggle="datetimepicker" data-target="#edit_id_sched_d" value="<?php echo date('d/m/Y',strtotime(@$transaction->sched_induction)); ?>">
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Time of Admission</td>
						<td>  
							<div class="col-3">
								<div class="row">								
									<select class="custom-select custom-select-sm" name="edit_sched_induction_ta">
										<?php foreach(opt_date_time() as $t): ?>
											<option value="<?php echo $t; ?>" <?php echo ($transaction->sched_induction_ta==$t)?'selected="selected"':''; ?>  ><?php echo $t; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Induction Type</td>
						<td> 
							<div class="col-3">
								<div class="row">								
										<div>
										<?php 

											foreach(opt_induction_type() as $key=>$val):

										?>
										<div class="custom-control custom-radio d-block" style="float:none !important">
										    <input type="radio" class="custom-control-input" id="induct_type_id_<?php echo $key; ?>" value="<?php echo $val; ?>" name="edit_sched_induction_type" <?php echo ($transaction->sched_induction_type==$val)?'checked="checked"':''; ?>>
										    <label class="custom-control-label" for="induct_type_id_<?php echo $key; ?>"><?php echo $val; ?></label>
										</div> 
										<?php endforeach; ?>
										</div>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>INDUCTION Notes</td>
						<td> 
							<div class="col-6">
								<div class="row">
									<textarea class="form-control form-control-sm" name="edit_sched_induction_notes" required=""><?php echo $transaction->sched_induction_notes; ?></textarea>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="button" id="in_btn_save" class="btn btn-sm btn-primary pt-0 pb-0" onclick="Booking.editinduction_btn_toggle(<?php echo @$transaction->tran_id; ?>, 2);" >Save</button>
							<button type="button" id="in_btn_cancel" class="btn btn-sm btn-secondary pt-0 pb-0" onclick="Booking.editinduction_btn_toggle(<?php echo @$transaction->tran_id; ?>,3);"  >Cancel</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<?php endif; ?>


	<div class="row border mt-2" id="booking-details-navbar">
		<!-- Caller Form, Hospital Form, Labour Progress, Collector, On Call --> 
		
		<div class="col-sm-12 pl-0 pr-0">
			<nav class="navbar navbar-expand-lg navbar-light bg-feijoa font-weight-bold">
		  		<a class="navbar-brand"  >Menu</a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
			  	</button>	
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						 
						<li class="nav-item box-callerform border border-light mr-1 rounded <?php echo ($section == 'caller')?'active':''?> ">
							<a class="nav-link" href="<?php echo $d_navbar_url.'&section=caller'; ?>">Caller Form</a>
						</li>

						<?php if( isset($transaction->tran_id) ) : ?>
							<li class="nav-item box-hospital border border-light mr-1 rounded <?php echo ($section == 'hospital')?'active':''?>">
								<a class="nav-link" href="<?php echo $d_navbar_url.'&section=hospital'; ?>">Hospital Form</a>
							</li>
							<li class="nav-item box-clienthospital border border-light mr-1 rounded <?php echo ($section == 'clienthospital')?'active':''?>">
								<a class="nav-link" href="<?php echo $d_navbar_url.'&section=clienthospital'; ?>">Client/Hospital</a>
							</li>
							<li class="nav-item box-labourprogress border border-light mr-1 rounded <?php echo ($section == 'labourprogress')?'active':''?>">
								<a class="nav-link" href="<?php echo $d_navbar_url.'&section=labourprogres'; ?>s">Labour Progress</a>
							</li>
							<li class="nav-item box-collector border border-light mr-1 rounded <?php echo ($section == 'collector')?'active':''?>">
								<a class="nav-link" href="<?php echo $d_navbar_url.'&section=collector'; ?>">Collector</a>
							</li>
							<li class="nav-item box-oncallnurse border border-light mr-1 rounded <?php echo ($section == 'oncall')?'active':''?>">
								<a class="nav-link" href="<?php echo $d_navbar_url.'&section=oncall'; ?>">On Call</a>
							</li>
							<li class="nav-item border border-light mr-1 rounded <?php echo ($section == 'callhistory')?'active':''?>">
								<a class="nav-link" href="<?php echo $d_navbar_url.'&section=callhistory'; ?>">Call History</a>
							</li>
						<?php endif; ?>
					</ul>

					<?php if( isset($transaction->tran_id) ) : ?>
					<ul class="navbar-nav ml-auto">
						<li class="nav-item "> 
							<a href="javascript:;" class="btn btn-sm btn-lightblue mr-1" onclick="BookingDetailsNavbar.smscollector_click_btn(event,<?php echo $transaction->tran_id; ?>)" ><i class="fas fa-sms"></i> SMS Collector</a>
						</li>
						<li class="nav-item "> 
							<a href="javascript:;" class="btn btn-sm btn-reminder" onclick="BookingDetailsNavbar.reminder_click_btn(event,<?php echo $transaction->tran_id; ?>)" ><i class="fas fa-business-time"></i> New Reminder</a>
						</li>
					</ul>
					<?php endif; ?> 
				</div>

			</nav>
 		
 		</div>
	</div>

</div>



<?php 

	if( $section == 'caller'){
		echo $this->load->view('pages/booking/section_callerform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}

	if( $section == 'hospital'){
		echo $this->load->view('pages/booking/section_hospitalform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}

	if( $section == 'labourprogress'){
		echo $this->load->view('pages/booking/section_labourprogressform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}
	
	if( $section == 'oncall'){
		echo $this->load->view('pages/booking/section_oncallform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}
	
	if( $section == 'collector'){
		echo $this->load->view('pages/booking/section_collectorform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}

	if( $section == 'callhistory'){
		echo $this->load->view('pages/booking/call_history', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}

	if( $section == 'clienthospital'){
		echo $this->load->view('pages/booking/section_clienthospitalform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}

	if( $section == 'cccverify'){
		echo $this->load->view('pages/booking/section_cccverifyform', array('customer'=>$customer, 'transaction'=>$transaction), TRUE);
	}
?>