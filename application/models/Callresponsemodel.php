<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Callresponsemodel extends CI_Model { 


    function insert($set){

        $this->db->db_debug = FALSE;

        try {

            
            if( empty($set) ) throw new Exception("Empty insert data");
            
            $call_response_id = $this->_add($set);

            if( $call_response_id > 0 ){

                $this->db->db_debug = TRUE;

                return $call_response_id;
            }else{
                            
                    $this->_add($set);
            
            }
            

        } catch (Exception $e) {
            return false;
        }

    }


    function _add($set){

        if( $this->db->insert('call_responses', $set) ){          
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
  

    function update($call_response_id, $set){

        $call_response_id = trim($call_response_id);

        try {
            
            if( empty($call_response_id) ) throw new Exception("call_response_id is required", 1);
            if( empty($set) ) throw new Exception("set param is required", 1);
                        
            $this->db->where('call_res_id', $call_response_id);
            return $this->db->update('call_responses', $set);

        } catch (Exception $e) {
            return false;
        }

    }

  	
  	function row($params){

  		try {
  			
 			if( empty($params) ) throw new Exception("Params is required", 1);

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

  			$query = $this->db->get('call_responses');

  			return $query->row();

  		} catch (Exception $e) {
  			return 0;
  		}
  	}

    /**
     * Basic Results
     * @param  array $params where, where_str, select, result
     * @return object/array/false
     */
    function basic_results($params){

      try {
        
      if( empty($params) ) throw new Exception("Params is required", 1);

      //where clause
      if(isset($params['where'])){
        $this->db->where($params['where']);
      }   

      if(isset($params['where_str']) && $params['where_str']!='' ){
        $this->db->where($params['where_str'], null, false);
      }   

      if(isset($params['select']) && $params['select']!='' ){
        $this->db->select($params['select'], null, false);
      }

        $query = $this->db->get('call_responses');

        return (isset($params['result']) AND @$params['result'] == 'array')?$query->result_array():$query->result();

      } catch (Exception $e) {
        return 0;
      }
    }


      function listing($params=array(), $paging=TRUE){

        try {       

          //if( empty($params) ) throw new Exception("Can't return full list", 1);
          
          if( $paging ){

            //TOTAL ROWS
            
            //where clause
            if(isset($params['where'])){
              $this->db->where($params['where']);
            }

            if(isset($params['where_str']) && $params['where_str']!='' ){
              $this->db->where($params['where_str'], null, false);
            }

            $this->db->select('count(*) as total');

            $query = $this->db->get('call_responses');
            
            $total_rows = $query->row()->total;
            $query->free_result(); //free results
          }
          
          //RESULTS
          
          //where clause
          if(isset($params['where'])){
            $this->db->where($params['where']);
          }   

          if(isset($params['where_str']) && $params['where_str']!='' ){
            $this->db->where($params['where_str'], null, false);
          }

          //limits
          if(isset($params['limits'])){
            $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
          }

          //sorting
          if( isset($params['sorting']) ){
            if( is_array($params['sorting']) ){
              $this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
            }else{
              $this->db->order_by($params['sorting']);
            }

          }else{
            //$this->db->order_by('`call_responses`.`fullname`', 'desc');
          }
/*          echo '<pre>';
          print_r($params);
          echo '<pre>';
          exit();*/
          $query = $this->db->get('call_responses');



          $result = $query->result();


          $query->free_result(); //free results

           
          return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
           

        } catch (Exception $e) {
          return false;
        }

  }

  
    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


    function get_audit_trail( $pro_id = ''){
    
      try{

      if($pro_id=='') throw new Exception("Error : pro_id is required", 1);

      $query = $this->db
              ->where('ref_table', 'client_procedure')
              ->where('ref_field','pro_id')
              ->where('ref_val',$pro_id)
              ->get('table_audit_trail'); 

       
      return $query->result(); 
      

    }catch(Exception $error){ 
      return  0;
    }
    }  

  

}