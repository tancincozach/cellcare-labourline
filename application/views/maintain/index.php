
<?php 
	
	//print_r($maintain);

?>

<div class="col-sm-2 maintain-menu">

    <?php echo $this->load->view('maintain/menu', array('menu_active'=>$maintain['menu_active']), true); ?>

</div>

<div class="col-sm-10 maintain-content">
    <?php echo $this->load->view($maintain['view_file'], @$maintain['data'], true); ?>
</div>