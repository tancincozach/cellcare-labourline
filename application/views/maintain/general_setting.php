<h1>General Setting</h1>

<div class="row">
	<div class="col-sm-6">
		<table class="table table-sm">			
			<thead>
				<tr>
					<th>Name</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Email</td>
					<td><?php echo $email==1?'<strong class="text-success">Yes</strong>':'<strong class="text-danger">No</strong>'; ?></td>
				</tr>
				<tr>
					<td>SMS</td>
					<td><?php echo $sms==1?'<strong class="text-success">Yes</strong>':'<strong class="text-danger">No</strong>'; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>