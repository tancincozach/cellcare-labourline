<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {
	
	public function __construct()	{

			parent::__construct();


		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/customers';	
		$this->load->model('Commonmodel','common');
		$this->load->model('Customermodel','customer');
	}

	public function index(){

		$data['fields']  = '';
		$data['results'] = '';




		$params = $this->input->get();



		if(isset($params['del'])){

			try {			

			$cust_id = $this->customer->delete($params['del']);

				if(empty($cust_id)){

					throw new Exception("Error: Unable to delete customer");
					
				}

				$this->session->set_flashdata('msg','Customer successfuly deleted.');

				redirect('maintain/customers');
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());
				
			}

		}

		if(isset($_FILES['customer_file']['tmp_name'])){

			try {

				$return = $this->upload_customers( $_FILES );

				if(array_key_exists('error',$return)){

					throw new Exception($return['error']);
					
				}
				if(array_key_exists('msg',$return)){

					$this->session->set_flashdata('msg',$return['msg']);
					
				}				
				
			} catch (Exception $e) {

				$this->session->set_flashdata('flash_error_msg',$e->getMessage());
				
			}

			redirect('maintain/customers');
			
		}


		try {

		$params = $this->input->get();


		$data = '';

		$data['filters'] = $params;


		$params = '';

			$query_params = '';
			$params_query = @http_build_query($params);

			$query_params['table'] = 'customers';
			$query_params['where']['type'] = 'customerfield';

			$result_array = array();

			$result_array  = $this->common->get_csv_table($query_params);

			//$data['results'] 	= $result_array['results'];

			if(array_key_exists('error',$result_array)){

				throw new Exception($result_array['error']);
				
			}


			$data['fields'] = @$result_array['custom_fields'];



			//$data['results'] = @$result_array['results'];


			

		} catch (Exception $e) {

			$this->session->set_flashdata('flash_error_msg',$e->getMessage());
			
		}

	/*	$this->view_data['css_file'] = '<link rel="stylesheet" type="text/css" href="assets/vendor/datatables/css/dataTables.bootstrap4.min.css"/>'; 
		$this->view_data['js_file'] .= '<script src="assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>'; 		*/

		$this->view_data['css_file'] = '<link rel="stylesheet" type="text/css" href="assets/vendor/datatables/css/dataTables.bootstrap4.css"/>'; 
		$this->view_data['js_file'] .= '<script src="assets/vendor/datatables/js/jquery.dataTables.min.js"></script>'; 		
		$this->view_data['js_file'] .= '<script src="assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>'; 		
		$this->view_data['js_file'] .= '<script src="assets/js/pages/customers.js"></script>'; 		

		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'customers';
		$this->load->view('template', $this->view_data);

	}

	public function customer_form( $cust_id = "" ){


		$data = '';

		$result_array = array();

		$customer = '';

		if($_POST){

			$post = $this->input->post();

			$in_arr = array();

			try {
				
				foreach ($post as $key => $value) {
						$in_arr[$key] = addslashes($value);
				}
				

				if(isset($post['cust_ID']) && $post['cust_ID']!=''){

					// update
					unset($in_arr['cust_ID']);
					
					if(!$this->customer->update($cust_id,$in_arr)){
						throw new Exception("Error : Unable to update customer");						
					}

					$this->session->set_flashdata('msg','Successfully updated a customer.');

				}else{

					if(!$this->customer->insert($in_arr)){
						throw new Exception("Error : Unable to add customer");
					}


					$this->session->set_flashdata('msg','Successfully added a customer.');

				}

					
			} catch (Exception $e) {

				$this->session->set_flashdata('error',$e->getMessage());		

			}
			
			redirect('maintain/customers');
		}




		if($cust_id){

			 $where['where']['CustomerID'] = $cust_id;

			 $customer = $this->customer->row( $where );
		}



		$data['customer'] = $customer;
		$query_params['table'] = 'customers';
		$query_params['where']['type'] = 'customerfield';

		$data['fields']   = $this->common->get_csv_table($query_params,true);


		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'customers';
		$this->view_data['maintain']['view_file'] = 'maintain/customer-form';
		$this->load->view('template', $this->view_data);

	}

	public function  upload_customers( $file_data ){


		$return = array();


		try {
			
			if(empty($file_data['customer_file']['tmp_name'])) throw new Exception("Error: No file to be uploaded.");			

			$params['file'] = 'customer_file';
			$params['table'] = 'customers';
			$params['where'] = array('type'=>'customerfield');

	        $this->common->import_csv_report($params);

			

			$return['msg'] = 'Upload Successful!.';

			
		} catch (Exception $e) {
			

			$return['error'] = $e->getMessage();
			
			
		}

		return $return;

	}

}

?>