<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header bg-palegreen">
                <h5 class="modal-title ">Alert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body text-center">
                <h5>We do not disturb client during the evening</h5>
                
                <br />

                <h4>Advise Client</h4>
                <p>
                    Please call immediate if labour starts or as soon as waters broken or 
                    membranes ruptured .. OR at 8am (local time), whichever comes first
                </p>

                <br />
                <h4>AGENT NOTE</h4>
                <p class="font-weight-bold">Set reminder for 8am (local time) for followup</p>
            </div>
            <div class="modal-footer bg-palegreen text-white">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
            </div>            
        </div>
    </div>
</div>