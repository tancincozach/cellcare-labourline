<?php
 
 /* ------------------------------------------------------------------------
 * Cma_external
 * ------------------------------------------------------------------------
 */

/**
 * Cma_external - CMA external connection class
 * @date_created	2015-05-20
 * @date_updated	
 */


class Cma_external{
	
	public $CI;
	public $dbconn = null; //db connection handler

	function __construct() {
       $this->CI =& get_instance();    
	}

   /**
   * Database settings
   * Set $dbconn
   * @access private
   * @param array $con 
   */
	function set_db_conn($con){
		
		try{		  

			$config['hostname'] = '192.168.209.204';
			$config['username'] = 'root';
			$config['password'] = 'A110uRdat4';
			//$config['database'] = '';

			if( isset($con['hostname']) ) $config['hostname'] = $con['hostname'];
			
			if( isset($con['database']) ) {
				$config['database'] = $con['database'];
			}else{
				throw new Exception('Class Cma_external: Database Name is not set');
			}
		 	 
		 	if( $_SERVER['SERVER_NAME'] == 'localhost' OR $_SERVER['SERVER_NAME'] == 'cellcare-labourline-test.welldone.net.au'  ){

				$config['hostname'] = 'localhost';

		 	}

			$config['dbdriver'] = "mysqli";
			$config['dbprefix'] = "";
			$config['pconnect'] = FALSE;
			$config['db_debug'] = TRUE;
			$config['cache_on'] = FALSE;
			$config['cachedir'] = "";
			$config['char_set'] = "utf8";
			$config['dbcollat'] = "utf8_unicode_ci";
			$config['autoinit'] = TRUE; 
			$config['stricton'] = FALSE;

			$this->dbconn = $this->CI->load->database($config, TRUE); 
			$this->dbconn->reconnect();

		} catch(Exception $e){
			echo 'Caught exception: '.  $e->getMessage(). "\n";
		}
	}


	/**
	*Retrieved record on the intensifier
	*@param $a type array
	*@param $tbl_type (events or followup) 
	**/	
	public function retrieve_intensifier($last_id){
 
		try {
			$insert_id = '';

			$db_config['database'] = 'intensifier';	
			$this->set_db_conn($db_config);

			if( $last_id == '' OR $last_id == '0' ){
				$sql = "SELECT * FROM `events` WHERE `added` > ".strtotime('now - 10 mins');
			}else{
				$sql = 'SELECT * FROM `events` WHERE `id` > '.$last_id;

			}

			//echo $sql;

		 	$query = $this->dbconn->query($sql);

		 	$result = $query->result();
			$this->dbconn->close(); 

		 	/*echo '<pre>';
		 	print_r($result);
		 	echo '</pre>';*/


			return $result;

		}catch(Exception $e){
			return 'Caught exception: '.  $e->getMessage()."\n";
		}
	}

	/**
	*Insert record to intensifer database
	*@param $a type array
	*@param $tbl_type (events or followup) 
	**/	
	public function insert_intensifier($a=array(), $tbl_type='followup'){
 
		try {
			$insert_id = '';

			$db_config['database'] = 'intensifier';	
			$this->set_db_conn($db_config);

			if(!isset($a['message'])){
				$a['message'] = "Test message from ".$_SERVER['SERVER_NAME'];
			}

			if(!isset($a['cust_id']) || !isset($a['cma_id'])){
				$a['site_id'] = 1;
				$a['cust_id'] = 172;
				$a['cma_id'] = 205;
				$a['cust_name'] = "Well Said Co";
				$a['name'] = "Test Name";
				$a['phone'] = "1111111111";
			}

			if(!isset($a['added'])){
					$a['added'] = date('U');
			}
			if(!isset($a['agent_name'])){
					$a['agent_name'] = "ExternalApp";
			}
			if(!isset($a['expiry'])){
				$a['expiry'] = date('U') + 1800;
			}


			$set['site_id'] 	= $a['site_id'];
			$set['cust_id'] 	= $a['cust_id'];
			$set['cma_id'] 		= $a['cma_id'];
			$set['msg_id'] 		= '0';
			$set['cust_name'] 	= @$a['cust_name'];
			$set['agent_name'] 	= @$a['agent_name'];
			$set['added'] 		= $a['added'];
			$set['expiry']	 	= $a['expiry'];
			$set['name'] 		= @$a['name'];
			$set['phone'] 		= @$a['phone'];
			$set['message'] 	= $a['message'];
			//$set['completed'] = '';
			$set['completed_timestamp'] = '0';
			$set['completed_agent'] 	= '';
			//$set['completed_msgid'] = '';

			/*if( $tbl_type == 1 ){
				
				$this->dbconn->insert('events', $set);

			}else if( $tbl_type == 2 ){
				
				$this->dbconn->insert('followup', $set);

			}*/

			$this->dbconn->insert($tbl_type, $set);

			$insert_id  = $this->dbconn->insert_id();

			$this->dbconn->close();

			$result_set['intensifier_tbl'] = $tbl_type;
			$result_set['intensifier_id'] = $insert_id;

			return $result_set;

		}catch(Exception $e){
			return 'Caught exception: '.  $e->getMessage()."\n";
		}
	}

	/**
	* $a = array('id', 'tbl_type', 'agent_name' )
	**/
	public function remove_intensifier($a){

		try{

			$db_config['database'] = 'intensifier';	
			$this->set_db_conn($db_config); 

			if( !isset($a['tbl_type']) ){
				throw new Exception('Intensifier table name does not exist');
			}

			$set['completed'] = 1;
			$set['completed_timestamp'] = date('U');
			$set['completed_agent'] = 'Deleted by: '.$a['agent_name'];

			$this->dbconn->where('id', $a['id']);
			return $this->dbconn->update($a['tbl_type'], $set);

		} catch(Exception $e) {
			echo 'Caught exception: '.  $e->getMessage(). "\n";
		}
	}

	public function insert_calls_log($a){

		try {

			if( !isset($a['cma_db']) OR $a['cma_db']=='' ){
				throw new Exception('CMA DB not set');
			}

			$db_config['database'] = $a['cma_db'];
			$this->set_db_conn($db_config);


			if(!isset($a['cust_id']) || !isset($a['contact_id']) || !isset($a['message'])){

				$a['cust_id'] 	= 172; // Well said co
				$a['contact_id']= 12480;
				$a['contact_id']= 12480;
				$a['message'] 	= "A test insert from ".$_SERVER['PHP_SELF'];

			}
			// $a['cust_id'] 	= 172; // Well said co
			// $a['contact_id']= 16405;

			if(!isset($a['sms_count'])){$a['sms_count'] = 0;}
			if(!isset($a['ob1_phone'])){$a['ob1_phone'] = "";}		
			if(!isset($a['ob1_name'])){$a['ob1_name'] = "";}

			$msg_counter = $this->next_cntr($a['cust_id']);

			$datetime_now	=	date("Y-m-d H:i:s");

			$message = @$a['message'];
			
			/*// Start strip out undesirable characters
			$badGuys = array(
				array(chr(10),"<br />"),
				array(chr(39),"&#39;"),
				array("\"",""),
				array(chr(96),""), 
				array(chr(127),""),
				array(chr(9)," "),
				array(" ".chr(38)." ", " &#38; ")); // remove specific bad guys
			for($t=0;$t<count($badGuys);$t++){$message = str_replace($badGuys[$t][0],$badGuys[$t][1],$message);}
			for($t=0;$t<32;$t++){$message = str_replace(chr($t),"",$message);} // remove control bad guys
			for($t=127;$t<255;$t++){$message = str_replace(chr($t),"",$message);} // remove control bad guys too
			*/
		
			$message = str_replace(array('<br/>', '<br />', '<br>'), PHP_EOL, $message);


			// End strip out undesirable characters

			$agent_id = 60;
			if( isset($a['agent_id']) AND !empty($a['agent_id']) ){
				$agent_id = $a['agent_id'];
			}
			
			$set['ParentMsgID'] 	= '0';
			$set['CustID'] 			= @$a['cust_id'];
			$set['ContactID'] 		= @$a['contact_id'];
			$set['Company'] 		= '';
			$set['Name'] 			= '';
			$set['Phone1'] 			= '';
			$set['Phone2'] 			= '';
			$set['Phone1_Area'] 	= ''; 
			$set['Phone2_Area'] 	= '';			
			$set['Message'] 		= $message;
			$set['xFax'] 			= '0';
			$set['xMail'] 			= '0';
			$set['xSms'] 			= '0';
			$set['xPager'] 			= '0';
			$set['xFaxProcessed'] 	= '0';
			$set['DateTime'] 		= $datetime_now;
			$set['DateTimeStart'] 	= $datetime_now;
			$set['AgentID'] 		= $agent_id;
			$set['MsgCounter'] 		= $msg_counter;
			$set['SecondsOnCall'] 	= '0';
			$set['StartTimestamp'] 	= '0';
			$set['sms_counter'] 	= $a['sms_count'];
			$set['ob1_phone'] 		= @$a['ob1_phone'];
			//$set['d'] 				= '';
			$set['ob1_name'] 		= $a['ob1_name'];
			//$set['email'] 			= '';
			//$set['client_code'] 	= '';

			$this->dbconn->insert('calls', $set);

			$insert_id  = $this->dbconn->insert_id();

			return $insert_id;

		}catch(Exception $e){
			return 'Caught exception: '.  $e->getMessage(). "\n";
			//return 0;
		}
		 

	}

	function next_cntr($cust_id){
		$row = $this->dbconn->query("SELECT `MsgCounter` FROM `customers` WHERE `CustID`= '".$cust_id."'")->row();
		$msg_counter = @$row->MsgCounter;

		$msg_counter++;

		$this->dbconn->query("UPDATE `customers` SET `MsgCounter`='".$msg_counter."' WHERE `CustID`='".$cust_id."'");

		return $msg_counter;
	} 
}

?>
