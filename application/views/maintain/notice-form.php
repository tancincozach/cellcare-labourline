<h1>Notice</h1>

<?php if($this->session->flashdata('msg')!=''):?>
<div class="alert alert-success" role="alert">
		 <?php echo $this->session->flashdata('msg');?>
</div>
<?php
endif; ?>
<?php 
if($this->session->flashdata('error')!=''):?>
	<div class="alert alert-danger" role="alert">
	 <?php echo $this->session->flashdata('error');?>
	</div>

<?php
endif;

 echo form_open('',' class="form-horizontal" method="POST"');
 ?>
	<input type="hidden" name="notice_id" value="<?php echo @$notice_row->id;?>">
	<input type="hidden" name="expiry_date_hidden" value="0000-00-00 00:00:00">

	<div class="row">
		<div class="col-md-8">

			<div class="table-responsive">
				
					<table class="table">
							<tbody>
								<tr>
									<td>Name</td>
									<td><input type="text" name="response_name" class="form-control form-control-sm" value="<?php echo @$notice_row->content_title;?>"></td>
								</tr>
								<tr>
									<td>Content</td>
									<td><textarea class="form-control" name="script_content" id="script_content"><?php echo @$notice_row->content_html;?></textarea></td>
								</tr>
								<tr>
									<td>Expiry</td>
									<td><input type="text" name="expiry_date" class="form-control form-control-sm" value="<?php echo isset($notice_row->expire_date) ? date('d/m/Y H:i:s',strtotime($notice_row->expire_date)):'';?>"></td>
								</tr>

								<tr>
									<td>Group</td>
									<td>
									<select class="form-select form-control form-control-sm" name="notice_group">
										<option value="1" <?php echo isset($notice_row->notice_group) && (int)$notice_row->notice_group==1 ? "SELECTED":"";?>>DEFAULT</option>
										<option value="2" <?php echo isset($notice_row->notice_group) && (int)$notice_row->notice_group==2 ? "SELECTED":"";?>>COLLECTOR</option>
									</select>
									</td>
								</tr>
								<tr>
									<td>Status</td>
									<td>
									<select class="form-select form-control form-control-sm" name="status">
										<option value="1" <?php echo isset($notice_row->notice_status) && (int)$notice_row->notice_status==1 ? "SELECTED":"";?>>Active</option>
										<option value="0" <?php echo isset($notice_row->notice_status) && (int)$notice_row->notice_status==0 ? "SELECTED":"";?>>Inactive</option>
									</select>
									</td>
								</tr>
						</tbody>
					</table>
			</div>
			
		</div>
	</div>

<div class="form-group">
	<div class="form-input col-md-4">
		<button type="submit" class="btn btn-info">Submit</button>
		<a href="maintain/notices" class="btn btn-primary">Cancel</a>
	</div>
</div>	
</form>