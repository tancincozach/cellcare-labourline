$(function(){
  
	(function update_time(){

		var tp = moment();

		var now;
		 

		/*if( timezone != 'Australia/Sydney' && timezone != '' ){
			now = moment.tz(timezone);
			//now = now.clone().tz(timezone);
		}else{
			now = moment.tz("Australia/Sydney");
		}*/
		//console.log(hcd_globals.timezone);
		now = moment.tz(hcd_globals.timezone);
	  
		days = now.format("ddd D/M/YYYY");
		time = now.format("HH:mm:ss");
				
		 
		//console.log(now+' @ '+hcd_globals.timezone);
		$('#clock_div').html(days+' <strong class="top-clock-time">'+time+'</strong> @ '+hcd_globals.timezone);
		

		setTimeout(update_time, 1000);

	})(); 

});