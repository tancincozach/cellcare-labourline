<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usermodel extends CI_Model { 


    function insert($set){

        $this->db->db_debug = FALSE;

        try {
            
            if( empty($set) ) throw new Exception("Empty insert data", 1);
            
            $user_id = $this->_add($set);

            if( $user_id > 0 ){

                $this->db->db_debug = TRUE;

                return $user_id;
            }else{
                            
                    $this->_add($set);
            
            }
            

        } catch (Exception $e) {
            return false;
        }

    }


    function _add($set){
                 

        if( $this->db->insert('users', $set) ){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
  

    function update($user_id, $set){

        $user_id = trim($user_id);

        try {
            
            if( empty($user_id) ) throw new Exception("user_id is required", 1);
            if( empty($set) ) throw new Exception("set param is required", 1);
                        
            $this->db->where('id', $user_id);
            return $this->db->update('users', $set);

        } catch (Exception $e) {
            return false;
        }

    }

  	
  	function row($params){

  		try {
  			
 			if( empty($params) ) throw new Exception("Params is required", 1);

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

  			$query = $this->db->get('users');

  			return $query->row();

  		} catch (Exception $e) {
  			return 0;
  		}
  	}

    /**
     * Basic Results
     * @param  array $params where, where_str, select, result
     * @return object/array/false
     */
    function basic_results($params){

      try {
        
      if( empty($params) ) throw new Exception("Params is required", 1);

      //where clause
      if(isset($params['where'])){
        $this->db->where($params['where']);
      }   

      if(isset($params['where_str']) && $params['where_str']!='' ){
        $this->db->where($params['where_str'], null, false);
      }   

      if(isset($params['select']) && $params['select']!='' ){
        $this->db->select($params['select'], null, false);
      }

        $query = $this->db->get('users');

        return (isset($params['result']) AND @$params['result'] == 'array')?$query->result_array():$query->result();

      } catch (Exception $e) {
        return 0;
      }
    }


      function listing($params=array(), $paging=TRUE){

        try {       

          //if( empty($params) ) throw new Exception("Can't return full list", 1);
          
          if( $paging ){

            //TOTAL ROWS
            
            //where clause
            
            if(isset($params['where'])){
              $this->db->where($params['where']);
            }

      

            if(isset($params['where_str']) && $params['where_str']!='' ){
              $this->db->where($params['where_str'], null, false);
            }

            $this->db->select('count(*) as total');

            $query = $this->db->get('users');
            
            $total_rows = $query->row()->total;
            $query->free_result(); //free results
          }
          
          //RESULTS
          
          //where clause
          
          if(isset($params['select'])){
              $this->db->select($params['select'],FALSE);
          }

          if(isset($params['where'])){
            $this->db->where($params['where']);
          }   

          if(isset($params['where_str']) && $params['where_str']!='' ){
            $this->db->where($params['where_str'], null, false);
          }

          //limits
          if(isset($params['limits'])){
            $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
          }

          //sorting
          if( isset($params['sorting']) ){
            if( is_array($params['sorting']) ){
              $this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
            }else{
              $this->db->order_by($params['sorting']);
            }

          }else{
            $this->db->order_by('`users`.`fullname`', 'desc');
          }

          
          $query = $this->db->get('users');

          $result = $query->result();


          $query->free_result(); //free results

           
          return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
           

        } catch (Exception $e) {
          return false;
        }

  }

  
    function insert_audit_trail( $params ) {   

        try
            {
            if(count($params)==0) throw new Exception("Error : Empty Parameter", 1);

            if(!isset($params['ref_table']))
                throw new Exception("Error : Table name must not be empty.");

            if(!isset($params['ref_field']) && !isset($params['ref_val']))
                throw new Exception("Error : Table index name and value  must not be empty.");      
            if(!isset($params['created_by']))
                throw new Exception("Error : Agent Name  must not be empty.");
            if(!isset($params['data_json']))
                throw new Exception("Error : Message Activity  must not be empty.");            

            return ($this->db->insert('table_audit_trail', $params))?$this->db->insert_id():0;

        }catch(Exception $error) { 
            return  $error->getMessage();
        }
    }


    function get_audit_trail( $pro_id = ''){
    
      try{

      if($pro_id=='') throw new Exception("Error : pro_id is required", 1);

      $query = $this->db
              ->where('ref_table', 'client_procedure')
              ->where('ref_field','pro_id')
              ->where('ref_val',$pro_id)
              ->get('table_audit_trail'); 

       
      return $query->result(); 
      

    }catch(Exception $error){ 
      return  0;
    }
    }  

  

}