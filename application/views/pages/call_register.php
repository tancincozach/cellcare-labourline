 
<div class="col-sm-12">

	<h5>Call Register</h5>

	
	<?php echo form_open('call_register', ' method="get" name="LabourProgress-form" id="LabourProgress-form"  '); ?>
		 
			<div class="row">
				<div class="col">
					<div class="input-group input-group-sm mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1">Search</span>
						</div>
					
                        <select name="search" class="custom-select">
                            <option value=""></option>
                            <option value="Calling for another Reason" <?php echo trim(@$_GET['search'] == 'Calling for another Reason')?'selected="selected"':''; ?>>Calling for another Reason</option>
                            <option value="Disconnect on Greeting" <?php echo trim(@$_GET['search'] == 'Disconnect on Greeting')?'selected="selected"':''; ?>>Disconnect on Greeting</option>
                            <option value="Wrong Number" <?php echo trim(@$_GET['search'] == 'Wrong Number')?'selected="selected"':''; ?>>Wrong Number</option>
                        </select>
                        <div class="input-group-append">
							<button type="submit" class="btn btn-primary btn-sm">Search</button>
							<a class="btn btn-secondary btn-sm" href="call_register">Clear</a>
						</div>					
					</div>
					
				</div>
				<div class="col">
					
				</div>
			</div>
		 
	</form>
	
</div>

<div class="col-sm-12">
  
    <table class="table table-sm">
        <thead>
            <tr class="bg-dark text-white">
                <th>Date</th>
                <th>Time</th>
                <th>Caller Name</th> 
                <th>Caller Phone</th> 
                <th>Type</th> 
                <th>Agent Name</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $row): 

                $dt = explode(' ', $row->tran_updated);
            ?>

            <tr>
                <td><?php echo date('d/m/Y',strtotime($dt[0])); ?></td>
                <td><?php echo date('H:i',strtotime($dt[1])); ?></td>
                <td><?php echo $row->CallerName; ?></td>
                <td><?php echo $row->CallerPhone; ?></td>
                <td><?php echo $row->tran_type; ?></td>
                <td><?php echo $row->agent_name; ?></td>
            </tr>

            <?php  endforeach; ?>
        </tbody>
    </table>

    <div class="row">
        <div class="col-sm-6 justify-content-start">
             <?php echo $showing; ?>
        </div>
        <div class="col-sm-6 d-flex justify-content-end">            
            <?php echo $links; ?>
        </div> 
    </div>

</div>