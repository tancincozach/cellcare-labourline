<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en" class="h-100">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<base href="<?php echo base_url(); ?>" />

	<!-- Bootstrap CSS -->
	<link href="assets/vendor/bootstrap-4.2.1/css/bootstrap.min.css" rel="stylesheet">

	<link href="assets/vendor/jquery/jquery-ui-1.12.1/default/jquery-ui.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">     -->    
	<link href="assets/vendor/fontawesome-free-5.6.3/css/all.min.css" rel="stylesheet">
	<link href="assets/css/styles.css?v=0.0.1" rel="stylesheet">

  <?php echo @$css_file; ?>

	<title>Cell Care Labour Line</title>


  <script type="text/javascript">
    
    var cookie_name = "<?php echo $this->config->item('sess_cookie_name'); ?>";

  </script>

</head>
<body class="d-flex flex-column h-100" >
   	
   	<div class="<?php echo isset($container)?$container:'container'; ?> pl-0 pr-0">
		<header class="blog-header py-3">
		    <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-3">
                    <img src="assets/images/logo1.png" height="46px">
                </div>
                <div class="col-6 text-center">
                    <h2>Cell Care Labour Line</h2>
                </div>
                <div class="col-3 d-flex justify-content-end align-items-center">
                    <div class="col-sm-6">
                        <strong>Callback # 1800 183 180</strong>
                    </div>
                    <div  class="col-sm-6" style="font-size: 10px">
                        Collector Times <br/>
                        Day  = 7am-7pm <br/>
                        Night  = 7pm-7am

                    </div>
                </div>
            </div>
	    </header>


        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Menu</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="dashboard">Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="current_calls">Current Calls</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="all_calls">All Calls</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="call_register">Call Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reminders">Reminders</a>
                    </li>
         
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Contacts
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php  if(!empty($this->contacts_results)):?>
                                <?php foreach( $this->contacts_results as $row): ?>
                                  <a class="dropdown-item" href="contacts/?id=<?php echo $row->contact_id;?>" target="_blank" ><?php echo $row->contact_title;?></a>                                                    
                                <?php endforeach;?>
                            <?php  endif;?>
                                  <a class="dropdown-item" href="contacts/collectors" target="_blank">Collectors</a> 
                        </div>
                    </li>
                </ul>
              <?php
                $notice_count = count($this->notices);
                $notices_collector_count = count($this->notices_collector);
              ?>
                <ul class="navbar-nav ml-auto mr-auto  notices-list">
                    <li class="nav-item dropdown">
                        <a class="nav-link  tool_tip" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" data-placement="top"   title="Notice" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"  <?php echo (!empty($this->notices)? ' style="color:red"':'');?>></i>

                                  <div>
                                    <span class="glyphicon glyphicon-bell"></span>
                                    <?php  echo ( $notice_count > 0)?'<span class="label-notice" style="background-color: #FFFF66; color:#000" >'. $notice_count.'</span>':''; ?> 
                                  </div>
      

                        </a>

                        <ul class="dropdown-menu">
                          <li class="head text-light bg-info">
                            <div class="row">
                              <div class="col-lg-12 col-sm-12 col-12">
                                <a href="notices/?grp=1" class="float-left text-light">View All</a>
                              </div>
                            </div>
                          </li>
                          <li>
                              <ul  class="notices-content" <?php echo (empty($this->notices) ? ' style="height:55px;overflow: hidden !important;"':'');?> >
                                  <?php 

                                    if(!empty($this->notices)):

                                        $ctr=0;
                                        foreach($this->notices as $row):    
                                        $bg = '';
                                        /*if(date('Y-m-d',strtotime($row->date_created))==date('Y-m-d')){
                                            $bg = 'bg-warning';
                                        }*/
                                  ?>
                                  <li class="notification-box <?php echo  $bg;?>">
                                    <div class="row">  
                                      <div class="col-lg-12 col-sm-12 col-md-12">                                   
                                        <div>
                                          <strong><?php  echo $row->content_title;?></strong>
                                          <?php  echo $row->content_html;?>
                                        </div>
                                        <small class="text-primary"><?php echo date('Y-m-d H:i:s',strtotime($row->date_created));?></small>
                                      </div>    
                                    </div>
                                  </li>
                              <?php     $ctr++;
                                        endforeach;
                                    else:
                                      ?>
                                   <li class="notification-box">
                                    <div class="row">  
                                      <div class="col-lg-12 col-sm-12 col-md-12 ">                                   
                                        <div>
                                            <label class="text-danger"><strong>No active notice(s) found.</strong></label>
                                        </div>
                       
                                      </div>    
                                    </div>
                                  </li>
                                      <?php
                                    endif;

                               ?>      
                              </ul>
                          </li>                                                                                                
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link <?php echo ($notices_collector_count > 0)?'myblink':''; ?> tool_tip" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"  data-placement="top" title="Collector Notice" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"  <?php echo (!empty($this->notices_collector)? ' style="color:red"':'');?>></i>

                                  <div>
                                    <span class="glyphicon glyphicon-bell"></span>
                                    <?php  echo ( $notices_collector_count > 0)?'<span class="label-notice" style="background-color: #FFFF66; color:#000" >'. $notices_collector_count.'</span>':''; ?> 
                                  </div>
      

                        </a>

                        <ul class="dropdown-menu collector_list">
                          <li class="head text-light bg-primary">
                            <div class="row">
                              <div class="col-lg-12 col-sm-12 col-12">
                                <a href="notices/?grp=2" class="float-left text-light" >View All</a>
                              </div>
                            </div>
                          </li>
                          <li>
                              <ul  class="notices-content" <?php echo (empty($this->notices_collector) ? ' style="height:55px;overflow: hidden !important;"':'');?> >
                                  <?php 

                                    if(!empty($this->notices_collector)):

                                        $ctr=0;
                                        foreach($this->notices_collector as $row):    
                                        $bg = '';
                                        /*if(date('Y-m-d',strtotime($row->date_created))==date('Y-m-d')){
                                            $bg = 'bg-warning';
                                        }*/
                                  ?>
                                  <li class="notification-box <?php echo  $bg;?>">
                                    <div class="row">  
                                      <div class="col-lg-12 col-sm-12 col-md-12">                                   
                                        <div>
                                          <strong><?php  echo $row->content_title;?></strong>
                                          <?php  echo $row->content_html;?>
                                        </div>
                                        <small class="text-primary"><?php echo date('Y-m-d H:i:s',strtotime($row->date_created));?></small>
                                      </div>    
                                    </div>
                                  </li>
                              <?php     $ctr++;
                                        endforeach;
                                    else:
                                      ?>
                                   <li class="notification-box">
                                    <div class="row">  
                                      <div class="col-lg-12 col-sm-12 col-md-12 ">                                   
                                        <div>
                                            <label class="text-danger"><strong>No active notice(s) found.</strong></label>
                                        </div>
                       
                                      </div>    
                                    </div>
                                  </li>
                                      <?php
                                    endif;

                               ?>      
                              </ul>
                          </li>                                                                                                
                        </ul>
                    </li>

                    <!-- <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)" onClick="StickyNotes.activate('activate')"><i class="fas fa-sticky-note"></i></a>
                    </li> -->
                </ul>
                <ul class="navbar-nav ml-auto"> 
                    <li class="nav-item">
                        <a class="nav-link" href="reporting">Reporting</a>
                    </li>
                    <?php if($this->session->userdata('user_lvl')!=51):?>
                    <li class="nav-item">
                        <a class="nav-link" href="maintain/generalsettings">Maintenance</a>
                    </li>
                    <?php endif;?>
                </ul>

         
            </div>
        </nav>
 
   	</div>

    <!-- <div id="mystickynote"></div> -->

    <main role="main" class="flex-shrink-0  mb-5">
        <div class="<?php echo isset($container)?$container:'container'; ?>">
            <div class="row">
            <?php $this->load->view($view_file, @$data); ?>
            </div>    
        </div>
    </main>


    <footer class="footer mt-auto py-3">
        <div class="container">
            <div class="row"> 
                <div class="col-sm-6"><span class="text-muted">&copy; 2019 Well Done International</span></div>
                <div class="col-sm-6 text-right">
                    <a href="logout">Logout</a>
                </div>
            </div>
        </div>
    </footer>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="assets/vendor/jquery/jquery-ui-1.12.1/default/jquery-ui.min.js"></script> 
    <script src="assets/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js"></script>
    <!-- <script src="assets/vendor/holder.min.js"></script> -->
     
    <script src="assets/vendor/moment/2.21.0/moment.min.js"></script>
    <script src="assets/vendor/moment/en-au.js"></script>
    
    <script src="assets/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>

    <script type="text/javascript" src="assets/vendor/jquery.cookie.js"></script>

    <script src="assets/js/apps.js?v=0.0.1"></script>

    <?php echo @$js_file; ?>

    <script type="text/javascript">
      $('.tool_tip').tooltip();  
    </script>
  </body>
</html>