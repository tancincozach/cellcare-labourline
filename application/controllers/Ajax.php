<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	public function __construct(){

		parent::__construct(); 
		 
	
		if(! $this->input->is_ajax_request()){
		   	exit('No direct script access allowed');
		}
	}


	/**
	 * [search_customer description]
	 * @url booking/search
	 * @js js/pages/ac_booking_search_customer.js [AUTOCOMPLETE customer]
	 * @return json
	 */
	public function search_customer(){

		try { 
			$this->load->model('Customermodel');

			$get = $this->input->get();
			$search = $get['name_startsWith'];


			$params['where_str'] = " CustomerID like '%".$search."%' OR concat(FirstName, ' ', LastName) like '%".$search."%'";
			$params['select'] = " concat(FirstName, ' ', LastName) as 'Name', CustomerID";
			$params['result'] = "array";
			$customer = $this->Customermodel->basic_results($params);
			
			echo json_encode($customer);

		} catch (Exception $e) {
			
		}
	}

	/**
	 * [search_customer description]
	 * @url booking/open
	 * @js js/pages/ac_booking_open_hospital.js [AUTOCOMPLETE hospital]
	 * @return json
	 */
	public function search_hospital(){

		try {
			
			$hosptals = array();

			$get = $this->input->get();
			$search = $get['name_startsWith'];


			$select = "Hospital, HospitalStreet, HospitalCity, HospitalRegion, HospitalPostalCode, HospitalPhone,
				concat(HospitalStreet, ' ',HospitalCity, ' ',HospitalRegion, ' ',HospitalPostalCode, ' ',HospitalPhone) as Address";

			$this->db->select($select);
			$this->db->like('Hospital', $search, 'both');
			$query = $this->db->get('hospitals');

			if( $query->num_rows() > 0 ){

				$hosptals = $query->result_array();

			}else{
				$this->load->model('Customermodel'); 

				$this->db->select($select);
				$this->db->like('Hospital', $search, 'both');
				$this->db->group_by('Hospital, HospitalStreet, HospitalRegion, HospitalPostalCode, HospitalPhone');
				$query = $this->db->get('customers');
				$hosptals = $query->result_array();
 

			}
			
			echo json_encode($hosptals);


		} catch (Exception $e) {
			
		}

	}

	public function modal_redirect(){

		try {
			$get = $this->input->get();
			
			$vars = $get;

			echo $this->load->view('pages/booking/modal/common_redirect', $vars, TRUE);

		} catch (Exception $e) {
			
		} 

	}

	public function collection_kit_modal(){

		try {
			$get = $this->input->get();
			
			$vars['CustomerID'] = $get['CustomerID'];
			
			if(isset($get['issue_type']))
				$vars['issue_type'] = @$get['issue_type'];

			$vars['tran_id'] = $get['tran_id'];
			$vars['button_text'] = 'Proceed to On-Call Nurse';			
			$content = '<p class="text-center">
				Advise Caller <br /><br /> 
				We will contact the On-Call Nurse who will call you and make arrangements to provide a kit.  <br /><br />
				Please keep your phone on and expect a return call within 15 minutes
				</p>';			

			$vars['content'] = $content;

			echo $this->load->view('pages/booking/modal/collection_kit', $vars, TRUE);

		} catch (Exception $e) {
			
		} 

	}

	public function caesar_induction_modal(){

		try {
			$get = $this->input->get();
			
			$vars['CustomerID'] = $get['CustomerID'];
			$vars['tran_id'] = $get['tran_id'];
			$vars['button_text'] = 'Proceed';		
				
			$content = 'Go to INDUCTION or CAESAR screen';			
			$vars['content'] = $content;

			echo $this->load->view('pages/booking/modal/caesar_induction', $vars, TRUE);

		} catch (Exception $e) {
			
		} 

	}


	public function induction_type_modal(){

		try {
		 	$vars = '';
			echo $this->load->view('pages/booking/modal/lp_induction_type', $vars, TRUE);

		} catch (Exception $e) {
			
		} 

	}


	/*public function labourprogress_question_save($type){

		try {
			$this->load->model('Transactionmodel');

			$post = $this->input->post();


			$CustomerID = $post['CustomerID'];
			$tran_id = $post['tran_id'];
 
			 

			$arr = array('status'=>'1', 'msg'=>'Save Successfully');
			switch ($type) {
				case 'Waters_Broken':
					
					$set = array();
					$set['waters_broken'] = $post['waters_broken'];					
					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$arr['status'] = 0;
						$arr['msg'] = 'Error on save';
					}
					break;
				case 'Epidural':
					$set = array();
					$set['epidural'] = $post['epidural'];					
					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$arr['status'] = 0;
						$arr['msg'] = 'Error on save';
					}
					break;
				case 'Hormone_Started':
					$set = array();
					$set['hormone_started'] = $post['hormone_started'];					
					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$arr['status'] = 0;
						$arr['msg'] = 'Error on save';
					}
					break;
				case 'Client_Status':
					$set = array();
					$set['client_status'] = $post['client_status'];					
					if( !$this->Transactionmodel->update($tran_id, $set) ){
						$arr['status'] = 0;
						$arr['msg'] = 'Error on save';
					}
					break;
				
				default:
					# code...
					break;
			}

			echo json_encode($arr);

		} catch (Exception $e) {
			
		}

	}*/




	public function important_notes_save(){

		try {
			$this->load->model('Transactionmodel');
			$this->load->model('Callsmodel');
			
			$arr = array('status'=>'1', 'msg'=>'Save Successfully');

			$post = $this->input->post(); 
			$tran_id = $post['tran_id'];
 
 			$set = array(); 
			$set['notes'] = $post['notes'];		
			$set['tran_updated'] = date('Y-m-d H:i:s');

			if( !$this->Transactionmodel->update($tran_id, $set) ){
				throw new Exception("Error on save", 1);
			}
					
			$callset = array();
			$callset['tran_id'] 		= $tran_id; 
			$callset['more_text'] 		= json_encode(array('Important Notes'=>$post['notes']));  
			$callset['call_type'] 		= 'update';
			$callset['agent_name'] 		= $this->user->name;
			$callset['user_id'] 		= $this->user->agent_id;

			if( !$call_id = $this->Callsmodel->insert($callset) ){

			}
		 

			echo json_encode($arr);

		} catch (Exception $e) {
			$arr['status'] = 0;
			$arr['msg'] = $e->getMessage();
			echo json_encode($arr);
		}

	}

	public function ObsHistoryNotes_edit_save(){

		try {
			$this->load->model('Transactionmodel');
			$this->load->model('Callsmodel');
			
			$arr = array('status'=>'1', 'msg'=>'Save Successfully');

			$post = $this->input->post(); 
			$tran_id = $post['tran_id'];
 
 			$set = array(); 
			$set['ObsHistoryNotes_edit'] = $post['notes'];		
			$set['tran_updated'] = date('Y-m-d H:i:s');

			if( !$this->Transactionmodel->update($tran_id, $set) ){
				throw new Exception("Error on save", 1);
			}
					
			$callset = array();
			$callset['tran_id'] 		= $tran_id; 
			$callset['more_text'] 		= json_encode(array('Obstetric Notes'=>$post['notes']));  
			$callset['call_type'] 		= 'update';
			$callset['agent_name'] 		= $this->user->name;
			$callset['user_id'] 		= $this->user->agent_id;

			if( !$call_id = $this->Callsmodel->insert($callset) ){

			}
		 

			echo json_encode($arr);

		} catch (Exception $e) {
			$arr['status'] = 0;
			$arr['msg'] = $e->getMessage();
			echo json_encode($arr);
		}

	}



	public function csection_edit_save(){

		try {
			$this->load->model('Transactionmodel');
			$this->load->model('Customermodel');
			$this->load->model('Callsmodel');
			
			$arr = array('status'=>'1', 'msg'=>'Save Successfully');

			$post = $this->input->post(); 
			$post = $post['data'];
			// echo $post['edit_sched_ceasar_d'];
			// print_r($post);
			$tran_id = $post['tran_id'];
 
 			$set = array(); 
			$set['sched_ceasar'] = format_datetime($post['edit_sched_ceasar_d']);	
			$set['sched_ceasar_ta'] = $post['edit_sched_ceasar_ta'];		
			$set['sched_ceasar_to'] = $post['edit_sched_ceasar_to'];		
			$set['sched_ceasar_notes'] = addslashes($post['edit_sched_ceasar_notes']);
			$set['tran_updated'] = date('Y-m-d H:i:s');

			if( !$this->Transactionmodel->update($tran_id, $set) ){
				throw new Exception("Error on save", 1);
			}
			
			$more_text = array();
			$more_text['Date of C-Section'] = $post['edit_sched_ceasar_d'];
			$more_text['Time of Admission'] = $post['edit_sched_ceasar_ta'];
			$more_text['Time of Operation'] = $post['edit_sched_ceasar_to'];
			$more_text['C-Section Notes'] = $post['edit_sched_ceasar_notes'];

			$callset = array();
			$callset['tran_id'] 		= $tran_id; 
			$callset['more_text'] 		= json_encode($more_text);  
			$callset['call_type'] 		= 'update';
			$callset['agent_name'] 		= $this->user->name;
			$callset['user_id'] 		= $this->user->agent_id;

			if( $call_id = $this->Callsmodel->insert($callset) ){

				$tran_row = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));
				$customer = $this->Customermodel->details(array('where'=>array('CustomerID'=>$tran_row->CustomerID)));

				$template = $this->Commonmodel->booking_sms_template($tran_row, $customer);
				$message = @$template->message;
				$sched_type = @$template->sched_type;


				//echo $message;
				$email_param = array(); 
				$email_param['tran_id'] = $tran_id;							
				$email_param['call_id'] = $call_id;							
				$email_param['to']		= $tran_row->collector_phone;
				$email_param['message']	= $message;
				$this->Commonmodel->send_sms($email_param);		

			}
		 

			echo json_encode($arr);

		} catch (Exception $e) {
			$arr['status'] = 0;
			$arr['msg'] = $e->getMessage();
			echo json_encode($arr);
		}

	}

	public function induction_edit_save(){

		try {
			$this->load->model('Transactionmodel');
			$this->load->model('Callsmodel');
			
			$arr = array('status'=>'1', 'msg'=>'Save Successfully');

			$post = $this->input->post(); 
			$post = $post['data'];
			// echo $post['edit_sched_ceasar_d'];
			// print_r($post);
			$tran_id = $post['tran_id'];
 
 			$set = array(); 
			$set['sched_induction'] = format_datetime($post['edit_sched_induction_d']);	
			$set['sched_induction_ta'] = $post['edit_sched_induction_ta'];		
			$set['sched_induction_type'] = $post['edit_sched_induction_type'];		
			$set['sched_induction_notes'] = addslashes($post['edit_sched_induction_notes']);
			$set['tran_updated'] = date('Y-m-d H:i:s');

			if( !$this->Transactionmodel->update($tran_id, $set) ){
				throw new Exception("Error on save", 1);
			}
			
			$more_text = array();
			$more_text['Date of Induction'] = $post['edit_sched_induction_d'];
			$more_text['Time of Admission'] = $post['edit_sched_induction_ta'];
			$more_text['Induction Type'] = $post['edit_sched_induction_type'];
			$more_text['INDUCTION Notes'] = $post['edit_sched_induction_notes'];

			$callset = array();
			$callset['tran_id'] 		= $tran_id; 
			$callset['more_text'] 		= json_encode($more_text);  
			$callset['call_type'] 		= 'update';
			$callset['agent_name'] 		= $this->user->name;
			$callset['user_id'] 		= $this->user->agent_id;

			if( !$call_id = $this->Callsmodel->insert($callset) ){

			}
		 

			echo json_encode($arr);

		} catch (Exception $e) {
			$arr['status'] = 0;
			$arr['msg'] = $e->getMessage();
			echo json_encode($arr);
		}

	}

	public function labourprogress_selector(){

		try {
			
			$post = $this->input->post();

			$baby = @$post['baby'];
			$frequency = '';
			$length = '';

			if( $baby == 'Multi' ){
				$frequency = trim(@$post['freq_contraction']);
				$length = trim(@$post['length_contraction']);
			}

			$dilation = trim(@$post['cent_dilated']);
			
			//echo is_numeric($dilation)?1:0;
			// $baby = 'Multi';
			// $frequency = '2';
			// $length = 'g40';
			// $dilation = '9';
			//$post['custom_pselector'] = 'Client in labour or possibility of ceasarean';
 
			if( isset($post['custom_pselector']) ){
				$this->db->where('client_status', $post['custom_pselector']);
			}else{
				//$this->db->where('client_status != ', 'Urge to push OR ready to push');
				//$this->db->where('client_status != ', 'Client in labour or possibility of ceasarean');
				$this->db->where('search_criteria != ', '');
				$this->db->where('baby', $baby);
			}
			$this->db->where('ps_status', '1');
			$progress = $this->db->get('progress_selector')->result();
			//debug_print($progress);
			$result_display = array(); 

			if( isset($post['custom_pselector']) ){
				$result_display[] = $progress[0];
			}else{
				 
				foreach ($progress as $key=>$row) {
					//echo '<pre>';
					$counter = 0;
					//$c_success = $row->criteria_count;

					$c_dilation = explode(',', $row->c_dilation); 
					$c_freq = explode(',', $row->c_freq);
					$c_length = explode(',', $row->c_length);

					if( in_array($dilation, $c_dilation) ) { $counter++; }
					
					if( $baby == 'Multi' ){
						if( in_array($frequency, $c_freq) ){ $counter++; }
						if( in_array($length, $c_length) ){ $counter++; }
					}

					if( $counter == $row->criteria_count) {
						$result_display[] = $row;
						break;
					}

					
					// print_r($c_dilation);
					// print_r($c_freq);
					// print_r($c_length);
					// //print_r($row);
					// echo 'counter:'.$counter.' of '.$row->criteria_count.' <br />';
					// echo '</pre>';
				}

			}


			//echo $counter;
			//debug_print($result_display);

			if( empty($result_display) ){

				$this->db->where('client_status', 'Does not fit into above criteria');
				$progress = $this->db->get('progress_selector')->result();

				$result_display = $progress;
			}


			//ksort($result_display);
			$vars['post_data'] = $post;
			$vars['progress'] = $result_display;
			echo $this->load->view('pages/booking/modal/progress_selector', $vars, TRUE);

		} catch (Exception $e) {
			
		}

	}	



	/*public function labourprogress_selector(){

		try {
			
			$post = $this->input->post();

			$baby = @$post['baby'];
			$freq_contraction = '';
			$length_contraction = '';

			if( $baby == 'Multi' ){
				$freq_contraction = @$post['freq_contraction'];
				$length_contraction = @$post['length_contraction'];
			}

			$cent_dilated = @$post['cent_dilated'];
			
			//echo is_numeric($cent_dilated)?1:0;
			// $baby = 'Multi';
			// $freq_contraction = '';
			// $length_contraction = '';
			// $cent_dilated = '2';
			//$post['custom_pselector'] = 'Client in labour or possibility of ceasarean';

			// debug_print('baby: '.$baby);
			// debug_print('freq_contraction: '.$freq_contraction);
			// debug_print('length_contraction: '.$length_contraction);
			// debug_print('cent_dilated: '.$cent_dilated);

			if( isset($post['custom_pselector']) ){
				$this->db->where('client_status', $post['custom_pselector']);
			}else{
				//$this->db->where('client_status != ', 'Urge to push OR ready to push');
				//$this->db->where('client_status != ', 'Client in labour or possibility of ceasarean');
				$this->db->where('search_criteria != ', '');
				$this->db->where('baby', $baby);
			}
			
			$progress = $this->db->get('progress_selector')->result();
			//debug_print($progress);
			$result_display = array();
			$result_display2 = array();

			if( isset($post['custom_pselector']) ){
				$result_display[100] = $progress[0];
			}else{

				foreach ($progress as $key=>$row) {
					$criteria = (array)json_decode($row->search_criteria);
					//echo $row->search_criteria;
					//print_r($criteria);
					//debug_print($criteria);

					//echo $key.' '.PHP_EOL;
					if( empty($criteria) ){
						//$result_display[$key+30] = $row;
					}else{

						 
					 	$counter = 0;
					 	$total_c = 0;
						foreach (@$criteria as $k=>$c) {
							$c = (array) $c;
							$total_c += count($c);
							//debug_print($k);
							// echo '-----------';
							//debug_print($c); 
							// echo '-----------';

							switch ($k) {
								case 'freq_contraction': 

									if( isset($c['lt']) ){
										$counter += (is_numeric($freq_contraction) AND $freq_contraction < $c['lt']) ? 1: 0;
									}
									
									if( isset($c['lte']) ){
										$counter += (is_numeric($freq_contraction) AND $freq_contraction <= $c['lte']) ? 1: 0;
									}
									
									if( isset($c['gt']) ){
										$counter += (is_numeric($freq_contraction) AND $freq_contraction > $c['gt']) ? 1: 0;
									}
									
									if( isset($c['gte']) ){
										$counter += (is_numeric($freq_contraction) AND $freq_contraction >= $c['gte']) ? 1: 0;								
									}
									
									if( isset($c[0]) AND @$c[0] ==  $freq_contraction ) $counter += 1;  

									break;
								case 'length_contraction':
									
									 
									//if( isset($c['l40']) ) $counter += ($length_contraction < 40) ? 1: 0;
									//if( isset($c['g40']) ) $counter += ($length_contraction > 40) ? 1: 0;
									$counter += ( isset($c[0]) AND $c[0] ==  $length_contraction )?1:0;  
									 

									break;
								case 'cent_dilated':
									 
									if( isset($c['lt']) ){
										$counter += (is_numeric($cent_dilated) AND $cent_dilated < $c['lt'] ) ? 1: 0; 
									}
									
									if( isset($c['lte']) ){
										$counter += (is_numeric($cent_dilated) AND $cent_dilated <= $c['lte'] ) ? 1: 0;
									}
									if( isset($c['gt']) ){
										$counter += (is_numeric($cent_dilated) AND $cent_dilated > $c['gt']) ? 1: 0;
									}

									if( isset($c['gte']) ) {
										$counter += (is_numeric($cent_dilated) AND $cent_dilated >= $c['gte']) ? 1: 0;
									}

									if( isset($c[0]) AND @$c[0] ==  $cent_dilated ) $counter += 1;  	
									
									//if $counter += ( $c[0] ==  $cent_dilated )?1:0;  
						 			 
									break;
								case 'cent_dilated_full':
									if( @$c[0] ==  'full' ) $counter += 1;  	

									break;
								default:
								 
									break;
							}

						}
						// echo PHP_EOL.$counter.PHP_EOL;
						// echo PHP_EOL.$total_c.PHP_EOL;
						// echo '------------------'.PHP_EOL;
						if( $counter == $total_c ) $result_display[$key] = $row;

					}

				}

			}



			//echo $counter;
			//debug_print($result_display);

			if( empty($result_display) ){

				$this->db->where('client_status', 'Does not fit into above criteria');
				$progress = $this->db->get('progress_selector')->result();

				$result_display = $progress;
			}


			ksort($result_display);
			$vars['post_data'] = $post;
			$vars['progress'] = $result_display;
			echo $this->load->view('pages/booking/modal/progress_selector', $vars, TRUE);

		} catch (Exception $e) {
			
		}

	}*/
	

	/**
	 * This ajax post request was used from booking.js LabourProgress myObject.advise_script_save.
	 * This is will save the script, it will update the previous call with the selected Advise script.
	 * @return string
	 */
	public function labourprogress_advicescript_save(){

		try {
			
			$post = $this->input->post(); 
			$call_id = $post['call_id'];

			$this->db->where('call_id', $call_id);
			$call = $this->db->get('calls')->row();

			$more_text_json = json_decode($call->more_text);
			$advise_script = (array)explode('; ', $more_text_json->advise_script); 

			$merge = array_merge(array_values($advise_script), $post['advise_script']);

			$more_text_json->advise_script = implode('; ', $merge);

			$set['more_text'] = json_encode($more_text_json);
			$this->db->where('call_id', $call_id);
			$this->db->update('calls', $set);

			echo 'Advised script Successfully updated';
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}


	public function reminder_modal_form(){

		$this->load->model('Transactionmodel');
		$this->load->model('Customermodel');

		try {
			
			$data = array();

			$get = $this->input->get(); 

			$tran_id = $get['tran_id'];

			$transaction = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));
			$customer = $this->Customermodel->details(array('where'=>array('CustomerID'=>$transaction->CustomerID)));

			$list = $this->db->get('list_remindertypes');
			$data['reminder_type'] = $list->result();

			$data['transaction'] = $transaction;
			$data['customer'] = $customer;

			echo $this->load->view('pages/booking/modal/reminder', $data, TRUE);

		} catch (Exception $e) {
			
		} 

	}

	public function reminder_save(){

		$this->load->library("Cma_external");
		$this->load->model('Transactionmodel');
		$this->load->model('Sitesettingsmodel');
		$this->load->model('Callsmodel');

		$jsonobj = array('status'=>1);

		try {
			$cma = $this->Sitesettingsmodel->get_row(array('name'=>'CMA_SETTINGS'));

			$post = $this->input->post();

			$tran_id	= $post['tran_id'];
			$timezone 	= $post['timezone'];
			$state 		= $post['state'];
			$message = addslashes($post['message'].' - '.$post['reminder_type']);


			$CustomerLocalDateTime = '';
			$set = array();

			if( isset($post['reminder_expirey1']) ) {
				//$set['reminder'] = format_datetime($post['reminder_expirey1']);

				$reminder_expirey1 = format_datetime($post['reminder_expirey1']);

				$remindertime = $this->Commonmodel->customer_local_server($reminder_expirey1, $timezone);

				$set['reminder'] = $remindertime;

				$CustomerLocalDateTime = $reminder_expirey1;
				$ServerDateTime = $remindertime;
			}

			if( isset($post['reminder_expirey2']) ) {

				$reminder_expirey2 = date('Y-m-d H:i:s', strtotime('now + '.$post['reminder_expirey2'].' minute '));
				$remindertime = $this->Commonmodel->customer_server_local($reminder_expirey2, $timezone);

				$set['reminder'] = $reminder_expirey2;

				$CustomerLocalDateTime = $remindertime;
				$ServerDateTime = $reminder_expirey2;
			}
			 
			if( !$this->Transactionmodel->update($tran_id, $set) ) throw new Exception("Error adding reminder", 1);

			$callset = array();
			$callset['tran_id'] 		= $tran_id;

			$more_text = array();
			$more_text['State'] = $state;
			$more_text['CustomerLocalDateTime'] = $CustomerLocalDateTime;
			$more_text['ServerDateTime'] = $ServerDateTime;
			$more_text['Reminder'] = $set['reminder'];
			$more_text['Message'] = $message;

			$callset['more_text'] 		= json_encode($more_text);  
			$callset['call_type'] 		= 'reminder';
			$callset['agent_name'] 		= $this->user->name;
			$callset['user_id'] 		= $this->user->agent_id;

			$call_id = $this->Callsmodel->insert($callset);


			$cma = json_decode($cma->value);
			
			$a = array();

			$a['message'] 	= $message;
			$a['site_id'] 	= $cma->site_id;  
			$a['cust_id'] 	= $cma->cust_id;  
			$a['cma_id'] 	= $cma->cma_id;  
			$a['cust_name'] = $cma->cust_name; 
			$a['agent_name']= $this->user->name; 
			$a['expiry'] 	= strtotime($set['reminder']);

			$intensifier_res = $this->cma_external->insert_intensifier($a, 'events');
	 	
	 		if( !isset($intensifier_res['intensifier_id']) ) {

				$jsonobj['status'] = 0;
				$jsonobj['error_msg'] = 'Failed to send reminder to Escalation System';
	 			
	 		}


	        $audit = array(
	            'audit_type' => 'intensifier',
	            'audit_status' => isset($intensifier_res['intensifier_id'])?1:0,
	            'tran_id' => @$tran_id,
	            'call_id' => @$call_id,
	            'message' => 'Reminder: '.$set['reminder'].' Message: '.$message
	        );

	        $this->Commonmodel->insert_audit_trail($audit);


		 	echo json_encode($jsonobj);

		} catch (Exception $e) {
			$jsonobj['status'] = 0;
			$jsonobj['error_msg'] = $e->getMessage();

			echo json_encode($jsonobj);
		}

	}

	public function smscollector_modal_form(){

		$this->load->model('Transactionmodel');
		$this->load->model('Customermodel');

		try {
			$get = $this->input->get(); 

			$tran_id = $get['tran_id'];

			$transaction = $this->Transactionmodel->row(array('where'=>array('tran_id'=>$tran_id)));
			$customer = $this->Customermodel->details(array('where'=>array('CustomerID'=>$transaction->CustomerID)));

			$data = array();
			$data['transaction'] = $transaction;
			$data['customer'] = $customer;


			$_msg = array();
			$_msg[] = "Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;
			$_msg[] = $transaction->CustomerID;
			$_msg[] = $customer->MobilePhone;
			$_msg[] = $customer->Hospital;
			$_msg[] = $customer->EDD;
			$_msg[] = 'CBT';
			$_msg[] = $customer->PartnersMobilePhone;
			$_msg[] = $customer->PartnersFirstName;
			$_msg_f = array_filter($_msg);
			$data['message'] = implode(' - ', $_msg_f);


			echo $this->load->view('pages/booking/modal/sms_collector', $data, TRUE);

		} catch (Exception $e) {
			
		} 

	}

	public function smscollector_save(){
 
		$this->load->model('Transactionmodel'); 
		$this->load->model('Callsmodel');

		$jsonobj = array('status'=>1);

		try {

			$post = $this->input->post();

			$tran_id = trim($post['tran_id']);
			$CustomerID = trim($post['CustomerID']);

			$caller_name = trim($post['caller_name']);
			$caller_phone = trim($post['caller_phone']);
			$message = trim($post['sms_message']);

			$callset = array();
			$callset['tran_id'] 		= $tran_id;
			$callset['caller_name'] 	= $caller_name;
			$callset['caller_phone'] 	= $caller_phone;
			$callset['more_text'] 		= json_encode( array('SMS'=>$message) );
			 
			$callset['call_type'] 		= 'manualsms';
			$callset['agent_name'] 		= $this->user->name;
			$callset['user_id'] 		= $this->user->agent_id;

			$call_id = $this->Callsmodel->insert($callset);

			$email_param = array(); 
			$email_param['tran_id'] = $tran_id;							
			$email_param['call_id'] = $call_id;							
			$email_param['to']		= $caller_phone;
			$email_param['message']	= $message;

			$sms_status = $this->Commonmodel->send_sms($email_param);

			if( $sms_status != 'message_sent' ){

				$jsonobj['status'] = 0;
				$jsonobj['error_msg'] = 'Failed to send SMS';

			}

		
			echo json_encode($jsonobj);

		} catch (Exception $e) {
			
			$jsonobj['status'] = 0;
			$jsonobj['error_msg'] = $e->getMessage();

			echo json_encode($jsonobj);
		}

	}

	public function autosubmit_save(){
 
		$this->load->model('Transactionmodel');  

		$jsonobj = array('status'=>1);

		try {

			$post = $this->input->post(); 
			  
 
 			$set = array(); 
			$set['agent_name'] 		= $this->user->name; 
			$set['tran_status']		= 0; 
			$set['tran_type'] 		= $post['tran_type']; 
			$set['tran_updated'] = date('Y-m-d H:i:s');
			
			if( !$this->Transactionmodel->insert($set) ){
				throw new Exception("Error on save", 1);
			}

		
			echo json_encode($jsonobj);

		} catch (Exception $e) {
			
			$jsonobj['status'] = 0;
			$jsonobj['error_msg'] = $e->getMessage();

			echo json_encode($jsonobj);
		}

	}

	function app_eligible_modal(){
		$vars = '';

		$get = $this->input->get();

		if( isset($get['tran_id'])  AND @$get['tran_id'] > 0 ){
			
			$set = array();
			$set['app_elligible_shown'] = 1;
			$this->db->where('tran_id', $get['tran_id']);
			$this->db->update('transaction', $set);
		}

		echo $this->load->view('pages/booking/modal/app_popup', $vars, TRUE);	

	}

	function collector_distance_modal(){
		$vars = '';
		echo $this->load->view('pages/booking/modal/collector_distance_popup', $vars, TRUE);	

	}

	public function customers(){

		$this->load->model('Customermodel','customer');

			
		$result_array = array();

		$params = $this->input->get();


		$params_query = @http_build_query($params);

		$query_params = array();

		$post = $this->input->post();


		$page = @$post['start'];
		$per_page =  @$post['length'];

		$query_params['col_index'] = @$post['order'][0]['column']; // Column index
		$query_params['col_name'] = @$post['columns'][$col_index]['data']; // Column name
		$query_params['col_sort'] = @$post['order'][0]['dir']; // asc or desc
		$query_params['search_value'] = @$post['search']['value']; // Search value
		$query_params['order'] = @$post['order'];


		$result  = $this->customer->ajax_result_with_pagination($query_params);	

		$total_rows = $result['total_records'];

		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 
		$query_params['sorting']['order'] = $query_params['col_sort'];
		$query_params['sorting']['sort'] = $query_params['col_name'];

		$result  = $this->customer->ajax_result_with_pagination($query_params);	


		$temp_arr = array();

		foreach ($result as $row) {
			$new_array = array();
			$options  = '<a href="maintain/customers/customer_form/'.$row->CustomerID.'" class="btn  btn-info btn-sm"><i class="fas fa-pencil-alt"></i></a>&nbsp;';
			$options .= '<a alt="delete" onclick="return Misc.confirm(\'Are you sure to delete this customer ?\');" href="maintain/customers/?del='.$row->CustomerID.'" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></a>';
			$new_array['Options'] = $options;

			$temp_arr[]=  array_merge($new_array,(array)$row);
		
		}
		$result['draw'] =  intval( @$_REQUEST['draw'] );			
		$result['iTotalRecords'] = 1;
		$result['iTotalDisplayRecords' ] =  $total_rows ;

		$result['data'] = $temp_arr;		
		echo json_encode($result);
		
	}

}
