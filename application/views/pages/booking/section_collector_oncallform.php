
<div class="col-sm-12 mb-5">
	<div class="row border mt-1 ">
		
		<div class="col-sm-12">
			<h4 class="text-center">Call Activity - Collector On Call </h4>
		</div>

		<div class="col-sm-8 ml-auto mr-auto">
		 	<table class="table table-sm table-borderless bg-secondary text-white text-center">
				<tr>
					<th rowspan="2" class="text-center align-middle">CALLER DETAILS</th>
					<th>NAME</th>
					<th>PHONE</th>
					<th>RELATIONSHIP</th>
				</tr>
				<tr>
					<td><?php echo @$transaction->CallerName; ?></td>
					<td><?php echo @$transaction->CallerPhone; ?></td>
					<td><?php echo @$transaction->ClientRelation; ?></td> 
				</tr>
			</table>
		</div>

		<?php echo form_open('booking/save', 'name="OnCall-form" id="OnCall-form" class="col-sm-12" onsubmit="return Booking.confirm()" '); ?>
		
	 		<input type="hidden" name="section" value="oncall">
	 		<input type="hidden" name="CustomerID" value="<?php echo @$customer->CustomerID; ?>">
	 		<input type="hidden" name="tran_id" value="<?php echo @$transaction->tran_id; ?>">	

		 
 			<div class="row">
 				<div class="col-4">
 					<table class="table table-sm table-borderless">
 						<tr>
 							<td width="40%"><!-- Caller Type --></td>
 							<td width="60%">
 								<!-- <select name="call_type" required="">
 									<option value="On Call"></option> 									
 								</select> -->
 								&nbsp;
 							</td>
 						</tr>
 						<tr>
 							<td>Name of Caller</td>
 							<td>
 								<input type="text" class="form-control form-control-sm"  name="caller_name">
 							</td>
 						</tr>
 						<tr>
 							<td>Phone</td>
 							<td>
 								<input type="text" class="form-control form-control-sm"  name="caller_phone">
 							</td>
 						</tr>
 						<tr>
 							<td>Call Response</td>
 							<td>
 								<select class="custom-select custom-select-sm" name="call_res_id" required="">
 									<option value=""></option> 								
								<?php 
									//$attr = 'class="custom-select custom-select-sm"  ';
 									//echo form_dropdown('call_res_id', @$callresponses, '', $attr );
 									foreach ($callresponses as $row): 
 								?>
 									<option value="<?php echo $row->call_res_id; ?>" ><?php echo $row->call_res_text; ?></option>
 								<?php endforeach; ?>
 								</select>
 							</td>
 						</tr>
 					</table>
 				</div>
 				<div class="col-3">
 					<p>Notes</p>
 					<textarea class="form-control form-control-sm" name="notes" rows="5"></textarea>
 					<br />
 					 

 					<button class="btn btn-sm btn-primary" type="submit">Submit</button>
 				</div>
 				<div class="col-5">
 					<p><strong>Cell Care On Call Details</strong> - Choose applicable on On Call</p>
 					<table class="table table-sm">

 						<?php foreach($cellcare_oncall_details as $pcall): ?>
 						<tr>
 							<td style="width: 145px;"><button type="button" class="btn btn-sm btn-info p-1" onclick="OnCall.select(this)"><i class="fas fa-arrow-left"></i> <?php echo $pcall->name; ?></button></td> 
 							<td><?php echo $pcall->phone; ?></td>  
 							<td><?php echo $pcall->notes; ?></td>  
 						</tr> 
 						<?php endforeach; ?>

 						<tr>
 							<td colspan="3"> 								
 								<div class="border text-center bg-yellow">
 									<p>IMPORTANT</p>
 									<p>Collector & ON Call need to be called</p>
 									<p>After call Collector</p>
 									<p>another callactivity will display so On Call can be called.</p>
 								</div>
 							</td>
 						</tr>
 					</table>
 				</div>
 			</div> 

		</form>
		 
	</div>
</div>