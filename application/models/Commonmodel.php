<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonmodel extends CI_Model { 


    /**
     * 
     * @param  Strting $type 
     * @param  String $code
     * @param  Integer $single return value or array of items
     * @return array object /string
     */
    function hash($type, $code='', $single=''){

        try {
            
            if($code != '')
                 $this->db->where('code', $code);

            $this->db->where('type', $type);
            $this->db->select('code, value, options');
            $query = $this->db->get('hashes');
            
            if( $single ){
                $result = $query->row();
                $items = @$result->value;

            }else{
                $result = $query->result();
                
                $items = array();
                foreach ($result as $row) {
                   $items[$row->code] = $row;
                }
            }

            return $items;
        } catch (Exception $e) {
            return false;
        }

    }
      
    function hash_array($type){

        try {
             
            $this->db->where('type', $type);
            $this->db->select('code, value');
            $query = $this->db->get('hashes'); 

            $result = $query->result();
            
            $items = array();
            foreach ($result as $row) {
               $items[$row->code] = $row->value;
            }
           

            return $items;
        } catch (Exception $e) {
            return false;
        }

    }

    function client_status_text($code){

        return $this->hash('client_status', (!empty($code)?$code:1), 1);

    }

    function flashmsg($msg){
       
        $alert = ''; 
        $msg = explode(';', $msg); 
        foreach ($msg as $value) {
            $msg = explode('|', $value);
            $alert[$msg[0]][] =  $msg[1];
        }

        $alert_msgs = '';
        if( isset($alert['success']) ){

            $alert_msgs .= '<div class="alert alert-success" role="alert">
                        '.implode('<br />', $alert['success']).'
                    </div>';
        }
        if( isset($alert['error']) ){

            $alert_msgs .= '<div class="alert alert-error" role="alert">
                        '.implode('<br />', $alert['error']).'
                    </div>';
        }

        return $alert_msgs;
    }

    /**
     * Get server time from customer local time.
     * @param  datetime $dt       
     * @param  $state_tz
     * @return string           server time
     */
    function customer_local_server($dt, $state_tz){

        $sql = "SELECT CONVERT_TZ('$dt', '$state_tz', 'Australia/NSW') AS servertime";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result->servertime;

    }
    /**
     * Get customer local time from customer local time.
     * @param  datetime $dt       
     * @param  $state_tz
     * @return string           server time
     */
    function customer_server_local($dt, $state_tz){

        $sql = "SELECT CONVERT_TZ(CURRENT_TIMESTAMP, 'Australia/NSW', '$state_tz') AS local_tz";
        $query = $this->db->query($sql);
        $result = $query->row();
        return $result->local_tz;

    }

    function pagination_config( $set_config = array() ) { 

        $config = array(
                'per_page'=>15,
                'uri_segment'=>3
        ); 

        $config = $set_config;

        //config for bootstrap pagination class integration

        $config["enable_query_strings"] = TRUE; 
        $config["page_query_string"]    = TRUE;  

        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span></li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        return $config;

    }

    public function get_csv_table( $params = array(),$return_fields_only=false){

        $return = array(); 

        try {
            
            if(empty($params['table']))

             throw new Exception("Error: Empty Parameters method:get_csv_table ");
                
            if(isset($params['where'])){

                if(is_array($params['where'])){

                    $this->db->where($params['where']);
                    
                }else{

                    $this->db->where($params['where']);
                }
            }

            $custom_fields_qry = $this->db->get('hashes');

            $return['custom_fields'] = $custom_fields_qry->result();

            if($return_fields_only==true){
                
                 return $return['custom_fields'];
            }

            
            $result = $this->db->get($params['table']);
            

            $return['total_rows'] = $result->num_rows();

            //limits
              if(isset($params['limits'])){
                $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
              }


            $result = $this->db->get($params['table']);


            $return['results'] =  $result->result();
            

            
        } catch (Exception $e) {
                
            $return['error'] =  $e->getMessage();
        }



        return $return;

    }


   function import_csv_report($params = array())
    {
        
        $return_array = array() ;

        try {

            if(empty($params['table']))

                throw new Exception("Error : Table not found");

                 $file_path =  $_FILES[$params['file']]['tmp_name'];     

                $handle = fopen($file_path, "r");

                $delimiter = ","; 

                $row=1;
                $csv_report_data = array();
                $row_ctr = 0;
                $ctr=0;

                $fields_values_array = array();

                $required_num_of_insert = 50;
                

                while (($data = fgetcsv( $handle, 0, $delimiter)) !== FALSE) 
                {     

         
                    if($row > 1)
                    { 
                        $csv_report_data[]=$data;   
                        $row_ctr++;
                    }else{

                        $field_headers[] = $data;   
                    }           
                    $row++;
                }                

                $total_records = $row_ctr;

                $field_headers = $field_headers[0];   

                switch ($params['table']) {

                    case 'customers':

            
                        foreach ($csv_report_data as $data_value) {


                            $column_fields ='';

                            $column_value='';

                            $update_set = '';
                                             
                               foreach($data_value as $k=>$v){


                                    $_column_fields =  @$field_headers[$k];                    

                                    $_column_value = addslashes(trim($v));

                                    $json_obj_data[$_column_fields] = $column_value; 


                                    $column_value .="'".$_column_value."',";

                                    $column_fields .= '`'.$_column_fields.'`,';
                                   
                                    if($_column_fields=='CustomerID'){

                                        $this->db->where('CustomerID', $_column_value);

                                        $existing_cust = $this->db->get('customers')->row();

                                    }

                                    if(!empty($existing_cust) && $_column_fields!='CustomerID'){

                                        $update_set.= $_column_fields."="."'".$_column_value."',";
                                         
                                    }

                                }


                                if(!empty($existing_cust)){

                                    $update_set = substr($update_set, 0, -1);      

                                    $_sql_query = "update {$params['table']} SET {$update_set} WHERE CustomerID='".$existing_cust->CustomerID."'";                                  

                                    $this->db->query($_sql_query);

                                }


                                $column_fields = substr($column_fields, 0, -1);

                                $column_value = substr($column_value, 0, -1);   


                              if(empty($existing_cust)){

                                    $fields_values_array[] = '('.$column_value.')'; 

                              }


                              if(count($fields_values_array) > 0){

                                    if((intval($required_num_of_insert) == $ctr) OR ($ctr==($total_records-1)) ){
                                      $insert_values = implode(',',$fields_values_array);   
                                      
                                      
                                      $_sql_query = "insert into {$params['table']} ({$column_fields})values{$insert_values}";                     

                                      $this->db->query($_sql_query);

                                      $fields_values_array=array();             
                                      $total_records = $total_records-$ctr;           
                                      
                                      $ctr=0;
                                    }
                                
                              }


                                $ctr++;  
                        }
                        
                        break;
                    
                    default:

                        $this->db->truncate($params['table']);


                        foreach ($csv_report_data as $data_value) {


                            $column_fields ='';
                            $column_value='';
                                
                               foreach($data_value as $k=>$v){

                                    $_column_fields =  $field_headers[$k];                    

                                    $_column_value = addslashes(trim($v));

                                    $json_obj_data[$_column_fields] = $column_value; 

                                    $column_value .="'".$_column_value."',";
                                    $column_fields .= '`'.$_column_fields.'`,';
                                }

                                $column_fields = substr($column_fields, 0, -1);
                                $column_value = substr($column_value, 0, -1);       
                                $fields_values_array[] = '('.$column_value.')'; 


                           if( (intval($required_num_of_insert) == $ctr) OR ($ctr==($total_records-1)) )
                                {
                                  $insert_values = implode(',',$fields_values_array);   
                                  
                                  
                                  $_sql_query = "insert into {$params['table']} ({$column_fields})values{$insert_values}";                     

                                  $this->db->query($_sql_query);

                                  $fields_values_array=array();             
                                  $total_records = $total_records-$ctr;           
                                  
                                  $ctr=0;
                                }
                                $ctr++;  
                        }
     
                        break;
                }


                $return_array['msg'] = 'Importing of '.ucfirst($params['table']).' data was successfuly uploaded';
            
        } catch (Exception $e) {


                $return_array['error'] =  $e->getMessage();
            
        }
       

        return $return_array;

    }


    function insert_audit_trail($set){
         
        if( is_array($set) AND !empty($set) ){
            $this->db->insert('calls_audit', $set);
        }

    }

    function cma_auto_log($params){
        
        $this->load->library("Cma_external");

        $audit = array(
            'audit_type' => 'cmaautolog',
            'tran_id' => @$params['tran_id'],
            'call_id' => @$params['call_id'],
            'message' => @$params['message'], 
            'more_info' => ''
        );

        $app_config = $this->db->get_where('configs', array('name'=>'CMA_SETTINGS'))->row();
       
        $cma = json_decode($app_config->value);
         
        //Auto log calls to CMA                  
        $set_cma['cma_db']      = @$cma->cma_db;
        $set_cma['site_id']     = @$cma->site_id;
        $set_cma['cma_id']      = @$cma->cma_id;
        $set_cma['cust_id']     = @$cma->cust_id;
        $set_cma['contact_id']  = @$cma->contact_id_callsmade;  

        if(isset($params['ob1_phone']))
            $set_cma['ob1_phone']   = @$params['ob1_phone'];               
        if(isset($params['ob1_name']))
            $set_cma['ob1_name']    = @$params['ob1_name'];

        $set_cma['message']     = @$params['message'];
        $set_cma['agent_id']    = @$params['agent_id'];
                 
        $insert_calls = $this->cma_external->insert_calls_log($set_cma);


        //audit trail
        $audit['audit_status']  =  $insert_calls > 0?1:0;
        if( !$audit['audit_status']   )
            $audit['audit_status_error']=  $insert_calls;
        $audit['message']       = stripslashes(nl2br($params['message']));
 
        $this->insert_audit_trail($audit);

    } 

    function send_sms($params){

        $provider = (!isset($params['provider']))?'messagenet':$params['provider'];

        $app_config = $this->db->get_where('configs', array('name'=>'ENABLE_SMS'))->row();
        $sms_enabled = @$app_config->value;

        $audit = array(
            'audit_type' => 'sms',
            'tran_id' => @$params['tran_id'],
            'call_id' => @$params['call_id'],
            'message' => @$params['message'],
            'audit_to' => (is_array($params['to']) AND !empty($params['to']) ) ? implode(',', $params['to']) : $params['to'],
            'more_info' => array('provider'=>$provider, 'enabled'=>$sms_enabled)
        );

        if( $provider == 'messagenet' ){

            $this->load->library('email');

            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.welldone.com.au';
            $config['smtp_user'] = 'collection@welldone.com.au';
            $config['smtp_pass'] = 'C0113Ct10n';
            $config['smtp_port'] = '25';
            $config['mailtype'] = 'text';
            $config['wordwrap'] = TRUE;
            $config['charset'] = 'iso-8859-1';

            $this->email->initialize($config);


            $send_to = $params['to'];
            $send_to = str_replace(" ","",$send_to);
            $send_to = str_replace(";",",",$send_to);

            $send_to = explode(',', $send_to);
            
            foreach($send_to as $phoneNumber){ 
               
                $to = str_replace(' ', '', $phoneNumber);

                if(!preg_match('/^(\+6|6).*/',$to)) {
                    if(preg_match('/^0(.*)/',$to, $match)) $to = preg_replace('/^0(.*)/','61$1',$to);
                    else $to = '61'.$to;
                }

                if( trim($to) != '' ){
                    $to = $to.'@messagenet.com.au';
                     
                    $this->email->from(SYS_EMAIL);
                    $this->email->to(trim($to));
                    $this->email->subject(' ');
                    $this->email->message(stripslashes($params['message']));
        

                   //if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1', 'cellcare-labourline-test.welldone.net.au', 'cellcare-labourline.welldone.net.au'))  ){
                   if( $sms_enabled  ){

                        if($this->email->send(FALSE)){
                            $result = 'message_sent';
                        }else{

                            $result = $this->email->print_debugger(array('headers'));
                        }
                    
                    }else{
                        $result = 'message_sent - '.$_SERVER['SERVER_NAME'];
                    }

                    $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
                    $audit['audit_to']      = $phoneNumber;
                    $audit['audit_status']  =  strpos(' '.$result, 'message_sent')?1:0;
                    if( !$audit['audit_status']   )
                        $audit['audit_status_error']=  $result;
                    $audit['message']       = stripslashes(nl2br($params['message']));

                    $audit['more_info']['to'] = $to;
                    $audit['more_info']     = json_encode($audit['more_info']);
                    $this->insert_audit_trail($audit);
                }
            }

            return 'message_sent';            

        }

        if( $provider == 'smsglobal' ){

            //https://www.smsglobal.com/http-api.php?action=sendsms&user=testuser&password=secret&&from=Test&to=61447100250&text=Hello%20world
                        
            //$url = "https://www.smsglobal.com/http-api.php?";     
            $url = "https://api.smsglobal.com/http-api.php?";
     

            $build_query['action']   = 'sendsms';
            $build_query['user']     = 'ylrjh0kx';
            $build_query['password'] = 'wQVvWX6m';
            $build_query['from']     = '13CURE';
 

            $send_to = $params['to'];
            $send_to = str_replace(" ","",$send_to);
            $send_to = str_replace(";",",",$send_to);

            $send_to = explode(',', $send_to);

            foreach ($send_to as $phoneNumber) {
                
                if( trim($phoneNumber) != '' ){

                    //if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1', 'cellcare-labourline-test.welldone.net.au', 'cellcare-labourline.welldone.net.au'))  ){
                    if( $sms_config->value  ){
                        if(!preg_match('/^(\+6|6).*/',$phoneNumber)) {
                            if(preg_match('/^0(.*)/',$phoneNumber, $match)) $phoneNumber = preg_replace('/^0(.*)/','61$1',$phoneNumber);
                            else $phoneNumber = '61'.$phoneNumber;
                        }
                        
                        $build_query['to']       = $phoneNumber;
                        $build_query['text']     = $messsage;

                        $url .= http_build_query($build_query);      
                          
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_HEADER, TRUE);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                        curl_setopt($ch, CURLOPT_USERPWD, $build_query['user'].":".$build_query['password']);
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        $head = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        curl_close($ch);

                        if( !strpos(' '.$head, 'ERROR')){
                            $result = 'message_sent';                
                        }else{
                            $result = $head; 
                        }

                    }else{
                        $result = 'message_sent - '.$_SERVER['SERVER_NAME'];
                    }

                     
                    $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
                    $audit['audit_to']      = $to;
                    $audit['audit_status']  =  strpos(' '.$result, 'message_sent')?1:0;
                    if( !$audit['audit_status']   )
                        $audit['audit_status_error']=  $result;
                    $audit['message']       = stripslashes(nl2br($message)); 
                    //$audit['more_info']     = json_encode(array('to'=>$to));
                    $audit['more_info']['to'] = $to;
                    $audit['more_info']     = json_encode($audit['more_info']);

                    $this->insert_audit_trail($audit);
                }
            }

            return 'message_sent';

        }

    }


    /**
     *  params
     *      from, to, subject, cc, bcc
     *      tran_id
     */
    function send_email($params=array()){

        $this->load->library('email');

        $app_config = $this->db->get_where('configs', array('name'=>'ENABLE_SMS'))->row();
        $email_enabled = @$app_config->value;


        $audit = array(
            'audit_type' => (isset($params['audit_type']) AND @$params['audit_type'] != '')?$params['audit_type']:'email',
            'tran_id' => @$params['tran_id'],
            'call_id' => @$params['call_id'],
            'message' => @$params['message'],
            'audit_to' => (is_array($params['to']) AND !empty($params['to']) ) ? implode(',', $params['to']) : $params['to'],
            'more_info' => array('enabled'=>$email_enabled)
        );

        try {
            
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.welldone.com.au';
            $config['smtp_user'] = 'collection@welldone.com.au';
            $config['smtp_pass'] = 'C0113Ct10n';
            $config['smtp_port'] = '25';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;
            $config['charset']  = 'iso-8859-1';
            $config['crlf']     = '\r\n';
            $config['newline']  = '\r\n';

            $this->email->initialize($config);

            $this->email->clear(TRUE);

            $to         = trim(@$params['to']);
            $subject    = trim(@$params['subject']);
            $message    = @$params['message']; 


            if(!is_array($to)) { 
                $to = str_replace(" ","",$to);
                $to = str_replace(";",",",$to);
                $to = explode(",",$to); 
            }

            $to = array_filter($to, 'strlen');

            if( empty($to) ) throw new Exception("Mailer Error: Empty TO", 1);
            if( empty($subject) ) throw new Exception("Mailer Error: Empty Subject", 1);
            

            if( isset($params['reply_to']) AND trim(@$params['reply_to']) != '' ){
                $this->email->reply_to($params['reply_to']);
            }


            if( isset($params['from']) AND trim(@$params['from']) != '' ){
                $this->email->from($params['from']);
            }else{
                //$this->email->from('escalationapp@welldone.com.au', 'Escalation System');
                $this->email->from(SYS_EMAIL, SYS_EMAIL_NAME);
            }
            
            if( isset($params['cc']) AND trim(@$params['cc']) != '' ){
                $this->email->cc($params['cc']);
            }
            
            if( isset($params['bcc']) AND trim(@$params['bcc']) != '' ){
                $this->email->bcc($params['bcc']);
            }
            

            $this->email->to($to);
            
            //$this->email->cc('another@another-example.com');
            //$this->email->bcc('them@their-example.com'); 
            

            $this->email->subject($subject);
            $this->email->message($message);


            //if( !in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1', 'cellcare-labourline-test.welldone.net.au', 'cellcare-labourline.welldone.net.au'))  ){
            if( $email_enabled ){

                if($this->email->send(FALSE)){
                    $result = 'message_sent';
                }else{

                    $result = "Mailer Error ".$this->email->print_debugger(array('headers'));
                }
            
            }else{
                $result = 'message_sent - '.$_SERVER['SERVER_NAME'];
            }

            
           /* if($this->email->send(FALSE)){
                $result = 'message_sent';
            }else{

                $result = "Mailer Error ".$this->email->print_debugger(array('headers'));
            }*/

   
            $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
            $audit['audit_to']      = implode(',', $to);
            $audit['audit_status']  =  strpos(' '.$result, 'message_sent')?1:0;
            if( !$audit['audit_status']   )
                $audit['audit_status_error']=  $result;
            $audit['message']       = ($this->isHTML($message))?stripslashes($message):stripslashes(nl2br($message));
            
             
            $audit['more_info']['subject'] = $subject;
            $audit['more_info']['agent_name'] = isset($params['agent_name'])?$params['agent_name']:@$this->agent_name;
            
            //$audit['more_info']     = json_encode($more_info);
            $audit['more_info']     = json_encode($audit['more_info']);       
   
            $this->insert_audit_trail($audit);

            return $result;
            



        } catch (Exception $e) {

            $audit['audit_from']    = (isset($params['from']) AND trim(@$params['from']) != '')?@$params['from']:SYS_EMAIL;
            $audit['audit_to']      = implode(',', @$to);
            $audit['audit_status']  = 0;        
            $audit['audit_status_error']= $e->getMessage();        
            $audit['message']       = stripslashes(nl2br($message));
            //$audit['more_info']     = '';
            $this->insert_audit_trail($audit);

            return $e->getMessage();
        }
 
    }


    function progress_selector_text($id=''){

        if($id=='') return '';

        $this->db->where('id', $id);
        
        $row = $this->db->get('progress_selector')->row();

        return @$row;

    }

    function booking_sms_template($tran_row, $customer) {

        $return = array('sched_type'=>'', 'message'=>'');

        $_msg = array();

        if( $tran_row->sched_induction != '' ){
            $_msg[] = "INDUCTION:  Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;
            
            $sched_type = 'induction';

        }else if( $tran_row->sched_ceasar != '' ) {
            $_msg[] = "C-SECTION BOOKED ".date('d/m/Y', strtotime($tran_row->sched_ceasar))." OT: ".$tran_row->sched_ceasar_to;

            if( $tran_row->sched_ceasar != '' AND !in_array($tran_row->sched_ceasar_to, array('AM', 'PM', 'UNKNOWN')) ) {

                $_msg[] = "arrive 1 hour prior";

            }

            $_msg[] = "Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;

            $sched_type = 'c-section';

        }else{
            $_msg[] = "Cellcare Client: ".@$customer->FirstName.' '.@$customer->LastName;
        }
        
        $StorageProduct = trim(str_replace(' ', '', $customer->StorageProduct));
        
        $storage_initial = '';
        if( $StorageProduct == 'CordBloodOnly' ) $storage_initial = 'CBO';
        elseif( $StorageProduct == 'CordBloodPlusTissue' ) $storage_initial = 'CBT';



        $_msg[] = 'ID:'.$customer->CustomerID;
        $_msg[] = @$tran_row->baby;
        $_msg[] = $customer->MobilePhone;
        $_msg[] = $customer->Hospital;
        $_msg[] = $customer->EDD;
        $_msg[] = $storage_initial;
        $_msg[] = $customer->PartnersMobilePhone;
        $_msg[] = $customer->PartnersFirstName;

        if( $tran_row->sched_induction != '' ) {
            $_msg[] = " Booked ".date('d/m/Y', strtotime($tran_row->sched_induction))." ".$tran_row->sched_induction_ta.' - '.@$tran_row->sched_induction_type;
            

        }

        /*elseif( $tran_row->sched_ceasar != '' AND !in_array($tran_row->sched_ceasar_to, array('AM', 'PM', 'UNKNOWN')) ) {

            $_msg[] = "arrive 1 hour prior";

        }*/
        //else if( $tran_row->sched_ceasar != '' ) $_msg[] = " arrive 1 hour prior";

        $_msg_f = array_filter($_msg);
        $message = implode(' - ', $_msg_f);

        $return['sched_type'] = $sched_type;
        $return['message'] = $message;

        return (object)$return;

    }

}