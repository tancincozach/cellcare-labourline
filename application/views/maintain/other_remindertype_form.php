<h1>Reminder Type</h1>


<?php
 
	FLASH_SESSION_MSG();

	echo form_open('',' class="form-horizontal" method="POST" onsubmit="return confirm(\'Confirm Submit\')" ');
?>
	<input type="hidden" name="id" value="<?php echo @$row->id;?>">
	<input type="hidden" name="form_type" value="<?php echo @$form_type;?>">

	<div class="row">
		<div class="col-md-8">

			<div class="table-responsive">
				
				<table class="table">
					<tbody> 
						<tr>
							<td>Reminder Type</td>
							<td>
								<input type="text" name="content" value="<?php echo stripslashes(@$row->content);?>" class="form-control form-control-sm">
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td>
								<button type="submit" class="btn btn-info">Submit</button>
								<a href="maintain/others/remindertype" class="btn btn-primary">Cancel</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
		</div>
	</div>

 
</form>