<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transactionmodel extends CI_Model { 


    function insert($set){

        $this->db->db_debug = FALSE;

        try {
            
            if( empty($set) ) throw new Exception("Empty insert data", 1);
            
            $tran_id = $this->_add($set);

            if( $tran_id > 0 ){

                $this->db->db_debug = TRUE;

                return $tran_id;
            }else{
                if( strpos('_'.$this->db->_error_message(), 'ref_number_Index') !== false ){                
                    $this->_add($set);
                }
            }
            

        } catch (Exception $e) {
            return false;
        }

    }


    function _add($set){
         
        $query_res = $this->db->query('SELECT ref_number  FROM transaction ORDER BY tran_id desc LIMIT 1')->row();
        $ref_number = $query_res->ref_number+1;
        
        $set['ref_number']  = $ref_number;  

        if( $this->db->insert('transaction', $set) ){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
  

    function update($tran_id, $set){

        $tran_id = trim($tran_id);

        try {
            
            if( empty($tran_id) ) throw new Exception("tran_id is required", 1);
            if( empty($set) ) throw new Exception("set param is required", 1);
                        
            $this->db->where('tran_id', $tran_id);
            return $this->db->update('transaction', $set);

        } catch (Exception $e) {
            return false;
        }

    }

  	
  	function row($params){

  		try {
  			
 			if( empty($params) ) throw new Exception("Params is required", 1);

			//where clause
			if(isset($params['where'])){
				$this->db->where($params['where']);
			}		

			if(isset($params['where_str']) && $params['where_str']!='' ){
				$this->db->where($params['where_str'], null, false);
			}

  			$query = $this->db->get('transaction');

  			return $query->row();

  		} catch (Exception $e) {
  			return 0;
  		}
  	}

    /**
     * Get customer details
     * @param  array $params where, where_str
     * @return object customer details
     */
    /**
     * @TODO connect to customer_update table to get the updated hospital
     */
    function details($params){

        try {
            
            if( empty($params) ) throw new Exception("Params is required", 1);

            //where clause
            if(isset($params['where'])){
                $this->db->where($params['where']);
            }       

            if(isset($params['where_str']) && $params['where_str']!='' ){
                $this->db->where($params['where_str'], null, false);
            }

            $query = $this->db->get('transaction');

            return $query->row();

        } catch (Exception $e) {
            return 0;
        }

    }

    function listing($params=array(), $paging=TRUE){

        try {           

            if( empty($params) ) throw new Exception("Can't return full list", 1);
            

            if( $paging ){

                //TOTAL ROWS
                
                //where clause
                if(isset($params['where'])){
                    $this->db->where($params['where']);
                }

                if(isset($params['where_str']) && $params['where_str']!='' ){
                    $this->db->where($params['where_str'], null, false);
                }

                $this->db->select('count(*) as total');

                $this->db->join('customers', 'customers ON customers.CustomerID = transaction.CustomerID', 'LEFT OUTER');
                $this->db->join('customers_update', 'customers_update ON customers_update.CustomerID = transaction.CustomerID', 'LEFT OUTER');
                $query = $this->db->get('transaction');
                
                $total_rows = $query->row()->total;
                $query->free_result(); //free results
            }            
            //RESULTS
            
            //where clause
            if(isset($params['where'])){
                $this->db->where($params['where']);
            }       

            if(isset($params['where_str']) && $params['where_str']!='' ){
                $this->db->where($params['where_str'], null, false);
            }

            //limits
            if(isset($params['limits'])){
                $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
            }

            //sorting
            if( isset($params['sorting']) ){
                if( is_array($params['sorting']) ){
                    $this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
                }else{
                    $this->db->order_by($params['sorting']);
                }

            }else{ 
                
                $this->db->order_by('tran_status DESC');
                $this->db->order_by('tran_updated DESC');
            }

            $customers_fields = $this->db->list_fields('customers');
           
            unset($customers_fields[array_search('CustomerID', $customers_fields)]);
            unset($customers_fields[array_search('Hospital', $customers_fields)]);
            unset($customers_fields[array_search('HospitalStreet', $customers_fields)]);
            unset($customers_fields[array_search('HospitalCity', $customers_fields)]);
            unset($customers_fields[array_search('HospitalRegion', $customers_fields)]);
            unset($customers_fields[array_search('HospitalPostalCode', $customers_fields)]);
            unset($customers_fields[array_search('HospitalPhone', $customers_fields)]);
            unset($customers_fields[array_search('cust_lastupdate', $customers_fields)]);
          

            $select = ' transaction.*, 
                IF( `transaction`.`reminder` IS NULL, CURRENT_TIMESTAMP, `transaction`.`reminder`) AS reminder_order,
                '.implode(',', $customers_fields).',
                IF( customers_update.Hospital IS NULL, customers.Hospital, customers_update.Hospital ) AS Hospital,
                IF( customers_update.HospitalStreet IS NULL, customers.HospitalStreet, customers_update.HospitalStreet ) AS HospitalStreet,
                IF( customers_update.HospitalCity IS NULL, customers.HospitalCity, customers_update.HospitalCity ) AS HospitalCity,
                IF( customers_update.HospitalRegion IS NULL, customers.HospitalRegion, customers_update.HospitalRegion ) AS HospitalRegion,
                IF( customers_update.HospitalPostalCode IS NULL, customers.HospitalPostalCode, customers_update.HospitalPostalCode ) AS HospitalPostalCode,
                IF( customers_update.HospitalPhone IS NULL, customers.HospitalPhone, customers_update.HospitalPhone ) AS HospitalPhone';
            
            $this->db->select($select, false);
            $this->db->join('customers', 'customers ON customers.CustomerID = transaction.CustomerID', 'LEFT OUTER');
            $this->db->join('customers_update', 'customers_update ON customers_update.CustomerID = transaction.CustomerID', 'LEFT OUTER');
            $query = $this->db->get('transaction');

            $result = $query->result();
            $query->free_result(); //free results
      
            return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
             

        } catch (Exception $e) {
            return false;
        }

    }   


    function listing_basic($params=array(), $paging=TRUE){

        try {           

            if( empty($params) ) throw new Exception("Can't return full list", 1);
            

            if( $paging ){

                //TOTAL ROWS
                
                //where clause
                if(isset($params['where'])){
                    $this->db->where($params['where']);
                }

                if(isset($params['where_str']) && $params['where_str']!='' ){
                    $this->db->where($params['where_str'], null, false);
                }

                $this->db->select('count(*) as total'); 

                $query = $this->db->get('transaction');
                
                $total_rows = $query->row()->total;
                $query->free_result(); //free results
            }            
            //RESULTS
            
            //where clause
            if(isset($params['where'])){
                $this->db->where($params['where']);
            }       

            if(isset($params['where_str']) && $params['where_str']!='' ){
                $this->db->where($params['where_str'], null, false);
            }

            //limits
            if(isset($params['limits'])){
                $this->db->limit($params['limits']['limit'], $params['limits']['start']); 
            }

            //sorting
            if( isset($params['sorting']) ){
                if( is_array($params['sorting']) ){
                    $this->db->order_by($params['sorting']['sort'], $params['sorting']['order']);
                }else{
                    $this->db->order_by($params['sorting']);
                }

            }else{ 
                
                $this->db->order_by('tran_updated desc');
            }


            $select = ' *,';
            
            $this->db->select($select); 
            $query = $this->db->get('transaction');

            $result = $query->result();
            $query->free_result(); //free results
      
            return ($paging) ? array('results'=>$result, 'total_rows'=>$total_rows) : $result;
             

        } catch (Exception $e) {
            return false;
        }

    }   
  

}