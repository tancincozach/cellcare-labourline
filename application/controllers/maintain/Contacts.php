<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MY_Controller {

	public function __construct(){

		parent::__construct();

		if( $this->user->level == 0 ){
			$this->session->set_flashdata('flash_error_msg', "You don't have permission to access the previous page");
			redirect('dashboard');
		}

		$this->view_data['view_file'] = 'maintain/index';
		$this->view_data['menu_active'] = 'maintain';
		$this->view_data['container'] = 'container-fluid';
		$this->view_data['maintain']['view_file'] = 'maintain/contacts';		
	}



    /**
     * [create/update user accounts]          
     */
    /**
     * @TODO audit trail
     */

	public function index()
	{

		$this->load->model('Contactsmodel','contacts');
		$this->load->library("pagination");

		$params = $this->input->get();

		$page = (isset($params['per_page'])) ? $params['per_page'] : 0;
		unset($params['per_page']);

		$data = '';

		$data['filters'] = $params;		

		//$query_params = $this->build_where($params);
		$query_params = '';
		$params_query = @http_build_query($params);

		$per_page = 15;
		$query_params['limits']['start'] = $page;
		$query_params['limits']['limit'] = $per_page; 
		$query_params['where']      = $params ;

		$results 				    = $this->contacts->listing($query_params);

		$p_config["base_url"] 		= base_url() . "maintain/contacts/?".$params_query;
		$p_config["total_rows"] 	= $results['total_rows'];
		$p_config["per_page"] 		= $per_page;
		$p_config["uri_segment"] 	= 3; 
		$config = $this->Commonmodel->pagination_config($p_config);	 
		$this->pagination->initialize($config);

		$data['results'] 	= $results['results'];
		$data["links"] 		= $this->pagination->create_links();
		$data['showing']	= 'Showing '.(($page==0)?1:$page+1).' to '.($page+count($data['results'])).' of '.$results['total_rows'].' entries';

		$params = $this->input->get();

		if(isset($params['del'])){

			try {			

			$contact_id = $this->contacts->delete($params['del']);

				if(empty($contact_id)){

					throw new Exception("Error: Unable to delete the Contact");
					
				}

				$this->session->set_flashdata('msg','Contact successfuly deleted.');

				redirect('maintain/contacts');
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());
				
			}

		}
	
			
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'contacts';
		$this->load->view('template', $this->view_data);
	}



	public function contacts_form( $contact_id = ''){

		$this->load->model('Contactsmodel','contacts');

		$data = '';		

		if($_POST){

			try {

				$post = $this->input->post();


				if(empty($post['content']))

					throw new Exception("Error : Input Required Fields");

				$set['contact_title']  = @$post['title'];;
				$set['contact_html']  = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', "", $_REQUEST['content'] );				
				$set['agent_name'] = $this->session->userdata('agent_name');
				$set['user_id'] = $this->session->userdata('user_id');
				$set['contact_status'] = isset($post['status']) ? $post['status']:'';


				if(!empty($post['contact_id'])){

		
					$contact_id = $this->contacts->update($post['contact_id'],$set);

					if(!$contact_id) 
						throw new Exception("Error: Failure to Update Call Response");
					

				}else{

					$contact_id = $this->contacts->insert($set);

					if(!$contact_id)
						throw new Exception("Error: Failure to Add Call Response");

				}

				if(!empty($post['contact_id'])){

					$this->session->set_flashdata('msg','Update Successful');
					redirect('maintain/contacts/contacts_form/'.$post['contact_id']);

				}else{
					
					$this->session->set_flashdata('msg','Adding of Contact is Successful');
					redirect('maintain/contacts');
					
				}

					
				
			} catch (Exception $e) {

					$this->session->set_flashdata('error',$e->getMessage());

			}				
		}





		if($contact_id!=''){

			$where = array();

			$where['where']['contact_id'] = $contact_id;

			$data['contacts_row'] = $this->contacts->row($where);			


		}
		$this->view_data['js_file'] = '<script src="assets/vendor/tinymce/js/tinymce/tinymce.min.js"></script>'; 
		$this->view_data['js_file'] .= '<script src="assets/js/pages/contacts.js"></script>'; 
		$this->view_data['maintain']['data'] = $data;
		$this->view_data['maintain']['menu_active'] = 'contacts';
		$this->view_data['maintain']['view_file'] = 'maintain/contacts-form';
		$this->load->view('template', $this->view_data);

	}


}
